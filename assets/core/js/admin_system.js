var custom_form_render = function (parent){
    var ms = $('#branch_group_id').data('magic');
    $(ms).on('selectionchange', function(e,m){
        var branch_group_ids = this.getValue();
        var branch_ids = $('#branch_id').data('magic');
        if(isset(branch_ids)){
            branch_ids = isset(branch_ids)?branch_ids.getValue():[];
        }else{
            branch_ids = IsJsonString($('#branch_id').val())?JSON.parse($('#branch_id').val()):[];
        }

        $('#branch_id_container').html('');

        $.post(url + 'admin/branch/suggest_by_branch_group',{minimal:true,branch_group_id:branch_group_ids},function(data){
            data = JSON.parse(data);
            var allowValue = [];
            $.each(data,function(obj){
                allowValue.push(this.value);
            });
            var newValue = _.intersection(branch_ids,allowValue);
            var new_tag = $('<input>',{id:'branch_id', value:JSON.stringify(newValue), name:'branch_id'});
            $('#branch_id_container').append(new_tag);
            var magic = $(new_tag).magicSuggest({
                allowFreeEntries: false,
                placeholder: 'Input tags',
                data:data,
                valueField: 'value',
                cls : 'insert multi-select',
                displayField : 'label',
                resultAsString: true,
            });

            $('#branch_id').data('magic',magic);
        });

    });
    custom_form_render_admin_system();
};

var custom_form_render_admin_system = function(){};