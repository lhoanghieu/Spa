var control = "sales/";
var current_category = null;
$(function(){
    preBillData = JSON.parse(sessionStorage.getItem('pre_bill_data'));
    if(preBillData != null) {
        if (preBillData.customer_id.length > 0 && preBillData.customer_id != '') {
            set_customer(preBillData.customer_id);
            preBillData.customer_id = '';
            sessionStorage.setItem('pre_bill_data', JSON.stringify(preBillData));
        }
    }
    load_customer_change();
    load_sale_change();

    $('#grid_row_items').delegate('.itemChange','change',function(){
        if($(this).hasClass('multi-select')){
            return;
        }
        update_item($(this).closest('tr').attr('rowid'));
    });

    $('#grid_row_items').delegate('.item_delete','click', function()
    {
        var rowid = $(this).attr('rowid');

        $.system_process({
            url : url + control + 'delete_row',
            param: { rowid : rowid},
            success: function(ret,more){
                reload_table(ret['data']);
                load_right_panel();
            }
        });

    });

    $('#category_item_selection_wrapper').on('click','.category_item.category', function(event)
    {
        event.preventDefault();
        current_category = $(this).attr('data-id');

        $.system_process({
            url : url + control +  'items',
            param : {category: current_category},
            success: function(ret,more){
                processItemsResult(ret['data']);
            }
        });
    });

    $("#category_item_selection_wrapper").on('click', '#back_to_categories', function(event)
    {
        event.preventDefault();
        load_categories();
    });

    $('#item,#customer').click(function()
    {
        $(this).attr('value','');
    });

    $('#customer').blur(function()
    {
        $(this).attr('value',"Start Typing customer's name...");
    });


    $("#show_grid").click(function()
    {
        $("#category_item_selection_wrapper").slideDown();
        $("#show_hide_grid_wrapper").css('bottom', '-56px');
        $("#show_grid").hide();
        $("#hide_grid").show();
    });

    $("#hide_grid,#hide_grid_top").click(function()
    {
        $("#category_item_selection_wrapper").slideUp();
        $("#show_hide_grid_wrapper").css('bottom', '-63px');
        $("#show_grid").show();
        $("#hide_grid").hide();
    });

    $('#item_search').render_autocomplete({
        source: url + 'items/' + 'suggest',
        select: function (event, ui) {
            set_item_into_carts(ui.item.value);
            return false;
        }
    }).focus(function(){
        $(this).autocomplete('search', $(this).val());
    });

    $('#retrieve_bill').data('action-url', url + control + '/retrieve_bill_form');
    $('#retrieve_bill').data('action-success', function(param, more){
        $('.retrieve_bill_btn').on('click',function(){
            var bill_id = $(this).attr('bill-id');
            $.system_process({
                url: url + control + 'retrieve_bill/' + bill_id,
                success: function(ret){
                    localStorage.setItem('reload',ret['data']['reload']);
                    load_customer_change();
                    load_sale_change();
                }
            });
        });
        $('.delete_bill_btn').on('click',function(){
            var bill_id = $(this).attr('bill-id');
            $.system_confirm({
                url: url + control + 'delete_suspended_bill/' + bill_id,
                content: 'Are you sure to delete this suspended bill?',
                success: function(ret){
                    localStorage.setItem('reload',ret['data']['reload']);
                    $('#retrieve_bill').trigger('click');
                }
            });
        });
    });
    /* ----- End of Left Panel Script ------ */

    /* ----- Start of Right Panel Script ---- */
    $('#right_panel').delegate('#delete_customer','click',function(){
        $.system_process({
            url : url + control + 'detach_customer',
            success: function(ret,more){
                load_customer_change();
                load_sale_change();
            }
        });
    });

    $('#right_panel').delegate('#payment_type','change', function(){
        var payment_id = $(this).val();
        $.system_process({
            url : url + control + 'payment_type_change',
            param : {payment_id : payment_id},
            success: function(ret,more){
                load_right_panel();
            }
        });
    });

    $('#right_panel').delegate('#add_payment_button','click',function(){
        var str = {};
        var val,name;
        $(".w-select").each(function(){
            if($(this).hasClass('multi-select')){
                name = $(this).attr('id');
                val = [];
                $(this).find('input[name="'+name+'[]"]').each(function(){
                    val.push($(this).val());
                });
            }else{
                name = $(this).attr('id');
                val = $(this).val();
                if(IsJsonString(val)){
                    val = JSON.parse(val);
                }
            }
            str[name] = val;
        });

        if(str.giftcard_list_redeem == '' && str.giftcard_values_redeem != ''){
            alert('Please Choose which one gift card will use to redeem');
            return false;
        }
        $.system_process({
            url : url + control + 'add_payment',
            param : {payment_values : JSON.stringify(str)},
            success: function(ret,more){
                load_sale_change();
            }
        });
    });

    $('#right_panel').delegate('#cancel_sale_button','click',function(){
        $.system_process({
            url : url + control + 'cancel_sale',
            success: function(ret,more){
                load_sale_change();
            }
        });
    });

    $('#right_panel').delegate('#delete_payment','click',function(){
        $.system_process({
            url : url + control + 'delete_payment',
            param : {id : $(this).attr('value')},
            success: function(ret,more){
                load_sale_change();
            }
        });
    });


    $('#right_panel').delegate('#finish_sale_btn','click',function(){
        $content = 'Are you sure to process this bill?';
        if(sessionStorage.getItem('is_deposit') == 1){
            $content = 'Are you sure to process this bill using <b style="color : red;">Deposit Payment</b>?';
        }
        $.msgBox({
            title:"Message",
            type:"alert",
            content:$content,
            buttons:[{value:"Yes"},{value:"Cancel"}],
            success: function(result) {
                if(result == 'Yes'){
                    $('input.msgButton[name="Yes"]').prop('disabled',true);
                    $.system_process({
                        url : url + control + 'check_need_pin',
                        success: function(ret,more){
                            complete_bill(1);
                        },
                        fail : function(ret,more){
                            $.msgBox({
                                title: "Enter Customer Pin",
                                type: "confirm",
                                content: "<input id='enter-pin-complete-bill' type='password'> </input>",
                                buttons: [{value: "Submit"}, {value: "Cancel"}],
                                success: function(result){
                                    if(result == 'Submit'){
                                        $.system_process({
                                            url : url + control + 'check_need_pin',
                                            param : {pin:$('#enter-pin-complete-bill').val()},
                                            success: function(){
                                                complete_bill(0);
                                            }
                                        });
                                    }
                                }
                            });
                            $("#enter-pin-complete-bill").focus();
                        }
                    });
                }
            }
        });

        function complete_bill(need_check){
            if(need_check == 1){
                $.system_process({
                    url : url + control + 'check_customer_pin',
                    success : function(ret, more){
                        var data = ret['data'];
                        if(data['process_type'] == 1){
                            $('#finish_sale_btn').prop('disabled',true);
                            $.system_process({
                                url : url + control + 'complete',
                                param : {
                                    comment : $('#comment').val(),
                                    is_deposit : sessionStorage.getItem('is_deposit')
                                },
                                async : false,
                                success: function(ret,more){
                                    localStorage.setItem('reload',ret['data']['reload']);
                                    window.location = url + control + 'receipt/' + ret['data']['bill_id'];
                                }
                            });
                        }
                        else {
                            var type = data['process_type']=='need_pin'?'Pin':'Mobile Number';
                            $.msgBox({
                                title: "Enter Customer " + type,
                                type: "confirm",
                                content: "<input id='enter-pin-complete-bill' type='password'> </input>",
                                buttons: [{value: "Submit"}, {value: "Cancel"}],
                                success: function(result){
                                    if(result == 'Submit'){
                                        $.system_process({
                                            url : url + control + 'check_customer_pin',
                                            param : {pin:$('#enter-pin-complete-bill').val()},
                                            success: function(){
                                                $('#finish_sale_btn').prop('disabled',true);
                                                    $.system_process({
                                                 url : url + control + 'complete',
                                                 param : {
                                                     comment : $('#comment').val(),
                                                     is_deposit : sessionStorage.getItem('is_deposit')
                                                 },
                                                 async : false,
                                                 success: function(ret,more){
                                                 localStorage.setItem('reload',ret['data']['reload']);
                                                 window.location = url + control + 'receipt/' + ret['data']['bill_id'];
                                                 }
                                                 });
                                            }
                                        });
                                    }
                                }
                            });
                            $("#enter-pin-complete-bill").focus();
                        }
                    }
                });
            }
            else{
                $.system_process({
                    url : url + control + 'complete',
                    param : {
                        comment : $('#comment').val(),
                        is_deposit: sessionStorage.getItem('is_deposit')
                    },
                    async : false,
                    success: function(ret,more){
                        localStorage.setItem('reload',ret['data']['reload']);
                        window.location = url + control + 'receipt/' + ret['data']['bill_id'];
                    }
                });
            }
        }
    });

    $('#right_panel').delegate('.r-select','change',function(){
        var data = {};
        $('.r-select').each(function(){
            data[$(this).attr('id')] = $(this).val();
        });

        $.system_process({
            url : url + control + 'update_right_panel',
            param : {data : JSON.stringify(data)},
            success: function(ret,more){
                load_customer_change();
            }
        });


    });

    $('#right_panel').delegate('#suspend_sale_button','click',function(){
        $.system_process({
            url : url + control + 'suspend',
            success: function(ret,more){
                localStorage.setItem('reload',ret['data']['reload']);
                load_customer_change();
                load_sale_change();
            }
        });
    });
});

var reload_table = function(data){
    if(IsJsonString(data)){
        data = JSON.parse(data);
    }

    $("#grid_row_items").html(data.content);
    render_form('#register');
};

function load_categories()
{
    $.system_process({
        url : url + control + 'categories',
        success: function(ret,more){
            processCategoriesResult(ret['data']);
        }
    });
}

function load_cart(){
    $.system_process({
        url: url+control+'getList',
        type: 'POST',
        success: function(ret,more){
            reload_table(ret['data']);
        }
    });

}

function processCategoriesResult(json)
{
    $("#category_item_selection_wrapper .pagination").removeClass('items').addClass('categories');
    $("#category_item_selection").html('');
    $.each(json, function(idx, obj) {
        var category_item = $("<div/>").attr('class', 'category_item category col-md-2 col-sm-3 col-xs-6')
            .attr('data-id',obj['category_id'])
            .attr('action-type','process')
            .attr('action-url',url+'sales/item')
            .attr('action-param','{"categories":"'+obj['category_id']+'"}')
            .attr('action-success','processItemsResult')
            .append('<p>'+obj['category_name']+'</p>');
        $("#category_item_selection").append(category_item);
    });
}

function processItemsResult(json)
{
    $("#category_item_selection_wrapper .pagination").removeClass('categories').addClass('items');
    $("#category_item_selection").html('');
    var back_to_categories_button = $("<div/>").attr('id', 'back_to_categories').attr('class', 'category_item back-to-categories col-md-2 col-sm-3 col-xs-6 ').append('<p>&laquo; '+"Back To Upper Categories"+'</p>');

    $("#category_item_selection").append(back_to_categories_button);
    $.each(json, function(idx, obj) {
        if(obj['category_id']) {
            var category_item = $("<div/>").attr('class', 'category_item category col-md-2 col-sm-3 col-xs-6')
                .attr('data-id', obj['category_id'])
                .attr('action-type', 'process')
                .attr('action-url', url + 'sales/item')
                .attr('action-param', '{"categories":"' + obj['category_id'] + '"}')
                .attr('action-success', 'processItemsResult')
                .append('<p>' + obj['category_name'] + '</p>');
            $("#category_item_selection").append(category_item);
        }
        else if(obj['item_id']){
            var item_parent_class = "item_parent_class";
            var item = $("<div/>").attr('class', 'category_item item col-md-2 col-sm-3 col-xs-6'+item_parent_class).attr('data-id',obj['item_id']).append('<p>'+obj['item_name']+'</p>');
            $(item).attr('clickable',1);
            if(obj['credit_avail'] == 1){
                $(item).css('background-color','#00B16A');
                $(item).css('color','#fff');
            }else if(obj['credit_avail'] == -1){
                $(item).css('background-color','#c0392b');
                $(item).css('color','#fff');
                $(item).attr('clickable',0);
            }
            $("#category_item_selection").append(item);
        }


    });
}



$('#category_item_selection_wrapper').on('click','.category_item.item', function(event)    {
    event.preventDefault();
    if($(this).attr('clickable') == 1){
        set_item_into_carts($(this).data('id'));
    }else{
        $.msgBox({
            type : 'error',
            title: 'Error',
            content: 'This item is not available for current customer',
            button: [{value:'Cancel'}]
        });
    }
    return false;
});

$('#item').keypress(function(e) {
    var code = e.keyCode || e.which;
    var item = $("#item").val();
    if (code == 13) {
        set_item_into_carts(item);
        return false;
    }
});

var transform_multiselect = function(parent){
    var objs;
    if(isset(parent)){
        objs = $(parent).find('.multi-select');
    }else{
        objs = $('.multi-select');
    }
    objs.each(function(){
        var data = $(this).attr('raw-data');
        var id = $(this).attr('id');
        var rowid = $(this).attr('rowid');
        if(isset(data)){
            data = JSON.parse(data);
            var selected = [];
            data.select.forEach(function(entry){
                selected.push(entry.value);
            });
            if(selected){
                $(this).attr('value',JSON.stringify(selected));
            }
            var magic = $(this).magicSuggest({
                allowFreeEntries: false,
                placeholder: 'Input tags',
                data: data.total,
                valueField: 'value',
                cls : 'itemChange insert multi-select',
                renderer: function(data){
                    return data.name;
                },
                resultAsString: true
            });
            $(this).data('magic',magic);

        }
        else{
            var data_source = $(this).attr('data-source');
            if(isset(data_source)){
                magic = $(this).magicSuggest({
                    allowFreeEntries: false,
                    placeholder: 'Input tags',
                    data: data_source,
                    valueField: 'value',
                    cls : 'itemChange insert multi-select',
                    displayField : 'label',
                    resultAsString: true
                });
            }else{
                magic = $(this).magicSuggest({
                    allowFreeEntries: true,
                    placeholder: 'Input tags',
                    cls : 'itemChange insert multi-select',
                    displayField : 'label',
                    resultAsString: true
                });
            }
            $('#'+$(this).attr('id')).data('magic',magic);
        }
        setTimeout(function(){
            $(magic).on('selectionchange', function(){
                update_item(rowid);
            });
        },1500);

    });
};


function set_item_into_carts(item) {

    $.system_process({
        url: url + control + 'insert_row',
        param: {item:item},
        success: function (ret,more) {
            reload_table(ret['data']);
            load_right_panel();
        }
    });

}

function get_row_values(rowid){

    var str = {};
    $("tr[rowid='"+rowid+"'] .itemChange").each(function(){
        var name, val;
        var type = $(this).attr('type');
        if($(this).hasClass('multi-select')){
            var key = $(this).parent().attr('key');
            if(key){
                name = $(this).parent().attr('name');
                val = {};
                var array = [];
                $(this).find('input[name="'+name+'[]"]').each(function(){
                    array.push($(this).val());
                });
                val[key] = array;
            }else{
                name = $(this).parent().attr('name');
                val = [];
                $(this).find('input[name="'+name+'[]"]').each(function(){
                    val.push($(this).val());
                });
            }
        }else if($(this).hasClass('radioType')){
            if($(this).prop('checked') == false)
                return;
            name = $(this).attr('field_name');
            val = $(this).val();
        }
        else if($(this).hasClass('package')){
            name = $(this).attr('field_name');
            val = $(this).val();
        }
        else{
            name = $(this).attr('name');
            val = $(this).val();
            if(IsJsonString(val)){
                val = JSON.parse(val);
            }
        }
        if($(this).hasClass('package')){
            if(str[name]){
                str[name].push(val);
            }else{
                str[name] = [val];
            }

        }else{
            if(isset(str[name])){
                str[name] = $.extend(true,str[name],val);
            }else{
                str[name] = val;
            }

        }
    });
    return str;
}

function update_item(rowid){
    var row_values = get_row_values(rowid);

    $.system_process({
        url : url + control + 'update_row',
        param : {rowid : rowid, row_values : JSON.stringify(row_values)},
        success : function(ret, more){
            reload_table(ret['data']);
            load_right_panel();
        }
    });
}

var set_customer = function(id){
    $.system_process({
        url: url + control + 'set_customer',
        param: {customer_id: id},
        success: function (ret, more) {
            var data = ret['data'];
            if (data['process_type'] == 1) {
                load_customer_change();
                load_sale_change();
            }
        }
    });
};

function load_right_panel(){
    $.system_process({
        url : url + control + 'right_panel',
        success: function(ret,more){
            var json = ret['data'];
            $('#right_panel').html(json.content);
            if($('#customer').length == 1){
                $('#customer').render_autocomplete({
                    source: url + 'customers/suggest?all=true&extend=true',
                    select: function (event, ui) {
                        $(this).val(ui.item.label);
                        var id = ui.item.value;
                        set_customer(id);
                        return false;
                    }
                }).focus(function(){
                    $(this).autocomplete('search', $(this).val());
                });
            }
        }
    });

}

function update_customer(id){
    $.system_process({
        url: url + 'customers/' + "getForm/" + id,
        param: {'inCustomer' : 0},
        success: function(ret){
            $("#birthday").datepicker({changeYear: true, dateFormat: 'yy-mm-dd'});
        },
        button: {success: {success: 'load_right_panel'}}
    });
}

var before_set_pin = function(data){
    data.field_post.pin = $('#pin').val();
    return data;
};

function load_sale_change(){
    load_cart();
    load_right_panel();
}

function load_customer_change(){
    load_categories();
    load_recent_bill();
}

/* --- End of right panel script --- */


/* --- Recent Sales --- */

function load_recent_bill(){
    $.system_process({
        url: url + control + 'recent_bills',
        success: function(ret){
            data = ret.data;
            $('#recent_sale').html(data.content);
        }
    });
}

/* --- End of Recent Sales --- */

function clear_discount(){
    $.system_process({
        url: url + control + 'clear_discount',
        success: load_right_panel
    });
}

function package_change(type,sender){
    if(type == 'delete'){
        var package_id = $(sender).siblings('select').val();
    }else if(type == 'add'){
        var package_id = [];
        $(sender).parent().children('select').each(function(){
            package_id.push($(this).val());
        });
    }
    var rowid = $(sender).closest('tr').attr('rowid');
    var row_values = get_row_values(rowid);

    $.system_process({
        url : url + control + 'package_change',
        param : {rowid:rowid,package_id:package_id,type:type, row_values: row_values},
        success: function(ret,more){
            var data = ret['data'];
            reload_table(data);
            load_right_panel();
        }
    });

}

function convert_customer(customer_id){
    $.system_process({
        url : url + 'customers/' + 'convert_to_member/' + customer_id,
        more: {customer_id:customer_id},
        success: function(ret,more){
        }
    });
}

function before_submit(param,more){
    return {pin:$('#pin').val()};
}

function after_convert_customer(ret,more){
    $('#wf_page_dialog').modal('hide');
    set_customer(more.customer_id)
}