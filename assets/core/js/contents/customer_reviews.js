$(document).ready(function(){
    $('#customer-reviews tfoot th').each( function () {
        var title = $(this).text();
        if(title != 'ID') {
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    } );

    var tables = $('#customer-reviews').DataTable({
        ajax          : staff_site_url,
        deferRender   : true,
        columns       : [
            { data: "id" },
            { data: "createdAt" },
            { data: "review_author" },
            { data: "review_title" },
            { data: "review_body" },
            { data: "review_rating" },
            { data: "bill_code" }
        ],
        paging        : true,
        ordering      : true,
        info          : true,
        initComplete: function ()
        {
            var r = $('#customer-reviews tfoot tr');
            r.find('th').each(function(){
                $(this).css('padding', 8);
            });
            $('#customer-reviews thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
    });

    tables.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });

    $('#reload-table').on('click', function(){
        tables.ajax.reload();
    });
})