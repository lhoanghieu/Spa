/**
 * Created by gento on 2/6/2015.
 */
$(function(){
    $('[name=all_time]').on('change',function(){
        if($(this).prop('checked')){
            $('[name=start_date]').prop('disabled',true);
            $('[name=end_date]').prop('disabled',true);
        }else{
            $('[name=start_date]').prop('disabled',false);
            $('[name=end_date]').prop('disabled',false);
        }
    });

    $('[name=all_time]').trigger('change');
    /*$('#customer_id').bind('input propertychange', function() {
        $('.ms-trigger').trigger('click');
    });*/
});


$('#customer').render_autocomplete({
    source: url + 'customers/suggest?all=true&extend=true',
    select: function (event, ui) {
        $(this).val(ui.item.label);
        var id = ui.item.value;
        $("#customer_id").val(id);
        return false;
    }
}).focus(function(){
    $(this).autocomplete('search', $(this).val());
});

$("#reportform").submit(function(){
    if($("#customer").val() == '')
        $("#customer_id").val(null);
});