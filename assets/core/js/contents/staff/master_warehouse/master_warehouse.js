/**
 * Created by gento on 23/4/2015.
 */


var add_quantity_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_data_before_add_quantity', success:'after_success', more:table_data}};
    $.system_dialog({
        url : url + 'add_quantity_form/'+id,
        more :table_data,
        button: buttons
    });
};


var remove_quantity_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_data_before_remove_quantity', success:'after_success', more:table_data}};
    $.system_dialog({
        url : url + 'remove_quantity_form/'+id,
        more :table_data,
        button: buttons
    });
};

var get_data_before_remove_quantity = function(){
    var id = $('#id').val();
    var new_amount = Number($('#master_quantity').val()) - Number($('#item_change').val());
    $('#master_quantity').val(new_amount);
    $('#item_change').val(0);
    var data = get_insert_values();
    data.id = id;
    data.field_post['log_data'] = {log_type : 'import_pro'};
    return data;
};

var get_data_before_add_quantity = function(){
    var id = $('#id').val();
    var new_amount = Number($('#master_quantity').val()) + Number($('#item_change').val());
    $('#master_quantity').val(new_amount);
    $('#item_change').val(0);
    var data = get_insert_values();
    data.id = id;
    data.field_post['log_data'] = {log_type : 'import_pro'};
    return data;
};