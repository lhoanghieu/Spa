/**
 * Created by gento on 23/4/2015.
 */
var custom_form_render = function(){
    $('#service').on("autocompleteselect", function(event, ui){
        addServiceToRoom(ui.item.value);
        return false;
    });
};


function addServiceToRoom(id){
    var data = $('#service_ids').val();
    data = JSON.parse(data);
    if(data.indexOf(id) != -1){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'You have already added this service to the room.',
            buttons:[{value:'Cancel'}]
        });
        return;
    }

    $.system_process({
        url : url + 'items/getItem/' + id,
        success: function(ret,more){
            var result = ret['data'];
            var html = "";
            html += '<td> <a onclick="return deleteRoomService(this, '+result.id+');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
            html += '<td class="modelname">'+result.name+'</td>';
            var row = document.createElement('tr');
            $('#grid_room_services').append(row);
            $(row).html(html);
            data.push(id);
            $('#service_ids').val(JSON.stringify(data));
            $('#service').val('');
        },
        close_modal: false
    });
}


function deleteRoomService(element,id){
    id = id.toString();

    $(element).parent().parent().remove();
    data = JSON.parse($('#service_ids').val());

    var i = data.indexOf(id);
    if(i!=-1){
        data.splice(i,1);
    }
    $('#service_ids').val(JSON.stringify(data));
}

function disable_room(id,url,table_id, element){

    var table_data = {
        table_id : table_id,
        url : url
    };
    $.system_process({
        url : url + 'disable_room/' + id,
        success: after_success,
        more: table_data
    });
}

function enable_room(id,url,table_id, element){
    var table_data = {
        table_id : table_id,
        url : url
    };
    $.system_process({
        url : url + 'enable_room/' + id,
        success: after_success,
        more: table_data
    });
}