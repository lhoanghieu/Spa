/**
 * Created by Vu Huy on 1/6/2015.
 */
var employees = employees;
var bookings = bookings;
var room_section = [];
var data = [];
var edit_form = $('#my_form');
//Some helper
var html = function(id) { return document.getElementById(id); };
var standard_time_string = scheduler.date.date_to_str("%H:%i");
var standard_date_time_string = scheduler.date.date_to_str("%Y-%m-%d %H:%i:%s");
var to_standard_date_time = scheduler.date.str_to_date('%Y-%m-%d %H:%i:%s');

$(function(){
    render_form();
    ready_data();
    init();
    $('#submit-btn').on('click',function(){
        var id = $('#room_chosen_id').val();
        window.location.href = url + 'staff/scheduler/index/room/' + id;
    });
});
/**
 * Created by Vu Huy on 1/12/2015.
 */

var init = function() {
    scheduler.locale.labels.unit_tab = "Unit";
    scheduler.locale.labels.grid_tab = "Grid";
    scheduler.config.multi_day = true;
    scheduler.config.multisection = true;
    scheduler.config.event_duration = 30;
    scheduler.config.auto_end_date = true;
    scheduler.config.dblclick_create = true;
    scheduler.config.drag_create = true;
    scheduler.config.time_step = 10;
    scheduler.config.details_on_create = true;
    scheduler.config.details_on_dblclick = true;
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.config.limit_time_select = true;
    scheduler.config.first_hour = store_open_hour[0];
    scheduler.config.last_hour = store_open_hour[1];
    scheduler.config.icons_select = [
        "icon_details"
    ];
    if( right['d']){
        scheduler.config.icons_select.push("icon_delete");
    }
    scheduler.config.drag_resize = false;
    scheduler.config.check_limits = false;
    scheduler.config.collision_limit = 100;
    scheduler.config.mark_now = false;

    if(! right['i']){
        scheduler.config.drag_create = false;
        scheduler.config.dblclick_create = false;
    }

    var sections = room_section;
    scheduler.createUnitsView({
        name:"unit",
        property:"section_id",
        list:sections,
        size:10,
        step:10
    });
    scheduler.createGridView({
        name:"grid",
        fields:[    // defines columns of the grid
            {id:"created_date",   label:'Created Time', width:150,sort:'date'},
            {id:"start_date",   label:'Start Time', width:150,sort:'date'},
            {id:"end_date",   label:'End Time', width:150, sort:'date'},
            {id:"customer_name",   label:'Customer',   sort:'str'},
            {id:"employee_name",   label:'Employee',   sort:'str'},
            {id:"service_name",   label:'Service',   sort:'str'},
            {id:"room_name",   label:'Room',   sort:'str'},
            {id:"comment", label:'Comment', sort:'date', width:'*'}
        ]
    });

    scheduler.templates.event_class = function(start,end,event){
        var css = "";
        switch(Number(event.status)){
            case 1:
                css += 'event_complete';
                break;
            case 2:
                css += 'event_hold';
                break;
        }

        return css; // default return
    };

    scheduler.templates.unit_scale_text = function(key, label, unit) {
        return "<a href='{0}'>{1}</a>".format(site_url + 'staff/scheduler/index/room/' + key, label);
    };


    var format = scheduler.date.date_to_str("%h %i %A");

    scheduler.templates.hour_scale = function(date){
        return format(date);
    };

    scheduler.templates.quick_info_content = function(start, end, ev){
        return event_to_text(ev);
    };

    /*--------------------------------*/
    scheduler.attachEvent("onBeforeEventChanged", function(ev, e, is_new){
        if(! isset(ev.is_old)){
            if(! right['i']){
                no_permission_message('You don\'t have permission to insert new booking');
                ev.client_delete = true;
                scheduler.deleteEvent(id);
                return false;
            }
        }else{
            if(! right['e']){
                no_permission_message('You don\'t have permission to edit a booking');
                return false;
            }
        }

        if(is_new)
            return true;
        var isCollisionWithAnotherEvent = scheduler.checkCollision(ev);
        if(!isCollisionWithAnotherEvent){
            $.msgBox({
                type : 'error',
                title: 'message',
                content: 'This employee is not available for this service at this time. Please choose another time'
            });
            return false;
        }

        var isLimitViolation = scheduler.checkLimitViolation(ev);
        if(!isLimitViolation){
            $.msgBox({
                type : 'error',
                title: 'message',
                content: 'This employee is not available for this service at this time. Please choose another time'
            });
            return false;
        }
        $.system_process({
            url : url + 'staff/scheduler/save_event',
            more: {event_id : ev.id},
            param: {field_post: {
                customer_id: ev.customer_id, room_id : ev.section_id,
                employee_id : ev.employee_id, start_time: standard_date_time_string(ev.start_date),
                comment : ev.comment, service_id : ev.service_id,
                is_old : ev.is_old,
                book_id: ev.book_id
            }},
            success: 'after_save_event'
        });
        return false;
    });
    scheduler.attachEvent('onBeforeLightBox',function(id){
        var ev = scheduler.getEvent(id);
        if(! isset(ev.is_old)){
            if(! right['i']){
                no_permission_message('You don\'t have permission to insert new booking');
                ev.client_delete = true;
                scheduler.deleteEvent(id);
                return false;
            }
        }else{
            if(! right['e']){
                no_permission_message('You don\'t have permission to edit a booking');
                return false;
            }
        }

        if(!isset(ev.section_id)){
            ev.section_id = current_room_id;
        }
        popUpFormReady(ev);
        edit_form.modal('show');
    });
    scheduler.attachEvent("onClick", function (id, e){
        //any custom logic here
        var ev = scheduler.getEvent(id);
        scheduler.deleteMarkedTimespan({
            start_date: ev.start_date,
            end_date: ev.end_date,
            sections: {week:ev.section_id,unit:ev.section_id}
        });
        return true;
    });

    scheduler.attachEvent('onBeforeEventDelete',function(id,e){
        var ev = scheduler.getEvent(id);
        if(!isset(ev)){
            return true;
        }else{
            if(isset(ev.is_old)){
                if(! right['d']){
                    no_permission_message("You don't have permission to delete this booking");
                    return false;
                }
            }
        }
        if(isset(ev.client_delete) && ev.client_delete){
            return true;
        }
        $.system_process({
            url : url + 'staff/scheduler/delete_event',
            param: {field_post: {
                book_id: ev.book_id
            }},
            more: {event_id : ev.id},
            success: 'after_delete_event'
        });
        return false;
    });

    scheduler.attachEvent("onTemplatesReady", function() {
        var highlight_step = 60; // we are going to highlight 30 minutes timespan

        var highlight_html = "";
        var hours = scheduler.config.last_hour - scheduler.config.first_hour; // e.g. 24-8=16
        var times = hours*60/highlight_step; // number of highlighted section we should add
        var height = scheduler.config.hour_size_px*(highlight_step/60);
        for (var i=0; i<times; i++) {
            highlight_html += "<div class='highlighted_timespan' style='height: "+height+"px;'></div>"
        }
        scheduler.addMarkedTimespan({
            days: "fullweek",
            zones: "fullday",
            html: highlight_html
        });

        /*---- Set blocked date ----*/
        branch_data.branch_offline_data.forEach(function(obj){
            scheduler.addMarkedTimespan({
                start_date : to_standard_date_time(obj.start_time),
                end_date   : to_standard_date_time(obj.end_time),
                type       :  "dhx_time_block"
            });

            var start_date = to_standard_date_time(obj.start_time);
            start_date = moment(start_date);
            start_date = start_date.format('YYYY-MM-DD');
            scheduler.addMarkedTimespan({
                start_date : to_standard_date_time(start_date + ' 00:00:00'),
                end_date   : to_standard_date_time(start_date + ' 23:59:59'),
                html    : highlight_html,
                type    : 'highlight'
            });
        });
    });

    scheduler.attachEvent("onAfterSchedulerResize",function(){

        var position = $('#scheduler_here').css('position');

        if(position != 'absolute'){
            $('#menu').removeClass('hidden');
        }else{
            $('#menu').addClass('hidden');
        }

        if(position != 'absolute'){
            $('#header').removeClass('hidden');
        }else{
            $('#header').addClass('hidden');
        }
        return true;
    });

    if(date){
        date = to_standard_date_time(date);
    }else{
        date = new Date();
    }
    if(isset(current_room_id))
        scheduler.init('scheduler_here', date, "week");
    else{
        scheduler.init('scheduler_here', date, "unit");
    }
    scheduler.parse(data,"json");
};



function ready_data(){
    var key;
    for(key in rooms){
        room_section.push({
            key : key,
            label : rooms[key].name
        });
    }

    for(key in bookings){
        var obj = bookings[key];
        data.push({
            created_date    :obj.created_date,
            start_date : obj.start_time,
            end_date   : obj.end_time,
            employee   : obj.employee_detail,
            room       : obj.room_detail,
            service    : obj.service_detail,
            customer   : obj.customer_detail,
            employee_id: obj.employee_detail.id,
            customer_id: obj.customer_detail.id,
            service_id : obj.service_detail.id,
            employee_name   : obj.employee_detail.first_name + ' ' + obj.employee_detail.last_name,
            service_name    : obj.service_detail.name,
            customer_name   : obj.customer_detail.first_name + ' ' + obj.customer_detail.last_name,
            room_name       : obj.room_detail.name,
            room_id    : obj.room_id,
            code       : obj.code,
            book_id    : obj.id,
            status     : obj.status,
            comment    : obj.comment,
            text       : obj.service_detail.name,
            section_id : obj.room_detail.id,
            is_old     : 1
        });
    }
}


function show_minical(){
    if (scheduler.isCalendarVisible())
        scheduler.destroyCalendar();
    else
        scheduler.renderCalendar({
            position:"dhx_minical_icon",
            date:scheduler._date,
            navigation:true,
            handler:function(date,calendar){
                scheduler.setCurrentView(date);
                scheduler.destroyCalendar()
            }
        });
}


var popUpFormReady = function(ev){
    $('').system_dialog({
        url     : url + 'staff/scheduler/getForm/' + ev.book_id,
        param   : {room_id : ev.section_id, start_time : standard_date_time_string(ev.start_date), event_id : ev.id},
        type    : 'dialog'
    });
};


/*------ Edit Form Handler Functions ------*/
$(function(){
    var set_room = function(rooms){
        $('#room_id_typeahead').removeAttr('disabled');
        var value, suggestion;
        branch_data.branch_offline_data.forEach(function(obj){
            scheduler.addMarkedTimespan({
                start_date : to_standard_date_time(obj.start_time),
                end_date   : to_standard_date_time(obj.end_time),
                type       :  "dhx_time_block"
            });
        });

        if(Object.keys(rooms).length){
            var section_id = $('#section_id').val();
            value = {value : 0, label : ''};
            suggestion = [];
            for(key in rooms){
                if(key == section_id){
                    value = {value : $('#section_id').val(), label : rooms[key].name};
                }
                suggestion.push({value : key, label : rooms[key].name});
            }
            $('#room_id_typeahead').render_autocomplete({
                value : value,
                source : suggestion,
                target : $(this).attr('target'),
                local : true
            });
        }else{
            value = {value : 0, label : 'No room available for this service and time'};
            suggestion = [{value : 0, label : 'No room available for this service and time'}];
            $('#room_id_typeahead').render_autocomplete({
                value : value,
                source : suggestion,
                target : $(this).attr('target'),
                local : true
            });
        }
    };
    var set_employee = function(employees){
        $('#employee_id_typeahead').removeAttr('disabled');
        var value, suggestion;
        if(Object.keys(employees).length){
            value = {value : 0, label : ''};
            suggestion = [];
            for(key in employees){
                suggestion.push({value : key, label : employees[key].name});
            }
            $('#employee_id_typeahead').render_autocomplete({
                value : value,
                source : suggestion,
                target : $(this).attr('target'),
                local : true
            });
        }else{
            value = {value : 0, label : 'No employee available for this time and service'};
            suggestion = [{value : 0, label : 'No employee available for this time and service'}];
            $('#employee_id_typeahead').render_autocomplete({
                value : value,
                source : suggestion,
                target : $(this).attr('target'),
                local : true
            });
        }
    };
    $('#wf_page_dialog').delegate('#service_id','change',function(){
        if($(this).val() == 0){
            return;
        }
        var id = $('#id').val();
        if(!isset(id)) id = 0;
        $.system_process({
            url : url + 'staff/scheduler/data_for_time_change',
            param : {id : id, service_id:$(this).val(),start_time : ($('#start_time').val())},
            success: function(ret,more){
                var data = ret['data'];
                $('#duration_span').html(data.service_duration);
                $('#duration').val(data.service_duration);
                /*-----Set room -----*/
                set_room(data.rooms);
                /*-----Set Employees ---*/
                set_employee(data.employees);
            }
        });
    });

    $('#wf_page_dialog').delegate('#start_time','change',function(){
        var start_time = $(this).val();
        var service_id = $('#service_id').val();
        var id = $('#id').val();
        $.system_process({
            url : url + 'staff/scheduler/data_for_time_change',
            param: {id : id, service_id : service_id, start_time: start_time},
            success: function(ret,more){
                var data = ret['data'];
                /*-----Set room -----*/
                set_room(data.rooms);
                /*-----Set Employees ---*/
                set_employee(data.employees);
            }
        });
    });
});
/*-----------------------------------------*/

var before_save_event = function(param,more){
    if( ! get_insert_values()){
        return false;
    }
    var customer_id = $('#customer_id').val();
    var service_id  = $('#service_id').val();
    var room_id     = $('#room_id').val();
    var employee_id = $('#employee_id').val();
    var start_time  = $('#start_time').val();
    var comment     = $('#comment').val();
    var is_old      = $('#is_old').val();
    var duration    = $('#duration').val();
    var book_id     = $('#id').val();
    /*----- Check if it is collision or not -----*/
    var ev = scheduler.getEvent(more.event_id);
    var st = to_standard_date_time(start_time);
    var et = moment(st);
    et = et.add(duration,'m').toDate();
    ev.section_id   = room_id;
    ev.employee_id  = employee_id;
    ev.start_date   = st; ev.end_date = et;

    var isCollisionWithAnotherEvent = scheduler.checkCollision(ev);
    if(!isCollisionWithAnotherEvent){
        $.msgBox({
            type : 'error',
            title: 'message',
            content: 'This employee is not available for this service at this time. Please choose another time'
        });
        return false;
    }

    var isLimitViolation = scheduler.checkLimitViolation(ev);
    if(!isLimitViolation){
        $.msgBox({
            type : 'error',
            title: 'message',
            content: 'This employee is not available for this service at this time. Please choose another time'
        });
        return false;
    }

    return {field_post: {
        customer_id: customer_id, room_id : room_id,
        employee_id : employee_id, start_time: start_time,
        comment : comment, service_id : service_id,
        is_old: is_old, book_id : book_id
    }};
};

var after_save_event = function(post_back,more){
    var ev = scheduler.getEvent(more['event_id']);
    ev.client_delete = 1;
    scheduler.deleteEvent(more['event_id']);
    var booking_data = post_back['data'];
    scheduler.addEvent({
        created_date    : obj.created_date,
        start_date : to_standard_date_time(booking_data.start_time),
        end_date   : to_standard_date_time(booking_data.end_time),
        employee   : booking_data.employee_detail,
        room       : booking_data.room_detail,
        service    : booking_data.service_detail,
        customer   : booking_data.customer_detail,
        employee_id: booking_data.employee_detail.id,
        customer_id: booking_data.customer_detail.id,
        service_id : booking_data.service_detail.id,
        employee_name   : booking_data.employee_detail.first_name + ' ' + booking_data.employee_detail.last_name,
        service_name    : booking_data.service_detail.name,
        customer_name   : booking_data.customer_detail.first_name + ' ' + booking_data.customer_detail.last_name,
        room_name       : booking_data.room_detail.name,
        room_id    : booking_data.room_id,
        code       : booking_data.code,
        book_id    : booking_data.id,
        status     : booking_data.status,
        comment    : booking_data.comment,
        text       : booking_data.service_detail.name,
        section_id : booking_data.room_detail.id,
        is_old     : 1
    });
};


$('#wf_page_dialog').on('hidden.bs.modal', function () {
    $(this).find('.lightbox-close[data-dismiss=modal]:first').click();
});

var delete_new_event = function(param,more){
    var ev = scheduler.getEvent(more['event_id']);
    if(ev && typeof ev.is_old == 'undefined' && !ev.is_old){
        ev.client_delete = 1;
        scheduler.deleteEvent(more['event_id']);
    }
};


function full_screen(){
    $('.dhx_expand_icon').click();
}

var after_delete_event = function(param,more){
    var ev = scheduler.getEvent(more['event_id']);
    ev.client_delete = 1;
    scheduler.deleteEvent(more['event_id']);
};

var event_to_text = function(ev){
    return ev.service.name
    + '<br>' + 'Customer: ' + ev.customer.full_name
    + '<br>' + 'Room: ' + ev.room.name
    + '<br>'+ 'Comment: ' + ev.comment;
};


var no_permission_message = function(msg){
    if(!isset(msg)){
        msg = "You don't have permission";
    }
    $.msgBox({
        title : 'No Permission',
        type : 'error',
        content : msg
    });
};


var create_branch_offline_form = function(){
    $.system_dialog({
        url : url + 'config/get_branch_offline_form',
        success: function(){
            var dialog = $('#wf_page_dialog');
            render_form(dialog);
            dialog.find('#offline_date').datetimepicker({
                datepicker: true,
                timepicker: false,
                format:'Y-m-d'
            });
            dialog.find('#offline_from, #offline_to').datetimepicker({
                datepicker: false,
                timepicker: true,
                format:'H:i'
            });
        }
    });
};

var delete_branch_offline_date = function(id){
    $.system_process({
        url: url + 'config/delete_branch_offline_date',
        param: {id:id},
        success: function(result){
            refresh_scheduler();
        }
    });
};

var add_branch_offline_date = function(){
    var id = $('#branch_id').val();
    var data = {
        date : $('#offline_date').val(),
        offline_from : $('#offline_from').val(),
        offline_to   : $('#offline_to').val(),
        offline_whole_day : ($('#offline_whole_day').prop('checked')) ? 1 : 0
    };
    if(data.date && ((data.offline_from && data.offline_to) || data.offline_whole_day)){
        $.system_process({
            url: url + 'config/change_offline',
            param: {id: id,data: data},
            success: function(ret, more){
                refresh_scheduler();
            }
        });
    }else{
        $.msgBox({
            title: "Message",
            type: "error",
            content: 'Missing information. Please choose date and time',
            buttons: [{value: "Cancel"}]
        });
    }
};
var branch_offline_date_checkbox_click = function(element){
    if($(element).prop('checked')){
        $('[key=checkbox-relate]').attr('disabled','disabled');
    }else{
        $('[key=checkbox-relate]').removeAttr('disabled');
    }
};

var refresh_scheduler = function(){
    var date = scheduler.getState().date;
    window.location.href = current_url + "?date=" + standard_date_time_string(date);
};

var get_updated_data = function(){
    $.ajax({
        method: "POST",
        url : url + 'staff/scheduler/get_updated_data',
        data: {marked_time : marked_time},
        dataType : 'json'
    }).done(function( ret ) {
        marked_time = ret.marked_time;
        var new_bookings=  ret.new_bookings;
        if(new_bookings.length){
            var message = 'You have ' + new_bookings.length + ' new booking(s)';
            $.each(new_bookings,function() {
                var booking_data = this;
                message += "<br>Booking on: <a href='{0}'>{1}</a> with <a href='{2}'>{3}</a> at <a href='{4}'>{5}</a>".format(
                    url + 'staff/scheduler/index?date=' + booking_data.start_time, booking_data.start_time,
                    url + 'staff/scheduler/index/employee/' + booking_data.employee_id + '?date=' + booking_data.start_time ,employee_map[booking_data.employee_id].first_name + ' ' +   employee_map[booking_data.employee_id].last_name,
                    url + 'staff/scheduler/index/room/' + booking_data.room_id + '?date=' + booking_data.start_time ,rooms[booking_data.room_id].name
                );
            });
            $.notify({
                message: message,
                type : 'info'
            },{
                allow_dismiss: true,
                placement: {
                    from: "top",
                    align: "center"
                },
                offset : {
                    y : 50
                },
                position: 'fixed'
            });
        }
    });
};
setInterval(get_updated_data,10000);
