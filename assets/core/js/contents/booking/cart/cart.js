/**
 * Created by Vu Huy on 12/15/2014.
 */
$(function(){
    $('input.qty').on('change',function(){
        var rowid = $(this).attr('rowid');
        var qty = $(this).val();

        $.system_process({
            url : url + 'booking/cart/update',
            param :{rowid : rowid, qty : qty},
            success: function(ret,more){
                var data = ret['data'];
                $('#subtotal').text(data['subtotal']);
            }
        });

    });
});

function delete_item(rowid){

    $.system_process({
        url : url + 'booking/cart/update',
        param : {rowid : rowid, qty:0},
        success: function(ret,more){
            var data = ret['data'];
            $('#'+rowid).remove();
            $('#subtotal').text(data['subtotal']);
        }
    });

}

function clear_cart(){

    $.system_process({
        url : url + 'booking/cart/clear',
        success: function(ret,more){
            window.location.reload();
        }
    });

}