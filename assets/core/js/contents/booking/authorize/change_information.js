/**
 * Created by Vu Huy on 12/26/2014.
 */
$('#birthday').datetimepicker({
    timepicker: false,
    format: 'd-m-Y'
});

$('#submit-btn').on('click',function(){
    var data = get_insert_values();
    if(data == false){
        return;
    }

    $.system_process({
        url     : url + 'booking/authorize/change_information',
        param   : {data:JSON.stringify(data.field_post)},
        success: function (res) {
            $('.modal-content').mouseover(function(event) {
                window.location.href = url + 'booking/book';
            });
        }
    });
});