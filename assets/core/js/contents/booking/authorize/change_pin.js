/**
 * Created by Vu Huy on 12/13/2014.
 */
$(function (){
    $('#submit-btn').click(function (e) {
        e.preventDefault();
        var $this = $(e.currentTarget),
            inputs = {};
        // Send all form's inputs
        var form_pin_current = $('#form_pin_current').val();
        var form_pin_first = $('#form_pin_first').val();
        var form_pin_second = $('#form_pin_second').val();
        if(form_pin_first != form_pin_second){
            $.msgBox({
                title:"Message",
                type:"alert",
                content:"Pin and repin is not the same",
                buttons:[{value:"Cancel"}]
            });
            return false;            }
        else
        {
            // Send form into ajax
            $('#gento_loading').slideDown(100);
            $.system_process({
                url     : url + 'booking/authorize/change_pin',
                param   : {old_pin : form_pin_current, new_pin : form_pin_first},
                success: function (res) {
                    setTimeout(function () {
                        window.location.href = url + 'booking/book';
                    }, 1900);
                }
            });
        }


    });
});