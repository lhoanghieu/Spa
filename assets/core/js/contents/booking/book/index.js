/**
 * Created by Vu Huy on 12/16/2014.
 */
var step = 1;
var step_progress_value;
var maxStep;
var intvalid;
var promotion = 0;
var detectSrc = '';
$(function(){
    var isInIframe = (parent !== window),
    parentUrl = null;
    if (isInIframe) {
        parentUrl = document.referrer;
    }
    detectSrc = parentUrl;

    if(message){
        bootbox.alert(message);
    }

    maxStep = $('td.process').length;

    if(sessionStorage.getItem('is_scheduler') != 1 || urlParam('rid') == null){
        load_step();
    }

    $('body').delegate('.process.passed','click',function(){
        step = $(this).attr('value');
        change_section(step);
    });
    $('div.row[step="5"]').delegate('.back-process','click',function(){
        step = $(this).attr('value');
        change_section(step);
    });
    //var percent = $('.menu-step.text-center.process')[0].style.width;
    step_progress_value = 100/maxStep;

    if(promotion == 1){
        $('#service-chosen').val(sv_id_url);
        $('#branch-chosen').val(br_id_url);
        sessionStorage.setItem('service_name',sv_nm_url);
        sessionStorage.setItem('branch_name',br_nm_url);
        load_step(2,{branch_id:br_id_url});
        $('.service_id_363').css('font-weight','bold');
    }
    else{
        sessionStorage.setItem('service_name','');
        sessionStorage.setItem('branch_name','');
        $('#service-chosen').val('');
        $('#branch-chosen').val('');
    }
});

editapp();

function change_section(_step){
    if(typeof _step == 'undefined'){
        _step = 1;
    }
    step = _step;
    $('.section-show').addClass('hidden');
    $('.section-show[step="' + step + '"]').removeClass('hidden');
    for(var i = 1; i < step; i++){
        $('td.process[value="'+i+'"]').addClass('passed');
    }
    for(var i = step; i <= maxStep; i++){
        $('td.process[value="'+i+'"]').removeClass('passed');
    }
    $('#progressbar').progressbar({value:(step)*step_progress_value});
    load_step(step);
}

function load_step(id,data,previous,type){
    if(typeof(id) == 'undefined'){
        id = 1;
    }
    if(typeof(data) == 'undefined'){
        data = [];
    }
    var login_step = sessionStorage.getItem('login_step');
    if(typeof login_step !== 'undefined' && login_step !== '' && login_step !== null){
        id = parseInt(login_step);
        sessionStorage.removeItem('login_step');
    }

    var async = true;
    if(id == 1 || id == 2 || id == 3 || id == 4){
        async = true;
    }
    else{
        async = false;
    }

    $.system_process({
        url : url + 'booking/book/get_step',
        param : {step : id, data: JSON.stringify(data), previous: previous, referrer: detectSrc},
        async: async,
        success: function(ret,more){
            var data = ret['data'];
            $('.section-show').addClass('hidden');
            $('.section-show[step="' + id + '"]').removeClass('hidden');
            $('div[step="' + id + '"]').html(data['html']);
            $('#progressbar').progressbar({value:(id)*step_progress_value});

            for(i = 1; i < id; i++){
                $('td.process[value="'+i+'"]').addClass('passed');
            }
            for(i = id; i <= maxStep; i++){
                $('td.process[value="'+i+'"]').removeClass('passed');
            }
            if (id == 1) {
                load_service();
            }

            step = id==1?id:id - 1;
            if(step==5){
                window.history.pushState('', '', url + 'booking/book/confirmed');
            }
            if(step==6){
                for (i = 0; i <= maxStep; i++) {
                    $('td.process[value="' + i + '"]').removeClass('passed').removeClass('process');
                }
                $('#progressbar').progressbar({value: 100});
            }

            sessionStorage.setItem('currentUniqueId', JSON.stringify(data['currentUniqueId']));
            sessionStorage.setItem('session',JSON.stringify(data['session']));
            sessionStorage.setItem('pending_appointment',JSON.stringify(data['pending_appointment']));
            sessionStorage.setItem('step',JSON.stringify(id));
            id = parseInt(id);
            window.clearInterval(intvalid);
            var prepareData = $.parseJSON(sessionStorage.getItem('prepareData'));
            var html = '';var ip = 1;
            switch(id){
                case 1: {
                    $('#progressbar').progressbar({value: 7});
                    if(typeof data['session']['branch_id'] !== 'undefined') {
                        var ele = '.branch_id_' + data['session']['branch_id'];
                        $(ele).css('color', '#593729');
                    }
                    $('#load_backward').html('');
                    $('#load_backward').removeClass('alert alert-info custom-backward');
                } break;
                case 2: {
                    $('#progressbar').progressbar({value: 23});
                    var ele = '.service_id_'+data['session']['service_id'];
                    $(ele).css('color','#593729');
                    html = renderTrackback(data['session'],prepareData);
                    $('#load_backward').html('');
                    $('#load_backward').html(html);
                    $('#load_backward').addClass('alert alert-info custom-backward');
                    if(promotion == 1){
                        load_service(1);
                    }
                } break;
                case 3: {
                    $('#progressbar').progressbar({value: 40});
                    html = renderTrackback(data['session'],prepareData);
                    $('#load_backward').html('');
                    $('#load_backward').html(html);
                    $('#load_backward').addClass('alert alert-info custom-backward');
                } break;
                case 4: {
                    $('#progressbar').progressbar({value: 57});
                    html = renderTrackback(data['session'],prepareData);
                    $('#load_backward').html('');
                    $('#load_backward').html(html);
                    $('#load_backward').addClass('alert alert-info custom-backward');

                    if(sessionStorage.getItem('ctm_information')){
                        var ctm = $.parseJSON(sessionStorage.getItem('ctm_information'));
                        $('#ctm_firstname').val(ctm['firstname']);
                        $('#ctm_lastname').val(ctm['lastname']);
                        $('#ctm_phone').val(ctm['phone']);
                        $('#ctm_email').val(ctm['email']);
                        $('#ctm_comment').val(ctm['comment']);
                    }
                    var is_login = data['session']['is_login'];
                    if(sessionStorage.getItem('is_scheduler') != 1 || urlParam('rid') == null){
                        startTimer(300,$('.minsremaining'));
                    }
                } break;
                case 5: {
                    $('#progressbar').progressbar({value: 73});
                    $('#load_backward').html('');
                    $('#load_backward').removeClass('alert alert-info custom-backward');
                    if(sessionStorage.getItem('is_scheduler') != 1 || urlParam('rid') == null){
                        startTimer(300,$('.minsremaining'));
                    }
                } break;
                case 6: {
                    $.each(data['session'], function(i,item){
                        Send_booking(item['code']);
                    });

                    sessionStorage.removeItem('session');
                    sessionStorage.removeItem('step');
                    sessionStorage.removeItem('login_step');
                    fb_tracking();
                }
            }
            //$('#progressbar').remove();
            $('.ui-progressbar-value').append('<img id="progressbar_end_img" src="'+url+'assets/core/images/progressbar_end.png" />');
        }
    });

    var branch_id = data.branch_id;
    var the_session = sessionStorage.getItem('session');
    if(typeof(branch_id) == 'undefined'){
        branch_id = 1;
        if(the_session != null)
            branch_id = JSON.parse(sessionStorage.getItem('session'))['branch_id']
    }
    if(id == 1){
        $.system_process({
            url : url + 'booking/book/get_branch_info',
            param : {id: branch_id},
            async: false,
            success: function(result){
                var data = result.data;
                /*var html =  '<tr><td>Hours</td><td>Mon</td><td> '+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Tue</td><td>'+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Wed</td><td>'+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Thu</td><td> '+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Fri</td><td> '+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Sat</td><td>'+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>'+
                    '<tr><td></td><td>Sun</td><td>'+moment((moment().format('YYYY-MM-DD ')+data.start_time.value)).format('hh:mma')+' - ' + moment((moment().format('YYYY-MM-DD ')+data.end_time.value)).format('hh:mma') +'</td></tr>';
                    html += '<tr><td style="width: 80px;">Phone</td><td colspan="2">'+data.branch_phone+'</td></tr>';
                    html += '<tr><td style="width: 80px;" valign="top">Address:</td><td colspan="2">'+data.branch_add+'</td></tr>';
                    html += '<tr><td style="width: 80px;" valign="top">Website</td><td colspan="2"><a style="text-decoration: underline;" href="http://www.healingtouch.com.sg">www.healingtouch.com.sg</a></td></tr>';*/
                var html =  '<tr><td style="width: 100px;">Operating Hours</td><td style="padding-left: 5px;">Daily 11am - 10:30pm</td></tr>'+
                    '<tr><td>Last appointment</td><td style="padding-left: 5px;">9pm</td></tr>';
                    html += '<tr><td style="width: 80px;">Phone</td><td colspan="2" style="padding-left: 5px;">'+data.branch_phone+'</td></tr>';
                    html += '<tr><td style="width: 80px;" valign="top">Address:</td><td colspan="2" style="padding-left: 5px;">'+data.branch_add+'</td></tr>';
                    html += '<tr><td style="width: 80px;" valign="top">Website</td><td colspan="2" style="padding-left: 5px;"><a style="text-decoration: underline;" href="http://www.healingtouchspa.com">www.healingtouchspa.com</a></td></tr>';
                if(the_session == null) {
                    html =  '<tr><td style="width: 100px;">Operating Hours</td><td style="padding-left: 5px;">Daily 11am - 10:30pm</td></tr>';
                    html +='<tr><td>Last appointment</td><td style="padding-left: 5px;">9pm</td></tr>';
                    html += '<tr><td style="width: 80px;" valign="top">Website</td><td colspan="2" style="padding-left: 5px;"><a style="text-decoration: underline;" href="http://www.healingtouchspa.com">www.healingtouchspa.com</a></td></tr>';
                }
                $('#business_info').html('');
                $('#business_info').html("<tbody>"+html+"</tbody>");
            }
        });
    }
    else{
        $('#business_info').html('<tr><td>Operationg Hours</td><td style="padding-left: 5px;">Daily 11am - 10:30pm</td></tr>'+
            '<tr><td>Last appointment</td><td style="padding-left: 5px;">9pm</td></tr>'+
            '<tr><td>Hotline</td><td style="padding-left: 5px;">6715 1515</td></tr>'+
            '<tr><td>Website</td><td style="padding-left: 5px;"><a href="http://healingtouchspa.com">www.healingtouchspa.com</a></td>'+
            '</tr>');
    }
}

function temp_load_step(step, uniqueId, is_res){
    $.ajax({
        url : url + 'booking/book/switchCurrentUniqueId',
        type: 'POST',
        data: {
            uniqueId : uniqueId,
        },
        async: false,
        success: function(res){
            load_step(step,is_res);
        }
    });
}

function renderTrackback(data, prepareData){
    var html = '';
    var ip = 1;
    $.each(data,function(i,item){
        html += '<div><span><b>Appointment '+ip+':</b></span>';
        if(item.service_id){
            html += '<br><span class="booking_trackback_span">- You has choosen <b>'+prepareData.serviceData[parseInt(item.service_id)].name+'</b></span><span class="link_change_process" onclick="temp_load_step(2,\''+i+'\');">Edit</span>';
        }
        if(item.branch_id){
            html += '<br><span class="booking_trackback_span">- in <b>'+prepareData.branchData[parseInt(item.branch_id)].name+'</b></span><span class="link_change_process" onclick="temp_load_step(1,\''+i+'\');">Edit</span>';
        }
        if(item.staff_id){
            html += '<br><span class="booking_trackback_span">- with <b>'+((prepareData.staffData[parseInt(item.staff_id)]) ? prepareData.staffData[parseInt(item.staff_id)].first_name : 'No Preference')+'</b></span><span class="link_change_process" onclick="temp_load_step(3,\''+i+'\');">Edit</span>';
        }
        if(item.date_time){
            html += '<br><span class="booking_trackback_span">- at <b>'+item.date_time+'</b></span><span class="link_change_process" onclick="temp_load_step(3,\''+i+'\');">Edit</span>';
        }
        html += '</div>';
        ip++;
    });
    return html;
}

function load_service(category_id){
    if(typeof(category_id) == 'undefined'){
        category_id = $('.show-service:first').attr('category-id');
        sender = $('.show-service:first');
    }

    $('.show-service').addClass('hidden');
    $('.child-categories').removeClass('child-active');

    $('.child-categories[category-id="'+category_id+'"]').addClass('child-active');
    $('.show-service[category-id="'+category_id+'"]').removeClass('hidden');
    if(promotion == 1){
        $('.service_id_363').css('font-weight','bold');
    }
}

function choose_branch(id,ele){
    $('#branch-chosen').val(id);
    sessionStorage.setItem('branch_name',$.trim($(ele).children().first().text()));
    load_step(step=2,{
        currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
        branch_id:id,
        user_time: moment().format('HH:mm')
    });
}

function choose_service(id,ele){
    $('#service-chosen').val(id);
    var branch_id = $('#branch-chosen').val();
    if(branch_id == ''){
        branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
    }
    sessionStorage.setItem('service_name',$.trim($(ele).children().first().text()));
    load_step(step=3,{
        currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
        branch_id:branch_id,
        service_id:id
    });
}

function choose_staff(id){
    $('#staff-chosen').val(id);
    var service_id = $('#service-chosen').val();
    if(service_id == ''){
        service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
    }
    var branch_id = $('#branch-chosen').val();
    if(branch_id == ''){
        branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
    }
    var date_time = $('#time-chosen').val();
    var staff_id = $('#employee-chosen').serializeJSON(); staff_id = staff_id['employee_id'];



    load_step(step=4,{
        currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
        staff_id:id,
        service_id:service_id,
        branch_id:branch_id,
        date_time: date_time
    });
}

function choose_staff_indate(more){
    var continues = false;
    if(more === 'MORE'){
        $.ajax({
            url: url+'booking/book/checkMaxNumAppts',
            async: false,
            data: {
                branch_id : $('#branch-chosen').val() != '' ? $('#branch-chosen').val() : JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id
            },
            success: function(res){
                res = $.parseJSON(res);
                if(res.status === 'F'){
                    continues = true;
                    alert(res.message);
                    return false;
                }
                else{
                    continues = false;
                }
            }
        });
    }

    if(continues){
        return false;
    }

    if(($('#employee_id_choosen').val() <= -2)) {
        alert('Please choose Employee first.');
        return false;
    }
    if($('td.xdsoft_current.xdsoft_disabled').length > 0){
        alert('Please choose the day.');
        return false;
    }
    if($('.xdsoft_time.xdsoft_current').length == 0 || (sessionStorage.getItem('select_time') == undefined || sessionStorage.getItem('select_time') != 1)){
        alert('Please choose the time.');
        return false;
    }
    if(($('#employee_id_choosen').val() <= -2)) {
        alert('Please choose Employee first.');
        return false;
    }

    var service_id = $('#service-chosen').val();
    if(service_id == ''){
        service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
    }
    var branch_id = $('#branch-chosen').val();
    if(branch_id == ''){
        branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
    }
    var date_time = $('#time-chosen').val();
    var staff_id = $('#employee-chosen').serializeJSON();
    staff_id = parseInt(staff_id['employee_id']);
    if(more === 'MORE'){
        load_step(step = 4, {
            type : 'MORE',
            currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
            staff_id: staff_id,
            service_id: service_id,
            branch_id: branch_id,
            date_time: date_time
        }, previous_step = 3);
    }
    else{
        sessionStorage.setItem('staff_name', $('#employee_id_choosen :selected').text());
        sessionStorage.setItem('datetime_label', $('#time-chosen').val());
        load_step(step = 4, {
            currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
            staff_id: staff_id,
            service_id: service_id,
            branch_id: branch_id,
            date_time: date_time
        }, previous_step = 3);
    }
}

function check_input_booking(){
    if($('#ctm_firstname').val() == '' || $('#ctm_firstname').val() == undefined || $('#ctm_firstname').val() == null){
        $('#check_fill_information').addClass('alert alert-danger')
            .html('You must enter a first name.');
        return false;
    }
    // if($('#ctm_lastname').val() == '' || $('#ctm_lastname').val() == undefined || $('#ctm_lastname').val() == null){
    //     $('#check_fill_information').addClass('alert alert-danger')
    //         .html('You must enter a last name.');
    //     return false;
    // }
    if($('#ctm_phone').val() == '' || $('#ctm_phone').val() == undefined || $('#ctm_phone').val() == null){
        $('#check_fill_information').addClass('alert alert-danger')
            .html('You must enter a valid contact phone number.');
        return false;
    }
    else{
        if(!isNaN($('#ctm_phone'))){
            $('#check_fill_information').addClass('alert alert-danger')
                .html('Contact phone number must be a number.');
            return false;
        }
        else{
            if($('#ctm_phone').val().length < 8 || $('#ctm_phone').val().length > 17){
                $('#check_fill_information').addClass('alert alert-danger')
                    .html('You must enter a valid contact phone number.');
                return false;
            }
        }
    }
    if($('#ctm_email').val() == '' || $('#ctm_email').val() == undefined || $('#ctm_email').val() == null){
        $('#check_fill_information').addClass('alert alert-danger')
            .html('You must enter a valid email.');
        return false;
    }
    else{
        if( !isValidEmailAddress( $('#ctm_email').val() ) ){
             $('#check_fill_information')
                 .addClass('alert alert-danger')
                 .html('You must enter a valid format email.');
             return false;
        }
    }
    return true;
}

function finish_enter_comment(more){
    var continues = false;
    if(more === 'MORE'){
        $.ajax({
            url: url+'booking/book/checkMaxNumAppts',
            async: false,
            data: {
                branch_id : $('#branch-chosen').val() != '' ? $('#branch-chosen').val() : JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id
            },
            success: function(res){
                res = $.parseJSON(res);
                if(res.status === 'F'){
                    continues = true;
                    alert(res.message);
                    return false;
                }
                else{
                    continues = false;
                }
            }
        });
    }

    if(continues){
        return false;
    }


    var service_id = $('#service-chosen').val();
    if(service_id == ''){
        service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
    }
    var branch_id =  $('#branch-chosen').val();
    if(branch_id == ''){
        branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
    }
    var staff_id = $('#employee-chosen').serializeJSON();
    staff_id = staff_id['employee_id'];
    var comment = $('#your_comment').val();
    var date_time = $('#time-chosen').val();

    if(!check_input_booking()){
        return false;
    }

    var pending_appointment = JSON.parse(sessionStorage.getItem('pending_appointment'));
    pending_appointment.start_time = moment().format('YYYY-MM-DD HH:mm:');
    var ctm_information = {
        'firstname' : $('#ctm_firstname').val(),
        'lastname'  : $('#ctm_lastname').val(),
        'phone'     : $('#ctm_phone').val(),
        'email'     : $('#ctm_email').val(),
        'comment'   : $('#ctm_comment').val()
    };
    sessionStorage.setItem('ctm_information', JSON.stringify(ctm_information));

    if(more === 'MORE'){
        load_step(step=5,{
            currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
            type : 'MORE',
            service_id      : service_id,
            branch_id       : branch_id,
            staff_real      : (staff_id==-1?staff_id:''),
            staff_id        : (staff_id==-1?JSON.parse(sessionStorage.getItem('session'))[$.parseJSON(sessionStorage.getItem('currentUniqueId'))].staff_id:staff_id),
            ctm_information  : ctm_information,
            date_time       : date_time
        }, previous_step = 4);
    }
    else{
        load_step(step=5,{
            currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
            service_id      : service_id,
            branch_id       : branch_id,
            staff_real      : (staff_id==-1?staff_id:''),
            staff_id        : (staff_id==-1?JSON.parse(sessionStorage.getItem('session'))[$.parseJSON(sessionStorage.getItem('currentUniqueId'))].staff_id:staff_id),
            ctm_information  : ctm_information,
            date_time       : date_time
        }, previous_step = 4);
    }
}

function finish(more){
    var continues = false;
    if(more === 'MORE'){
        $.ajax({
            url: url+'booking/book/checkMaxNumAppts',
            async: false,
            data: {
                branch_id : $('#branch-chosen').val() != '' ? $('#branch-chosen').val() : JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id
            },
            success: function(res){
                res = $.parseJSON(res);
                if(res.status === 'F'){
                    continues = true;
                    alert(res.message);
                    return false;
                }
                else{
                    continues = false;
                }
            }
        });
    }

    if(continues){
        return false;
    }

    if(more !== 'MORE'){
        if ($('input#agreement').is(':checked')) ;
        else{
            $.msgBox({
                title: "Message",
                type: "error",
                content: 'Please check I have read the spa policy',
                buttons: [{value: "Cancel"}]
            });
            return false;
        }
    }



    var service_id = $('#service-chosen').val();
    if(service_id == ''){
        service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
    }
    var branch_id =  $('#branch-chosen').val();
    if(branch_id == ''){
        branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
    }
    var staff_id = $('#employee-chosen').serializeJSON();
    staff_id = staff_id['employee_id'];
    var comment = $('#your_comment').val();
    var date_time = $('#time-chosen').val();
    var ctm_information = {
        'firstname' : $('#ctm_firstname').val(),
        'lastname'  : $('#ctm_lastname').val(),
        'phone'     : $('#ctm_phone').val(),
        'email'     : $('#ctm_email').val(),
        'comment'   : $('#ctm_comment').val()
    };

    if(more === 'MORE'){
        load_step(step = 6, {
            type : 'MORE',
            currentUniqueId: $.parseJSON(sessionStorage.getItem('currentUniqueId')),
            staff_id: staff_id,
            service_id: service_id,
            branch_id: branch_id,
            date_time: date_time
        });
    }
    else{
        load_step(step=6);
    }
}

//print booking
function Print_booking(code_booking){
    var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    disp_setting += "scrollbars=yes,width=1000, height=700, left=0.5, top=0.5";
    window.open(url +'booking/book/print_booking/'+code_booking,"certificate",disp_setting);
}

function Save_booking(code_booking){
    window.location.href = url + "booking/book/save_booking/" + code_booking;
}

function Send_booking(code_booking){
    $.system_process({
        url : url + 'booking/book/send_booking/' + code_booking
    });
}

function login_booking_system(){
    $.system_process({
        url : url + 'booking/authorize/login',
        param : {_email : $('#txt-email2').val(), _password: $('#txt-password2').val(), _step: sessionStorage.getItem('step')},
        async: false,
        success: function(ret,more){
            sessionStorage.setItem('login_step',4);
            location.reload();
        }
    });
}

function on_show_tooltip_login(){
    if($('#form-login-process').hasClass('is_open')){
        $('#form-login-process').hide();
        $('#form-login-process').removeClass('is_open');
    }
    else{
        $('#form-login-process').show();
        $('#form-login-process').addClass('is_open');
    }
}

function startTimer(duration, display) {
    var p_a = JSON.parse(sessionStorage.getItem('pending_appointment'));
    duration = duration - (moment(moment().utcOffset(480).format('YYYY-MM-DD HH:mm:ss')).unix() - moment(p_a.start_time).unix());
    var timer = duration, minutes, seconds;
    if(timer > 0) {
        intvalid = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.text(minutes + ":" + seconds);
            if (--timer < 0) {
                clearInterval(intvalid);
                var pending_appointment = JSON.parse(sessionStorage.getItem('pending_appointment'));
                $.system_process({
                    url : url + 'booking/book/delete_pending_appointment',
                    param : {pending_id: pending_appointment.pending_id},
                    success: function(ret,more){
                    }
                });
            }
        }, 1000);
    }
    else{
        var pending_appointment = JSON.parse(sessionStorage.getItem('pending_appointment'));
        $.system_process({
            url : url + 'booking/book/delete_pending_appointment',
            param : {pending_id: pending_appointment.pending_id},
            success: function(ret,more){
            }
        });

    }
}

function cancelapp(){
    $.system_process({
        url : url + 'booking/book/cancelapp',
        success: function(ret){
            if(ret.data == "success"){
                alert("Your appointment has been cancelled");
                window.location.href = url + "booking/book";
            }
            else {
                alert("Error when cancel appointment");
                window.location.href = url + "booking/book";
            }
        }
    });
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}


function fb_tracking(){
    (function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded){
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s); _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6026383898764',
        {'value':'0.00','currency':'USD'}
    ]);
}

function editapp() {
    var aid = urlParam('rid');
    var isLogged = urlParam('isl');
    if (aid != null) {
        $.system_process({
            url: url + 'booking/book/check_valid_editapp',
            param: {aid: aid, isLogged: isLogged},
            type: 'POST',
            async: false,
            success: function (ret) {
                var newdata = ret.data;
                if (ret.status == "S") {
                    sessionStorage.setItem('currentUniqueId', newdata.currentUniqueId);
                    sessionStorage.setItem('branch_name', newdata.branch_name);
                    sessionStorage.setItem('service_name', newdata.service_name);
                    sessionStorage.setItem('staff_name', newdata.employeename);
                    sessionStorage.setItem('datetime_label', newdata.start_time);

                    sessionStorage.setItem('is_scheduler',1);
                    $('#service-chosen').val(newdata.service_id);
                    $('#branch-chosen').val(newdata.branch_id);
                    temp_load_step(5, newdata.currentUniqueId);
                }
            }
        });
    }
}

function canceleditapp() {
    $.system_process({
        url: url + 'booking/book/resetBooking',
        type: 'POST',
        success: function (ret) {
            sessionStorage.removeItem('session');
            sessionStorage.removeItem('step');
            sessionStorage.removeItem('login_step');
            sessionStorage.removeItem('currentUniqueId');
            sessionStorage.removeItem('branch_name');
            sessionStorage.removeItem('service_name');
            sessionStorage.removeItem('staff_name');
            sessionStorage.removeItem('datetime_label');
            sessionStorage.removeItem('is_scheduler');
            window.location.href = url + "booking/book";
        }
    });
}

function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}


function fb_tracking(){
    (function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded){
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s); _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6026383898764',
        {'value':'0.00','currency':'USD'}
    ]);
}