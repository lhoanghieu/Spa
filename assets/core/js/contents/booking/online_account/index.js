var sectionBookingTable;
var sectionCreditHistory;
var resetCustomerPin;


$('.tabs>nav>ul>li>a').on('click',function(){
    var sectionId = $(this).attr('href').substr(1);

    $('.tabs>nav>ul>li').removeClass('tab-current');
    $(this).parent().addClass('tab-current');

    $('#'+sectionId).block({
        message: '<img src="'+url+'assets/core/images/loader.svg" alt="Loading...">',
        css: {
            'opacity' : 1
        }
    });
    switch (sectionId){
        case 'section-booking': {
            $('#section-credit-history').hide();
            $('#section-booking').show();
            if(!$.fn.DataTable.fnIsDataTable($('#section-booking-table'))) {
                sectionBookingTable = $('#section-booking-table').DataTable({
                    ajax: sectionBookingData,
                    deferRender: true,
                    columns: [
                        {data: "booking_code"},
                        {data: "start_time"},
                        {data: "therapist"},
                        {data: "branch_name"},
                        {data: "service"},
                        {data: "status"}
                    ],
                    columnDefs: [{
                         "targets": [6],"data": null,
                        "render": function ( data, type, full, meta ) {
                            if(data.allow_edit === 1){
                                return '<button title="Reschedule" type="button" class="btn btn-default btn-sm rescheduler" data-row-id="'+data.id+'"><span class="glyphicon glyphicon-pencil"></span></button>'+
                                       '<button title="Cancel" type="button" class="btn btn-default btn-sm cancel" data-row-id="'+data.id+'"><span class="glyphicon glyphicon-remove"></span></button>';
                            }
                            else{
                                return '';
                            }
                        }
                     }],
                    paging: true,
                    ordering: false,
                   // order: [[1,"asc"]],
                    info: true,
                    initComplete: function () {
                        $('#' + sectionId).unblock();
                    }
                });
            }
            else{
                sectionBookingTable.ajax.reload(function(){
                    $('#' + sectionId).unblock();
                });
            }
        } break;
        case 'section-credit-history' : {
            $('#section-booking').hide();
            $('#section-credit-history').show();
            bootbox.confirm({
                title: 'Confirm',
                message: '<div><p>Please enter your PIN to view Credit History</p><div>' +
                'If you have forgotten your pin (<a href="' + url + 'booking/authorize/change_pin' + '" class="create-reset-customer-pin">CLICK HERE</a>), to create your new pin <br><br>' +
                '<input type="password" id="checkPin" class="form-control input-sm" /></div></div>',
                callback: function(result){
                    if(result) {
                        $.ajax({
                            url: sectionCreditHistory,
                            data: {
                                pin: $('#checkPin').val()
                            },
                            type: 'POST',
                            success: function (res) {
                                res = $.parseJSON(res);
                                $('#' + sectionId).html(res.message);
                            },
                            complete: function () {
                                $('#' + sectionId).unblock();
                            }
                        });
                    }
                    else{
                        return true;
                    }
                },
                onEscape: function() {},backdrop: true
            });
            $('#section-credit-history').html('You have not input your pin or it is incorrect, if you have forgotten your pin please set up <a href="' + url + 'booking/authorize/change_pin' + '" class="create-reset-customer-pin" style="color:#3c2313">HERE</a>.');
        } break;
        case 'section-review': {
            $('#' + sectionId).unblock();
        } break;
    }
});

$('body').delegate(".crete-reset-customer-pin",{
    click: function(){
        bootbox.dialog({
            title: 'Select type to send notification',
            message: 'Please choose the methods you preferer : <br><br> '+
            '<input type="radio" name="send_method" value="1"> SMS<br>'+
            '<input type="radio" name="send_method" value="2"> Email<br>'+
            '<input type="radio" name="send_method" value="3" selected="selected"> Both SMS and Email',
            onEscape: function() {},backdrop: true,
            buttons: {
                Send: {
                    label: "Send",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            url: resetCustomerPin,
                            type: 'POST',
                            data : {
                                send_method : $('input[name="send_method"]:checked').val()
                            },
                            success : function (res) {
                                res = $.parseJSON(res);
                                if(res.status === 'S'){
                                    bootbox.alert(res.message);
                                }
                                else{
                                    bootbox.alert(res.message);
                                }
                            }
                        })
                    }
                },
                Cancel: {
                    label: "Cancel",
                    className: "btn-danger",
                    callback: function() {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }
})

$(document).ready(function(){
    $('#section-booking').delegate('.rescheduler','click',function(){
        var bookingId = $(this).attr('data-row-id');
        if(bookingId !== null && bookingId !== undefined && bookingId !== ''){
            bootbox.confirm('Do you really want to Reschedule?',function(result){
                if(result) {
                    window.open(url+'booking/book?rid='+bookingId+'&isl=1');
                }
            });
        }
    });

    $('#section-booking').delegate('.cancel','click',function(){
        var bookingId = $(this).attr('data-row-id');
        if(bookingId !== null && bookingId !== undefined && bookingId !== ''){
            bootbox.confirm('Do you really want to CANCEL appointment?',function(result){
                if(result) {
                    $.ajax({
                        url : url+'api/cancelAppointment?id='+bookingId,
                        success : function(res){
                            res = $.parseJSON(res);
                            if(res.result){
                                bootbox.alert('Appointment cancelled successfully.');
                            }
                            else{
                                bootbox.alert(res.message);
                            }
                            sectionBookingTable.ajax.reload();
                        }
                    });
                }
            });
        }
    });

    $('#section-credit-history').delegate('#package_credit_history','change',function(){
        var id = $(this).val();
        $('.credit_item').fadeOut(0).hide();
        if(id !== '') {
            $('.' + id).removeClass('hide').fadeIn(500).show();
        }
    });

    // new CBPFWTabs($('.tabs')[0]);

    $('li.tab-current>a').trigger('click');
});

