/**
 * Created by Vu Huy on 11/5/2014.
 */
$(function(){
    render_form();

    $('input[type=checkbox]').on('click',function(){
        var target = $(this).attr('target');
        if($(this).prop('checked')){
            $.system_process({
                url : url + 'config/get_config_inherit_value/' + target,
                success: function(ret,more){
                    $('#' + target).attr('old-data',$('#' + target).val());
                    $('#' + target).val(ret['data']);
                }
            });

            $('#' + target).attr('disabled','disabled');
        }else{
            $('#' + target).removeAttr('disabled','');
            $('#' + target).val($('#' + target).attr('old-data'));
        }
    });

    $('input[type=checkbox]').each(function(){
        var target = $(this).attr('target');
        if($(this).prop('checked')){
            $('#' + target).attr('disabled','disabled');
        }else{
            $('#' + target).removeAttr('disabled','');
        }
    });
});
