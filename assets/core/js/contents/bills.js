/**
 * Created by Vu Huy on 12/8/2014.
 */

$(function(){
    render_form();

    var get_filter=  function(){
        var start_date = $('#start_datepicker').val();
        var end_date = $('#end_datepicker').val();
        var code = [];
        var customer_id = [];
        var payment_type_id = [];
        var employee_id = [];
        var item_id = [];
        var bill_status = [];
        var package_payment = 0;
        $('[name="code[]"]').each(function(obj){
            code.push($(this).val());
        });
        $('[name="customer[]"]').each(function(obj){
            customer_id.push($(this).val());
        });
        $('[name="item[]"]').each(function(obj){
            item_id.push($(this).val());
        });
        $('[name="employee[]"]').each(function(obj){
            employee_id.push($(this).val());
        });
        $('[name="payment_type[]"]').each(function(obj){
            payment_type_id.push($(this).val());
        });
        $('[name="bill_status[]"]:checked').each(function(obj){
            bill_status.push($(this).val());
        });
        return {start_date : start_date, end_date : end_date,
                customer_id : customer_id, payment_type_id : payment_type_id,
                status : bill_status, code : code,
                item_id : item_id, employee_id : employee_id

             }
    };

    $('#choose_report').data('action-url', url + 'bills/getReportForm');
    $('#choose_report').data('action-before', function(param){ return get_filter(); });

    $('#submit_search').data('action-url', url + 'bills/setSearchCondition');
    $('#submit_search').data('action-before', function(param){ return get_filter(); });
    $('#submit_search').data('action-success', function(ret,more){ $('#bill_list').transform_datatable('refresh'); });
});


function Delete(ids,url,table)
{
    var ids = [];
    $("[name='keys[]']").each(function() {
        if ($(this).is(":checked"))
        {
            ids.push($(this).val());
        }
    });
    void_bill(ids,url,table,'multiple');
}

function void_btn(id,url,table){
    void_bill([id],url,table,'one');
}

function void_bill(ids,url,table,type){
    $.msgBox({
        title:"Message",
        type:"alert",
        content:'Are you sure want to void?<br><textarea id="void-comment" cols="35" placeholder="Type your comment"></textarea>',
        buttons:[{value:"Yes"},{value:"Cancel"}],
        success: function(result) {
            if (result == "Yes") {
                var comment = $('#void-comment').val();
                if(comment.length == 0){
                    $.msgBox({
                        title: "Message",
                        type: "error",
                        content: "Comment field can not be blank",
                        buttons: [{value: "Cancel"}],
                        success: function(){
                            if(type == 'one'){
                                Delete(ids,url,table);
                            }
                            else{
                                void_btn(ids[0],url,table);
                            }
                        }
                    });
                    return;
                }
                $.system_process({
                    url : url + "void",
                    param : {ids : ids, comment : comment},
                    success: function(ret,more){
                        $("#"+table).transform_datatable('refresh');
                    }
                });
            }
        }
    });
}

var custom_get_insert_values = function(root){
    var replace_bill_id = $('input[name=replace_bill_id]:checked').val();
    root.replace_bill_id = replace_bill_id;
    return root;
};

$('#wf_page_dialog').delegate('.add-employee','click', function(){
    var employee_sample = $('#employee_sample').html();
    $(this).siblings('.bill_item_employee_list').append($(employee_sample));
});

$('#wf_page_dialog').delegate('.delete-employee','click', function(){
    $(this).parent().remove();
});

function refresh_payment(){
    var total = 0;
    $('.payment_amount').each(function(){
        total += Number($(this).val());
    });
    $('.total_paid').html(total);
    return total;
}

function remove_payment(sender){
    $(sender).closest('tr').remove();
    refresh_payment();
}

function add_payment(){
    var payment_sample = $('#payment_sample').find('tr').clone();
    $('#payment_list').prepend(payment_sample);
}

$('#wf_page_dialog').delegate('.payment_amount','change', function(){
    refresh_payment();
});

$('#wf_page_dialog').delegate('tr','click',function(){
    var id = $(this).attr('for');
    $('#'+id).prop('checked',true);
});

var sale_id_format = function(data_text, data_row){
  if(data_row['replace_bill_code']){
      data_text = data_text +  ' <i class="fa fa-arrow-right"></i> ' + data_row['replace_bill_code'];
  }
    return data_text;
};

var before_replace = function(param,more){
    var a = $('#replaced-hidden').val();
    if(isset(a)){
        $.msgBox({
            title  : 'Message',
            content: "This bill was used to replaced. Are you sure replace it for another bill?",
            type : 'alert',
            buttons: [{value:'Submit'},{value:'Cancel'}],
            success: function(result){
                if(result=='Submit'){
                    var id = $('#id').val();
                    var data = {id : id};
                    $.extend(data,get_insert_values());

                    $.system_process({
                        url : url + 'bills/edit',
                        param : data,
                        more : more,
                        success: 'after_success'
                    });
                }
            }
        });
    }else{
        var id = $('#id').val();
        var data = {id : id};
        $.extend(data,get_insert_values());

        $.system_process({
            url : url + 'bills/edit',
            param : data,
            more : more,
            success: 'after_success'
        });
    }
};

function get_report_detail(){
    var report = $('[name=report_id]:checked').val();
    if(!isset(report) || report == 0){
        $.msgBox({
            'title' : 'Error',
            'content' : 'Please choose a report'
        });
        return false;
    }
    window.open(url  + 'bills/getReportDetail/' + report);
}

function before_clear_search(url,table_id){
    window.location.href = url;
}

function backdate_bill(bill_id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url,
        bill_id : bill_id
    };

    $.system_dialog({
        url : url + 'get_backdate_form/' + bill_id,
        more : table_data,
        button : [{url : url + 'backdate_bill/' + bill_id, text : 'Save', class : 'btn-primary',before: get_data_for_backdate, type : 'process', success: after_success, more:table_data}, {text : 'Cancel'}],
        success: function(){
            render_form('#wf_page_dialog');
        }
    });
}

var get_data_for_backdate = function(){
    var date = $('#date').val();
    return {date : date};
};