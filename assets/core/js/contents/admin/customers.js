
var control = 'customers/';
$(document).ready(function () {

    $('#export_customer_data').on('click',function(){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'Please confirm you want to export customer Data ?',
            buttons:[{value:"Yes"},{value:"Cancel"}],
            success: function(result) {
                if(result == 'Yes'){
                    window.location = url + control + 'export';
                }
            }
        });
    });

    $('#import_customer_data').on('click',function(){
        var html = '<h5>Choose a file to import (.csv, .xls).</h5><br><input type="file" id="customer_import" name="customer_import" size="1" /><br>';
        $.msgBox({
            title:"Import Customer Data",
            type:"confirm",
            content: html,
            buttons:[{value:"Yes"},{value:"Cancel"}],
            success: function(result) {
                if(result == 'Yes'){
                    if($('#customer_import').prop('files').length == 0){
                        bootbox.dialog({
                            title: 'Error',
                            message: 'Please choose import file.',
                            onEscape: function() {},backdrop: true,
                        });
                        return false;
                    }
                    else{
                        var file_data = $('#customer_import').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', file_data);
                        $.ajax({
                            'url' : sessionStorage.getItem('base_url') + control + 'ImportCustomerData',
                            dataType: 'json',
                            cache: false,
                            //async: false,
                            contentType : false,
                            processData: false,
                            data: form_data,
                            type: 'POST',
                            beforeSend: function(){
                                bootbox.dialog({
                                    title: 'Importing',
                                    message: 'Please wait few minutes, we are importing customer data to database.',
                                    onEscape: function() {},backdrop: true
                                })
                            },
                            success: function(res) {
                                bootbox.hideAll();
                                if(!res.status){
                                    bootbox.dialog({
                                        title: 'Error',
                                        message: res.message,
                                        onEscape: function() {},backdrop: true
                                    })
                                }
                                else{
                                    bootbox.dialog({
                                        title: 'Complete',
                                        message: res.message,
                                        onEscape: function() {},backdrop: true
                                    })
                                }
                            }
                        });
                    }
                }
            }
        });
    });

    $('#wf_page_dialog').delegate('input[question-type="y-n"]','click',function(){
        if($(this).val()==1){
            $(this).parent().siblings('.answermore').removeClass('hidden').children('input').attr('value','');
        }
        else {
            $(this).parent().siblings('.answermore').addClass('hidden').children('input').attr('value', 'No');
        }
    });

    $('#wf_page_dialog').delegate('#customer_type','change', function(){
        if($(this).val()==1){
            $('#email').removeClass('required').parent().siblings('label').removeClass('required');
            $('#nric').removeClass('required').parent().siblings('label').removeClass('required');
        }else{
            $('#email').addClass('required').parent().siblings('label').addClass('required');
            $('#nric').addClass('required').parent().siblings('label').addClass('required');
        }
    });
});

var view_pin = function (id, url, table_id) {
    $.system_process({url: url + 'view_pin/' + id});
};

var view_pin_only = function(id,url,table_id){
    $.system_process({url: url + 'view_pin/' + id + "/1"});
};

var before_edit_pin = function(data){
    if($(".insert").customValidate()===false){
        return false;
    }
    data.opin = $('#opin').val();
    data.pin = $('#pin').val();
    return data;
};

var after_edit_pin = function(ret, more){
    if(isset(ret['validate_error'])){
        handleErrors(ret['validate_error']);
    }
};

var before_set_pin = function(data){
    data.field_post.pin = $('#pin').val();
    return data;
};

var credit_history = function(id, url, table_id){
    $.system_process({url: url + 'credit_history/' + id});
};

var printCustomerCreditHistory = function(id,url, table_id){
    window.location.href = url + "printCustomerCreditHistory/" + id;
}

var insert_to_branch = function (id, url, table_id) {
    $.system_dialog({
        url: url + 'get_form_insert_to_branch/' + id,
        button: {
            success: {success: "after_success"}
        }
    });
};

var custom_form_render = function (parent) {
    $("#birthday").datetimepicker(
        {
            timepicker: false,
            format: 'Y-m-d'
        }
    );
};


function reset_password(id) {
    bootbox.dialog({
        title: 'Select type to send notification',
        message: 'Please choose the methods you preferer : <br><br> '+
        '<input type="radio" name="send_method" value="1"> SMS<br>'+
        '<input type="radio" name="send_method" value="2"> Email<br>'+
        '<input type="radio" name="send_method" value="3" selected="selected"> Both SMS and Email',
        onEscape: function() {},backdrop: true,
        buttons: {
            Send: {
                label: "Send",
                className: "btn-success",
                callback: function() {
                    $.ajax({
                        url: url + control + "resetPassword/",
                        type: 'POST',
                        data : {
                            id : id,
                            send_method : $('input[name="send_method"]:checked').val()
                        },
                        success : function (res) {
                            res = $.parseJSON(res);
                            if(res.status === 'S'){
                                bootbox.hideAll();
                                bootbox.alert(res.message);
                            }
                            else{
                                bootbox.alert(res.message);
                            }
                        }
                    })
                }
            },
            Cancel: {
                label: "Cancel",
                className: "btn-danger",
                callback: function() {
                    bootbox.hideAll();
                }
            }
        }
    });
}

var field_list;
var id;

function view_survey(id, url, table_id) {
    $.system_process({url: url + "question_form/" + id});
}


var question_answer_json;
var save_question = function(){
    question_answer_json = get_answers();
};

var get_answers = function(){
    var arrayAnswer = [];
    $(".insert-question").each(function (e) {
        answer = {
            question_id: $(this).attr('question-id'),
            answer: $(this).val()
        };
        arrayAnswer.push(answer);
    });
    return arrayAnswer;
};

var before_edit_survey = function(data){
    data.field_post = get_answers();
    data.id = $('#customer_id').val();
    return data;
};

var field_post;
var save_info = function(){
    field_post = get_insert_values();
    if (field_post === false) {
        return false;
    }

    return field_post;
};

var pre_edit_2_success = function(ret,more){
    if(isset(ret['validate_error'])){
        handleErrors(ret['validate_error']);
    }
};

var before_set_identity_to_branch = function(param,more){
    return {identity : $('#identity').val()};
};

var get_credit_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_credit_adjustment', success:'after_success', more:table_data}};
    $.system_dialog({
        url     : url + 'get_credit_adjustment_form/' + id,
        button  : buttons
    });
};

var get_credit_adjustment = function(param,more){
    $('#wf_page_dialog form .credit_value').prop('disabled',false);
    var data = $('#wf_page_dialog form').serializeJSON();
    return data;
};

var get_transfer_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_credit_adjustment', success:'after_success', more:table_data}};
    $.system_dialog({
        url     : url + 'get_transfer_form/' + id,
        button  : buttons
    });
};

var send_sms_onetime_pwd = function(id){
    bootbox.confirm({
        title: 'Notice',
        message: 'Are you sure about sending an OTP to this customer?',
        callback: function(result){
            if(result) {
                $.ajax({
                    url: url + 'booking/authorize/sendSmsOTP',
                    data: {
                        customer_id: id
                    },
                    success: function (res) {
                        res = $.parseJSON(res);
                        bootbox.hideAll();
                        bootbox.dialog({
                            title: 'Notice',
                            message: res.message
                        });
                    }
                })
            }
            else{
                return true;
            }
        }
    });
}