/**
 * Created by lhoanghieu on 09/03/2016.
 */
$(document).ready(function(){
    $('#audit-trails tfoot th').each( function () {
        var title = $(this).text();
        if(title != 'ID') {
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    } );

    var tables = $('#audit-trails').DataTable({
        ajax          : admin_site_url,
        deferRender   : true,
        columns       : [
            { data: "id" },
            { data: "employee_user" },
            { data: "createAt" },
            { data: "module" },
            { data: "object" },
            { data: "action" },
            { data: "object_detail" }
        ],
        paging        : true,
        ordering      : true,
        info          : true,
        initComplete: function ()
        {
            var r = $('#audit-trails tfoot tr');
            r.find('th').each(function(){
                $(this).css('padding', 8);
            });
            $('#audit-trails thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
    });

    tables.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });

    $('#reload-table').on('click', function(){
        tables.ajax.reload();
    });
})
