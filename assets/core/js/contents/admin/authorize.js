var control = 'admin/authorize/';
$(function () {
    var runAlert = function (msg) {
        $("#messageBox").addClass('messagebox_error');
        $("#messageBox").html(msg).fadeIn().fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
    };

    $('form[name="login_form"]').submit(function (event) {
        event.preventDefault();
        var param = {};
        $.each($(this).serializeArray(), function(i, v){
            param[v.name] = v.value;
        });
        $.system_process({
            url : url + control + "login",
            param : param,
            success: function(ret, more){
                var data = ret['data'];
                if(data == 1){
                    window.location.href = BASE_URL+'admin/home';
                } else{
                    runAlert(data);
                    return false;
                }
            }
        });
    });
});