var control = "employees/";
var is_first_change = true;
var old_username = '';
var old_password = '';
var old_cpassword = '';
$(document).ready(function(){
    $('#wf_page_dialog').delegate('#department_id','change',function(){
        if($('#password').val() != 'therapist'){
            old_username  = $('#username').val();
            old_password  = $('#password').val();
            old_cpassword = $('#cpassword').val();
        }
        var department_id = $('#therapist_id').val().split(',');
        var therapist_id  = $(this).val();
        var currentid = $("#id").val();
        var email = $('#email').val();
        if(_.contains(department_id,therapist_id)){
            $('#username').val('therapist_' + email);
            $('#password').val('therapist');
            $('#cpassword').val('therapist');
            $('#login_section').addClass('hidden');
        }else if(!is_first_change){
            $('#username').val(old_username);
            $('#password').val(old_password);
            $('#cpassword').val(old_cpassword);
            if(currentid != ""){
                $('#username').val($("#current_user").val());
            }
            $('#login_section').removeClass('hidden');
        }else{
            is_first_change = false;
        }
    });

    $('#wf_page_dialog').delegate('#email','change',function(){
        var department_id = $('#therapist_id').val().split(',');
        var therapist_id  = $('#department_id').val();
        var email = $(this).val();
        if(_.contains(department_id,therapist_id)){
            $('#username').val('therapist_' + email);
        }
    });


    $('body').delegate('input.date-header','click',function(){
        var id_ref = $(this).attr('header-id');
        var is_checked = $(this).prop('checked');
        $('input.time-picker[refer="'+id_ref+'"]').each(function(){
            if(is_checked){
                $(this).removeAttr('disabled',0);
            }else{
                $(this).attr('disabled','true');
            }
        });
    });

    $('body').delegate('#absent_whole_day','click',function(){

        if($(this).prop('checked')){
            $('[key=checkbox-relate]').attr('disabled','disabled');
        }else{
            $('[key=checkbox-relate]').removeAttr('disabled');
        }
    });

    $('body').delegate('#absent_add','click',function(){
        var id = $('#employee_id').val();
        var data = {
            date : $('#absent_date').val(),
            absent_from : $('#absent_from').val(),
            absent_to   : $('#absent_to').val(),
            absent_whole_day : ($('#absent_whole_day').prop('checked')) ? 1 : 0
        };
        if(data.date && ((data.absent_from && data.absent_to) || data.absent_whole_day)){
            $.system_process({
                url: url + 'employees/change_absent',
                param: {id: id,data: data},
                success: function(ret, more){
                    update_absent(id,url+'employees/');
                }
            });
        }else{
            $.msgBox({
                title: "Message",
                type: "error",
                content: 'Missing information. Please choose date and time',
                buttons: [{value: "Cancel"}]
            });
        }
    });

    $('#employee_list').data('format_function', function(data_cell, data_row){

    });
});

function custom_get_insert_values(data){
    var calendar_data =  get_calendar_data();
    for(e in calendar_data){
        if(IsJsonString(calendar_data[e]))
        {
            calendar_data[e] = JSON.parse(calendar_data[e]);
        }
    }
    data = $.extend(data,get_calendar_data());
    return data;
}


function insert_to_branch(id,url,table_id){
    $.system_confirm({
        url     : url + "insert_to_branch",
        param   : {id:id},
        content : "Do you want to add this employee to this branch?",
        more    : {table_id: table_id},
        success: function(ret,more){
            $('#'+more['table_id']).transform_datatable('refesh');
        }
    });
}

function update_calendar(id,url,table_id){
    var buttons = [];
    if(right['e']){
        buttons.push({text:'Save',url : url + 'change_calendar/' + id, before: 'get_calendar_data', type :'process'});
    }
    buttons.push({text:'Cancel'});
    $('body').system_dialog({
        type    : 'dialog',
        url     : url + 'get_calendar/' + id,
        button  : buttons,
        success : 'calendar_render'
    });
}

function calendar_render(parent){
    $('.date-header').each(function(){
        id_ref = $(this).attr('header-id');
        is_checked = $(this).prop('checked');
        $('input.time-picker[refer="'+id_ref+'"]').each(function(){
            if(is_checked){
                $(this).removeAttr('disabled');
            }else{
                $(this).attr('disabled','true');
            }
        });
    });
}

function get_calendar_data(){
    data = {};
    $('.insert.time-picker').each(function(){
        if($(this).attr('disabled') =='disabled'){

        }else{
            data[$(this).attr('id')] = $(this).val();
        }
    });
    var custom_days = $("#altField").val();
    return {calendar:data,custom_days:custom_days};
}

function update_absent(id,url,table_id){
    $(this).system_dialog({
        url: url + 'get_absent',
        param: {id: id},
        success: function(param, more){
            var dialog = more.dialog;
            render_form(more.dialog);
            dialog.find('#absent_date').datetimepicker({
                datepicker: true,
                timepicker: false,
                format:'Y-m-d'
            });
            dialog.find('#absent_from, #absent_to').datetimepicker({
                datepicker: false,
                timepicker: true,
                format:'H:i'
            });
        }
    });
}

function delete_absent_date(id){
    var employee_id = $('#employee_id').val();
    $.system_process({
        url: url+'employees/delete_absent_date/',
        success: function(result){
            update_absent(employee_id, url + 'employees/');
        }
    });
}
var custom_form_render_admin_system = function(){
    setTimeout(change_employee,500);
};
$('#wf_page_dialog').delegate('#type','change',change_employee);

function change_employee(){
    var val = $('#type').val();
    if(val == 4){
        $('#branch_group_id').addClass('hidden');
        $('#branch_id').addClass('hidden');

        $("#branch_group_id :input").prop("disabled", true);
        $("#branch_id :input").prop("disabled", true);
    }else if(val == 3){
        $('#branch_group_id').removeClass('hidden');
        $('#branch_id').addClass('hidden');
        $("#branch_group_id :input").prop("disabled", false);
        $("#branch_id :input").prop("disabled", true);
    }else{
        $('#branch_group_id').removeClass('hidden');
        $('#branch_id').removeClass('hidden');
        $("#branch_group_id :input").prop("disabled", false);
        $("#branch_id :input").prop("disabled", false);
    }
}

function nice_first_name(datatext, data_row){
    return '<span style="color:pink">' + datatext + '</span>';
}

if(typeof(update_assigned_service) == 'undefined' ) {
    function update_assigned_service(id, current_url, table_id) {
        $(this).system_dialog({
            url: current_url + 'get_assigned_service_form/'+id,
            success: function (param, more) {
                render_form('#wf_page_dialog');
                $('#wf_page_dialog').delegate('#category_id','change',function(){
                    var category_id = $(this).val();
                    if(isset(category_id)){
                        $.system_process({
                            url : url + 'admin/items/suggestByCategory/'+category_id,
                            param: {minimal : true},
                            success:function(ret,more){
                                $('#item_id').html('');
                                var list_item_array = new Array();
                                $.each(ret['data'],function(key,obj){
                                    $('#item_id').append($('<option>',{
                                        text  : obj.label,
                                        value : obj.value
                                    }));
                                    list_item_array.push(obj.value);
                                });
                                sessionStorage.setItem('list_service_item',list_item_array);
                                $('#item_id').prepend($('<option>',{
                                    text  : 'Entire Category',
                                    value : 0,
                                    selected: true,
                                }));
                            }
                        });
                    }
                });
                setTimeout(function(){
                    $('#category_id').trigger('change');
                },1000);
            }
        });
    }
}
if(typeof(deleteAssignedService) == 'undefined' ) {
    var deleteAssignedService = function (element,id) {
        id = id.toString();

        $(element).parent().parent().remove();
        data = JSON.parse($('#service_ids').val());

        var i = data.indexOf(id);
        if(i!=-1){
            data.splice(i,1);
        }
        $('#service_ids').val(JSON.stringify(data));
    };
}
if(typeof(addAssignedItemToEmployee) == 'undefined' ) {
    var addAssignedItemToEmployee = function (id) {
        var data = $('#service_ids').val();
        data = JSON.parse(data);
        if(data.indexOf(id) != -1){
            $.msgBox({
                title:"Message",
                type:"alert",
                content:'You have already added this item to the package.',
                buttons:[{value:'Cancel'}]
            });
            return;
        }

        if(id == 0){
            return false;
        }

        $.system_process({
            url : url + 'items/' + 'getItem/' + id,
            success: function(ret,more){
                var result = ret['data'];
                var html = "";
                try{
                    if(result.type != 1)
                        throw "Not a service item";
                    if(result.status == 4)
                        throw "Item disabled";
                    html += '<td> <a onclick="return deleteAssignedService(this, '+result.id+');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
                    html += '<td class="modelname">'+result.name+'</td>';
                    html += '<td class="modelname">'+result.service.duration+'</td>';
                    var row = document.createElement('tr');
                    $('#grid_item').append(row);
                    $(row).html(html);
                    data.push(id);
                    $('#service_ids').val(JSON.stringify(data));
                    $('#service_suggest').val('');
                }
                catch(msg){
                    $.msgBox({
                        type : 'error',
                        title: 'message',
                        content: msg,
                    });
                    return false;
                }
            },
            close_modal : false
        });
    };
}

var get_assigned_service_before_edit = function(){
    return {
        'service_id' : JSON.parse($('#service_ids').val())
    };
};

var custom_form_render_admin_system = function(){
    /*$('#service_suggest').on("autocompleteselect", function(event, ui){
        addAssignedItemToEmployee(ui.item.value);
        return false;
    });*/
    $('#item_id').on("change", function(event){
        var myval = $(this).val();
        addAssignedItemToEmployee(myval);
        return false;
    });
};

