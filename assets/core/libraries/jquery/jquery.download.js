$.redirectWithData = function(method,url,params){
  /*---- Generate form data -----*/
    var form = $('<form>');
    $(form).attr('method',method);
    $(form).attr('action',url);
    for(var key in params) {
        var hiddenFields = generate_field(key, params[key]);
        $.each(hiddenFields, function (obj) {
            $(form).append($(this));
        });
    }
    $('body').append(form);
    form.submit();
};

var generate_field = function(key,value){
    var list_fields = [];
    if(typeof value == 'undefined'){
       return [];
    }
    else if(typeof value == 'array'){
        $.each(value,function(obj){
            list_fields = $.extend(list_fields,generate_field_child(key + '[]',obj));
        });
    }
    else if(typeof value == 'object'){
        $.each(value,function(n_key,obj){
            if(typeof n_key == 'number'){
                n_key = '';
            }
            var nodes = generate_field_child(key + '[' + n_key + ']',obj);
            $.each(nodes,function(obj){
                list_fields.push(this);
            });
        });
    }else{
        var hiddenField = $('<input>');
        hiddenField.attr("type", "hidden");
        hiddenField.attr("name", key);
        hiddenField.attr("value", value);
        list_fields.push(hiddenField);
    }
    return list_fields;
};

var generate_field_child = function(key,value){
    var list_fields = [];
    if(typeof value == 'undefined'){
        return [];
    }
    else if(typeof value == 'object'){
        $.each(value,function(n_key,obj){
            if(typeof n_key == 'number'){
                n_key = '';
            }
            var nodes = generate_field_child(key + '[' + n_key + ']',obj);
            $.each(nodes,function(obj){
                list_fields.push(this);
            });
        });
    }else{
        var hiddenField = $('<input>');
        hiddenField.attr("type", "hidden");
        hiddenField.attr("name", key);
        hiddenField.attr("value", value);
        list_fields.push(hiddenField);
    }
    return list_fields;
};