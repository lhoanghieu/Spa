function checkall(){
	$("#checkAll").click(function(){
		$(":checkbox[name='keys[]']").attr('checked', $('#checkAll').is(':checked'));   
	});
	$(":checkbox[name='keys[]']").click(function(){
			var len = $(":checkbox[name='keys[]']:checked").length;
			var len2 = $(":checkbox[name='keys[]']").length;
			$ch = (len==len2)?true:false;
			$("#checkAll").attr('checked',$ch);
	});
}
function cdialog(id,url,control,item,str_json){
	$('#'+id).dialog({
					autoOpen: false,
					width: 350,
					height:160,
					modal: true,
					title:'Delete',
					buttons: {
						"Delete": function() {
                            var ids = "";
                            $("[name='keys[]']").each(function() {
                                if ($(this).is(":checked"))
                                {
                                    name  = $(this).attr('id');
                                    val   = $(this).val();
                                    ids  += ',"'+name+'":"'+val+'"';
                                }
                            });
                            var chkbox_str = "{" + ids.substring(1) + "}";

							$.post(url+control+'/delete',{ids:chkbox_str,item:item},function(data){
                                    if(data == 1){
                                        getSearch(page=0);
                                    }
							});
							$(this).dialog("close");
							return false;
						},
						"Cancel": function() {
							$(this).dialog("close");
						}
					}
				});
}
function masager(id,url,control,item){
	$('#'+id).dialog({
					autoOpen: false,
					width: 300,
					height:120,
					modal: true,
					title:'Thông báo',
					buttons: {
						"Đóng": function() {
							$(this).dialog("close");
						}
					}
				});
}

function set_with_footer(){
			w_south = $("#main-layout-south").width();
			w_south_copyright = $("#main-layout-south").find("#copyright").width()+20;
			
			w_south_time = $("#main-layout-south").find("#time").width()+20;
			
			w_south_version = $("#main-layout-south").find("#version").width() + 21;
			
			w_south_account = $("#main-layout-south").find("#account").width() + 2;
			
			$("#main-layout-south").find("#center").width(w_south - (w_south_copyright + w_south_time + w_south_version + w_south_account));
	}
	
	function set_with_header(){
			w = $("#header").width();
			w_home= $("#header").find("#home").width()+10;
			
			w_button = $("#header").find("#button").width()+20;
			

			
			$("#header").find("#menu-main").width(w - (w_button + w_home));
	}