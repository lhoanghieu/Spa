jQuery.extend({
	random: function(X) {
	    return Math.floor(X * (Math.random() % 1));
	},
	randomBetween: function(MinV, MaxV) {
	  return MinV + jQuery.random(MaxV - MinV + 1);
	}
});

(function(jQuery)
{
  jQuery.fn.clock = function(options)
  {
    var defaults = {
	  year:2011, 
	  month:11, 
	  date:11, 
	  hour:11, 
	  minute:19, 
	  second:23,
      numday:30
    };
    var _this = this;
    var opts = jQuery.extend(defaults, options);

    setInterval( function() {
      
  		opts.second++;
		if(opts.second > 59 )
		{
			opts.second = 0;
			opts.minute++;
		}
		
		if(opts.minute > 59 )
		{
			opts.minute = 0;
			opts.hour++;
		}
		
		if(opts.hour > 23 )
		{
			opts.hour = 0;
			opts.date++;
		}
		
		if(opts.date > opts.numday)
		{
			opts.date = 1;
			opts.month++;
		}
		
		if(opts.month > 12 )
		{
			opts.month = 1;
			opts.year++;
		}
		
		
			
		second = opts.second;
		if(second < 10  )
			second = "0"+second;

		minute = opts.minute;
		if(minute < 10  )
			minute = "0"+minute;
			
		hour = 	opts.hour;
		if(hour < 10  )
			hour = "0"+hour;
		date = opts.date;
		if(date < 10  )
			date = "0"+date;

		month = opts.month;
		if(month < 10  )
			month = "0"+month;
			
		var str = hour+':'+minute+':'+second+' '+date+'-'+month+'-'+opts.year;
        jQuery(_this).html(str);
    }, 1000 );

  }
})(jQuery);
