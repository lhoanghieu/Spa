<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 1/6/2015
 * Time: 2:42 PM
 */

$libCssFiles = ['scheduler/dhtmlxscheduler_flat'];
$libJsFiles  = ['moment','scheduler/dhtmlxscheduler','scheduler/ext/dhtmlxscheduler_timeline','scheduler/ext/dhtmlxscheduler_units',
    'scheduler/ext/dhtmlxscheduler_minical','scheduler/ext/dhtmlxscheduler_limit','scheduler/ext/dhtmlxscheduler_serialize',
    'scheduler/ext/dhtmlxscheduler_all_timed',
    'scheduler/ext/dhtmlxscheduler_quick_info',
    'scheduler/ext/dhtmlxscheduler_grid_view',
    'scheduler/api'
];