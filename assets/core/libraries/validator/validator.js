/**
 * Created by Vu on 11/8/2014.
 */
var focus;

$.fn.customValidateRequired = function(){
    var val = this.val();
    if(!isset(val) || val == ''){
        return false;
    }
    else{
        return true;
    }
};

$.fn.customValidateEmail = function(){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test($(this).val());
};

$.fn.customValidateCPassword = function(){
    passwordFieldId = $(this).attr('target');
    passVal = $('#'+passwordFieldId).val();
    cpassVal = $(this).val();

    return passVal==cpassVal;
};

$.fn.addAlert = function(options){
    var alertNode = document.createElement('span');
    $(alertNode).addClass('error');
    $(alertNode).text(options.text);
    $(this).after(alertNode);
    $(this).addClass('error');
    $(this).attr('title',options.text);
};

$.fn.customErrorHandle = function(options){
    $(this).addClass('error');
    if(focus==null){
        focus = $(this);
    }
    if($(this).siblings('span.error').length == 0){
        $(this).addAlert({text : options.text});
    }
};

$.fn.clearCustomErrorHandle = function(){
    $(this).removeClass('error');
    $(this).siblings('span.error').remove();
    $(this).attr('title',"");
};

$.fn.customValidate = function(){
    totalPass = true;
    focus = null;
    this.each(function(){
        isPass = true;
        if($(this).hasClass('required')){
            isPass = $(this).customValidateRequired()
            if(isPass===false){
                $(this).customErrorHandle({text : 'Field is required'});
                totalPass = false;
                return;
            }else{
                $(this).clearCustomErrorHandle();
            }
        }
        if($(this).hasClass('email-validate')) {
            if ($(this).hasClass('required')==false && $(this).val().length==0) {
                isPass = true;
            }
            else {
                isPass = $(this).customValidateEmail();
            }

            if (isPass === false) {
                totalPass = false;
                $(this).customErrorHandle({text : 'Wrong email format'});
            }else{
                $(this).clearCustomErrorHandle();
            }
        }

        else if($(this).hasClass('cpassword-validate')) {
            isPass = $(this).customValidateCPassword();

            if (isPass === false) {
                $(this).customErrorHandle({text : 'Password do not match'});
                totalPass = false;
            }else{
                $(this).clearCustomErrorHandle();
            }
        }
    });
    if(focus != null){
        $(focus).focus();
    }
    return totalPass;
};

