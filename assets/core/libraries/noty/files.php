<?php
$libCssFiles = ['themes/bootstrap'];
$libJsFiles  = [
    'packaged/jquery.noty.packaged.min',
    'layouts/bottom',
    'layouts/bottomRight',
    'layouts/top',
    'layouts/topRight',
];