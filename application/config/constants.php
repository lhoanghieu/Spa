<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('VIEW_EXT', 'php');
define('DATABASE_DATE_FORMAT', 'Y-m-d');
define('DATABASE_TIME_FORMAT', 'H:i:s');
define('BASE_TIMEZONE', 'UTC');
define('BASE_DATE_FORMAT', 'd/m/Y');
define('BASE_DATE_MONTH__FORMAT', 'm/Y');
define('BASE_DATE_FORMAT_LONG', 'M d, Y');
define('BASE_TIME_FORMAT', 'h:i A');

define('REQUEST_FAIL', 'F');
define('REQUEST_SUCCESS', 'S');
/* End of file constants.php */
/* Location: ./application/config/constants.php */