<?php
// start define functions to get array and value of system variable
//function STATUS($search = ""){
//
//}

class Permission_Value{
    const ADMIN = 4;
    const BRANCH_GROUP = 3;
    const BRANCH = 2;
    const GROUP =1;
}

function PAYPAL_AUTHORIZE($search = ''){
    $array = array(
        'ClientID' => 'AXrr5UzlDvVY8EdNDDA_gdUsw2fKupcvISW79_9Fj5qa1HjKQFFUJi2skPqIyV8z5fIyWXFDXVMQuKW2',
        'ClientSecrect' => 'EPX6Gpgl93rxPtVOzc9EKm3Ja2LOS1Bi6srIinN2j0VRUEY3HAiVi87yTtHUNxJGjCnAWHbBZxPJvaHF'
    );
    return _get_system_constant_data($array,$search);
}

function GST_NUMBER($search = ''){
    $array = array(
        'HT'  => '52950522C',
        'HTG' => '201407782W',
        'SAP' => '201532219H'
    );
    return _get_system_constant_data($array,$search);
}

function UNIT_COGS_MULTIPLE($search = ''){
    $array = array(
        'Value' => 1.0636
    );
    return _get_system_constant_data($array,$search);
}

function PENDING_TIME($search = ""){
    $array = array(
        '300' => 'Booking'
    );
    return _get_system_constant_data($array,$search);
}

function ABSENT_TYPE($search = ""){
    $array = array(
        '6' => 'Away',
        '7' => 'Offline',
        '5' => 'Block'
    );
    return _get_system_constant_data($array, $search);
}

function EMPLOYEE_TYPE($search = ""){
    $array = array(
        '1' => 'Staff',
        '2' => 'Branch Manager',
        '3' => 'Branch Group Manager',
        '4' => 'Super admin'
    );
    return _get_system_constant_data($array, $search);
}

function MENU_TYPE($search = ""){
    $array = array(
        '1' => 'Staff',
        '2' => 'Admin',
        '3' => 'All',
    );
    return _get_system_constant_data($array, $search);
}

function CUSTOMER_STATUS($search = ""){
    $array = array(
        '1' => 'Active',
        '2' => 'Hold',
        '0' => 'Delete'
    );
    return _get_system_constant_data($array, $search);
}

function BILL_STATUS($search = ""){
    $array = array(
        '1' => 'Complete',
        '2' => 'Hold',
        '3' => 'Void',
        '4' => 'Invisible'
    );
    return _get_system_constant_data($array, $search);
}

function ITEM_TYPE($search = ""){
    $array = array(
        '1' => 'Service',
        '2' => 'Package',
        '3' => 'Product',
        '4' => 'Bundle',
        '5' => 'Gift_Card'
    );
    return _get_system_constant_data($array, $search);
}

function PROMOTION_TYPE($search = ""){
    $array = array(
        '1' => 'Discount Amount',
        '2' => 'Discount Percent',
    );
    return _get_system_constant_data($array, $search);
}

function CONFIG_TYPE($search = "")
{
    $array = array(
        '1' => 'textbox',
        '2' => 'numberbox',
        '3' => 'radiobox',
        '4' => 'timezone',
        '5' => 'select',
        '6' => 'time',
        '7' => 'textarea'
    );
    return _get_system_constant_data($array, $search);
}

function QUESTION_TYPE($search = ""){
    $array = array(
        '1' => 'y-n',
        '2' => 'one-choice',
        '3' => 'multiple-choice'
    );
    return _get_system_constant_data($array, $search);
}

function CUSTOMER_TYPE($search = ""){
    $array = array(
        '1' => 'Guest',
        '2' => 'Member',
    );
    return _get_system_constant_data($array, $search);
}

function COMMISSION_TYPE($search = "", $isSymbol = true){
    $array = array();
    if($isSymbol){
        $array[2] = "%";
        $array[1] = "$";
    }else{
        $array[2] = "Percent";
        $array[1] = "Amount";
    }
    return _get_system_constant_data($array, $search);
}

function COMMISSION_BUNDLE_TYPE($search = ""){
    $array = array(
        '1' => '$',
        '2' => '%',
        '3' => 'Inherit'
    );
    return _get_system_constant_data($array, $search);
}

function REPORT($id = ""){
    $CI =& get_instance();
    $array = array();
    $reports = $CI->load->table_model('report')->getTableMap('id');
    foreach($reports as $report_id=>$report){
        if(isset($report->parent) && $report->parent){
            $parent_report = $reports[$report->parent];
            if(! isset($array[$reports[$report->parent]->code])){
                $array[$parent_report->code] = array(
                    'child' => array(),
                    'name'  => $parent_report->name,
                    'id'    => $parent_report->id,
                    'system'=> $parent_report->system
                );
            }
            $array[$parent_report->code]['child'][$report->code] = array(
                'name'           => $report->name,
                'id'             => $report->id,
                'condition_form' => $report->condition_form,
                'system'         => $report->system,
            );
        }
    }
    if($id == ""){
        return $array;
    }else{
        foreach($array as $key => $value){
            if($key == $id){
                return $value;
            }else{
                if(isset($value['child'])){
                    foreach($value['child'] as $cKey => $cValue){
                        if($cKey == $id){
                            return $cValue;
                        }
                    }
                }
            }
        }
    }
    return array();
}

function WORKING_DAYS($search = ""){
    $array = array(
        '1' => 'Mon',
        '2' => 'Tuesday',
        '3' => 'Wed',
        '4' => 'Thu',
        '5' => 'Fri',
        '6' => 'Sat',
        '0' => 'Sun',
    );
    return _get_system_constant_data($array, $search);
}

function GENDER($search = ""){
    $array = array(
        '1' => 'Male',
        '2' => 'Female'
    );
    return _get_system_constant_data($array, $search);
}

function BOOKING_STATUS($search = ""){
    $array = array(
        '0' => 'Delete',
        '1' => 'Complete',
        '2' => 'Hold',
        '3' => 'Reserve',
        '4' => 'Comment',
        '5' => 'Block',
        '6' => 'Away',
        '7' => 'Offline',
        '8' => 'Payment',
        '9' => 'HoldBill'
    );
    return _get_system_constant_data($array, $search);
}

function BOOKING_TYPE($search = ""){
    $array = array(
        '1' => 'Online',
        '2' => 'On-line'
    );
    return _get_system_constant_data($array, $search);
}

function ALL_STATUS(){
    $array = array(
        0,1,2,3,4,
    );
    return $array;
}
