<?php
$lang['migration_class_doesnt_exist'] = "Please check migration class name!";
$lang['migration_multiple_version'] = "More than one migration files for 1 version!";
$lang['migration_not_found'] = "You have no migration files!";
$lang['migration_none_found'] = "You have no migration files!";
$lang['migration_invalid_filename'] = "Please check migration file name!";
$lang['migration_missing_up_method'] = "Please define up function for migration file!";
$lang['migration_missing_down_method'] = "Please define down function for migration file!";