<?php
/** convert a normal number if to float number with seprator
 * @param $number
 * @param int $precision
 * @param string $separator
 * @return string
 */
function floor_dec($number,$precision = 0,$separator = '.')
{
    $numberpart=explode($separator,$number);
    if(count($numberpart) < 2){
        return $number;
    }
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0)
    {$numberpart[1]=floor($numberpart[1]);}
    else
    {$numberpart[1]=ceil($numberpart[1]);}

    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}

/** round the grand total number when having cash payment, round to x.x5 or 0.x0
 * @param $value
 * @return float
 */
function sale_total_round($value){
    $over_value = intval($value * 100);
    $sub_value = $over_value % 5;
    return ($over_value - $sub_value) / 100;
//    return floor_dec($value,1,'.');
}

/** add duration (in minute) to the current time
 * @param $start_time
 * @param $duration
 * @param string $format
 * @return bool|int|string
 */
function add_duration($start_time, $duration, $format = ''){
    if(is_numeric($start_time)){
        $start_time = date('Y-m-d H:i:s',$start_time);
    }
    $date = date_create($start_time);
    $date = date_add($date, date_interval_create_from_date_string("$duration minutes"));
    if($format == ''){
        return strtotime(date_format($date,'Y-m-d H:i:s'));
    }else{
        return date_format($date, $format);
    }

}

/** convert a date from 1 timezone to another timezone
 * @param $date
 * @param string $fromTimezone
 * @param string $toTimezone
 * @param string $format
 * @return mixed
 */
function convert_date($date, $fromTimezone = "", $toTimezone = "", $format = "Y-m-d H:i:s")
{
    if ($fromTimezone == "") {
        $fromTimezone = BASE_TIMEZONE;
    }
    if ($toTimezone == "") {
        $toTimezone = BASE_TIMEZONE;
    }
    $d = new DateTime($date, new DateTimeZone($fromTimezone));
    if($fromTimezone != $toTimezone){
        $d->setTimezone(new DateTimeZone($toTimezone));
    }
    return $d->format($format);
}

/** convert a date to user's timezone
 * @param string $date
 * @param string $timezone
 * @param bool $hasTime
 * @return mixed|string
 */
function get_database_date($date = "", $timezone = "", $hasTime = true)
{
    $format = DATABASE_DATE_FORMAT;
    if ($hasTime) {
        $format .= " " . DATABASE_TIME_FORMAT;
    }
    if ($date != "") {
        if($timezone == ""){
            $timezone = get_user_timezone();
        }
        return convert_date($date, $timezone, "", $format);
    } else {
        return gmdate($format);
    }
}

/** convert a date to user's timezone
 * @param string $date
 * @param string $userTimezone
 * @param string $format
 * @param bool $hasTime
 * @return mixed
 */
function get_user_date($date = "", $userTimezone = "", $format = "", $hasTime = false)
{
    $ret = "";
    if($format == ""){
        $format = get_user_date_format();
    }
    if ($hasTime) {
        $format .= " " . get_user_time_format();
    }
    if($userTimezone == ""){
        $userTimezone = get_user_timezone();
    }
    if($date == ""){
        $date = gmdate($format);
    }
    return convert_date($date, BASE_TIMEZONE, $userTimezone, $format);
}

/** convert a sql date format to user date format
 * @param $date
 * @param bool $hasTime
 * @param bool $isLong
 * @return mixed
 */
function to_user_format($date, $hasTime = true, $isLong = false){
    $format = get_user_date_format($isLong);
    if($hasTime){
        $format .= " " . get_user_time_format();
    }
    return convert_date($date, "", "", $format);
}

 /** get user timezone
 * @return string
 */

function get_user_timezone(){
    $CI = & get_instance();
    if($CI->session->userdata('login') !== false){
        $login = $CI->session->userdata('login');
        if(isset($login->timezone)){
            return $login->timezone;
        }
        else{
            return $CI->system_config->get('timezone');
        }
    }
    $timezone = $CI->system_config->get('timezone');
    if($timezone){
        return $timezone;
    }
    return BASE_TIMEZONE;
}

/** return the user date format
 * @param bool $isLong
 * @return string
 */
function get_user_date_format($isLong = false){
    $CI = & get_instance();
    if($isLong && $CI->session->userdata('date_format_long') !== false){
        return $CI->session->userdata('date_format_long');
    }
    if($CI->session->userdata('date_format') !== false){
        return $CI->session->userdata('date_format');
    }
    return ($isLong) ? BASE_DATE_FORMAT_LONG : BASE_DATE_FORMAT;
}

/** return the user date format
 * @return string
 */
function get_user_time_format(){
    $CI = & get_instance();
    if($CI->session->userdata('time_format') !== false){
        return $CI->session->userdata('time_format');
    }else{
        return BASE_TIME_FORMAT;
    }
}

/** used to convert an array of object with no key into an array that have key-pair value. Or return an array collection of a property.
 * @param $values: the array data
 * @param string $key: the mapping key
 * @param string $field: the field that want to get collection from
 * @param bool $isUnique: is the key with unique data or a key with collection data
 * @return array
 */
function convert_to_array($values, $key = "", $field = "", $isUnique = true){
    $values = (array) $values;
    $ret = array();
    if($key == ""){
        if($field == ""){
            $ret = $values;
        }else{
            foreach($values as $value){
                if(is_array($value)){
                    if($isUnique){
                        if(!in_array($value[$field],$ret) && !is_null($value[$field])){
                            $ret[] = $value[$field];
                        }
                    }else{
                        $ret[] = $value[$field];
                    }

                }elseif(is_object($value)){
                    if($isUnique){
                        if(!in_array($value->{$field},$ret) && !is_null($value->{$field})){
                            $ret[] = $value->{$field};
                        }
                    }else{
                        $ret[] = $value->{$field};
                    }
                }
            }
        }
    }else{
        if($isUnique){
            if($field == ""){
                foreach($values as $value){
                    if(is_array($value)){
                        $keyString = $value[$key];
                        if(is_null($value) === false)
                            $ret[$keyString] = $value;
                    }elseif(is_object($value)){
                        $keyString = $value->{$key};
                        if(is_null($value) === false)
                            $ret[$keyString] = $value;
                    }
                }
            }else{
                foreach($values as $value){
                    if(is_array($value)){
                        $keyString = $value[$key];
                        $valueString = $value[$field];
                        if(is_null($value) === false){
                            $ret[$keyString] = $valueString;
                        }
                    }elseif(is_object($value)){
                        $keyString = $value->{$key};
                        $valueString = $value->{$field};
                        if(is_null($value) === false){
                            $ret[$keyString] = $valueString;
                        }
                    }
                }
            }
        }else{
            if($field == ""){
                foreach($values as $value){
                    if(is_array($value)){
                        $keyString = $value[$key];
                        if(!isset($ret[$keyString])){
                            $ret[$keyString] = array();
                        }
                        if(is_null($value) === false)
                        $ret[$keyString][] = $value;
                    }elseif(is_object($value)){
                        $keyString = $value->{$key};
                        if(!isset($ret[$keyString])){
                            $ret[$keyString] = array();
                        }
                        if(is_null($value) === false)
                            $ret[$keyString][] = $value;
                    }
                }
            }else{
                foreach($values as $value){
                    if(is_array($value)){
                        $keyString = $value[$key];
                        $valueString = $value[$field];
                        if(!isset($ret[$keyString])){
                            $ret[$keyString] = array();
                        }
                        $ret[$keyString][] = $valueString;
                    }elseif(is_object($value)){
                        $keyString = $value->{$key};
                        $valueString = $value->{$field};
                        if(!isset($ret[$keyString])){
                            $ret[$keyString] = array();
                        }
                        $ret[$keyString][] = $valueString;
                    }
                }
            }
        }
    }
    return $ret;
}

/** return a number into currency format eg: $12
 * @param $value
 * @return string
 */
function to_currency($value){
    $CI =& get_instance();
    $CI->load->model('TableModels/config_model', 'config_model');
    $currency = $CI->config->item('currency');
    return $currency . number_format(round($value,2), 2);
}

/** calculate the real discount value base on the given infos
 * @param $type
 * @param $value
 * @param $total
 * @return float
 */
function calculate_discount($type, $value, $total){
    if($type == 1){
        return $total * $value/100;
    }else{
        return $value;
    }
}

/** calculate the real commission value base on the given infos
 * @param $type
 * @param $value
 * @param $total
 * @param $num_emp
 * @param int $sub_value
 * @return float
 */
function calculate_commission($type, $value, $total, $num_emp,$sub_value = 0){
    if($sub_value){
        switch($type){
            case COMMISSION_BUNDLE_TYPE('%'):
                $parent_commission = ($total) * $value/100;
                return ($sub_value * $parent_commission /100) / $num_emp;
                break;
            case COMMISSION_BUNDLE_TYPE('$'):
                return $sub_value / $num_emp;
                break;
        }
    }else{
        if($type == 2){
            return ($total/$num_emp) * $value/100;
        }else{
            return $value / $num_emp;
        }
    }
}

/** into full name
 * @param $first_name
 * @param $last_name
 * @return string
 */
function to_full_name($first_name, $last_name){
    return str_replace("'"," ",(isset($first_name)&&strlen($first_name)?$first_name.' ':'').(isset($last_name)&&strlen($last_name)?$last_name.'':''));
}

/** return the booking system url
 * @param string $url
 * @return mixed
 */
function booking_url($url = ''){
    return site_url('booking/'.$url);
}

/** return the admin system url
 * @param string $url
 * @return mixed
 */
function admin_url($url = ''){
    return site_url('admin/'.$url);
}

/** return the staff system url
 * @param string $url
 * @return mixed
 */
function staff_url($url = ''){
    return site_url('staff/'.$url);
}

/** base on the array data given, generate the report and make it downloadalbe
 * @param $content
 * @param $report_name
 * @param string $format
 * @param string $d
 */
function download_report($content,$report_name, $format = 'excel', $d = "\t") {
    $CI = & get_instance();
    $CI->load->library('excel');
    $CI->excel->setActiveSheetIndex(0)->fromArray($content);
    if($format == 'excel'){
        $report_name = $report_name . ".xlsx";
        $objWriter = new PHPExcel_Writer_Excel2007($CI->excel);
    }else{
        $report_name = $report_name . ".csv";
        $objWriter = new PHPExcel_Writer_CSV($CI->excel);
        $objWriter->setDelimiter($d);
    }
    ob_start();
    $objWriter->save('php://output');
    $excelOutput = ob_get_clean();
    force_download($report_name,$excelOutput);
}

/**
 * sort 2 different array (sorted) to 1 array base on 2 different fields
 * @param $arr1
 * @param $arr2
 * @param $key1
 * @param $key2
 * @param string $type
 * @param string $return_type
 * @return array
 */
function array_sort_asc($arr1,$arr2,$key1,$key2,$type='asc',$return_type = ''){
    foreach($arr1 as &$row1){
        $row1 = (object)$row1;
    }
    foreach($arr2 as &$row2){
        $row2 = (object)$row2;
    }
    $result = array();
    $i = 0; $j = 0;
    while($i < count($arr1) || $j < count($arr2)){
        if($i == count($arr1)){
            $result[] = $arr2[$j++];
            continue;
        }
        if($j == count($arr2)){
            $result[] = $arr1[$i++];
            continue;
        }

        if($type == 'asc'){
            if($arr1[$i]->$key1 < $arr2[$j]->$key2){
                $result[] = $arr1[$i];
                $i++;
            }else{
                $result[] = $arr2[$j];
                $j++;
            }
        }else{
            if($arr1[$i]->$key1 > $arr2[$j]->$key2){
                $result[] = $arr1[$i];
                $i++;
            }else{
                $result[] = $arr2[$j];
                $j++;
            }
        }
    }
    if($return_type == 'array'){
        foreach($result as &$row){
            $row = (array)$row;
        }
    }
    if($return_type == 'object'){
        foreach($result as &$row){
            $row = (object)$row;
        }
    }
    return $result;
}

/**
 * generate as bool
 * @param $str
 * @return bool
 */
function to_b($str){
    if(is_string($str)){
        if($str == 'false' || $str == '' || $str=='0'){
            return false;
        }
        else{
            return true;
        }
    }
    return $str;
}

/**
 * used to count how many leaf are there in the tree structure
 * @param array $array : array to be counted
 * @param int $count : start of the count (optional)
 * @param int $level : level of the the array to be counted (optional)
 * @param int $current_level : current level of the array (for recursive purpose, don't touch this!)
 * @return int
 */

function count_deep($array = array(),$count = 0, $level = 0, $current_level = 0){
    if(! $level){
        foreach($array as $key=>$value){
            if(!is_array($value)){
                $count++;
            }else{
                $count = count_deep($value,$count);
            }
        }
    }else{
        if($current_level == $level){
            return ++$count;
        }else if($current_level <= $level){
            foreach($array as $key=>$value){
                $count = count_deep($value,$count,$level,$current_level+1);
            }
        }
    }
    return $count;
}

/** return as an array object, which will have all number key
 * @param $obj
 * @return array
 */
function to_array($obj){
    $arr = array();
    foreach($obj as $key=>$value){
        if(is_numeric($key)){
            $arr[intval($key)] = $value;
        }else{
            $arr[$key] = $value;
        }
    }
    return $arr;
}

function mapping_data_soft($param){
    $CI =& get_instance();
    $data_collection = array();
    $condition_collection = array();
    $main_key = null;

    $data = $param['data'];
    foreach($data as $key=>$each_row){
        if(!isset($main_key)){
            $main_key = $each_row['key'];
        }
        $df = array(
            'key' => '',
            'data'=> null,
            'map_from' => null,
        );
        $data[$key] = array_merge($df,$each_row);
    }
    foreach($data as $order=>$each_row){
        $table = $each_row['key'];
        if(isset($each_row['data'])){
            $data_collection[$each_row['key']]['data'] = get_data($each_row['data']);
        }else if(isset($each_row['map_from'])){
            $dest_id_field = $each_row['map_from']['key']['dest'];
            $src_id_field = $each_row['map_from']['key']['src'];
            $source_table = $each_row['map_from']['src'];
            $id_value = isset($condition_collection[$source_table][$src_id_field])?
                $condition_collection[$source_table][$src_id_field]:
                convert_to_array($data_collection[$source_table]['data'],'',$src_id_field);
            ;
            $data_collection[$table]['data'] = $CI->load->table_model($table)->get(array($table.'.'.$dest_id_field => $id_value));
        }else{
            $data_collection[$each_row['key']]['data'] = $CI->load->table_model($table)->get();
        }

        $data_collection[$table]['map_from'] = isset($each_row['map_from'])?$each_row['map_from']:array('src' => '', 'key' => array('src' => '', 'dest' => ''));

        $ready_condition_id_collection = array();
        for($i = $order + 1; $i < count($data); $i++){
            $row = $data[$i];
            if($row['map_from'] && $row['map_from']['src'] == $table){
                $ready_condition_id_collection[] = $row['map_from']['key']['src'];
            }
        }
        foreach($data_collection[$table]['data'] as $data_row){
            foreach($ready_condition_id_collection as $condition_id){
                if(!isset($condition_collection[$table][$condition_id])){
                    $condition_collection[$table][$condition_id] = array();
                }
                $condition_collection[$table][$condition_id][] = $data_row->$condition_id;
            }
        }
    }

    $expand_data = array();
    $main_data = $data_collection[$main_key]['data'];
    foreach($main_data as $row){
        $content = array();
        foreach($row as $key_field=>$value){
            $content[$main_key.'.'.$key_field] = $value;
        }
        $rows = __expand_data($content,$data_collection,$main_key);
        $expand_data = array_merge($expand_data,$rows);
    }

    $mapping_data_param = $param['mapping_data']; $is_unique = isset($mapping_data_param['is_unique'])?$mapping_data_param['is_unique']:false;

    $ret = mapping_data($expand_data, $mapping_data_param, $is_unique);

    return $ret;
}

function __expand_data($raw_expand_content,$data_collection, $parent_key = ''){
    $expand_content_list = array($raw_expand_content);
    foreach($data_collection as $key_data=>$field){
        $data = $field['data'];
        $map_from = $field['map_from'];
        if($map_from['src'] != $parent_key){
            continue;
        }
        $new_expansion_list = array();
        foreach($expand_content_list as $expand_content){
            foreach($data as $data_row){
                if($expand_content[$map_from['src'].'.'.$map_from['key']['src']] != $data_row->$map_from['key']['dest']){
                    continue;
                }
                $content = array();
                foreach($data_row as $key_field=>$value){
                    $content[$key_data.'.'.$key_field] = $value;
                }
                foreach($expand_content as $key=>$value){
                    $content[$key] = $value;
                }
                $rows = __expand_data($content,$data_collection,$key_data);
                $new_expansion_list = array_merge($new_expansion_list,$rows);
            }
        }
        $expand_content_list = $new_expansion_list;

    }
    return count($expand_content_list)?$expand_content_list:$expand_content_list;

}

/**
 * get data by function
 * @param null $data
 * @return null
 */
function get_data($data = null){
    if(is_callable($data)){
        return $data();
    }
    return $data;
}


function t_array($arr){
    if(is_string($arr) || is_numeric($arr)){
        return array($arr);
    }else if(is_array($arr)){
        return $arr;
    }
    return array();
}

/**
 * used to mapping a set of objects into a structured tree
 * @param array $array (set of records retrieve from database)
 * @param array $level_sort = array('name' => '', 'function' = function)
 * @return array
 */
function mapping_data($array, $level_sort = array(), $is_unique = false){
    $ret_array = array('rows' => array());
    foreach($array as $row){
        if(is_array($row)) $row = (object)$row;
        if(is_callable($level_sort['first_function'])){
            $level_sort['first_function']($ret_array,$row);
        }
        $each_level = &$ret_array['rows'];
        $current_level = 1;
        $max_level = count($level_sort['mapping_function']);
        foreach($level_sort['mapping_function'] as $name => $function){
            if(!isset($row->$name)){
                break;
            }
            if($current_level == $max_level){
                if($is_unique){
                    $each_level[$row->$name] = $row;
                }
                if(is_callable($function)){
                    $function($each_level[$row->$name],$row);
                }
                else{
                    if(is_callable($function)){
                        $function($each_level[$row->$name],$row);
                    }
                    $each_level[$row->$name]['rows'][] = $row;
                }
                if(is_callable($function)){
                    $function($each_level[$row->$name],$row);
                }
            }else{
                if(is_callable($function)){
                    $function($each_level[$row->$name],$row);
                }
                $each_level = &$each_level[$row->$name]['rows'];
            }
            $current_level++;
        }
    }

    return $ret_array;
}

/**
 * used to detect country calling code. this system using Singapore as default
 */
function convert_phonenumber($phoneNumber){
    if(!$phoneNumber){
        return false;
    }

    if(substr($phoneNumber, 0,2) != 65){
        $phoneNumber = '65' . $phoneNumber;
    }

    return $phoneNumber;
};

/**
 * Detect phone number with countrycalling code , default will be 65
 */
function detect_phonenumber($phoneNumber, $countryCallingCode = ''){
    if(!$phoneNumber){
        return false;
    }
    else{
        if($countryCallingCode == '') {
            if (substr($phoneNumber, 0, 2) != 65) {
                return array('phoneNumber' => $phoneNumber, 'countryCallingCode' => '');
            } else {
                return array('phoneNumber' => substr($phoneNumber, 2), 'countryCallingCode' => 65);
            }
        }
        else{
            $lengthStr = strlen($countryCallingCode);
            $subStr = substr($phoneNumber, 0, $lengthStr);
            if($subStr == $countryCallingCode){
                return array('phoneNumber' => substr($phoneNumber,$lengthStr),'countryCallingCode' => $subStr);
            }
            else{
                return array('phoneNumber' => $phoneNumber, 'countryCallingCode'=> $countryCallingCode);
            }
        }
    }
}