<?php
if ( ! function_exists('_get_system_constant_data'))
{
    function _get_system_constant_data($array, $search = ""){
        if($search === ""){
            return $array;
        }else{
            if(is_string($search)){
                foreach($array as $key => $value){
                    if(strtolower($value) == strtolower($search)){
                        return $key;
                    }
                }
            }else{
                if(is_array($search)){
                    $ret = array();
                    if(is_numeric($search[0])){
                        foreach($array as $key => $value){
                            if(in_array($key, $search)){
                                $ret[$key] = $value;
                            }
                        }
                    }elseif(is_string($search[0])){
                        foreach($array as $key => $value){
                            if(in_array($value, $search)){
                                $ret[] = $key;
                            }
                        }
                    }
                    return $ret;
                }
            }
        }
        return isset($array[$search]) ? $array[$search] : null;
    }
}
if(file_exists(APPPATH.'config/system_constant.php')){
    include(APPPATH.'config/system_constant.php');
}