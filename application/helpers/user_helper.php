<?php

function checkLoggedIn(){

    $CI =& get_instance();
    $User = $CI->session->userdata('login');
    if(isset($User) && $User){
        return $User;
    }
    else{
        redirect(base_url());
    }
}
function checkLoggedInWithoutRedirect(){

    $CI =& get_instance();
    $User = $CI->session->userdata('login');
    if(isset($User) && $User){
        return $User;
    }
    else{
        return false;
    }
}