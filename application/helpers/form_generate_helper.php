<?php

    /*
     * Params:
     * $control_id : name of the control
     * $display_name : the title of the control
     * $control_type : textbox, numberbox, checkbox, radiobox
     * $control_type:values : params for checkbox or radiobox. Input Object: title , value
     * $control_required: a bool value or not, define a field is required or not
     */
    function generate_form_control($row, $inherit_btn = true, $no_blank = true){
        $control_id   = $row->id;
        $display_name = $row->name;
        $control_type = $row->type;
        $control_value= $row->value;
        $control_type_values = $row->type_value;
        $control_required = $row->required;
        if($inherit_btn){
            $inherit_value = $row->inherit;
        }
        if(!$no_blank){
            $control_value = '';
        }

        $required = ($control_required)?'required':'';
        $html = '<div class="form-group">
                                    <label class="col-sm-4 col-md-4 col-lg-3 control-label '.$required.'">'.$display_name.'</label>
                                    <div class="col-sm-6 col-md-6 col-lg-9">';
        $html .= "<div class='col-md-4'>";
        if(CONFIG_TYPE($control_type) == "textbox"){
            $html .= '<input class="form-control" name="value['.$control_id.']" value="'.$control_value.'" class="" id="'.$control_id.'" type="text" '.$required.'>';
        }

        else if(CONFIG_TYPE($control_type) == "numberbox"){
            $html .= '<input class="form-control" value="'.$control_value.'" name="value['.$control_id.']" id="'.$control_id.'" type="number" required='.$required.'> ';
        }


        else if(CONFIG_TYPE($control_type) == "radiobox")
        {
            foreach($control_type_values as $control_type_value){
                if($control_type_value->value != $control_value)
                    $html .= '<input name="value['.$control_id.']" id="'.$control_id.'" value="'.$control_type_value->value.'" type="radio">';
                else
                    $html .= '<input name="value['.$control_id.']" id="product" value="'.$control_type_value->value.'" type="radio" checked>';
                $html .= '<span> &nbsp'.$control_type_value->title.'&nbsp </span>';
            }

        }

        else if(CONFIG_TYPE($control_type) == 'timezone'){
            $html .= "<select class='form-control' id='{$control_id}' name='value[{$control_id}]'>";
            foreach(DateTimeZone::listIdentifiers() as $timezone){
                if($control_value == $timezone){
                    $html .= "<option value='{$timezone}' selected>{$timezone}</option>";
                }else{
                    $html .= "<option value='{$timezone}'>{$timezone}</option>";
                }

            }
            $html .= "</select>";
        }
        else if(CONFIG_TYPE($control_type) == 'select'){
            $html .= "<select class='form-control' id='{$control_id}' name='value[{$control_id}]' data-value='{$control_value}' data-source='".site_url($control_type_values)."'></select>";
        }

        else if(CONFIG_TYPE($control_type) == 'time'){
            $html .= "<input class='form-control' class='time-picker' id='{$control_id}' name='value[{$control_id}]' value='{$control_value}'/>";
        }

        else if(CONFIG_TYPE($control_type) == 'textarea'){
            $html .= "<textarea class='form-control' id='{$control_id}' name='value[{$control_id}]'>$control_value</textarea>";
        }
        $html .= "</div>";
        if($inherit_btn){
            $html .= "<input target='{$control_id}' ".($inherit_value&&$no_blank?'checked':'')." style='margin-left:10px;' id='{$control_id}_checkbox' type='checkbox' name='value[{$control_id}]' value='inherit'/>&nbsp<label for='{$control_id}_checkbox'>Inherit</label>";
        }

        $html .= "</div></div>";
        return $html;
    }

    function pos_object_merge($context, $add_objects, $condition1, $condition2){
        foreach($context as $row){
            foreach($add_objects as $object){
                if($row->$condition1 == $object->$condition2)
                    $row->detail = $object;
            }
        }
        return $context;
    }

    function pos_add_object($context, $add_object, $condition_key, $name){
        $add_object_key = $add_object->$condition_key;
        if(isset($context[$add_object_key]) == false){
            return $context;
        }
        if(isset($context[$add_object_key]->$name) && is_array($context[$add_object_key]->$name)){
            $context[$add_object_key]->{$name}[] = $add_object;
        }else{
            $context[$add_object_key]->{$name} = array();
            $context[$add_object_key]->{$name}[] = $add_object;
        }
        return $context;
    }

    function pos_render_array_no_dupplicate($array,$key_array, $field_to_be_added = array()){
        if(is_string($field_to_be_added)){
               $field_to_be_added = array($field_to_be_added);
        }
        $data = array();

        foreach($array as $row){
            if($key_array != null){
                if(isset($data[$row->$key_array])){
                    $item = $data[$row->$key_array];
                }else{
                    $item = new stdClass();
                    $data[$row->$key_array] = array();
                }
            }
            else{
                $item = new stdClass();
            }

            foreach($row as $key=>$value) {
                if (isset($item->$key)) {
                    if (in_array($key, $field_to_be_added)) {
                        if (is_numeric($item->$key)) {
                            $item->$key += $value;
                        } else {
                            $item->$key .= ', ' . $value;
                        }
                    } else {
                        $item->$key = $value;
                    }
                } else {
                    $item->$key = $value;
                }
            }
            if($key_array != null){
                $data[$row->$key_array] = $item;
            }else{
                $data[] = $item;
            }

        }
        return $data;
    }

    function generate_control($array, $control_value, $type='select', $attribute=array()){
        $html = "";
        if($type == 'select'){
            $html = "<select ";
            foreach($attribute as $key=>$value){
                $html .= $key.'="'.$value.'" ';
            }
            $html .= '>';
            foreach($array as $key=>$value){
                if($key == $control_value)
                    $html .= "<option value='{$key}'  selected='true'>{$value}</option>";
                else{
                    $html .= "<option value='{$key}'>{$value}</option>";
                }
            }
            $html .= "</select>";
        }
        return $html;
    }