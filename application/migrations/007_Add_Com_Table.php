<?php
class Migration_Add_Com_Table extends CI_Migration{
    public function up(){

        if (! $this->db->table_exists('information')) {
            $this->db->query("CREATE TABLE `information` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(255) DEFAULT NULL,
          `code` varchar(255) DEFAULT NULL,
          `value` varchar(255) DEFAULT NULL,
          `created_date` datetime DEFAULT NULL,
          `updated_date` datetime DEFAULT NULL,
          `creator` int(11) DEFAULT NULL,
          `status` tinyint(4) DEFAULT '1',
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;");
                $this->db->query("INSERT INTO `information` VALUES ('1', 'Name', 'com_name', 'Healing Touch Spa', null, '2015-02-06 04:58:50', null, '1')");
                $this->db->query("INSERT INTO `information` VALUES ('2', 'Address', 'com_add', 'Nha tui do', null, '2015-02-06 04:58:50', null, '1')");
                $this->db->query("INSERT INTO `information` VALUES ('3', 'Phone', 'com_phone', 'Nha tui', null, '2015-02-06 04:58:50', null, '1')");
        }


        $branch_module_id = $this->db->get_where('module', array('code' => 'branch'))->result();
        if(count($branch_module_id)){
            $branch_module_id = $branch_module_id[0]->id;
            try{
                $this->db->insert('module_permission', array('module_id' => $branch_module_id, 'permission_code' => 'ep'));
            }catch(Exception $e){

            }
        }
        $branch_group_module_id = $this->db->get_where('module', array('code' => 'branch_group'))->result();
        if(count($branch_group_module_id)){
            $branch_group_module_id = $branch_group_module_id[0]->id;
            try{
                $this->db->insert('module_permission', array('module_id' => $branch_group_module_id, 'permission_code' => 'ep'));
            }catch(Exception $e){

            }
        }
    }

    public function down(){
        $this->db->query('DROP TABLE `information`');
    }
}