<?php
class Migration_Bundle extends CI_Migration{
    public function up(){
        $this->db->trans_start();
        $this->add_sub_commission_field_commission_table();
        $this->add_bill_sub_commission();
        $this->add_booking_table();
        $this->db->trans_complete();
    }

    function add_bill_sub_commission(){
        $fields = array(
            'sub_item_id' => array(
                'type' => 'int',
                'unsigned' => true,
                'null' => true
            )
        );
        $this->dbforge->add_column('bill_employee',$fields, 'commission_value');

        $fields = array(
            'sub_commission_value' => array(
                'type' => 'float',
                'default'    => 0,
                'null' => true
            )
        );
        $this->dbforge->add_column('bill_employee',$fields, 'sub_item_id');

        $this->db->query('ALTER TABLE bill_employee ADD CONSTRAINT `bill_employee_ibfk_3` FOREIGN KEY (`sub_item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->db->query('alter table bill_employee drop PRIMARY KEY');
        $this->db->query('alter table bill_employee add CONSTRAINT `bill_item_id_2` UNIQUE (`bill_item_id`,`employee_id`,`sub_item_id`)');
    }

    function add_sub_commission_field_commission_table(){
        $this->db->query('DROP TABLE IF EXISTS `item_bundle`;');
        $this->db->query('DROP TABLE IF EXISTS `item_bundle_item`;');
        $this->db->query('CREATE TABLE `item_bundle` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `item_bundle_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
');
        $this->db->query('CREATE TABLE `item_bundle_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `sub_item_id` int(10) unsigned DEFAULT NULL,
  `commission_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_item_id` (`sub_item_id`),
  KEY `item_bundle_item_ibfk_1` (`item_id`),
  CONSTRAINT `item_bundle_item_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `item_bundle_item_ibfk_2` FOREIGN KEY (`sub_item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
');
    }

    function add_booking_table()
    {
        $this->db->query('SET FOREIGN_KEY_CHECKS=0; ');
        $this->db->query('DROP TABLE IF EXISTS `booking`;');
        $this->db->query('CREATE TABLE `booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `bundle_id` int(10) unsigned DEFAULT NULL,
  `branch_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`) USING BTREE,
  KEY `branch_id` (`branch_id`) USING BTREE,
  KEY `bundle_id` (`bundle_id`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_3` FOREIGN KEY (`bundle_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

');
        $this->db->query('DROP TABLE IF EXISTS `booking_service`;');
        $this->db->query('CREATE TABLE `booking_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned DEFAULT NULL,
  `employee_id` int(10) unsigned DEFAULT NULL,
  `room_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `employee_id` (`employee_id`),
  KEY `room_id` (`room_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `booking_service_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_service_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_service_ibfk_3` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_service_ibfk_4` FOREIGN KEY (`service_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
');
        $this->db->query('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function down(){
    }
}