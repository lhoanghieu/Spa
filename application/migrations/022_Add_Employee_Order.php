<?php
class Migration_Add_Employee_Order extends CI_Migration{
    public function up(){
        $fields = array(
            'ordering' => array(
                'type' => 'int',
                'default'    => 0,
                'unsigned' => true,
                'null' => true
            )
        );
        $this->dbforge->add_column('employee',$fields, 'type');
    }

    public function down(){
    }
}