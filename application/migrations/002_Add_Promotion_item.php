<?php
class Migration_Add_Promotion_Item extends CI_Migration{
    public function up(){
        $fields = array(
            'promotion' => array(
                'type' => 'tinyint',
                'constraint' => 100,
                'default'    => 0,
                'null' => true
            )
        );
        $this->dbforge->add_column('item',$fields, 'credit_value');
    }

    public function down(){
        $this->dbforge->drop_column('item','promotion');
    }
}