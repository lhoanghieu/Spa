<?php
class Migration_Item_Package_Table extends CI_Migration{
    public function up(){
        $this->db->query("DROP TABLE IF EXISTS `item_package`");
        $this->db->query("CREATE TABLE `item_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  `expire_length` int(10) unsigned DEFAULT NULL,
  `creator` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`) USING BTREE,
  CONSTRAINT `item_package_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }

    public function down(){

    }
}