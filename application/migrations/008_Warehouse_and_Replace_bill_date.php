<?php
class Migration_Warehouse_and_Replace_bill_date extends CI_Migration{

    function warehouse(){
        $this->db->query("DROP TABLE IF EXISTS `warehouse_item`;");
        $this->db->query("DROP TABLE IF EXISTS `warehouse_branch`;");
        $this->db->query("DROP TABLE IF EXISTS `warehouse_branch_group`;");
        $this->db->query('DROP TABLE IF EXISTS `warehouse`;');

        $this->db->query("CREATE TABLE `warehouse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `global` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;");

        $this->db->query("CREATE TABLE `warehouse_branch` (
  `warehouse_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  KEY `warehouse_id` (`warehouse_id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `warehouse_branch_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `warehouse_branch_ibfk_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->db->query("CREATE TABLE `warehouse_branch_group` (
  `branch_group_id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `global` tinyint(4) DEFAULT '0',
  KEY `branch_group_id` (`branch_group_id`),
  KEY `warehouse_id` (`warehouse_id`),
  CONSTRAINT `warehouse_branch_group_ibfk_1` FOREIGN KEY (`branch_group_id`) REFERENCES `branch_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `warehouse_branch_group_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $this->db->query("CREATE TABLE `warehouse_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `warehouse_item_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `warehouse_item_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;");
    }

    function warehouse_module(){
        $fields = array(
            'type' => array(
                'type' => 'tinyint',
                'unsigned' => TRUE,
                'null' => true,
                'default' => 1
            )
        );
        $this->dbforge->add_column('module',$fields,'code');
        $this->db->update('module',array('type' => 1));

        $this->db->update('module',array('type' => 2),array('code' => 'branch'));
        $this->db->update('module',array('type' => 2),array('code' => 'branch_group'));

        $module_array = array(
            array(
                'name' => 'Warehouse',
                'code' => 'warehouse',
                'type' => 2,
            ),
            array(
                'name' => 'Warehouse Management',
                'code' => 'warehouse_management',
                'type' => 1,
            ),
        );

        $this->db->insert_batch('module',$module_array);
        foreach($module_array as $module){
            $id = $this->db->get_where('module',$module)->result();
            $id = $id[0]->id;
            $info = array(
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'v',
                ),
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'd',
                ),
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'e',
                ),
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'i',
                ),
            );
            $this->db->insert_batch('module_permission',$info);
        }

        $warehouse_module_id       = $this->db->get_where('module',$module_array[0])->result()[0]->id;
        $warehouse_man_module_id = $this->db->get_where('module',$module_array[1])->result()[0]->id;
        $warehouse_permission_id = $this->db->get_where('module_permission',array('module_id' => $warehouse_module_id, 'permission_code' => 'v'))->result()[0]->id;
        $warehouse_man_permission_id = $this->db->get_where('module_permission',array('module_id' => $warehouse_man_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $inventory = $this->db->get_where('menu',array('title' => 'Inventory'))->result()[0]->id;

        $this->db->insert('menu',array(
            'title'     => 'Warehouse',
            'link'      => 'admin/warehouse',
            'type' => MENU_TYPE('Admin'),
            'module_permission_id' => $warehouse_permission_id
        ));
        $this->db->insert('menu',array(
            'title' => 'Warehouse',
            'link'  => 'staff/warehouse_management',
            'parent'=> $inventory,
            'type'  => MENU_TYPE('Staff'),
            'module_permission_id' => $warehouse_man_permission_id
        ));
    }

    function replace_bill_date(){
        $fields = array(
            'replace_bill_date' => array(
                'type' => 'datetime',
                'null' => true
            ),
            'raw_created_date' => array(
                'type' => 'datetime',
                'null' => true,
            ),
        );
        $this->dbforge->add_column('bill',$fields,'void_date');
    }

    public function up(){
        $this->db->trans_start();
        $this->warehouse();
        $this->warehouse_module();
        $this->replace_bill_date();
        $this->db->trans_complete();
    }

    public function down(){
    }
}