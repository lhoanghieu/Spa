<?php
class Migration_Report_Permission extends CI_Migration{
    public function up(){
        $this->db->trans_start();
        $this->create_report_group_permission();
        $this->distribute_report_group_permission_data();
        $this->add_data();
        $this->add_group_menu_admin_system();
        $this->db->trans_complete();
    }

    function create_report_group_permission(){
        $this->db->query('SET FOREIGN_KEY_CHECKS=0;');
        $this->db->query('DROP TABLE IF EXISTS `report_group`;');
        $this->db->query('DROP TABLE IF EXISTS `report`;');
        $this->db->query("CREATE TABLE `report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned DEFAULT NULL,
  `condition_form` tinyint(4) DEFAULT '1' NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `system` varchar(255) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT NULL,
  `creator` int(10) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `report_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->db->query('CREATE TABLE `report_group` (
  `group_id` int(10) unsigned NOT NULL,
  `report_id` int(10) unsigned NOT NULL,
  KEY `group_id` (`group_id`),
  KEY `report_id` (`report_id`),
  CONSTRAINT `report_group_ibfk_2` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `report_group_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->db->query('SET FOREIGN_KEY_CHECKS=1;');
    }

    function distribute_report_group_permission_data(){
        $data =  $array = array(
            'commission' => array(
                'name' => "Commission Reports",
                "child" => array(
                    'employee_commission' => array('name' => 'Employee Commission Report', 'system' => 'all'),
                    'branch_commission' => array('name' => 'Branch Commission Report', 'system' => 'all')
                )
            ),
            'sales' => array(
                'name' => "Sales Reports",
                "child" => array(
                    'sales_category_report'         => array('name' => 'Category Report','system' => 'all'),
                    'receipt_report'                => array('name' => 'Receipt Reports','system' => 'all'),
                    'staff_sales_report'            => array('name' => 'Staff Report', 'system' => 'all'),
                    'item_summary_report'           => array('name' => 'Item Summary Report', 'system' => 'all'),
                    'void_transaction_report'       => array('name' => 'Void Transaction Report','system' => 'all'),
                    'backdated_transaction_report'  => array('name' => 'Backdated Transaction Report', 'system' => 'all')
                )
            ),
            'branch' => array(
                'name' => "Branch Reports",
                'child' => array(
                    'interbranch' => array('name' => 'Interbranch Report', 'system' => 'all')
                )
            ),
            'customer' => array(
                'name' => 'Customer Reports',
                'child' => array(
                    'mandatory_field_report'            => array('name' => 'Mandatory Field Report', 'condition_form' => false, 'system' => 'all'),
                    'no_visit_report'                   => array('name' => 'No Visit Customers Report', 'system' => 'all'),
                    'item_purchased_report'             => array('name' => '"Who purchased the item" Report', 'system' => 'staff'),
                    'item_no_purchased_report'          => array('name' => '"Who stop purchasing the item" Report', 'system' => 'all'),
                    'purchase_transaction_report'       => array('name' => 'Purchase Transaction Report', 'system' => 'all'),
                    'package_transaction_report'        => array('name' => 'Package Transaction Report', 'system' => 'all')
                )
            ),
            'package' => array(
                'name' => 'Package Reports',
                'child'=> array(
                    'unexpired_credit_report'          => array('name' => 'Expiring Credits Report', 'system' => 'all'),
                    'expired_credit_report'            => array('name' => 'Expierd Credit Report', 'system' => 'all')
                ),
            ),

            'warehouse' => array(
                'name' => 'Warehouse Reports',
                'child' => array(
                    'stock_balance_report'                     => array('name' => 'Stock Balance Report', 'system' => 'all'),
                    'stock_added_report'                       => array('name' => 'Stock Added into Inventory', 'system' => 'all'),
                    'stock_movement_report'                    => array('name' => 'Stock Movement Report', 'system' => 'all'),
                )
            ),
        );

        foreach($data as $parent_code => $parent_field){
            $this->db->insert('report',array(
                'name'  => $parent_field['name'],
                'code'  => $parent_code,
                'system'=> MENU_TYPE('All'),
                'status'=> 1
            ));
            $parent_id = $this->db->insert_id();
            foreach($parent_field['child'] as $child_code => $child_field){
                $df = array(
                    'name'   => '',
                    'system' => MENU_TYPE('All'),
                    'condition_form' => 1,
                    'parent' => $parent_id,
                    'code'   => $child_code,
                    'status' => 1
                );
                $child_field['system'] = MENU_TYPE($child_field['system']);
                $child_field = array_merge($df,$child_field);
                $this->db->insert('report',$child_field);
            }
        }
    }

    function add_data(){
        $report_list = $this->db->get('report')->result();
        $group_list = $this->db->get('group')->result();

        foreach($report_list as $report){
            foreach($group_list as $group){
                $this->db->insert('report_group',array('report_id' => $report->id,'group_id' => $group->id));
            }
        }
    }

    function add_group_menu_admin_system(){
        $result = $this->db->get_where('menu',array('title' => 'Groups'))->result_array();
        if(count($result) == 1){
            $result = $result[0];
            $will_insert_menu = array(
                'title' => $result['title'],
                'link'  => 'admin/groups',
                'module_permission_id' => $result['module_permission_id'],
                'type'  => MENU_TYPE('Admin')
            );
            $this->db->insert('menu',$will_insert_menu);
        }
        $this->db->update('module',array('type' =>  MENU_TYPE('All')), array('code' => 'groups'));
    }

}