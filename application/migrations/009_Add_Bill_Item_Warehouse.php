<?php
class Migration_Add_Bill_Item_Warehouse extends CI_Migration{
    public function up(){
        $fields = array(
            'warehouse_id' => array(
                'type' => 'int',
                'null' => true,
                'unsigned' => true,
            )
        );
        $this->dbforge->add_column('bill_item',$fields, 'credit_add');
        $this->db->query('alter table bill_item ADD CONSTRAINT `bill_item_ibfk_3` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE SET NULL ON UPDATE SET NULL');
    }

    public function down(){

    }
}