<?php
class Migration_Commission_Module extends CI_Migration{
    function commission_module(){
        $commission_module_id = $this->db->get_where('module',array('code' => 'commission'))->result()[0]->id;

        $commission_permission_id = $this->db->get_where('module_permission',array('module_id' => $commission_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'commission'));

        $this->db->insert('menu',array(
            'title'     => 'Commission',
            'link'      => 'admin/commission',
            'type'      => MENU_TYPE('Admin'),
            'module_permission_id' => $commission_permission_id
        ));
    }

    public function up(){
        $this->db->trans_start();

        $this->commission_module();

        $this->db->trans_complete();
    }

    public function down(){

    }
}