<?php
class Migration_Admin_Credit_Module extends CI_Migration{

    function add_module(){
        $customer_module_id = $this->db->get_where('module',array('code' => 'customers'))->result()[0]->id;

        $customer_module_permission_id = $this->db->get_where('module_permission',array('module_id' => $customer_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'customers'));

        $this->db->insert('menu',array(
            'title'     => 'Customers',
            'link'      => 'admin/customers',
            'type'      => MENU_TYPE('Admin'),
            'module_permission_id' => $customer_module_permission_id
        ));
    }

    function credit_adjustment_log(){
        $this->db->query('DROP TABLE IF EXISTS `credit_adjustment_log`;');
        $this->db->query('CREATE TABLE `credit_adjustment_log` (
  `credit_id` int(10) unsigned NOT NULL,
  `credit_value_before` int(11) DEFAULT NULL,
  `credit_change` int(10) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  KEY `credit_id` (`credit_id`),
  CONSTRAINT `credit_adjustment_log_ibfk_1` FOREIGN KEY (`credit_id`) REFERENCES `credit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }

    function credit_log(){
        $this->db->query('DROP TABLE IF EXISTS `credit_log`;');
        $this->db->query("CREATE TABLE `credit_log` (
  `id` int(11) unsigned NOT NULL,
  `customer_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `credit` float(11,2) DEFAULT NULL,
  `credit_original` float(11,2) DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `global` tinyint(3) unsigned DEFAULT '0',
  `log_time` datetime DEFAULT NULL,
  `log_user` int(10) unsigned DEFAULT NULL,
  `log_type` tinyint(3) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function up(){
        $this->db->trans_start();

        $this->add_module();
        $this->credit_adjustment_log();
        $this->credit_log();

        $this->db->trans_complete();
    }

    public function down(){

    }
}