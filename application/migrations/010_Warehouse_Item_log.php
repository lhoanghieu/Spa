<?php
class Migration_Warehouse_item_log extends CI_Migration{
    public function up(){
        $this->db->query("DROP TABLE IF EXISTS `warehouse_item_log`");
        $this->db->query("CREATE TABLE `warehouse_item_log` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `log_user` int(10) unsigned DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `log_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function down(){

    }
}