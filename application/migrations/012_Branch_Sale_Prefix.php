<?php
class Migration_Branch_Sale_Prefix extends CI_Migration{
    public function up(){
        if (! $this->db->field_exists('sale_id_prefix', 'branch'))
        {
            $fields = array(
                'sale_id_prefix' => array(
                    'type' => 'varchar',
                    'constraint' => 20,
                    'null' => true
                )
            );
            $this->dbforge->add_column('branch',$fields, 'fax');
        }
        $this->dbforge->modify_column('bill', array(
            'code' => array(
                'type' => 'varchar',
                'constraint' => 20,
            )
        ));
        $this->db->delete('config',array('code' => 'prefix'));
        $bills = $this->db->get('bill')->result();
        foreach($bills as $bill){
            $this->db->update('bill',array('code' => 'HT_'.$bill->code), array('id' => $bill->id));
        }

        $this->db->update('branch',array('sale_id_prefix' => 'YS'),array('name' => 'Yishun Branch'));
        $this->db->update('branch',array('sale_id_prefix' => 'TP'),array('name' => 'Tampines Branch'));
        $this->db->update('branch',array('sale_id_prefix' => 'UT'),array('name' => 'Thomson Branch'));
        $this->db->update('branch',array('sale_id_prefix' => 'NV'),array('name' => 'Novena Branch'));
        $this->db->update('branch',array('sale_id_prefix' => 'RV'),array('name' => 'River Valley Branch'));
        $this->db->update('branch',array('sale_id_prefix' => 'AL'),array('name' => 'Alexandra Branch'));
    }

    public function down(){

    }
}