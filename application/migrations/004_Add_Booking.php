<?php
class Migration_Add_Booking extends CI_Migration{
    public function up(){
        $is_exists = $this->load->table_model('module')->get(array('code' => 'scheduler'));
        if(! count($is_exists)){
             $this->db->insert('module',
                array('name' => 'Scheduler','code' => 'scheduler')
            );
            $module_id = $this->db->insert_id();
        }else{
            $module_id = $is_exists[0]->id;
        }


        $is_exists = $this->load->table_model('menu')->get(array('link' => 'staff/scheduler'));
        if(! count($is_exists)){
            $this->db->insert('menu',
                array('title' => 'Scheduler','link' => 'staff/scheduler', 'parent' => 0)
            );
            $menu_id = $this->db->insert_id();
        }else{
            $menu_id = $is_exists[0]->id;
        }

        $module_permission = $this->load->table_model('module_permission')->get(array('permission_code' => 'v','module_id' => $module_id));

        if(count($module_permission)){
            $this->db->update('menu',array('module_permission_id' => $module_permission[0]->id),array('id'=>$menu_id));
        }


    }

    public function down(){
        $this->load->table_model('module')->delete(array('code' => 'scheduler'));
    }
}