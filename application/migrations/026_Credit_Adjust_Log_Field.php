<?php
class Migration_Credit_Adjust_Log_Field extends CI_Migration{
    public function up(){
        $fields = array(
            'credit_id_transfer' => array(
                'type'      => 'int',
                'unsigned'  => true,
                'null'      => true
            )
        );
        $this->dbforge->add_column('credit_adjustment_log',$fields, 'credit_change');
    }

}