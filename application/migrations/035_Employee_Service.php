<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 27/6/2015
 * Time: 2:06 PM
 */

class Migration_Employee_Service extends CI_Migration{

    public function up(){
        $this->db->trans_start();
        $this->create_table();
        $this->insert_data();
        $this->db->trans_complete();
    }

    function create_table(){
        $this->db->query('DROP TABLE IF EXISTS `employee_service`;');
        $this->db->query('CREATE TABLE `employee_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `employee_service_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_service_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
    }

    function insert_data(){
        $employee_list = $this->load->table_model('employee')->get_minify(array(
            'condition'  => array('department_id' => $this->load->table_model('department')->get_department_serve_booking()),
            'user_level' => Permission_Value::ADMIN,
        ));
        $service_list = $this->load->table_model('item_service')->get();

        foreach($employee_list as $employee){
            foreach($service_list as $service){
                $this->db->insert('employee_service',array(
                    'employee_id' => $employee->id,
                    'item_id'  => $service->item_id,
                ));
            }
        }
    }

}