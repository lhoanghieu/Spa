<?php
class Migration_Booking_Reserve extends CI_Migration
{
    public function up(){
        $type_col = array(
            'type' => array(
                'type' => 'tinyint',
                'constraint' => 4,
                'null' => true
            )
        );
        $this->dbforge->add_column('booking',$type_col, 'status');

        $re_comment_col = array(
            'reserve_comment' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'default' => ''
            )
        );
        $this->dbforge->add_column('booking',$re_comment_col, 'comment');

    }
}