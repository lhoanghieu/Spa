<?php
class Migration_Start_Admin_System extends CI_Migration{
    function menu_add(){
        $fields = array(
            'type' => array(
                'type' => 'tinyint',
                'unsigned' => TRUE,
                'null' => true
            )
        );
        $this->dbforge->add_column('menu',$fields);
        $this->db->update('menu',array('type' => 1));

        $module_array = array(
            array(
                'name' => 'Branch',
                'code' => 'branch'
            ),
            array(
                'name' => 'Branch Group',
                'code' => 'branch_group'
            ),
        );

        $this->db->insert_batch('module',$module_array);
        foreach($module_array as $module){
            $id = $this->db->get_where('module',$module)->result();
            $id = $id[0]->id;
            $info = array(
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'v',
                ),
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'd',
                ),
                array(
                    'module_id'         => $id,
                    'global'            => 1,
                    'permission_code'   => 'e',
                ),
            );
            $this->db->insert_batch('module_permission',$info);
        }

        $branch_module_id       = $this->db->get_where('module',$module_array[0])->result()[0]->id;
        $branch_group_module_id = $this->db->get_where('module',$module_array[1])->result()[0]->id;
        $config_module_id       = $this->db->get_where('module',array('code' => 'config'))->result()[0]->id;
        $branch_permission_id = $this->db->get_where('module_permission',array('module_id' => $branch_module_id, 'permission_code' => 'v'))->result()[0]->id;
        $branch_group_permission_id = $this->db->get_where('module_permission',array('module_id' => $branch_group_module_id, 'permission_code' => 'v'))->result()[0]->id;
        $config_permission_id = $this->db->get_where('module_permission',array('module_id' => $config_module_id, 'permission_code' => 'v'))->result()[0]->id;
        $menu_array = array(
            array(
                'title'     => 'Config',
                'link'      => 'admin/config',
                'ordering'  => 1,
                'type' => MENU_TYPE('Admin'),
                'module_permission_id' => $config_permission_id
            ),
            array(
                'title' => 'Branch',
                'link'  => 'admin/branch',
                'ordering' => 2,
                'type'  => MENU_TYPE('Admin'),
                'module_permission_id' => $branch_permission_id
            ),
            array(
                'title'     => 'Branch Group',
                'link'      => 'admin/branch_group',
                'ordering'  => 3,
                'type' => MENU_TYPE('Admin'),
                'module_permission_id' => $branch_group_permission_id
            )
        );
        $this->db->insert_batch('menu',$menu_array);
    }

    function create_config_tree(){
        $this->db->query('DROP TABLE IF EXISTS `config_branch`');
        $this->db->query('CREATE TABLE `config_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `config_id` (`config_id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `config_branch_ibfk_1` FOREIGN KEY (`config_id`) REFERENCES `config` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `config_branch_ibfk_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
        $this->db->query('DROP TABLE IF EXISTS `config_branch_group`');
        $this->db->query('CREATE TABLE `config_branch_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) unsigned NOT NULL,
  `branch_group_id` int(10) unsigned NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `config_id` (`config_id`),
  KEY `branch_group_id` (`branch_group_id`),
  CONSTRAINT `config_branch_group_ibfk_1` FOREIGN KEY (`config_id`) REFERENCES `config` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `config_branch_group_ibfk_2` FOREIGN KEY (`branch_group_id`) REFERENCES `branch_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
    }

    function update_config(){
        $fields = array(
            'company_config' => array(
                'type' => 'tinyint',
                'unsigned' => TRUE,
                'null' => true
            )
        );
        $this->dbforge->add_column('config',$fields);
    }

    public function up(){
        $this->db->trans_start();
        $this->menu_add();
        $this->create_config_tree();
        $this->update_config();
        $this->db->trans_complete();
    }

    public function down(){
    }
}