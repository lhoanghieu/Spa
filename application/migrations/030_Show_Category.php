<?php
class Migration_Show_Category extends CI_Migration{
    public function up(){
        if(!$this->db->field_exists('is_show','category')) {
            $fields = array(
                'is_show' => array(
                    'type' => 'tinyint',
                    'constraint' => 100,
                    'default'    => 1,
                    'null' => true
                )
            );
            $this->dbforge->add_column('category',$fields, 'description');
        }
    }
}