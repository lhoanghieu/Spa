<?php
class Migration_Add_Employee_Calendar_Branch extends CI_Migration{
    public function up(){
        $is_exists = $this->load->table_model('module')->get(array('code' => 'rooms'));
        if(! count($is_exists)){
            $this->db->insert('module',
                array('name' => 'Rooms','code' => 'rooms')
            );
            $module_id = $this->db->insert_id();
        }else{
            $module_id = $is_exists[0]->id;
        }


        $is_exists = $this->load->table_model('menu')->get(array('link' => 'staff/rooms'));
        if(! count($is_exists)){
            $this->db->insert('menu',
                array('title' => 'Rooms','link' => 'staff/rooms', 'parent' => 0)
            );
            $menu_id = $this->db->insert_id();
        }else{
            $menu_id = $is_exists[0]->id;
        }

        $module_permission = $this->load->table_model('module_permission')->get(array('permission_code' => 'v','module_id' => $module_id));

        if(count($module_permission)){
            $this->db->update('menu',array('module_permission_id' => $module_permission[0]->id),array('id'=>$menu_id));
        }
        $fields = array(
            'branch_id' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'null' => true
            )
        );
        $this->dbforge->add_column('employee_calendar',$fields, 'end_time');
        $fields = array(
            'created_date' => array(
                'type' => 'datetime',
                'null' => true
            ),
            'updated_date' => array(
                'type' => 'int',
                'null' => true
            ),
        );
        $this->dbforge->add_column('room',$fields);

        $this->db->query('ALTER TABLE employee_calendar ADD CONSTRAINT `employee_calendar_ibfk_3` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE  ON UPDATE CASCADE');


    }

    public function down(){
        $this->load->table_model('module')->delete(array('code' => 'scheduler'));
        $this->dbforge->drop_column('employee_calendar','branch_id');
    }
}