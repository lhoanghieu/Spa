<?php
class Migration_Create_Table_Branch_Offline extends CI_Migration{
    public function up(){
        $this->db->query('DROP TABLE IF EXISTS `branch_offline`;');
        $this->db->query('CREATE TABLE `branch_offline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `creator` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `branch_offline_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;');
    }
}