<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 16/6/2015
 * Time: 1:02 PM
 */

class Migration_Edit_Menu_Attribute extends CI_Migration {

    public function up(){
        $this->db->trans_start();
        $inser_menu = array(
            'title'=>'Departments',
            'link' => 'admin/departments',
            'icon' => 0,
            'parent' => 0,
            'ordering' => 4,
            'module_id' => 8,
            'status' => 1,
            'module_permission_id' => 20,
            'type' => 2);
        $this->db->insert('menu', $inser_menu);
        // Groups only display on admin system
        $this->db->update('module',array('type' => 2), array('id' => 4));
        // Department only display on admin system
        $this->db->update('module',array('type' => 2), array('id' => 6));
        // Commission only display on admin system
        $this->db->update('module',array('type' => 2), array('id' => 10));
        // Room Type only display on admin system
        $this->db->update('module',array('type' => 2), array('id' => 19));
        $this->db->trans_complete();
    }

}