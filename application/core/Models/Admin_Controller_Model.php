<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller_model extends POS_Controller_Model {

    function __construct() {
        parent::__construct();
    }

    function insert_to_branch($id,$values,$type){
        if(!isset($this->config->item('permissionValue')[$this->main_table])){
            return;
        }
        $entity_branch_group_model = $this->load->table_model($this->main_table.'_branch_group');
        $entity_branch_model = $this->load->table_model($this->main_table.'_branch');
        $branch_group_ids   = isset($values->branch_group_id)?$values->branch_group_id:array();
        $branch_ids         = isset($values->branch_id)?$values->branch_id:array();
        $permissionValue = $this->config->item('permissionValue')[$this->main_table];

        if(isset($permissionValue[Permission_Value::BRANCH_GROUP])){
            if($type == 'update'){
                if($this->user_permission->check_level(Permission_Value::ADMIN)){
                    $entity_branch_group_model->delete(array($this->main_table.'_id' => $id));
                }
                else{
                    $entity_branch_group_model->delete(array($this->main_table.'_id' => $id,'branch_group_id' => $this->user_permission->get_branch_group()));
                }
            }
            $this->before_insert_to_branch_group($id,$branch_group_ids);
            foreach($branch_group_ids as $branch_group_id){
                $entity_branch_group_model->insert(array('branch_group_id' => $branch_group_id,$this->main_table.'_id' => $id));
            }
        }
        if(isset($permissionValue[Permission_Value::BRANCH])){
            if($type == 'update'){
                if($this->user_permission->check_level(Permission_Value::ADMIN)){
                    $entity_branch_model->delete(array($this->main_table.'_id' => $id));
                }
                else{
                    $entity_branch_model->delete(array($this->main_table.'_id' => $id,'branch_id' => $this->user_permission->get_branch()));
                }
            }
            $this->before_insert_to_branch($id,$branch_ids);
            foreach($branch_ids as $branch_id){
                $entity_branch_model->insert(array('branch_id' => $branch_id,$this->main_table.'_id' => $id));
            }
        }
    }

        /*---- Extend method ----*/
        function before_insert_to_branch_group($id,$branch_group_ids){

        }

        /*---- Extend method ----*/
        function before_insert_to_branch($id,$branch_id){

        }


    function update($id, $values){

        $this->db->trans_start();
        if(is_array($values))
            $values = (object)$values;
        $result = $this->update_extend($id,$values);
        $this->insert_to_branch($id,$values,'update');
        $this->db->trans_complete();
        return $result;
    }

    function update_extend($id,$values){
        $result = parent::update($id,$values);
        return $result;
    }

    function insert($values){
        $this->db->trans_start();
        if(is_array($values))
            $values = (object)$values;
        $id = $this->insert_extend($values);
        $this->insert_to_branch($id,$values,'insert');
        $this->db->trans_complete();
        return $id;
    }

    function insert_extend($values){
        $id = parent::insert($values,Permission_Value::ADMIN);
        return $id;
    }

    function getDataForViewForm($id){
        $data = new stdClass();
        if($id != 0){
            $data->item = $this->load->table_model($this->main_table)->get(array('id'=>$id));
            if(count($data->item))
                $data->item = $data->item[0];
        }else{
            $data->item = new stdClass();
            $field_list = $this->load->table_model($this->main_table)->getFieldList();
            foreach($field_list as $field){
                $data->item->{$field} = '';
            }
        }
        if(isset($data->item)){
            $item = $data->item;
            $permissionValue = $this->config->item('permissionValue');
            if(isset($permissionValue[$this->main_table]) && isset($permissionValue[$this->main_table][Permission_Value::BRANCH_GROUP])){
                $branch_group_id = $this->load->table_model($this->main_table.'_branch_group')->get(array($this->main_table.'_id' => $item->id));
                $item->branch_group_id = convert_to_array($branch_group_id,'','branch_group_id');
            }
            if(isset($permissionValue[$this->main_table]) && isset($permissionValue[$this->main_table][Permission_Value::BRANCH])){
                $branch_id = $this->load->table_model($this->main_table.'_branch')->get(array($this->main_table.'_id' => $item->id));
                $item->branch_id = convert_to_array($branch_id,'','branch_id');
            }
            $data->item = $item;
        }

        $data = $this->getDataForViewFormExtend($data);
        return $data;
    }


    function get_branch_data($id){
        $permissionValue = $this->config->item('permissionValue');

        $data = array();
        if(isset($permissionValue[$this->main_table]) && isset($permissionValue[$this->main_table][Permission_Value::BRANCH])){
            $data = $this->load->table_model($this->main_table.'_branch')->get(array($this->main_table.'_id' => $id));
            $data = convert_to_array($data,'','branch_id');
            $data = $this->load->table_model('branch')->getTableMap('id','',array('id' => $data));
        }else if(isset($permissionValue[$this->main_table]) && isset($permissionValue[$this->main_table][Permission_Value::BRANCH_GROUP])){
            $data = $this->load->table_model($this->main_table.'_branch_group')->get(array($this->main_table.'_id' => $id));
            $data = convert_to_array($data,'','branch_group_id');
            $data = $this->load->table_model('branch')->getTableMap('id','',array('branch_group_id' => $data));
        }
        return array('branches' => $data);
    }

}

