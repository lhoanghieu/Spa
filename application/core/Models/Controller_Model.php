<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class POS_Controller_Model extends POS_Model {

    protected $main_table;
    protected $suggestionSearch;
    protected $suggestionDisplay;
    protected $suggestionKey;
    protected $entity_status;
    protected $default_condition;
    function __construct() {
        parent::__construct();
        /* the fields that will be used to search in the table */
        $this->suggestionSearch = array();
        /* the fields from table which will be show when a record match the suggest */
        $this->suggestionDisplay = array();
        /* the default condition when getting dataset or suggestion*/
        $this->default_condition = array();
        /* the fields which will be showed if a record have no permission */
        $this->noPermissionDisplay = null;
        /* the field which will be the main key of a returned suggestion*/
        $this->suggestionKey = "id";
        /* the fields which will be showed if a suggestion search is minimal*/
        $this->minimalSuggestionDisplay = array('name');
        /* the available status that the controller will only look for the records have 1 in these status*/
        $this->entity_status = array(Status::Active);
        /* which field will be prior first, used in order by sql*/
        $this->suggestionOrder = "";
        /* an array of methods that will be called to retrieve a string when a record have no permission*/
        $this->extendNoPermissionDisplay = array('no_permission_display');
        /* custom display*/
        $this->custom_display = array();
    }

    /** return a data list for datatable and pagination info
     * @param int $page
     * @param array $condition
     * @return stdClass
     */
    function getDataSet($page = 1, $condition = array()){
        $condition = array_merge($condition,$this->default_condition);
        $modelName = $this->load->table_model($this->main_table);
        $data = new stdClass();
        // get limit row number and start row index
        $limit = $this->config->item('item_per_page');
        $offset= ($page-1) * $limit;
        if(!isset($condition['status']) && $this->entity_status){
            $condition['status'] = $this->entity_status;
        }
        if(isset($condition['id'])){
            $entity = $modelName->get($condition);
            if(count($entity)) $entity = $entity[0];
            $data->dataset = array();
            if($entity){
                $data->dataset[] = $entity;
            }
        }else{
            $data->dataset = $modelName->get($condition,"created_date desc", $limit,$offset);
        }
        $data->dataset = $this->renderDataSet($data->dataset);
        $data->total_rows = $modelName->getTotal(array_merge($condition));
        //generate Paging information
        $data->page = new stdClass();
        $data->page->current_page = $page;
        $data->page->total_page   = ceil($data->total_rows/$limit);
        $data->page->item_per_page = $limit;
        $data->dataset_option = $this->getDataSetOption();
        return $data;
    }

    /** return more option for the dataset
     * @return array
     */
    function getDataSetOption(){
        return array();
    }

    /**
     * render the dataset before return it
     * @param $data
     * @return mixed
     */
    function renderDataSet($data){
        return $data;
    }

    /** get the minimal suggestion data
     * @param $keyword
     * @param array $condition
     * @param bool $is_full
     * @param bool $extendPermissionDisplay
     * @param bool $is_all
     * @return array
     */
    function getMinimalSuggestion($keyword, $condition = array(), $is_full = false, $extendPermissionDisplay = true, $is_all = true){
        $temp = $this->suggestionDisplay;
        $this->suggestionDisplay = $this->minimalSuggestionDisplay;
        $ret = $this->getSuggestion($keyword,$condition, $is_full,$extendPermissionDisplay,$is_all);
        $this->suggestionDisplay = $temp;
        return $ret;
    }

    /** get the suggestion data base on the keyword and condition
     * @param $keyword
     * @param array $condition
     * @param bool $is_full
     * @param bool $extendPermissionDisplay
     * @param bool $is_all
     * @return array
     */
    function getSuggestion($keyword, $condition = array(), $is_full = false, $extendPermissionDisplay = true, $is_all = true)
    {
        $condition = array_merge($condition,$this->default_condition);
        $current_permission = $this->load->table_model($this->main_table)->get_is_all();
        $this->load->table_model($this->main_table)->set_is_all($is_all);
        $suggestion_array = array();
        $limit = $this->config->item('item_per_page');
        if(count($this->suggestionSearch) > 0 && count($this->suggestionDisplay) > 0){
            $fields = "";
            if($keyword != ""){
                foreach($this->suggestionSearch as $field){
                    if($fields != ""){
                        $fields .= " OR ";
                    }
                    if(strpos($field, '.') === false){
                        $field = $this->main_table . "." . $field;
                    }
                    $fields .= $field . " LIKE '%{$keyword}%'";
                }
            }
            if(strlen($fields)){
                $condition[] = '('.$fields.')';
            }

            if($this->entity_status){
                $condition['status'] = $this->entity_status;
            }
            if($is_full){
                $limit = "";
            }
            $suggestions = $this->load->table_model($this->main_table)->get($condition,$this->suggestionOrder,$limit);
            $total = $this->load->table_model($this->main_table)->getTotal($condition);
            $num_row = count($suggestions);
            if($num_row > 0){
                foreach($suggestions as $suggestion){
                    $string = "";
                    if((isset($suggestion->{$this->main_table.'_permission'})&&$suggestion->{$this->main_table.'_permission'}) || $this->noPermissionDisplay == null){
                        foreach($this->suggestionDisplay as $displayField){
                            if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                                if($string != ""){
                                    $string .= " ";
                                }
                                $string .= $suggestion->{$displayField};
                            }
                        }
                        foreach($this->custom_display as $key => $displayField){
                            if(isset($suggestion->{$key}) && $suggestion->{$key} != "" && $suggestion->{$key} != null){
                                if($string != ""){
                                    $string .= ' - ';
                                }
                                $check_array = $this->custom_display[$key];
                                $string .= $check_array[$suggestion->{$key}];
                            }
                        }
                    }else{
                        foreach($this->noPermissionDisplay as $displayField){
                            if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                                if($string != ""){
                                    $string .= " ";
                                }
                                $string .= $suggestion->{$displayField};
                            }
                        }
                        foreach($this->custom_display as $key => $displayField){
                            if(isset($suggestion->{$key}) && $suggestion->{$key} != "" && $suggestion->{$key} != null){
                                if($string != ""){
                                    $string .= ' - ';
                                }
                                $check_array = $this->custom_display[$key];
                                $string .= $check_array[$suggestion->{$key}];
                            }
                        }
                        foreach($this->extendNoPermissionDisplay as $row){
                            if($extendPermissionDisplay){
                                if(method_exists($this,$row)){
                                    $string .= /*$string* .*/ ' - ' . $this->$row($suggestion->id);
                                }
                            }
                        }
                    }
                    $row = array('label' => $string, 'value' => $suggestion->{$this->suggestionKey});
                    $suggestion_array[] = $row;
                }
                if($total > $num_row){
                    $more = $total-$num_row;
                    $row = array('label' => "And $more other row(s)...", 'value' => "disabled");
                    $suggestion_array[] = $row;
                }
            }else{
                $row = array('label' => "No result", 'value' => "disabled");
                $suggestion_array[] = $row;
            }
        }
        $suggestion_array = array_merge($suggestion_array,$this->getSuggestionExtend());
        $this->load->table_model($this->main_table)->set_is_all($current_permission);
        return $suggestion_array;
    }

    /** return more suggestion base on another logic ?
     * @return array
     */
    function getSuggestionExtend(){
        return array();
    }

    /**
     * a function called to delete some records base the related table model
     * @param $ids : the records's ids which will be deleted
     * @return mixed
     */
    function delete($ids){
        $modelName = $this->load->table_model($this->main_table);
        $this->db->trans_start();
        $result = $modelName->delete(array('id' => $ids));
        $this->db->trans_complete();
        return $result;
    }

    /** update a record
     * @param $id
     * @param $values
     * @param bool $constraint_check : define if the data need to be valdiate or not
     * @return mixed
     */
    function update($id, $values,$constraint_check = true){
        if(is_array($values))
            $values = (object)$values;

        $model = $this->load->table_model($this->main_table);

        $log_data = array();
        if(isset($data['log_data'])){
            $log_data = $values->log_data;
        }
        return $model->update($id,$values,$constraint_check,$log_data);
    }

    /** insert a record
     * @param $values
     * @param string $user_level
     * @param array $permission
     * @param bool $force_insert: force insert if there is already have id or not
     * @return mixed
     */
    function insert($values,$user_level = '', $permission = array(),$force_insert = false){
        if(is_array($values))
            $values = (object)$values;
        $log_data = array();
        if(isset($data['log_data'])){
            $log_data = $values->log_data;
        }
        $model = $this->load->table_model($this->main_table);
        return $model->insert($values,$user_level,$permission,$force_insert,$log_data);
    }

    /** used to get data for an insert form
     * @param $id
     * @return stdClass
     */
    function getDataForViewForm($id){
        $data = new stdClass();
        if($id != 0){
            $data->item = $this->load->table_model($this->main_table)->get(array('id'=>$id));
            if(count($data->item))
                $data->item = $data->item[0];
        }else{
            $data->item = new stdClass();
            $field_list = $this->load->table_model($this->main_table)->getFieldList();
            foreach($field_list as $field){
                $data->item->{$field} = '';
            }
        }
        $data = $this->getDataForViewFormExtend($data);
        return $data;
    }

    /** used to get more data for the insert form before it's being returned
     * @param $data
     * @return mixed
     */
    function getDataForViewFormExtend($data){
        return $data;
    }


}

