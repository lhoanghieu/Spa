<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class POS_Loader extends CI_Loader
{
    private $viewPath;
    private $layoutPath;
    private $contentPath;
    private $emailPath;
    private $modelPath;
    private $controllerModelPath;
    private $reportModelPath;
    private $_CI;

    public function __construct()
    {
        parent::__construct();
        /* View path of the system, the folder that will contains all subview */
        $this->viewPath = 'views/';
        /* Layout path of the system, the folder will contains all layout (subfolder of the view path) */
        $this->layoutPath = 'layouts/';
        /* Content path of the system, the folder will contains all content view (subfolder of view path) */
        $this->contentPath = "contents/";
        /* Email template path of the system, the folder will contains all email template (subfolder of the view) */
        $this->emailPath = "emails/";
        /* Model path of the system, the folder will contains all model of the system */
        $this->modelPath = "models/";
        /* ControllerModel path of the system, the folder will contains all ControllerModel of the system (subfolder of the model) */
        $this->controllerModelPath = "ControllerModels/";
        /* ReportModel path of the system, the folder will contains all ReportModel of the system (subfolder of the model) */
        $this->reportModelPath = "Reports/";
        $this->_CI = &get_instance();
    }

    /**
     * This function will return the content view base on the name and autoload any related css and js
     * @param $name
     * @param array $vars
     * @param bool $return
     * @return string
     */
    public function content($name, $vars = array(), $return = TRUE)
    {
        $name = $this->check_content_exists($name);
        /* check the content is exists or not */
        if ($name !== false) {
            $fileNavigation = explode('/', $name);
            $loadAssets = "";
            /* query each folder level to load the proper css and js files */
            foreach ($fileNavigation as $dir) {
                /* check if this is the end of the path or not */
                if ($loadAssets != "") {
                    $loadAssets .= "/";
                }
                $loadAssets .= $dir;
                /* load the proper css if exists */
                $this->_CI->assets->css($this->contentPath . $loadAssets);
                /* load the js if exists */
                $this->_CI->assets->js($this->contentPath . $loadAssets);
            }
            return $this->view($this->contentPath . $name, $vars, $return);
        }
        return "";
    }

    /**
     * this function is used to check a content view exists in content folder path or not
     * @param $name
     * @return bool|string
     */
    public function check_content_exists($name)
    {
        $path = APPPATH . $this->viewPath . $this->contentPath . $name . '.' . VIEW_EXT;
        if (@file_exists($path)) {
            /* this will check if the file name exists in contentPath folder '$viewPath/$contentPath/$name.php' */
            return $name;
        } else {
            /* this will check if the file name exists in a folder name which is the same name of the file name in contentPath folder*/
            $fileNavigation = explode('/', $name);
            $last = array_pop($fileNavigation);
            $name = $name . "/" . $last;
            $path = APPPATH . $this->viewPath . $this->contentPath . $name . '.' . VIEW_EXT;
            if (@file_exists($path)) {
                return $name;
            }
        }
        return false;
    }

    /**
     * this function is used to load the component and its css and js files
     * @param $name
     * @param array $vars
     * @param bool $return
     * @return mixed
     */
    public function component($name, $vars = array(), $return = TRUE)
    {
        $this->_CI->assets->css($name);
        $this->_CI->assets->js($name);
        return $this->view($name, $vars, $return);
    }

    /**
     * this function is used to load an entire page (with layout and its components + content viwe)
     * @param $contentName
     * @param array $vars
     * @param string $layout
     * @param bool $return
     * @return mixed
     */
    public function page($contentName, $vars = array(), $layout = 'default', $return = FALSE)
    {
        /* get the lay out data */
        $layout_data = $vars->layout_data;
        /* get the content html */
        $layout_data['content'] = $this->content($contentName, $vars, TRUE);
        /* load the layout view */
        $layout = $this->_layout($layout, $layout_data, TRUE);
        /* load the page view (views/page.php) */
        return $this->view('page', array('layout' => $layout), $return);
    }

    /**
     * this function is used to load an content page without any layout
     * @param string $contentName
     * @param array $vars
     */
    public function content_page($contentName = '', $vars = array())
    {
        $layout_data = array();
        /* get the content html */
        $layout_data['content'] = $this->content($contentName, $vars);
        /* load the layout view */
        $layout = $this->_layout('raw', $layout_data, TRUE);
        /* load the page with the raw layout */
        $this->view('page', array('layout' => $layout), FALSE);
    }

    /**
     * this function is used to load a layout and return its html
     * @param $name
     * @param array $vars
     * @param bool $return
     * @return mixed
     */
    private function _layout($name, $vars = array(), $return = TRUE)
    {
        $this->_CI->assets->css($this->layoutPath . $name,true);
        $this->_CI->assets->js($this->layoutPath . $name,true);
        return $this->view($this->layoutPath . $name, $vars, $return);
    }

    /**
     * this function is used to load a controller model
     * @param $name
     * @param null $current_system
     * @return null
     */
    public function controller_model($name,$current_system = null)
    {
        $temp = $name;
        /* check if it dont have 'controller' in the name, if not, it will add 'controller' to the name*/
        if (strpos($name, '_controller') === false) {
            $name .= '_controller';
        }
        /* check if it dont have 'model' in the name, if not, it will add 'model' to the name */
        if (strpos($name, '_model') === false) {
            $name .= '_model';
        }
        /* check if any system is pass, if not, get the current system or system route */
        if(!$current_system){
            /* get current system */
            $current_system = $this->_CI->config->item('current_system');
            /* if system_route is defined in for this request, assign the system route the current_system*/
            if(isset($this->_CI->config->item('system_controller_model_route')[$current_system])
                && isset($this->_CI->config->item('system_controller_model_route')[$current_system]['model'][$temp])
                && $this->_CI->config->item('system_controller_model_route')[$current_system]['model'][$temp]){
                $current_system = $this->_CI->config->item('system_controller_model_route')[$current_system]['target']. '/';
            }
            /* else get the current system path */
            else{
                $current_system = $this->_CI->config->item('current_system') . '/';
            }
        }else{
            $current_system = $current_system . '/';
        }

        $path = APPPATH . $this->modelPath . $this->controllerModelPath . $current_system . $name . '.php';
        /* check if the file exists in system folder */
        if (@file_exists($path)) {
            $this->model($this->controllerModelPath . $current_system . $name, $name);
            return $this->_CI->$name;
        }
        /* check if controllerModel with the file name {system}_{filename} is existed or not */
        else{
            $temp_system = substr($current_system,0,strlen($current_system)-1);
            $path = APPPATH . $this->modelPath . $this->controllerModelPath . $current_system . $temp_system . '_' . $name . '.php';
            if (@file_exists($path)) {
                $this->model($this->controllerModelPath . $current_system . $temp_system . '_' . $name, $temp_system . '_' . $name);
                return $this->_CI->{$temp_system . '_' . $name};
            }
        }
        /* check if the file exists in controllerModel foler (not in any system folder) */
        if($current_system){
            $path = APPPATH . $this->modelPath . $this->controllerModelPath . $name . '.php';
            if (@file_exists($path)) {
                $this->model($this->controllerModelPath . $name, $name);
                return $this->_CI->$name;
            }
        }

        return null;
    }

    /**
     * this function is used to load a table model
     * @param $name
     * @return mixed
     */
    public function table_model($name)
    {
        /* check if it dont have 'model' in the name, if not, it will add 'model' to the name */
        if (strpos($name, '_model') === false) {
            $name .= '_model';
        }
        /* check if the model is already loaded or not */
        if (!isset($this->_CI->$name)) {
            /* load the model */
            $this->model('TableModels/' . $name, $name);
        }
        /* return the model */
        return $this->_CI->$name;
    }

    /**
     * this function is used to load a report model
     * @param $name
     * @return null
     */
    public function report_model($name){
        if (strpos($name, '_report') === false) {
            $name .= '_report';
        }
        if (strpos($name, '_model') === false) {
            $name .= '_model';
        }
        $path = APPPATH . $this->modelPath . $this->reportModelPath . $name . '.php';
        if (@file_exists($path)) {
            $this->model($this->reportModelPath . $name, $name);
            return $this->_CI->$name;
        }
        return null;
    }

    /**
     * this function is used to load a email template view
     * @param $name
     * @param array $vars
     * @return mixed
     */
    public function email_content($name, $vars = array())
    {
        return $this->view($this->emailPath . $name, $vars, true);
    }

    /**
     * this function is used to load a third party library
     * @param $name
     */
    public function third_party($name){
        require_once APPPATH."third_party/{$name}";
    }
}
