<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 'On');
error_reporting(E_ALL);
class Staff_Controller extends POS_Controller {

    function __construct() {
        parent::__construct();
        $this->set_system("staff");
        $this->check_login();
        $this->defaultLayout = "default";
        $this->breadcrumbs[] = array('url' => '', 'text' => 'Dashboard');
    }

    /** index page
     * @throws Exception
     */
    function index(){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $data = new stdClass();
        $data->pageCode = $this->pageCode;
        $this->page($this->moduleName, $data);
    }

    /** get data list of the related entity to the controller
     * @param int $page
     * @return array
     */
    function getList($page=1)
    {
        if ($this->user_permission->checkPermission('v', $this->pageCode) == false) {
            return $this->ajax_content(REQUEST_FAIL, "You don't have this permission");
        }
        $condition = array();
        $login_data = $this->session->userdata('login');
        if ($this->moduleName == "employees")
            $condition['employee_branch_group.branch_group_id'] = $login_data->branch_group_id;
        if ($this->moduleName == "customers")
            $condition['customer_branch_group.branch_group_id'] = $login_data->branch_group_id;

        if ($this->moduleName == "warehouse_management"){
            $data = $this->load->controller_model($this->moduleName)->getDataSet($page);
        }else {
            $data = $this->load->controller_model($this->moduleName)->getDataSet($page, $condition);
        }
        //$data = $this->load->controller_model($this->moduleName)->getDataSet($page,$condition);
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    /** get the record of the related entity to the controller base on it's id
     * @param int $id
     * @return array
     */
    function get($id=0){
        $data = array();
        if($id==0){
            return $this->getList($page=1);
        }else{
            $modelName = $this->load->controller_model($this->moduleName);
            $data = $modelName->getDataSet(1, array('id' => $id));
        }
        return $this->ajax_content(REQUEST_SUCCESS, "", $data);
    }

    /**
     * used to retrieve a list of data base the suggestion keyword
     */
    function suggest(){
        /**
         * keyword : keyword to search in
         * isMinimal : display in minimal or full fields
         * isFull : display all at once or just some rows
         * isAll : Full permission display or limited permission
         */
        $keyword    = $this->input->get_post('term');
        $isMinimal  = to_b($this->input->get_post('minimal'));
        $isFull     = to_b($this->input->get_post('full'));
        $isAll      = to_b($this->input->get_post('all'));
        $isExtend   = to_b($this->input->get_post('extend'));
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword), array(), $isFull,$isExtend, $isAll);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword), array(), $isFull,$isExtend,$isAll);
        }
        echo json_encode($suggest);
    }

    /** a method handle delete request from browser
     * @return array
     * @throws Exception
     */
    function delete()
    {
        if ($this->user_permission->checkPermission('d', $this->pageCode) == false) {
            throw new Exception('You dont have permission for this action');
        }
        $ids = $this->input->post('ids');
        $ids = json_decode($ids);

        $modelName = $this->load->controller_model($this->moduleName);

        if ($this->moduleName == 'items') {
            foreach ($ids as $id) {
                $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);

                $this->load->controller_model('audit_trails')->log(array(), array(),
                    array('action' => 'Delete',
                        'object' => $oldRow->code,
                        'objectDetail' => $oldRow->name,
                        'module' => 'staff/' . $this->moduleName));
            }
        }

        $result = $modelName->delete($ids);
        if(is_numeric($result) || $result === true){
            return $this->ajax_content(REQUEST_SUCCESS,'The record has been deleted', array());
        }else{
            return $this->ajax_content(REQUEST_FAIL,$result, array());
        }
    }

    /** a method handle update or insert request from browser
     * @return array
     * @throws Exception
     */
    function edit(){
        $id = $this->input->post('id');
        $fields = $this->input->post('field_post');

        $modelName = $this->load->controller_model($this->moduleName);
        if($this->moduleName == 'items') {
            // GET OLD DATA FOR LOGGING
            $oldRow = $modelName->getItem($id);
            // END GET OLD DATA FOR LOGGING
        }
        if($id == 0) {
            //Insert to Database
            if ($this->user_permission->checkPermission('i', $this->pageCode) == false) {
                throw new Exception('You dont have permission for this action');
            }
            $result = $modelName->insert($fields);


            if ($this->moduleName == 'items') {
                $this->load->controller_model('audit_trails')->log($oldRow, $fields,
                    array('action' => 'Insert',
                        'object' => $fields['code'],
                        'objectDetail' => 'Item code: ' . $fields['code'],
                        'module' => 'staff/' . $this->moduleName));
            }
        }else{
            if($this->user_permission->checkPermission('e',$this->pageCode) == false){
                throw new Exception('You dont have permission for this action');
            }
            //Edit a record
            $result = $modelName->update($id, $fields);


            if ($this->moduleName == 'items') {
                $this->load->controller_model('audit_trails')->log($oldRow, $fields,
                    array('action' => 'Update',
                        'object' => $fields['code'],
                        'objectDetail' => 'Item code: ' . $fields['code'],
                        'module' => 'staff/' . $this->moduleName));
            }
        }
        $data = new stdClass();
        $data->option = array('close_modal' => 1);
        if(is_numeric($result) || $result === true){
            return $this->ajax_content(REQUEST_SUCCESS,'The record has been saved', $data);
        }else{
            return $this->ajax_content(REQUEST_FAIL, $result, $data);
        }
    }

    /** a method handle getting update or insert form
     * @param int $id
     */

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForViewForm($id);
        $data = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }
}

