<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends POS_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->set_system("admin");
        $this->defaultLayout = "default";
        $this->breadcrumbs[] = array('url' => 'admin', 'text' => 'Dashboard');
        $this->assets->js('admin_system');
    }

    function index(){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $data = new stdClass();
        $data->pageCode = $this->pageCode;
        $this->page('admin/'.$this->moduleName, $data);
    }

    function getList($page=1){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $data = $this->load->controller_model($this->moduleName)->getDataSet($page);
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    function get($id=0){
        if($id==0){
            $this->getList($page=1);
        }else{
            $modelName = $this->load->controller_model($this->moduleName);

            $data = $modelName->getDataSet(1, array('id' => $id));
        }
        return $this->ajax_content(REQUEST_SUCCESS, "", $data);
    }

    function suggest(){
        $keyword    = $this->input->get_post('term');
        $isMinimal  = to_b($this->input->get_post('minimal'));
        $isFull     = to_b($this->input->get_post('full'));
        $isAll      = to_b($this->input->get_post('all'));
        $isExtend   = to_b($this->input->get_post('extend'));
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword), array(), $isFull,$isExtend, $isAll);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword), array(), $isFull,$isExtend,$isAll);
        }
        echo json_encode($suggest);
    }

    function delete(){
        if($this->user_permission->checkPermission('d',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $ids = $this->input->post('ids');
        $ids = json_decode($ids);

        $modelName = $this->load->controller_model($this->moduleName);

        if ($this->moduleName == 'items') {
            foreach ($ids as $id) {
                $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);
                $this->load->controller_model('audit_trails')->log(array(), array(),
                    array('action' => 'Delete',
                        'object' => $oldRow->code,
                        'objectDetail' => $oldRow->name,
                        'module' => 'admin/' . $this->moduleName));
            }
        }

        $result = $modelName->delete($ids);

        if(is_numeric($result) || $result === true){
            return $this->ajax_content(REQUEST_SUCCESS,'The record has been deleted', array());
        }else{
            return $this->ajax_content(REQUEST_FAIL,$result, array());
        }
    }

    function edit(){
        try{
            $id = $this->input->post('id');
            $fields = $this->input->post('field_post');
            $modelName = $this->load->controller_model($this->moduleName);

            $oldRow = array();
            if($this->moduleName == 'items') {
                // GET OLD DATA FOR LOGGING
                $oldRow = $modelName->getItem($id);
                // END GET OLD DATA FOR LOGGING
            }
            if($id == 0){
                //Insert to Database
                if($this->user_permission->checkPermission('i',$this->pageCode) == false){
                    return;
                }
                $result = $modelName->insert($fields);
                if($this->moduleName == 'items') {
                    $this->load->controller_model('audit_trails')->log($oldRow, $fields,
                        array('action' => 'Insert',
                            'object' => $fields['code'],
                            'objectDetail' => 'Item code: ' . $fields['code'],
                            'module' => 'admin/' . $this->moduleName));
                }
            }else{
                if($this->user_permission->checkPermission('e',$this->pageCode) == false){
                    return;
                }
                //Edit a record
                $result = $modelName->update($id,$fields);
                if($this->moduleName == 'items') {
                    $this->load->controller_model('audit_trails')->log($oldRow, $fields,
                        array('action' => 'Update',
                            'object' => $fields['code'],
                            'objectDetail' => 'Item code: ' . $fields['code'],
                            'module' => 'admin/' . $this->moduleName));
                }
            }
            $data = new stdClass();
            $data->option = array('close_modal' => 1);
            if(is_numeric($result) || $result === true){
                return $this->ajax_content(REQUEST_SUCCESS,'The record has been saved', $data);
            }else{
                return $this->ajax_content(REQUEST_FAIL, $result, $data);
            }
        }catch(Exception $e){
            return $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }

    }

    function getForm($id = 0){
        $modelName      = $this->load->controller_model($this->moduleName);
        $data           = (array)$modelName->getDataForViewForm($id);
        $content   = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        if(isset($data['ret_option'])){
            $option = array_merge_recursive($option,$data['ret_option']);
        }
        return $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$content, 'type' => 'dialog', 'option' => $option));
    }

    function get_choose_branch_form($id,$list_name){
        $data = $this->load->controller_model($this->moduleName)->get_branch_data($id);
        $data['url'] = urlencode($this->moduleName."?tables[{$list_name}][entity_id]=$id");
        $data = $this->content('choose_branch_form',$data, true);
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data));
    }
}

