<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class POS_Lang extends CI_Lang {

    private $CI;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Set the user's chosen language.
     */
    function setLanguage($language) {
        $this->CI = & get_instance();
        $this->CI->session->set_userdata("language", $language);
    }

    /**
     * Get the user's chosen language.
     */
    function getLanguage() {
        $language = "";

        $this->CI =& get_instance();
        //If a language hasn't been set, then return the default language
        //from the config.
        $language = $this->CI->session->userdata("language");
        if (strlen($language) == 0)
            $language = $this->CI->config->item("language");

        return $language;
    }

}
