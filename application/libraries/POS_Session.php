<?php

class POS_Session extends CI_Session
{
    protected $_CI;

    public function __construct($params = array())
    {
        $this->_CI = &get_instance();
        parent::__construct($params);
    }

    function userdata($item, $global = 0)
    {
        if($global == 1)
            return ( ! isset($this->userdata[$item])) ? FALSE : $this->userdata[$item];
        else{
            $system = $this->_CI->config->item('current_system');
            if(! isset($this->userdata[$system]) || ! isset($this->userdata[$system][$item]) ){
                return false;
            }else{
                return $this->userdata[$system][$item];
            }
        }
    }

    function set_userdata($newdata = array(), $newval = '', $system = null)
    {
        if (is_string($newdata))
        {
            $newdata = array($newdata => $newval);
        }

        if (count($newdata) > 0)
        {
            if(! $system){
                $system = $this->_CI->config->item('current_system');
            }
            if(!$system || $system == 'global'){
                foreach ($newdata as $key => $val)
                {
                    $this->userdata[$key] = $val;
                }
            }else{
                $system_session = $this->userdata($system,1);
                if($system_session == false){
                    $system_session = array();
                }

                foreach ($newdata as $key => $val)
                {
                    $system_session[$key] = $val;
                }
                $this->userdata[$system] = $system_session;
            }
        }

        $this->sess_write();
    }

    // --------------------------------------------------------------------
    function unset_userdata($newdata = array())
    {
        if (is_string($newdata))
        {
            $newdata = array($newdata => '');
        }

        if (count($newdata) > 0)
        {
            $system = $this->_CI->config->item('current_system');

            foreach ($newdata as $key => $val)
            {
                unset($this->userdata[$system][$key]);
            }
        }

        $this->sess_write();
    }

    public function sess_update()
    {
        $CI = get_instance();

        if ( ! $CI->input->is_ajax_request())
        {
            parent::sess_update();
        }
    }


}