<?php
class Export_Original_Data{
    /**
     * this function is used to transform special report format into html
     * @param $content
     * @return string
     */
    static function parse_html($content){
        $df = array(
            'html_text' => '',
            'html_class' => '',
            'export_text' => '',
            'colspan' => '1',
            'rowspan' => '1',
            'export_type' => '',
            'label_text' => '',
            'label_class' => '',
            'style' => ''

        );
        $str = '';
        $str .= '<table class = "table table-bordered table-striped table-hover data-table tablesorter" id="sortable_table">';
        $str .= '<thead>';
        if(isset($content)){
            foreach ($content as $key => $value){
                if($key==0)
                {
                    $str .= '<tr>';
                    foreach ($value as $th => $value1) {
                        $value1 = array_merge($df, $value1);
                        $str .= '<th class="' . $value1['html_class'] . '" style="'.$value1['style'].'" rowspan="' . $value1['rowspan'] . '" colspan="' . $value1['colspan'] . '">' . $value1['export_text'] . '</th>';
                    }
                    $str .= '</tr>';
                    break;
                }
            }
            $str .= '</thead>';
            $str .= '<tbody>';

            foreach ($content as $key => $value)
            {
                if($key!=0) {

                    $str .= '<tr>';
                    foreach ($value as $th => $value1) {

                        $value1 = array_merge($df, $value1);
                        if ($value1['export_type'] == 'currency') {
                            $value1['export_text'] = to_currency($value1['export_text']);
                        }
                        if ($value1['label_text'] != '') {
                            $str .= '<td class="'.$value1['html_class'].'" style="'.$value1['style'].'" rowspan="'.$value1['rowspan'].'" colspan="'.$value1['colspan'].'"><span class='.$value1['label_class'].'>'.$value1['label_text'].'</span><br/>' . $value1['export_text'] . '</td>';
                        }
                        else {
                            $str .= '<td class="' . $value1['html_class'] . '" style="'.$value1['style'].'" rowspan="' . $value1['rowspan'] . '" colspan="' . $value1['colspan'] . '">' . $value1['export_text'] . '</td>';
                        }

                    }

                    $str .= '</tr>';
                }
            }
        }

        $str .= '</tbody>';
        $str .= '</table>';
        return $str;

    }

    /**
     * this function is used to transform special report format into report excel data
     * @param $content
     * @return array
     */
    static function parse_export($content){
        $df = array(
            'html_text' => '',
            'html_class' => '',
            'export_text' => '',
            'colspan' => '1',
            'rowspan' => '1',
            'export_type' => '',
            'label_text' => '',
            'label_class' => '',
            'style' => '',
            'export_style' => null,

        );
        $number_col = 0;
        $i=0;
        $array_th = array();
        $export = array();


        foreach ($content as $key => $value){
            $export_row = array();
            if($number_col>0){
                $number_col--;
                if($number_col==0){
                    for($j=0; $j<$i; $j++){
                        $array_th[$j] = -1;
                    }

                    $location=0;
                }
            }

            $flag_row=0;
            $flag = 0;
            foreach ($value as $th => $value1) {
                $value1 = array_merge($df, $value1);
                if ($value1['export_type'] == 'currency') {
                    $value1['export_text'] = to_currency($value1['export_text']);
                }
                if($value1['rowspan'] != 1 && $value1['rowspan'] != '' )
                {
                    $number_col = $value1['rowspan'];
                    $array_th[$i] = $th;
                    $i++;
                    $export_row[] =  array('value' => $value1['export_text'], 'style' => $value1['export_style']);
                    $flag_row = 1;
                }
                else {
                    if($number_col>0 && $flag_row==0){
                        if ($flag == 0){
                            for($j = 0; $j < $i; $j++)
                            {
                                if($array_th[$j]!=-1) {
                                    $export_row[$array_th[$j]] = '';
                                    $flag = 1;
                                    $location = $array_th[$j];
                                }
                            }
                        }
                        $location = $location +1;
                        $export_row[] =  array('value' => $value1['export_text'], 'style' => $value1['export_style']);
                    }
                    else {
                        if($value1['colspan'] != 1 && $value1['colspan'] != '' )
                        {
                            $export_row[] =  array('value' => $value1['export_text'], 'style' => $value1['export_style']);
                            $col = $value1['colspan']-1;
                            for($j=0; $j<$col; $j++){
                                $export_row[] = '';
                            }
                        }
                        else {
                            $export_row[] =  array('value' => $value1['export_text'], 'style' => $value1['export_style']);
                        }
                    }
                }

            }

            $export[] = $export_row;
        }

        return $export;
    }

}
