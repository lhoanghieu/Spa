<?php
class User_check
{
    var $CI;
    function __construct(){
        $this->CI =& get_instance();
    }

    /** check the user is loggging or not
     * @param bool $redirect
     * @return bool
     */
    function checkLoggedIn($redirect = true){
        $User = $this->CI->session->userdata('login');
        if(isset($User) && $User){
            return $User;
        }
        else{
            if($redirect)
                redirect(base_url());
            return false;
        }
    }

    /** get a field in the loggin session
     * @param $key
     * @return null
     */
    function get($key){
        $user = $this->CI->session->userdata('login');
        if($key){
            if(is_object($user))
                return isset($user->$key)?$user->$key:null;
            else
                return ($user[$key])?$user[$key]:null;
        }
        return $user;
    }

    /**
     * get branch id (this function is deprecated)
     * @param string $branch_id
     * @return string
     */
    function get_branch_id($branch_id = ''){
        if(!$branch_id){
            //$branch_id = $this->get('branch_id');
            $branch_id = $this->CI->load->table_model('employee')->get_branch($this->get('user_id'));
        }
        return $branch_id;
    }



}


