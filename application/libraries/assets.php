<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assets{

    private $_CI;
    public $assetsFolderName;
    private $cssPath;
    public $cssUrl;
    private $cssFiles;
    private $cssGlobalFiles;
    private $librariesGlobalFiles;
    private $jsPath;
    public $jsUrl;
    private $jsFiles;
    private $jsGlobalFiles;
    private $minPrefix;
    private $imagePath;
    public $imageURL;

    public $coreAssetsFolderName;
    private $coreCssPath;
    private $coreJsPath;
    private $coreCssURL;
    private $coreJsURL;
    private $coreImagePath;
    public $coreImageURL;

    public function __construct() {
        $this->_CI = &get_instance();
        /* set the path to assets/en for example, getLanguage() will return the current language of the request */
        $this->assetsFolderName = 'assets'.'/' .$this->_CI->lang->getLanguage();
        /* set the assets url that will be used to access from browser */
        $this->assetsUrl =  $this->cssUrl = $this->_CI->config->base_url($this->assetsFolderName) . '/' ;
        $this->cssPath = FCPATH . $this->assetsFolderName . '/css/';
        $this->cssUrl = $this->_CI->config->base_url($this->assetsFolderName.'/css') . '/' ;
        $this->cssFiles = array();
        /* load the css_autoload which is defined in the config page, the system will autoload these css files in every page request*/
        $this->cssGlobalFiles = $this->_CI->config->item('css_autoload');
        $this->jsPath = FCPATH . $this->assetsFolderName . '/js/' ;
        $this->jsUrl = $this->_CI->config->base_url($this->assetsFolderName.'/js') . '/' ;
        $this->jsFiles = array();
        /* load the js which is defined in the config page, the system will autoload these js files in every page request*/
        $this->jsGlobalFiles = $this->_CI->config->item('js_autoload');
        $this->minPrefix = $this->_CI->config->item('min_mode') ? '.min' : '';
        $this->imagePath = FCPATH .$this->assetsFolderName . '/images/';
        $this->imageURL  = $this->_CI->config->base_url($this->assetsFolderName.'/images') . '/' ;
        $this->libPath   = FCPATH .$this->assetsFolderName . '/libraries/';
        $this->libURL   = $this->_CI->config->base_url($this->assetsFolderName.'/libraries') . '/' ;
        /* load the libraries which is defined in the config page,
         the system will autoload these libraries in every page request
        libraries can contains both js and css
        */
        $this->librariesGlobalFiles = $this->_CI->config->item('libraries_autoload');

        /* if no files in the current language, the system will auto load the files in coreAssetsFolderName */
        $this->coreAssetsFolderName = 'assets'.'/' .'core';
        $this->coreCssPath          = FCPATH . $this->coreAssetsFolderName . '/css/' ;
        $this->coreCssURL           = $this->_CI->config->base_url($this->coreAssetsFolderName.'/css') . '/' ;
        $this->coreJsPath           = FCPATH . $this->coreAssetsFolderName . '/js/' ;
        $this->coreJsURL            = $this->_CI->config->base_url($this->coreAssetsFolderName.'/js') . '/' ;
        $this->coreImagePath        = FCPATH .$this->coreAssetsFolderName . '/images/';
        $this->coreImageURL         = $this->_CI->config->base_url($this->coreAssetsFolderName.'/images') . '/' ;
        $this->coreLibPath          = FCPATH .$this->coreAssetsFolderName . '/libraries/';
        $this->coreLibURL           = $this->_CI->config->base_url($this->coreAssetsFolderName.'/libraries') . '/' ;
    }

    /**
     * Load the lib name into 'load lib queue'
     * @param $lib
     */
    public function loadLibrary($lib){
        $this->librariesGlobalFiles[] = $lib;
    }

//    public function libPathJs($completeDir){
//        $coreFilePath = $this->coreLibPath . $completeDir  . ".js";
//        $filePath = $this->libPath . $completeDir  . ".js";
//        if(file_exists($filePath)){
//            $fileUrl = $this->libURL . $completeDir . ".js";
//            return "<script src=\"" . $fileUrl . "\"></script>\n";
//        }
//        if(file_exists($coreFilePath)){
//            $coreFileURL = $this->coreLibURL . $completeDir . ".js";
//            return "<script src=\"" . $coreFileURL . "\"></script>\n";
//        }
//
//        return "";
//    }
//
//    public function libPathCss($completeDir){
//        $coreFilePath = $this->coreLibPath .  $completeDir  . ".css";
//        $filePath = $this->libPath .  $completeDir  . ".css";
//        if(file_exists($filePath)){
//            $fileUrl = $this->libURL . $completeDir . ".css";
//            return "<link rel=\"stylesheet\" href=\"" . $fileUrl . "\">\n";
//        }
//
//        if(file_exists($coreFilePath)){
//            $coreFileURL = $this->coreLibURL . $completeDir . ".css";
//            return "<link rel=\"stylesheet\" href=\"" . $coreFileURL . "\">\n";
//        }
//
//
//        return "";
//    }

    /**
     * Used to load a css file name into queue
     * @param $fileName
     * @param bool $isFirst
     */
    public function css($fileName, $isFirst = false){
        if(!in_array($fileName, $this->cssFiles)){
            if($isFirst){
                array_unshift($this->cssFiles, $fileName);
            }else{
                $this->cssFiles[] = $fileName;
            }
        }
    }

    /**
     * Used to load a js file name into queue
     * @param $fileName
     * @param bool $isFirst
     */
    public function js($fileName, $isFirst = false){
        if(!in_array($fileName, $this->jsFiles)) {
            if($isFirst){
                array_unshift($this->jsFiles, $fileName);
            }else{
                $this->jsFiles[] = $fileName;
            }

        }
    }

    /** Load all css files in the queue to combine into a string
     * @return string
     */

    public function releaseCss(){
        $ret = "";

        if(count($this->librariesGlobalFiles) > 0){
            foreach($this->librariesGlobalFiles as $libGlobalFile){
                $completeDir = $libGlobalFile;
                $ret .= $this->_loadLibCss($completeDir);
            }
        }

        if(count($this->cssGlobalFiles) > 0){
            foreach($this->cssGlobalFiles as $cssGlobalFile){
                $completeDir = $cssGlobalFile;
                $ret .= $this->_loadCss($completeDir);
            }
        }

        if(count($this->cssFiles) > 0){
            foreach($this->cssFiles as $cssFile){
                $ret .= $this->_loadCss($cssFile);
            }
        }
        return $ret;
    }

    /**
     * Load the a file and convert it into a string which contains <link> tag
     * @param $completeDir
     * @return string
     */
    private function _loadCss($completeDir){
        $coreFilePath = $this->coreCssPath .  $completeDir  . ".css";
        $filePath = $this->cssPath .  $completeDir  . ".css";

        $returnString = "";
        /* Load Core first */
        if(file_exists($coreFilePath)){
            $coreFileURL = $this->coreCssURL . $completeDir . ".css";
            $returnString .= "<link rel=\"stylesheet\" href=\"" . $coreFileURL . "\">\n";
        }
        /* Then load Language files */
        if(file_exists($filePath)){
            $fileUrl = $this->cssUrl . $completeDir . ".css";
            $returnString .= "<link rel=\"stylesheet\" href=\"" . $fileUrl . "\">\n";
        }
        return $returnString;
    }

    /**
     * Load all css files in a library and convert it into a string which containts multiple <link> tag
     * @param $completeDir
     * @return string
     */
    private function _loadLibCss($completeDir){
        /* Load Language first */
        $returnString = "";
        if(file_exists($this->libPath.$completeDir.'/files.php')){
            $libCssFiles = array();
            include $this->libPath.$completeDir.'/files.php';
            foreach($libCssFiles as $cssFile){
                if(file_exists($this->libPath.$completeDir.'/'.$cssFile.'css')) {
                    $cssUrl = $this->libURL . $completeDir . '/' . $cssFile . '.css';
                    $returnString .= "<link rel=\"stylesheet\" href=\"" . $cssUrl . "\">\n";
                }
            }

            return $returnString;
        }
        /* If no language -> load Core */
        if(file_exists($this->coreLibPath.$completeDir.'/files.php')){
            $libCssFiles = array();
            include $this->coreLibPath.$completeDir.'/files.php';
            foreach($libCssFiles as $cssFile){
                if(file_exists($this->coreLibPath.$completeDir.'/'.$cssFile.'.css')) {
                    $cssUrl = $this->coreLibURL . $completeDir . '/' . $cssFile . '.css';
                    $returnString .= "<link rel=\"stylesheet\" href=\"" . $cssUrl . "\">\n";
                }
            }
            return $returnString;
        }
    }

    public function releaseJs(){
        $ret = "";

        if(count($this->librariesGlobalFiles) > 0){
            foreach($this->librariesGlobalFiles as $libGlobalFile){
                $completeDir = $libGlobalFile;
                $ret .= $this->_loadLibJs($completeDir);
            }
        }

        if(count($this->jsGlobalFiles) > 0){
            foreach($this->jsGlobalFiles as $jsGlobalFile){
                $completeDir = $jsGlobalFile;
                $ret .= $this->_loadJs($completeDir);
            }
        }
        if(count($this->jsFiles) > 0){
            foreach ($this->jsFiles as $jsFile) {
                $ret .= $this->_loadJs($jsFile);
            }
        }

        return $ret;
    }

    private function _loadJs($completeDir){

        $coreFilePath = $this->coreJsPath . $completeDir;
        $filePath = $this->jsPath . $completeDir;
        $version = date("Y-m-d H:i:s");
        $version = strtotime($version);
        
        $returnString = "";
        /* Load Core first */
        if(file_exists($coreFilePath.'.js')){
          //  $coreFileURL = $this->coreJsURL . $completeDir . ".js?v=".$version;
            $coreFileURL = $this->coreJsURL . $completeDir . ".js";
            $returnString .= "<script src=\"" . $coreFileURL . "\"></script>\n";
        }
        /* Then load Language files */
        if(file_exists($filePath.'.js')) {
            $fileUrl = $this->jsUrl . $completeDir . ".js";
            $returnString .= "<script src=\"" . $fileUrl . "\"></script>\n";
        }
        return $returnString;
    }

    private function _loadLibJs($completeDir){
        /* Load Language first */
        $returnString = "";
        if(file_exists($this->libPath.$completeDir.'/files.php')){
            $libJsFiles = array();
            include $this->libPath.$completeDir.'/files.php';
            foreach($libJsFiles as $jsFile){
                if(file_exists($this->libPath.$completeDir.'/'.$jsFile.'.js')){
                    $jsUrl = $this->libURL.$completeDir.'/'.$jsFile.'.js';
                    $returnString .= "<script src=\"" . $jsUrl . "\"></script>\n";
                }
            }

            return $returnString;
        }

        /* Load Core later */
        if(file_exists($this->coreLibPath.$completeDir.'/files.php')){
            $libJsFiles = array();
            include $this->coreLibPath.$completeDir.'/files.php';
            foreach($libJsFiles as $jsFile){
                if(file_exists($this->coreLibPath.$completeDir.'/'.$jsFile.'.js')) {
                    $jsUrl = $this->coreLibURL . $completeDir . '/' . $jsFile . '.js';
                    $returnString .= "<script src=\"" . $jsUrl . "\"></script>\n";
                }
            }
            return $returnString;
        }
    }

    public function releaseImage($completeDir){
        $imagePath = $this->imagePath . $completeDir;
        if(file_exists($imagePath)){
            $fileURL = $this->imageURL . $completeDir;
            return $fileURL;
        }

        $filePath = $this->coreImagePath . $completeDir;
        if(file_exists($filePath)){
            $coreFileURL = $this->coreImageURL . $completeDir;
            return $coreFileURL;
        }
        return "";
    }

    public function releaseProductImage($completeDir){
        $imagePath = 'assets/product/' . $completeDir;
        if(file_exists($imagePath)){
            $fileURL =$this->_CI->config->base_url('assets/product') . '/' . $completeDir;
            return $fileURL;
        }else{
            $fileURL =$this->_CI->config->base_url('assets/product') . '/' . 'default.png';
            return $fileURL;
        }
    }

    public function releaseSingleJs($completeDir){
        $coreFilePath = $this->coreJsPath . $completeDir . ".js";
        $filePath = $this->jsPath . $completeDir . ".js";

        $returnString = "";
        /* Load Core first */
        if(file_exists($coreFilePath)){
            $coreFileURL = $this->coreJsURL . $completeDir . ".js";
            $returnString .= "<script src=\"" . $coreFileURL . "\"></script>\n";
        }
        /* Then load Language files */
        if(file_exists($filePath)){
            $fileUrl = $this->jsUrl . $completeDir . ".js";
            $returnString .= "<script src=\"" . $fileUrl . "\"></script>\n";
        }
        return $returnString;
    }

    public function releaseSingleCss($completeDir){
        $coreFilePath = $this->coreCssPath . $completeDir  . ".css";
        $filePath = $this->cssPath . $completeDir  . ".css";

        $returnString = "";
        /* Load Core first */
        if(file_exists($coreFilePath)){
            $coreFileURL = $this->coreCssURL . $completeDir . ".css";
            $returnString .= "<link rel=\"stylesheet\" href=\"" . $coreFileURL . "\">\n";
        }
        /* Then load Language files */
        if(file_exists($filePath)){
            $fileUrl = $this->cssUrl . $completeDir . ".css";
            $returnString .= "<link rel=\"stylesheet\" href=\"" . $fileUrl . "\">\n";
        }
        return $returnString;
    }

    public function releaseCssWithoutGlobal(){
        $ret = "";
        if(count($this->cssFiles) > 0){
            foreach($this->cssFiles as $cssFile){
                $ret .= $this->_loadCss($cssFile);
            }
        }
        return $ret;
    }

    public function releaseJsWithoutGlobal(){
        $ret = "";
        if(count($this->jsFiles) > 0){
            foreach ($this->jsFiles as $jsFile) {
                $ret .= $this->_loadJs($jsFile);
            }
        }
        return $ret;
    }
}
