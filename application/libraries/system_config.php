<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/17/2014
 * Time: 4:32 PM
 */

class System_config{

    var $_CI;
    var $table_config;
    function __construct(){
        $this->_CI =& get_instance();
        $this->table_config = 'config';
        $this->table_branch = 'branch';
    }

    function get($code = '', $permission_fields = array()){
        if($code == ''){
            return null;
        }
        else{
            $config = $this->_CI->load->table_model($this->table_config)->get(array('code' => $code),$permission_fields);
            if(count($config)){
                return $config[0]->value;
            }else{
                $login_as_type = $this->_CI->user_check->get('login_as_type');
                if($login_as_type && $login_as_type <= 2){
                    $branch_id = $this->_CI->user_check->get('branch_id');
                    $branch = $this->_CI->load->table_model($this->table_branch)->getByID($branch_id);
                    if($branch && isset($branch->{$code})){
                        return $branch->{$code};
                    }
                }
                return null;
            }
        }
    }

    function get_by_code($code = '',$branch){
        $data = $this->_CI->load->table_model($this->table_config)->get_by_code($code,$branch);

        if(is_array($code)){
            $resArr = array();
            foreach($data as $item){
                if($item->value == 'inherit'){
                    $resArr[$item->code] = $item->default_inherite;
                }
                else{
                    $resArr[$item->code] = $item->value;
                }
            }
            return $resArr;
        }
        else{
            if($data->value == 'inherit'){
                $data = $data->default_inherite;
            }
            else{
                $data = $data->value;
            }
            return $data;
        }

    }
}