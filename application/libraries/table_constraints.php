<?php
/**
 * Created by PhpStorm.
 * User: Vu
 * Date: 11/9/2014
 * Time: 3:20 PM
 */

class Table_constraints{

    var $bugs;

    /**
     * used to validate data before insert or update
     * @param $values : data to be validated
     * @param $constraints : validation rules
     * @param $tableName : database table involve with the validation
     * @param $id : the id of existed record (use for update can be true|false)
     * @return array|bool
     */
    function checkDataConstraints($values, $constraints, $tableName, $id){
        if(is_array($values)){
            $values = (object) $values;
        }
        $this->bugs = array();
        foreach($constraints as  $key=>$array_constraints){
            //Check it is required or not
            if(in_array(Constraints::Required,$array_constraints)){
                if(isset($values->$key) && strlen($values->$key) > 0){
                    if($this->checkConstraint($values,$array_constraints,$key, $tableName, $id) == false){
                        return false;
                    }
                }
                else{
                    $this->bugReportCreate($key, 'Missing required field: '.$key);
                    $this->checkConstraint($values,$array_constraints,$key, $tableName, $id);
                }
            }
            //If not required
            else{
                if(isset($values->$key) && strlen($values->$key)){
                    if($this->checkConstraint($values,$array_constraints,$key, $tableName, $id) == false){
                        return false;
                    }
                }
            }
        }
        if(count($this->bugs) > 0){
            return $this->bugs;
        }
        return true;
    }


    /**
     * Redirect each field to special validation rule
     * @param $values
     * @param $array_constraints
     * @param $key
     * @param $tableName
     * @param $id
     * @return bool
     */
    function checkConstraint($values, $array_constraints, $key, $tableName ,$id){
        if(in_array(Constraints::Email, $array_constraints)){
            $this->validateEmail($values->$key, $key);
        }
        if(in_array(Constraints::Unique, $array_constraints)){
            $this->validateUnique($values->$key, $key, $tableName, $id);
        }

        return true;
    }

    /**
     * validate email
     * @param $email
     * @param $key
     */
    function validateEmail($email, $key){
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        if(preg_match($pattern, $email == false)){
            $this->bugReportCreate($key, 'Wrong Email format');
        }
    }

    /**
     * validate unique
     * @param $field_value
     * @param $key
     * @param $tableName
     * @param $id
     */
    function validateUnique($field_value, $key, $tableName, $id){
        $CI =& get_instance();
        $query = $CI->db->get_where($tableName, array($key => $field_value, 'status' => 1));
        if($query->num_rows > 0){
            $row = $query->result()[0];
            if($id = 0 || $id != $row->id)
                $this->bugReportCreate($key, 'Duplicate '.$key);
        }
    }

    /**
     * insert a bug into a queue
     * @param $key
     * @param $bug
     */
    function bugReportCreate($key, $bug){
        if(isset($this->bugs[$key])){
            $this->bugs[$key] .= ' and '.$bug;
        }
        else{
            $this->bugs[$key] = $bug;
        }
    }


}