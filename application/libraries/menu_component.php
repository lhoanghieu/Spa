<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Component{

    private $_CI;
    private $menu_model;
    private $category_model;

    public function __construct() {
        $this->_CI = &get_instance();
        $this->menu_model = $this->_CI->load->table_model('menu');
        $this->category_model = $this->_CI->load->table_model('category');
    }

    public function get_data($param = array()){
        $ret = array();
        $menus = $this->menu_model->getMenuToShow($param);
        foreach($menus as $menu){
            if(!isset($ret[$menu->parent])){
                $ret[$menu->parent] = array();
            }
            $ret[$menu->parent][] = $menu;
        }
        return $ret;
    }

    public function get_categories(){
        $categories = $this->category_model->select(array(
            'select' => array(
                'category' => array('id','code','name')
            ),
            'from'   => array(
                'item'          => array('table'=>'item'),
                'category_item' => array('table'=>'category_item', 'condition' => 'item.id = category_item.item_id'),
                'category'      => array('table'=>'category', 'condition' => 'category.id = category_item.category_id')
            ),
            'where'  => array('item.type'=>array(ITEM_TYPE('Package'),ITEM_TYPE('Product')))
        ))->result();
        $categories = convert_to_array($categories,'name');
        return $categories;
    }
}
