<?php

/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/5/2014
 * Time: 1:36 PM
 */
class User_permission
{

    /**
     * Used to get all user permission
     * @param array $permission_fields
     * @return mixed
     */
    function getUserPermissions($permission_fields = array())
    {
        $CI =& get_instance();
        $employee_model = $CI->load->table_model('employee');
        return $employee_model->getCurrentUserPermissions($permission_fields);
    }

    /**
     * used to check the user have the proper permission in a page
     * @param $permission_code
     * @param $pageCode
     * @return bool
     */
    function checkPermission($permission_code, $pageCode)
    {
        $CI =& get_instance();
        $module_permission_model = $CI->load->table_model('module_permission');
        $user = checkLoggedIn();

        if ($user == false || isset($user->user_id) == false) {
            return false;
        }
        if ($user->user_id == 1) {
            return true;
        }
        $lookForPermission = $module_permission_model->select(
            array(
                'select' => array('module_permission' => array('id')),
                'from' => array(

                    'module' => array('table' => 'module'),
                    'module_permission' => array('table' => 'module_permission', 'condition' => 'module.id = module_permission.module_id')
                ),
                'where' => array('module.code' => $pageCode, 'module_permission.permission_code' => $permission_code),
                'user_level' => $user->type
            )
        )->result();

        if (count($lookForPermission) == 0) {
            return false;
        }
        $user_permissions = $user->permissions;
        if (in_array($lookForPermission[0]->id, $user_permissions)) {
            return true;
        }
        return false;
    }

    /**
     * check the access level of the logged user
     * @param int $level
     * @return bool
     */
    function check_level($level = 1){
        $CI =& get_instance();
        $user_level = $CI->session->userdata('login');
        if(!$user_level){
            return false;
        }
        $result = $user_level->login_as_type >= $level;
        return $result;
    }

    /**
     * get the branch group id list of the logged user base on his access level and session
     * @return null | branch_group_ids
     */
    function get_branch_group(){
        $CI =& get_instance();
        $user = $CI->session->userdata('login');
        $have_branch_group_ids = null;
        if(isset($user->branch_group_id)){
            $have_branch_group_ids = $user->branch_group_id;
        }
        return $have_branch_group_ids;
    }

    /**
     * get the branch id list of the logged user base on his access level and session
     * @return array
     */
    function get_branch(){
        $CI =& get_instance();
        $user = $CI->session->userdata('login');
        if(isset($user->branch_id)){
            return $user->branch_id;
        }
        $branch_group_ids = $this->get_branch_group();
        $have_branch_ids = $CI->load->table_model('branch')->get(array('branch_group_id' => $branch_group_ids));
        $have_branch_ids = convert_to_array($have_branch_ids,'','id');
        return $have_branch_ids;
    }

    /**
     * get all branches available of employee
     * @return mixed
     */
    function get_all_branch(){
        $CI =& get_instance();
        return $CI->load->table_model('employee')->get_branch($CI->user_check->get('user_id'));
    }

    /**
     * get all branch groups available of employee
     * @return mixed
     */
    function get_all_branch_group(){
        $CI =& get_instance();
        return $CI->load->table_model('employee')->get_branch_group($CI->user_check->get('user_id'));
    }

    /**
     * check the current logged user have the input branch or not
     * @param $id
     * @return null
     */
    function check_branch($id){
        if(!isset($id)){
            return null;
        }

        $all_branch = $this->get_all_branch();
        if(in_array($id,$all_branch)){
            return $id;
        }
        return null;
    }

    /**
     * check the current branch group have the input branch group or not
     * @param $id
     * @return null
     */
    function check_branch_group($id){
        if(!isset($id)){
            return null;
        }

        $all_branch = $this->get_all_branch_group();
        if(in_array($id,$all_branch)){
            return $id;
        }
        return null;
    }

    /**
     * trim the input branch group, all $branch_group_ids have to exists in current loggging user's branch groups
     * @param $branch_group_ids
     * @param null $have_branch_group_ids
     * @return array
     */
    function trim_branch_group($branch_group_ids, $have_branch_group_ids = null){
        if(! $have_branch_group_ids){
            $have_branch_group_ids = $this->get_branch_group();
            if(!$have_branch_group_ids){
                return array();
            }
            if(!is_array($have_branch_group_ids)){
                $have_branch_group_ids = array($have_branch_group_ids);
            }
        }

        $temp = array_intersect($have_branch_group_ids,$branch_group_ids);
        $array = array();
        foreach($temp as $row){
            $array[] = $row;
        }
        return $array;
    }

    /**
     * trim the input branch, all $branch_group_ids have to exists in current loggging user's branches
     * @param $branch_ids
     * @param null $have_branch_ids
     * @return array
     */
    function trim_branch($branch_ids, $have_branch_ids = null){
        if(! $have_branch_ids){
            $have_branch_ids = $this->get_branch();
            if(!$have_branch_ids){
                return array();
            }
            if(!is_array($have_branch_ids)){
                $have_branch_ids = array($have_branch_ids);
            }
        }

        $temp = array_intersect($have_branch_ids,$branch_ids);
        $array = array();
        foreach($temp as $row){
            $array[] = $row;
        }
        return $array;
    }

    /**
     * get all report available for the current logging user
     * @param bool $full : used to determine if it just return a collection of report id or full format
     * @return array
     */
    function get_available_report($full = true){
        $CI =& get_instance();
        $group_id = $CI->user_check->get('group_id');
        $group = $CI->load->table_model('group')->getByID($group_id);
        $report_id_list = convert_to_array($group->report_list,'','id');
        if(!$full){
            return $report_id_list;
        }
        $array = array();
        $reports = $CI->load->table_model('report')->getTableMap('id');
        foreach($reports as $report_id=>$report){
            if(isset($report->parent) && $report->parent && in_array($report->id,$report_id_list)){
                $parent_report = $reports[$report->parent];
                if(! isset($array[$reports[$report->parent]->code])){
                    $array[$parent_report->code] = array(
                        'child' => array(),
                        'name'  => $parent_report->name,
                        'id'    => $parent_report->id,
                        'system'=> $parent_report->system
                    );
                }
                $array[$parent_report->code]['child'][$report->code] = array(
                    'name'           => $report->name,
                    'id'             => $report->id,
                    'condition_form' => $report->condition_form,
                    'system'         => $report->system,
                );
            }
        }
        return $array;
    }

    function checkReportPermission($code = ''){
        $CI =& get_instance();
        $user_permission = $this->get_available_report(false);
        $report = $CI->load->table_model('report')->get(array('code' => $code));
        if(count($report) == 0) return false;
        if(!in_array($report[0]->id,$user_permission)) return false;
        return true;
    }

}