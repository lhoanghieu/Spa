<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:53 PM
 */

class Employees extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "employees";
        $this->pageCode = "employees";
        $this->pageName = "Employees";
        $this->breadcrumbs[] = array('url' => 'admin/employees', 'text' => 'Employees');
    }

    function delete(){
        if($this->user_permission->checkPermission('d',$this->pageCode) == false){
            return;
        }
        $user = $this->session->userdata('login');
        $branch_id = isset($user->branch_id) ? $user->branch_id : '';
        $ids = $this->input->post('ids');
        $ids = json_decode($ids);

        $modelName = $this->load->controller_model($this->moduleName);

        $result = $modelName->delete($ids);
        if($result){
            foreach($ids as $id){
                if($branch_id != ''){
                    $this->send_employee_remove_email($id,$branch_id);
                }
            }

        }
        if(is_numeric($result) || $result === true){
            $this->ajax_content(REQUEST_SUCCESS,'The record has been deleted', array());
        }else{
            $this->ajax_content(REQUEST_FAIL,$result, array());
        }
    }

    function get_calendar($id = 0){
        $data = $this->load->controller_model($this->moduleName)->getDataForCalendar($id);
        $data->id = $id;
        $this->ajax_content(REQUEST_SUCCESS,'',array('content' => $this->content('admin/employees/calendar',$data,true)));
    }

    function get_absent(){
        $id = $this->input->post('id');
        $data = (object) ($this->load->controller_model($this->moduleName)->getDataForAbsent($id));
        $data->id = $id;
        $ret = new stdClass();
        $ret->content = $this->content('admin/employees/absent',$data,true);
        $ret->option = array(
            'title' => "Away Dates"
        );
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function change_calendar($id = 0){
        $data = $this->input->post('calendar');
        $result = $this->load->controller_model($this->moduleName)->changeCalendar($id,$data);
        if(is_numeric($result) || $result == true){
            $this->ajax_content(REQUEST_SUCCESS,'The calendar has been updated');
        }
    }

    function change_absent(){
        $data = (object) $this->input->post('data');
        $id = $this->input->post('id');
        $result = $this->load->controller_model($this->moduleName)->changeAbsent($id,$data);
        if($result){
            return $this->ajax_content(REQUEST_SUCCESS);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't save absent");
        }
    }

    function delete_absent_date(){
        $id = $this->input->post('id');
        $result = $this->load->controller_model($this->moduleName)->deleteAbsentDate($id);
        if($result){
            return $this->ajax_content(REQUEST_SUCCESS);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't delete absent");
        }
    }

    function suggest_therapist(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        $isFull = $this->input->get_post('full');
        $condition = array('department_id' => $this->load->table_model('department')->get_department_serve_booking());
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion($keyword,$condition,$isFull);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion($keyword, $condition,$isFull);
        }
        echo json_encode($suggest);
    }

    function get_assigned_service_form($id){
        $data = (object)$this->load->controller_model($this->moduleName)->get_assigned_service_form_data($id);
        $data->id = $id;
        $ret = new stdClass();
        $ret->content = $this->content('employees/assigned_service',(array)$data,true);
        $ret->option = array(
            'title' => "Assigned Service"
        );
        $buttons = array(
            'success' => array(
                'url'           => site_url($this->dir . '/save_assigned_service/' . $id),
                'text'          => 'Save',
                'class'         => "btn-primary",
                'before'        => 'get_assigned_service_before_edit',
                'close_modal'   => 1
            ),
            'cancel' => array(
                'text'  => 'Cancel'
            )
        );
        $ret->option['button'] = $buttons;
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function save_assigned_service($employee_id){
        $service_ids = $this->input->post('service_id');
        $this->load->controller_model($this->moduleName)->assign_service($employee_id,$service_ids);
        $this->ajax_content(REQUEST_SUCCESS,'Successfully assign services');
    }

}