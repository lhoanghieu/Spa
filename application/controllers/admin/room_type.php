<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_type extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "room_type";
        $this->pageCode = "room_type";
        $this->pageName = "Room Type";
        $this->breadcrumbs[] = array('url' => 'admin/room_type', 'text' => 'Room Type');
    }

}