<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commission extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->moduleName = "commission";
        $this->pageCode = "commission";
        $this->pageName = "Commission Rules";
        $this->breadcrumbs[] = array('admin/url' => 'staff/commission', 'text' => 'Commission Rules');
    }

    function suggest(){
        $keyword = $this->input->get('term');
        $id = $this->input->get('id');
        $modelName = $this->load->controller_model($this->moduleName);
        $suggest = $modelName->getItemSuggestion($id,$keyword);

        echo json_encode($suggest);
    }

}
