<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gift_Card extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "gift_card";
        $this->pageCode = "gift_card";
        $this->pageName = "Gift Card Management";
        $this->breadcrumbs[] = array('url' => 'admin/gift_card', 'text' => 'Gift Card');
    }

    function getData(){
        $data = $this->load->table_model('giftcard_list_detail')->getData();
        echo json_encode(array('data' => $data));
    }

    function getDetailData(){
        $giftcard_code = $this->input->get_post('giftcard_code',true);
        $data = $this->load->table_model('giftcard_list_detail')->getDetailData($giftcard_code);

        foreach($data as $key => $item){
            $data[$key]->created_date = get_user_date($item->created_date,'','Y-m-d H:i:s');
        }

        echo json_encode(array('data' => $data));
    }
}