<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rooms extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "rooms";
        $this->pageCode = "rooms";
        $this->pageName = "Rooms";
        $this->breadcrumbs[] = array('url' => 'rooms', 'text' => 'Rooms');
    }

    function suggestItem(){
        $keyword = $this->input->get('term');
        $modelName = $this->load->controller_model($this->moduleName);

        $suggest = $modelName->getItemSuggestion($keyword);

        echo json_encode($suggest);
    }

    function getItem($id){
        $modelName = $this->load->controller_model($this->moduleName);

        $item = $modelName->getItem($id);
        $this->ajax_content(REQUEST_SUCCESS,"",$item);
    }

}