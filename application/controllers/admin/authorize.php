<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Authorize extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "authorize";
    }

    public function index()
    {
        $User = checkLoggedInWithoutRedirect();
        if ($User != false) {
            redirect(site_url('admin/home'));
        }
        $data = new stdClass();
        $this->page($this->moduleName, $data, 'pos_login');
    }

    public function login()
    {
        $username = trim($this->input->post("username"));
        $password = $this->input->post("password");

        $employees_model = $this->load->table_model('employee');
        $employees = $employees_model->get(array(
            'username' => $username,
            'password' => md5($password),
        ));

        if (count($employees)) {
            $employee = $employees[0];
            //check branch
            $employee_temp = $employees_model->get(array('id' => $employee->id, "type >= ".Permission_Value::BRANCH_GROUP));
            if ($employee_temp !== null) {
                $department_group_model = $this->load->table_model('department_group');
                $groups = $department_group_model->get_group(array('department_id' => $employee->department_id));
                $userSession = new stdClass();
                $userSession->user_id = $employee->id;
                $userSession->Username = $employee->username;
                $userSession->Email = $employee->email;
                $userSession->FirstName = $employee->first_name;
                $userSession->LastName = $employee->last_name;
                $userSession->Phone = $employee->phone_number;
                $userSession->type = $employee->type;
                $userSession->login_as_type = $employee->type;
                $userSession->group_id = $groups;
                /*---- Get permissions -----*/
                if($employee->type == Permission_Value::ADMIN){
                    $userSession->branch_group_id = convert_to_array($this->load->table_model('branch_group')->get(),'','id');
                    $userSession->permissions = convert_to_array($this->load->table_model('module_permission')->get(),'','id');
                }else{
                    $userSession->branch_group_id = convert_to_array($this->load->table_model('employee_branch_group')->get(array('employee_id' => $employee->id)),'','branch_group_id');
                    $userSession->permissions = convert_to_array($this->load->table_model('module_permission')->get(array(),'','','',Permission_Value::BRANCH_GROUP,array('branch_group_id'=>$userSession->branch_group_id)),
                        '','id');
                }

                $this->session->set_userdata('login',$userSession);
                $userSession->timezone = $this->system_config->get('timezone',array('user_level' => Permission_Value::ADMIN));
                $this->session->set_userdata('login',$userSession);
                $this->ajax_content(REQUEST_SUCCESS,'',1);
            } else {
                $User = new stdClass();
                $User->login_as_type = Permission_Value::ADMIN;
                $this->session->set_userdata('login', $User);
                $this->ajax_content(REQUEST_SUCCESS,"","You do not have permission on this branch");
                exit;
            }
        } else {
            $this->ajax_content(REQUEST_SUCCESS,"","Wrong Username or Password!");
            exit;
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('login');
        redirect(admin_url('authorize'));
    }
}