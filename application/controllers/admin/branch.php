<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "branch";
        $this->pageCode = "branch";
        $this->pageName = "Branch";
        $this->breadcrumbs[] = array('url' => 'admin/branch', 'text' => 'Branch');
    }

    function get_suitable_employees(){
        if(! $this->user_permission->check_level(Permission_Value::BRANCH_GROUP)){
            echo "You dont have permission to access this content"; exit;
        }

        $employees = $this->load->controller_model('branch')->get_suitable_employee();
        echo json_encode($employees);
    }

    function get_permission_form($branch_group_id){
        if(! $this->user_permission->checkPermission('ep','branch')){
            $this->ajax_content(REQUEST_FAIL,'',"You don't have permission to access this content"); exit;
        }
        $permission_data = $this->load->controller_model('branch')->get_permission_data($branch_group_id);

        $data = $this->content($this->moduleName.'/permission_form',$permission_data, true);

        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data));
    }

    function edit_permissions($branch_group_id){
        if(! $this->user_permission->checkPermission('ep','branch')){
            $this->ajax_content(REQUEST_FAIL,'',"You don't have permission to access this content"); exit;
        }
        $permissions = $this->input->post('permissions');
        $remove_permissions = $this->input->post('removePermissions');
        $this->load->controller_model($this->moduleName)->edit_permissions($branch_group_id,$permissions,$remove_permissions);
        return $this->ajax_content(REQUEST_SUCCESS, "Permissions has been saved.");
    }

    function get_config_inherit_value($id,$branch_group_id){
        $condition = array();
        $condition['branch_group_id'] = $branch_group_id;
        $condition['user_level'] = Permission_Value::BRANCH_GROUP;
        $value = $this->load->controller_model('config')->get_config_inherit_value($id,$condition);
        $this->ajax_content(REQUEST_SUCCESS,'',$value);
    }

    function suggest_by_branch_group(){
        $keyword = $this->input->get_post('term');
        $branch_group_id = $this->input->get_post('branch_group_id');
        if(!$branch_group_id){
            $branch_group_id = $this->user_check->get('branch_group_id');
        }
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }
        echo json_encode($suggest);
    }

    function new_suggest_by_branch_group(){
        $keyword = $this->input->get_post('term');
        $branch_group_id = $this->input->get_post('branch_group_id');
        if($branch_group_id == 0)
            $branch_group_id = $this->load->table_model('employee')->get_branch_group($this->user_check->get('user_id'));
        if(!$branch_group_id){
            $branch_group_id = $this->user_check->get('branch_group_id');
        }
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }
        echo json_encode($suggest);
    }

    function suggest(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword));
        echo json_encode($suggest);
    }
}