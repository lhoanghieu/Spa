<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch_group extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "branch_group";
        $this->pageCode = "branch_group";
        $this->pageName = "Branch Group";
        $this->breadcrumbs[] = array('url' => 'admin/branch_group', 'text' => 'Branch Group');
    }

    function get_config_inherit_value($id,$branch_group_id = ''){
        $condition = array();
        if($branch_group_id){
            $condition['branch_group_id'] = $branch_group_id;
        }
        $condition['user_level'] = Permission_Value::ADMIN;
        $value = $this->load->controller_model('config')->get_config_inherit_value($id,$condition);
        $this->ajax_content(REQUEST_SUCCESS,'',$value);
    }

    function get_suitable_employees(){
        if(! $this->user_permission->check_level(Permission_Value::ADMIN)){
            echo "You don't have permission to access this content"; exit;
        }

        $employees = $this->load->controller_model('branch_group')->get_suitable_employee();
        echo json_encode($employees);
    }

    function get_permission_form($branch_group_id){
        if(! $this->user_permission->checkPermission('ep','branch_group')){
            $this->ajax_content(REQUEST_FAIL,'',"You don't have permission to access this content"); exit;
        }
        $permission_data = $this->load->controller_model('branch_group')->get_permission_data($branch_group_id);

        $data = $this->content($this->moduleName.'/permission_form',$permission_data, true);

        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data));
    }

    function edit_permissions($branch_group_id){
        if(! $this->user_permission->checkPermission('ep','branch_group')){
            $this->ajax_content(REQUEST_FAIL,'',"You don't have permission to access this content"); exit;
        }
        $permissions = $this->input->post('permissions');
        $remove_permissions = $this->input->post('removePermissions');
        $this->load->controller_model($this->moduleName)->edit_permissions($branch_group_id,$permissions,$remove_permissions);
        return $this->ajax_content(REQUEST_SUCCESS, "Permissions has been saved.");
    }

    function suggest(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('id' => $this->user_check->get('branch_group_id')));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('id' => $this->user_check->get('branch_group_id')));
        }
        echo json_encode($suggest);
    }

}