<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Items extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "items";
        $this->pageCode = "items";
        $this->pageName = "Items";
        $this->breadcrumbs[] = array('url' => 'admin/items', 'text' => 'Items');
    }

    function suggestItem(){
        $keyword = $this->input->get('term');
        $modelName = $this->load->controller_model($this->moduleName);

        $suggest = $modelName->getItemSuggestion($keyword);

        echo json_encode($suggest);
    }


    function getItem($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $item = $modelName->getItem($id);
        if($item == null){
            $item = false;
        }
        $this->ajax_content(REQUEST_SUCCESS,"",$item);
    }

    function suggestService(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion($keyword);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion($keyword);
        }
        echo json_encode($suggest);
    }
    function suggestByCategoryList(){
        $isMinimal = $this->input->get_post('minimal');
        $category_ids = $this->input->get_post('category_ids');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion('',array('category_item.category_id'=>$category_ids));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion('',array('category_item.category_id'=>$category_ids));
        }

        $this->ajax_content(REQUEST_SUCCESS,'',$suggest);
    }

    function suggestByCategory($category_id,$employee_id = ''){
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion('',array('category_item.category_id'=>$category_id));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion('',array('category_item.category_id'=>$category_id));
        }
        if(!empty($employee_id) && $employee_id > 0){
            $list_service = $this->load->controller_model($this->moduleName)->get_service_by_category_employee(array('category_id'=>$category_id,'employee_id'=>$employee_id));
            $new_list = array();
            foreach($list_service as $item){
                $new_list[] = $item->item_id;
            }
            foreach($suggest as $key => $it){
                if(!in_array($it['value'],$new_list)){
                    unset($suggest[$key]);
                }
            }
        }

        $this->ajax_content(REQUEST_SUCCESS,'',$suggest);
    }

    function suggestProduct(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('item.type' => ITEM_TYPE('Product')));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('item.type' => ITEM_TYPE('Product')));
        }
        echo json_encode($suggest);
    }


    function disable_item($id){
        try {
            $this->load->controller_model($this->moduleName)->disable_item($id);

            // GET OLD DATA FOR LOGGING
            $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);
            // END GET OLD DATA FOR LOGGING

            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Disable',
                    'object' => $oldRow->code,
                    'module' => 'admin/'.$this->moduleName));
            $this->ajax_content(REQUEST_SUCCESS,'The item has been disabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }
    function enable_item($id){
        try {
            $this->load->controller_model($this->moduleName)->enable_item($id);

            // GET OLD DATA FOR LOGGING
            $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);
            // END GET OLD DATA FOR LOGGING

            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Enable',
                    'object' => $oldRow->code,
                    'module' => 'admin/'.$this->moduleName));

            $this->ajax_content(REQUEST_SUCCESS,'The item has been enabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }
}