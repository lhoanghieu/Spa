<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "customers";
        $this->pageCode = "customers";
        $this->pageName = "Customers";
        $this->breadcrumbs[] = array('url' => 'admin/customers', 'text' => 'Customers');
    }

    function send_pin_email($customer_id)
    {
        $customer = null;
        if (is_object($customer_id)) {
            $customer = $customer_id;
        } else {
            $customer = $this->load->table_model('customer')->getByID($customer_id);
        }
        if(isset($customer) && $customer !== null && $customer->email) {
            $to = $customer->email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "Your Healing Touch Customer Account Pin";
                $body = $this->load->email_content('customer_pin', $customer);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return true;
                }
            }
        }
        return false;
//        return $this->email->print_debugger();
    }

    function getForm($id = 0)
    {
        $status = REQUEST_SUCCESS;
        $message = "";
        $replace_button = 0;
        $isNext = $this->input->post('isNext');
        $inCustomer = ($this->input->post('inCustomer') !== false) ? $this->input->post('inCustomer') : true;
        $customers_controller_model = $this->load->controller_model('customers');
        $success = "get_form_success";
        if($id || $isNext){
            $data = $customers_controller_model->getDataForViewForm($id);
            $buttons = array(
                'success' => array(
                    'url'       => site_url($this->dir . '/pre_edit/' . $id),
                    'text'      => 'Save',
                    'class'     => "btn-primary",
                    'before'    => 'get_data_before_edit',
                    'success'   => 'pre_edit_2_success'
                ),
                'cancel' => array(
                    'text'  => 'Cancel'
                )
            );
            if($inCustomer){
                $buttons['success']['success'] = 'after_success';
                $buttons['success']['more'] = array(
                    'table_id' => 'customer_list',
                    'url' => $this->dir . '/'
                );
            }else{
                $success = "";
            }
            $content = $this->content('customers/edit_form', $data, true);
        }else{
            $data = $customers_controller_model->getDataForQuestionForm($id);
            $buttons = array(
                array(
                    'text'   => 'Ignore Question',
                    'url'    => site_url($this->dir . '/getForm'),
                    'type'   => 'dialog',
                    'param'  => array('isNext'=>1)
                ),
                array(
                    'text'   => 'Next',
                    'url'    => site_url($this->dir . '/getForm'),
                    'type'   => 'dialog',
                    'before' => 'save_question',
                    'param'  => array('isNext'=>1)
                )
            );
            $replace_button = 1;
            $content = $this->content('customers/question_form', $data, true);
        }
        $option = array(
            'button' => $buttons,
            'replace_button' => $replace_button
        );
        if($success !== ""){
            $option["success"] = $success;
        }
        $this->ajax_content($status,$message,array('type' => 'dialog','content' =>$content, 'option' => $option));
    }

    function edit($id = 0)
    {
        $fields = (object) $this->input->post('field_post');
        $modelName = $this->load->controller_model($this->moduleName);
        if ($id == 0) {
            $raw_password = uniqid();
            $fields->password = md5($raw_password);
            //Insert to Database
            if ($this->user_permission->checkPermission('i', $this->pageCode) == false) {
                return $this->ajax_content(REQUEST_FAIL, "You don't have permission");
            }
            $result = $modelName->insert($fields);
            if($result !== false){
                //Send mail
                $fields->password = $raw_password;
                if ($fields->customer_type == CUSTOMER_TYPE('Member') && $this->send_pin_email($fields) !== true) {
                    return $this->ajax_content(REQUEST_FAIL, "Can't send email");
                }
            }
        } else {
            if ($this->user_permission->checkPermission('e', $this->pageCode) == false) {
                return $this->ajax_content(REQUEST_FAIL, "You don't have permission");
            }
            //Edit a record
            $raw_password = uniqid();
            $customer_model = $this->load->table_model('customer');
            $old_record = $customer_model->getByID($id);

            if ($fields->customer_type == CUSTOMER_TYPE('Member') && $old_record->customer_type == CUSTOMER_TYPE('Guest')) {
                //Send mail
                $fields->password = md5($raw_password);
                $result = $modelName->update($id, $fields);
                $fields->password = $raw_password;
                if (!$result || is_string($result)) {
                    return $this->ajax_content(REQUEST_FAIL, is_string($result)?$result:"Can't save customer information");
                }else{
                    if($this->send_pin_email($fields) !== true){
                        return $this->ajax_content(REQUEST_FAIL, "Can't send email");
                    }
                }
            }else{
                $result = $modelName->update($id, $fields);
                if (!$result || is_string($result)) {
                    return $this->ajax_content(REQUEST_FAIL, is_string($result)?$result:"Can't save customer information");
                }
            }

        }
        $option = array('close_modal' => 1);
        return $this->ajax_content(REQUEST_SUCCESS, "Customer information was saved successfully!", array('option' => $option));
    }


    function view_pin($id,$view_only=0){
        if($view_only == 1){
            if ($this->user_permission->checkPermission('vpin', $this->pageCode) == false){
                return $this->ajax_content(REQUEST_FAIL, "You don't have permission");
            }
        }
        else{
            if ($this->user_permission->checkPermission('epin', $this->pageCode) == false){
                return $this->ajax_content(REQUEST_FAIL, "You don't have permission");
            }
        }
        $pin = $this->load->controller_model($this->moduleName)->getCustomerPin($id);
        if($view_only == 0){
            $buttons = array(
                'success' => array(
                    'text'   => 'Save',
                    'url'    => site_url($this->dir . '/edit_pin/' . $id),
                    'before' => 'before_edit_pin',
                    'after'  => 'after_edit_pin',
                    'class'  => 'btn-primary',
                ),
                'cancel' => array(
                    'text'   => 'Close'
                )
            );
            $option = array(
                'button' => $buttons,
                'dialog' => array(
                    'size' => 'sm'
                )
            );
            $content = $this->content('customers/edit_pin_form', array('id' => $id, 'account_number' => $pin), true);
        }else{
            $buttons = array(
                'cancel' => array(
                    'text'   => 'Close'
                )
            );
            $option = array(
                'button' => $buttons,
                'dialog' => array(
                    'size' => 'sm'
                )
            );
            $content = $this->content('customers/view_pin_form', array('id' => $id, 'account_number' => $pin), true);
        }

        return $this->ajax_content(REQUEST_SUCCESS, "", array('content' => $content,'option' => $option, 'type'=>'dialog'));
    }
    function edit_pin($id){
        if ($this->user_permission->checkPermission('epin', $this->pageCode) == false){
            return $this->ajax_content(REQUEST_FAIL, "You don't have permission");
        }
        $pin = $this->input->post('pin');
        $opin = $this->input->post('opin');
        if(isset($id) && $pin !== false && $opin !== false){
            $ret = $this->load->controller_model($this->moduleName)->setCustomerPin($id,$pin, $opin);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Missing data post to server!");
        }
        if($ret === true){
            $option = array('close_modal' => 1);
            // CALL SEND SMS RESET PIN Notify
            $this->load->library('Smslib');
            $this->smslib->sendResetPin($id, $pin);
            // END
            return $this->ajax_content(REQUEST_SUCCESS, "Customer Pin was changed successfully!", array('option' => $option));
        }else{
            return $this->ajax_content(REQUEST_FAIL, "", array('validate_error' => $ret));
        }
    }

    function edit_survey()
    {
        $id = $this->input->post('id');
        $answers = $this->input->post('field_post');
        $customers_controller_model = $this->load->controller_model('customers');
        if($customers_controller_model->editSurvey($id, $answers)){
            $option = array('close_modal' => 1);
            return $this->ajax_content(REQUEST_SUCCESS, "Customer survey was changed successfully!", array('option' => $option));
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't save customer survey, please contact admin to check!");
        }
    }

    function question_form($id = 0){
        if(!isset($id) || $id == 0){
            $id = $this->input->post('id');
        }
        $status = REQUEST_SUCCESS;
        $message = "";
        $content = "";
        $customers_controller_model = $this->load->controller_model('customers');
        $data = $customers_controller_model->getDataForQuestionForm($id);
        $buttons = array(
            'success' => array(
                'text'   => 'Save',
                'url'    => site_url($this->dir . '/edit_survey'),
                'before' => 'before_edit_survey',
                'class' => 'btn-primary'
            ),
            'cancel' => array(
                'text'   => 'Close'
            )
        );
        $content = $this->content('customers/question_form', $data, true);
        $option = array(
            'button' => $buttons,
            'success'  => 'render_form'
        );
        $this->ajax_content($status,$message,array('type'=>'dialog', 'option'=>$option, 'content' =>$content));
    }

    function printCustomerCreditHistory($id){
        $customer = $this->load->table_model('customer')->getByID($id);
        if($customer !== null){
            $data = new stdClass();
            $data->customer_name = to_full_name($customer->first_name, $customer->last_name);
            $data->customer_code = $customer->code;
            $data->cutomer_credit = $this->load->controller_model('customers')->getCustomerCreditHistory($id);
            $data->export = 1;
            $data->content = $this->content('customers/credit_history', $data, true);

            $this->load->third_party('MPDF57/mpdf.php');
            $mpdf = new mPDF();
            $stylesheet = file_get_contents(base_url().'/assets/core/libraries/jquery-ui/jquery-ui.min.css');
            $stylesheet.= file_get_contents(base_url().'/assets/core/libraries/bootstrap/css/bootstrap.min.css');
            $stylesheet.= file_get_contents(base_url().'/assets/core/css/system.css'); // external css
            $stylesheet.= file_get_contents(base_url().'/assets/core/css/contents/customers.css'); // external css
            $stylesheet.= file_get_contents(base_url().'/assets/core/css/layouts/default.css'); // external css

            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($data->content,2);
            $mpdf->Output('credit_history_'.$data->customer_code.'_'.date('Y-m-d H:i:s').'.pdf','D');
        }
    }

    function credit_history($id){
        $status = REQUEST_SUCCESS;
        $customer = $this->load->table_model('customer')->getByID($id);
        $ui = new stdClass();
        if($customer !== null){
            $data = new stdClass();
            $data->customer_name = to_full_name($customer->first_name, $customer->last_name);
            $data->customer_code = $customer->code;
            $data->cutomer_credit = $this->load->controller_model('customers')->getCustomerCreditHistory($id);
            $data->content = $this->content('customers/credit_history', $data, true);
            $ui->content = $data->content;
            $ui->type = "dialog";
            $ui->option = array('title' => $data->customer_name . " Credit History");
        }

        $this->ajax_content($status, "", $ui);
    }

    function credit_history_report($id){
        $status = REQUEST_SUCCESS;
        $customer = $this->load->table_model('customer')->getByID($id);
        $ui = new stdClass();
        if($customer !== null){
            $data = new stdClass();
            $data->customer_name = to_full_name($customer->first_name, $customer->last_name);
            $data->customer_code = $customer->code;
            $data->cutomer_credit = $this->load->controller_model('customers')->getCustomerCreditHistory($id);
            $data->content = $this->content('customers/credit_history_report', $data, true);
        }
        echo $data->content;

        // $this->ajax_content($status, "", $ui);
    }

    function pre_edit($id = 0){
        $fields = (object) $this->input->post('field_post');
        $is_guest = true;
        $check_constraints = $this->load->table_model('customer')->checkDataConstraints($fields, $id);
        $customer = array();
        /* ---- Check constraints and another branch customer -----*/
        if($check_constraints !== true && $id != 0){
            $customer_id = false;
            if(isset($check_constraints['nric']) && $customer_id == false){
                $check_in_branch = $this->load->table_model('customer')->get(array('nric' => $fields->nric));
                if (count($check_in_branch[0]->customer_permission) != 0) {
                    $customer_id = $check_in_branch[0]->id;
                    $customer    = $check_in_branch[0];
                }
            }

            if(isset($check_constraints['email']) && $customer_id == false){
                $check_in_branch = $this->load->table_model('customer')->get(array('email' => $fields->email));
                if (count($check_in_branch[0]->customer_permission) != 0) {
                    $customer_id = $check_in_branch[0]->id;
                    $customer    = $check_in_branch[0];
                }
            }

            if($customer_id == false){
                $this->ajax_content(REQUEST_FAIL,'',array('validate_error' => $check_constraints));
                exit;
            }else{
                if(!isset($customer->customer_permission) || !$customer->customer_permission){
                    $buttons = array(
                        'success' => array(
                            'url'       => site_url($this->dir . '/insert_to_branch/'),
                            'text'      => 'Insert',
                            'before'    => 'before_set_identity_to_branch',
                            'success'   => 'after_success',
                            'param'     => array(
                                'id'        => $customer->id,
                                'type'      => $customer->customer_type
                            ),
                            'more'      => array(
                                'table_id' => 'customer_list',
                                'url' => $this->dir . '/',
                            ),
                            'close_modal' => true,
                            'class'     => 'btn-primary',
                        ),
                        'cancel' => array(
                            'text'  => 'Cancel'
                        )
                    );
                    $option = array(
                        'button' =>$buttons,
                        'title'  => 'The Customer already existed.',
                        'success' => ''
                    );
                    $content = $this->content('customers/enter_identity_form',$customer,true);
                    $this->ajax_content(REQUEST_SUCCESS,'',array('type' => 'dialog', 'option'=>$option,'content' => $content));
                    exit;
                }
            }
        }
        else{
            $check_constraints = $this->load->table_model('customer')->checkDataConstraints($fields,$id);
            if($check_constraints !== true){
                $this->ajax_content(REQUEST_FAIL,'',array('validate_error' => $check_constraints));
                exit;
            }
        }

//        // validation data
//        $validFields = $this->load->controller_model('customers')->validationCustomer($fields);
//
//        if(!$validFields['status']){
//            $this->ajax_content(REQUEST_FAIL,$validFields['message']);
//            exit;
//        }

        /*--------------------------------------------------*/
        if(isset($id) && $id != 0){
            $customer = $this->load->table_model('customer')->getByID($id);
            if($customer->customer_type != 1){
                $is_guest = false;
            }
        }
        if($is_guest && $fields->customer_type == CUSTOMER_TYPE('Member')){
            $buttons = array(
                'success' => array(
                    'url'   => site_url($this->dir . '/edit/' . $id),
                    'text'  => 'Submit',
                    'before'=> 'before_set_pin',
                    'success'=> 'after_success',
                    'param' => array(
                        'field_post' => $fields,
                        'id' => $id
                    ),
                    'more'      => array(
                        'table_id' => 'customer_list',
                        'url' => $this->dir . '/',
                    ),
                    'class'     => 'btn-primary',
                    'close_modal' => true,
                ),
                'cancel' => array(
                    'text'  => 'Cancel'
                )
            );
            $option = array(
                'button' =>$buttons,
                'dialog' => array(
                    'size' => 'sm'
                ),
                'success' => ""
            );
            $content = $this->content('customers/edit_pin_form', array('id' => $id, 'hide_pin' => true), true);
            $this->ajax_content(REQUEST_SUCCESS,'',array('type' => 'dialog', 'option'=>$option,'content' => $content));
        }else{
            /*---- Check if change customer email and a member ------*/
            if($id && $customer->customer_type == CUSTOMER_TYPE('Member')){
                if(($customer->email != $fields->email) OR ($customer->nric != $fields->nric)){
                    $buttons = array(
                        'success' => array(
                            'url'    => site_url($this->dir . '/check_customer_pin/' . $id),
                            'text'   => 'Submit',
                            'before' => 'before_set_pin',
                            'success'=> 'after_success',
                            'param' => array(
                                'field_post' => $fields,
                                'id' => $id,
                                'route' => 'edit',
                            ),
                            'more'      => array(
                                'table_id' => 'customer_list',
                                'url' => $this->dir . '/',
                            ),
                            'close_modal' => true,
                            'class' => 'btn-primary'
                        ),
                        'cancel' => array(
                            'text'  => 'Cancel'
                        )
                    );
                    $option = array(
                        'button' =>$buttons,
                        'dialog' => array(
                            'size' => 'sm'
                        ),
                        'success' => ""
                    );
                    $content = $this->content('customers/enter_pin_form', array('id' => $id, 'hide_pin' => true), true);
                    return $this->ajax_content(REQUEST_SUCCESS,'',array('type' => 'dialog', 'option'=>$option,'content' => $content));
                }
            }
            $this->edit($id);
        }
    }

    function check_customer_pin($id){
        $post   = $this->input->post('field_post');
        $pin    = $post['pin'];
        $customer = $this->load->table_model('customer')->getByID($id);
        if($customer->pin == $pin){
            $this->{$this->input->post('route')}($id);
        }else{
            throw new Exception('Customer pin is incorrect');
        }
    }

    function get_credit_adjustment_form($id){
        $data = $this->load->controller_model($this->moduleName)->get_credit_adjustment_data($id);
        $content = $this->content('customers/credit_adjustment_form', $data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/adjust_credit'),
                'text'      => 'Save',
                'class'     => "btn-primary",
                'close_modal' => 1
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$content, 'type' => 'dialog', 'option' => $option));
    }

    function adjust_credit(){
        $credit_data = $this->input->post('credit_data');
        $this->load->controller_model($this->moduleName)->adjust_credit($credit_data);
        $this->ajax_content(REQUEST_SUCCESS,"Successfully adjust customer's credit");
    }

    function get_transfer_form($id){
        $data =  $this->load->controller_model($this->moduleName)->get_credit_transfer_form_data($id);
        $content = $this->content('customers/package_transfer_form', $data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/transfer_credit'),
                'text'      => 'Save',
                'class'     => "btn-primary",
                'before'    => 'get_credit_transfer_data',
                'close_modal' => 1
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$content, 'type' => 'dialog', 'option' => $option));
    }

    function transfer_credit(){
        $credit_transfer_data = $this->input->post('credit_transfer_data');

        $this->load->controller_model($this->moduleName)->transfer_credit($credit_transfer_data);

        $this->ajax_content(REQUEST_SUCCESS,'The credit has been transfered successfully');
    }

    function ImportCustomerData(){
        $config['targetDir'] = './import/customer_data/';
        if(!isset($_FILES['file']['name'])){
            echo json_encode(array('result' => FALSE, 'message' => 'Files does not exist.'));
            exit;
        }
        $targetFile = $config['targetDir'].$_FILES['file']['name'];
        $fileExt = pathinfo($targetFile,PATHINFO_EXTENSION);
        $config['maximumSize'] = 4096;
        $config['allowExt'] = array('csv','xls','xlsx');

        if(file_exists($targetFile)){
            $_FILES['file']['name'] = md5($_FILES['file']['name'].microtime()) . date('Y_m_d_H_i_s') . '.' . $fileExt;
        }
        else{
            $_FILES['file']['name'] = $_FILES['file']['name'] . '_' . date('Y_m_d_H_i_s') . '.' . $fileExt;
        }
        $targetFile = $config['targetDir'].$_FILES['file']['name'];

        if(move_uploaded_file($_FILES['file']['tmp_name'], $targetFile )){
            $realFilePath = $targetFile;
        }
        else{
            echo json_encode(array('result' => FALSE, 'message' => 'Fails'));
        }

        $importData = array();
        $indexField = array();
        $temp = array();


        // DETECT AND CREATE OBJECT READER PHPEXCEL
        $inputFileName = $realFilePath;
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        // CASE : CSV
        if($fileExt == 'csv'){
            $worksheet = $objPHPExcel->getActiveSheet();
            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                foreach ($cellIterator as $key => $cell) {
                    $column = $cell::coordinateFromString($cell->getCoordinate());
                    if($row->getRowIndex() == 1 && $cell->getValue() != ''){
                        $indexField[$column[0]] = trim($cell->getValue());
                    }
                    if(!is_null($cell) && $row->getRowIndex() > 1 && isset($indexField[$column[0]])){
                        $temp[$indexField[$column[0]]] = $cell->getValue();
                    }
                }
                if(!empty($temp)){
                    $importData[] = $temp;
                }
            }
        }
        elseif($fileExt == 'xls' || $fileExt == 'xlsx') {
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $indexKey = array();
            //  Loop through each row of the worksheet in turn
            for ($row = 1; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                if($row == 1){
                    $tempi = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    foreach($tempi[0] as $item){
                        $indexKey[] = $item;
                        $indexField[$item] = $item;
                    }
                }
                else{
                    $convert = array();
                    $tempa = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    foreach($tempa[0] as $key => $ia){
                        $convert[$indexKey[$key]] =  $ia;
                    }
                    if($tempa[0][0] != '' && $tempa[0][3] != ''){
                        $importData[] = $convert;
                    }
                }
            }
        }

        // CHECK COLUMN FIELD EXIST OR NOT
        $listDatabaseFields = $this->db->query('SHOW COLUMNS FROM customer')->result_array();
        $temp = array();
        foreach($listDatabaseFields as $key => $item){
            if(!empty($item['Field']))
                $temp[$item['Field']] = $item;
        }
        $listDatabaseFields = $temp;
        $listMissingFields = array();
        foreach($indexField as $field){
            if(!isset($listDatabaseFields[$field]) && $field != ''){
                $listMissingFields[] = $field;
            }
        }

        if(count($listMissingFields) > 0){
            echo json_encode(array('status' => FALSE,'message' => 'These fields do not exist on database, please recheck your import file:<br>- "'.implode('", "',$listMissingFields).'"'));
            exit;
        }

        // BEGIN GENERATE CUSTOMER CODE AND IMPORT TO DATABASE
        $temp = array();
        $listCust = $this->load->table_model('customer')->getListCustID();
        foreach($listCust as $item){
            $temp[$item['id']] = $item;
        }
        $listCust = $temp;
        $temp = array();
        foreach($importData as $key => $cust){
            $code = 'SPA' . mt_rand(1000001, 9999999);
            while(isset($temp[$code]) && !empty($temp) && isset($listCust[$code])){
                $code = 'SPA' . mt_rand(1000001, 9999999);
            }
            $cust['code'] = $code;
            $temp[$code] = $cust;
        }
        $importData = $temp;
        $result = $this->load->table_model('customer')->importBatchCustomer($importData);

        if($result){
            echo json_encode(array('status' => TRUE, 'message' => 'Complete'));
        }
    }


}