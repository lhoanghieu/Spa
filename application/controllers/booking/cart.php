<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "product";
    }

    function index(){
        $this->page('cart',array());
    }

    function insert_item(){
        $model = $this->load->table_model('item');
        $code = $this->input->post('code');
        $qty  = $this->input->post('qty');
        $item = $model->get(array('code' => $code));
        if(count($item)){
            $item = $item[0];
            $rowid = $this->cart->insert(array(
                'id'    => $item->code,
                'qty'   => $qty,
                'price' =>$item->price,
                'name'  =>$item->name,
            ));
            if($rowid)
                $this->ajax_content(REQUEST_SUCCESS, "The item has been inserted to cart");
            else
                $this->ajax_content(REQUEST_FAIL,"Error when trying to insert cart. Please try again");
        }else{
            $this->ajax_content(REQUEST_FAIL,"No item exists");
        }
    }

    function update(){
        $rowid = $this->input->post('rowid');
        $qty = $this->input->post('qty')?$this->input->post('qty'):0;
        $data = array(
            'rowid' => $rowid,
            'qty'   => $qty,
        );
        $result = $this->cart->update($data);
        if($result){
            $this->ajax_content(REQUEST_SUCCESS,"",array('subtotal' => to_currency($this->cart->total())));
        }else{
            $this->ajax_content(REQUEST_FAIL,"This item was already deleted") ;
        }
    }

    function clear(){
        try{
            $this->cart->destroy();
        }
        catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
        $this->ajax_content(REQUEST_SUCCESS);
    }

}