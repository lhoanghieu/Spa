<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "category";
        $this->breadcrumbs[] = array('url' => 'category', 'text' => 'Category');
    }

    function index(){
        $code = $this->input->get('code');
        if($code == false){
            $code = '';
        }

        $category = $this->load->table_model('category_model')->get(array('code' => $code))[0];
        $this->breadcrumbs[] = array('url' => 'category?code='.$category->code, 'text' => $category->name);
        $item_model = $this->load->table_model('item');
        $data = array();
        $data['items'] = $item_model->getByCategory($code);
        $data['category_name'] = $category->name;
        $this->page('category/list',$data);
    }

}