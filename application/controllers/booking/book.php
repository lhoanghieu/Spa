<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "book";
        $this->breadcrumbs[] = array('url' => 'book', 'text' => 'Home');
        $this->steps = array(
            1   => array('module'=>'branch','text'=>'Branch'),
            2   => array('module'=>'service','text'=>'Service'),
            3   => array('module'=>'date','text'=>'Date & Time'),
            4   => array('module'=>'comment','text'=>'Your Detail'),
            5   => array('module'=>'confirm','text'=>'Confirm'),
            6   => array('module'=>'finished','text'=>'Finished')
        );
    }

    function index(){
        $config = $this->load->controller_model($this->moduleName)->getBookingConfig();

        $mode = $this->input->get_post('mode');
        if(!$mode || $mode == ''){
            $mode = FALSE;
        }
        $config = $this->load->controller_model($this->moduleName)->getBookingConfig();
        $customers_controller_model = $this->load->controller_model('customers');
        $logged_user = $this->session->userdata('login');

        /*if(isset($logged_user->id))
            $is_new = $customers_controller_model->getFirstlogin_status($logged_user->id);*/

        $this->session->set_userdata(array('booking_state' => TRUE));
        $this->session->set_userdata(array('booking_config' => $config['max_num_booking']));

        $user = $this->session->userdata('is_customer_logged');
        if($user){
            $config['popup_message'] = '';
        }

        $this->session->set_userdata(array(
            'popup_message' => $config['popup_message']
        ));
        $this->breadcrumbs[] = array('url' => 'book', 'text' => 'Booking');
        $booking_data = $this->session->userdata('booking_data');
        $this->session->set_userdata('is_reschedule',array());

        $prepareData = $this->load->controller_model($this->moduleName)->getPrepareData();
        // temp

        $is_new = 0;
        if(isset($is_new) && $is_new == 1 ){
            if(isset($_POST["agreement"])){
                $this->update_first_login($logged_user->id);
                $this->page('book/index',array('steps' => $this->steps,'booking_data'=>$booking_data));
            }
            else{
                $data = $customers_controller_model->getDataForQuestionForm($logged_user->id);
                $this->page('authorize/survey',array('questions' => $data->questions,'customer_id' => $logged_user->id));
            }
        }
        else{
            $update_app = "";
            if(isset($_GET['bid'])){
                $bid = $this->input->get('bid');
                if(ctype_digit($bid)){
                    $is_valid = $this->check_valid_cancelapp($bid);
                    $appdata["app_details"] = $this->get_cancel_app($bid);
                    $appdata["baseurl"] = base_url();
                    if(!$is_valid){
                        $update_app = "";
                    }
                    else{
                        if(empty($appdata["app_details"]))
                            $update_app = "";
                        else
                            $update_app = $this->load->view('contents/booking/book/cancelapp', $appdata, true);
                    }
                }
            }
            $this->page('book/index',array('steps' => $this->steps,'config'=>$config['popup_message'],'booking_data'=>$booking_data,'update_app' => $update_app,'simple_mode' => ($mode=='p'?TRUE:FALSE),'prepareData' => $prepareData));
        }
    }

    function switchCurrentUniqueId(){
        $uniqueId = $this->input->post('uniqueId');
        $this->session->set_userdata('currentUniqueId',$uniqueId);
        echo $this->session->userdata('currentUniqueId');
    }

    function checkMaxNumAppts(){
        // CHECK MAXIMUM
        $booking_data = $this->session->userdata('booking_data');
        //var_dump($booking_data);
        //$branchId = $this->input->get_post('branch_id',true);
        $maxBookingLimit = $this->db->get_where('config',array('code' => 'max_num_booking'))->row_array();
        if(!empty($maxBookingLimit) && count($booking_data) >= intval($maxBookingLimit['value'])){
            $this->ajax_content(REQUEST_FAIL,'Sorry, you only can book maximum '.$maxBookingLimit['value']. ' appointment at a same time.');
            exit;
        }
        else{
            $this->ajax_content(REQUEST_SUCCESS);
        }
    }

    function get_step(){
        $step = $this->input->post('step');
        $previous = $this->input->post('previous');
        $this->session->set_userdata('booking_step',$step);
        if(!empty($previous)){
            $this->session->set_userdata('booking_previous',$previous);
        }
        $data = $this->input->post('data')?json_decode($this->input->post('data')):array();

        // CHECK CTM INFORMATION
        if(isset($data->ctm_information) && !empty($data->ctm_information)){
            if(substr($data->ctm_information->phone, 0,2) != 65){
                $data->ctm_information->phone = '65' . $data->ctm_information->phone;
                $logged_user = $this->session->userdata('login');
                if(!empty($logged_user) && $logged_user->id != '') {
                    $this->db->update('customer', array('mobile_number' => $data->ctm_information->phone), array('id' => $logged_user->id));
                    $logged_user->mobile_number = $data->ctm_information->phone;
                    $this->session->set_userdata('login', $logged_user);
                }
            }
            $this->session->set_userdata('ctm_information',$data->ctm_information);
            unset($data->ctm_information);
        }

        // CHECK MULTIBOOKING
        $isMultiBooking = isset($data->type) && $data->type === 'MORE' ? true : false;
        if($isMultiBooking){
            unset($data->type);
        }

        // CHECK RESCHEDULE
        $is_check = $this->session->userdata('is_reschedule');
        if(!empty($is_check)) {
            if(!empty($data)){
                $temp = $this->session->userdata('is_reschedule');
                foreach($data as $key => $item){
                    $temp->{$key} = $item;
                }
            }
            $data = $this->session->userdata('is_reschedule');
            if(empty($data->currentUniqueId)){
                $data->currentUniqueId = $this->session->userdata('currentUniqueId');
            }
        }
        if(count($data) > 0) {
            $currentUniqueId = $data->currentUniqueId;
            unset($data->currentUniqueId);
            if($currentUniqueId != $this->session->userdata('currentUniqueId')){
                $this->session->userdata('currentUniqueId',$currentUniqueId);
            }
            elseif(empty($currentUniqueId) || !$currentUniqueId){
                $currentUniqueId = uniqid('book_');
                $this->session->set_userdata('currentUniqueId', $currentUniqueId);
            }
            $old_data = $this->session->userdata('booking_data');

            $item_data = new stdClass();
            if (empty($old_data)) {
                $old_data[$currentUniqueId] = $item_data;
            }
            foreach ($data as $key => $item) {
                $item_data->$key = $item;
            }
            if(is_array($old_data)){
                $old_data[$currentUniqueId] = $item_data;
            }
            else{
                $old_data = array($currentUniqueId => $item_data);
            }
            $this->session->set_userdata('booking_data', $old_data);
        }
        else{
            $booking_data = $this->session->userdata('booking_data');
            if (count($booking_data) <= 0) {
                $this->session->set_userdata('currentUniqueId', uniqid('book_'));
            }
        }

        if(!$isMultiBooking){
            $step = $this->steps[$step]['module'];
            $data = $this->load->controller_model($this->moduleName)->{'get_data_'.$step}($data);
        }
        else{
            $step = $this->steps[1]['module'];
            $data = $this->load->controller_model($this->moduleName)->{'get_data_'.$step}();
            $this->session->set_userdata('currentUniqueId', uniqid('book_'));
        }

        if(isset($data['error'])){
            $this->ajax_content(REQUEST_FAIL,$data['error']);
            return;
        }

        $pending_appointment = $this->session->userdata('pending_appointment');
        if(empty($pending_appointment)){
            $pending_appointment = '';
        }

        $result = array(
            'html'                  => $this->content($this->moduleName.'/'.$step,$data),
            'currentUniqueId'       => $this->session->userdata('currentUniqueId'),
            'session'               => $this->session->userdata('booking_data'),
            'step'                  => $step,
            'pending_appointment'   => $pending_appointment,
            'ctm_information'       => $this->session->userdata('ctm_information')
        );
        if($this->session->userdata('booking_step') == 6 && !$isMultiBooking){
            $pending_id = $this->session->userdata('pending_appointment')['pending_id'];
            $this->load->controller_model($this->moduleName)->delete_pending_appointment($pending_id);
            $this->session->set_userdata('booking_step','');
            $this->session->set_userdata('pending_appointment','');
            $this->session->set_userdata('booking_data','');
            $this->session->set_userdata('currentUniqueId','');
            $this->session->set_userdata('ctm_information','');
        }
        $this->ajax_content(REQUEST_SUCCESS,'',$result);
    }

    function print_booking($code){
        $book = $this->load->controller_model($this->moduleName)->get_booking_for_save_data($code);

        if(count($book)){
            echo $this->content($this->moduleName.'/print_booking',array('book'=>$book));
        }else{
            echo "No booking with this code";
        }
    }

    function save_booking($code){
        $book = $this->load->controller_model($this->moduleName)->get_booking_for_save_data($code);
        $branch_info = $this->load->table_model('branch')->get(array('name' => $book->branch_name));
        $book->branch_address = $branch_info[0]->address;
        $book->branch_phone = $branch_info[0]->phone_number;
        $book->branch_email = $branch_info[0]->email;
        if(count($book)){
            $this->load->view('contents/'.$this->system.'/'.$this->moduleName.'/save_booking_new',array('book'=>$book),true);
        }else{
            echo "No booking with this code";
        }
    }

    function send_booking($code){
        $book = $this->load->controller_model($this->moduleName)->get_booking_for_save_data($code);
        $branch_info = $this->load->table_model('branch')->get(array('name' => $book->branch_name));
        $book->branch_address = $branch_info[0]->address;
        $book->branch_phone = $branch_info[0]->phone_number;
        $book->branch_email = $branch_info[0]->email;
        $book->logged_user = $this->session->userdata('login');
        $book->baseurl = base_url();
        $book->is_valid = $this->check_valid_cancelapp($book->id);

        if($book){
            $to = $book->email;
            $ms = '<div style="color: #593729;font-size: 14px;">Your appointment has been confirmed</div><div style="color: #b4743b;font-size: 14px;">An email has been sent</div>';
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "Appointment confirm with Healing Touch";
                $body = $this->load->email_content('send_booking', array('book'=>$book));
                $this->email->from($branch_info[0]->email,"Healing Touch Spa - ".$branch_info[0]->name);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    $this->load->controller_model('customers')->UpdateTemp($book->customer_id,array(
                        'send_mail' => 1));

                    $this->ajax_content(REQUEST_SUCCESS);
                }else{
                    $this->load->controller_model('customers')->UpdateTemp($book->customer_id,array(
                        'send_mail' => 0));
                    $this->ajax_content(REQUEST_FAIL,"Sending Email error");
                }
            }

        }else{
            $this->ajax_content(REQUEST_FAIL,"No booking with this code");
        }
    }

    function get_avail_employee(){
        $data           = $this->input->post();
        $ret_data = $this->load->controller_model($this->moduleName)->get_branch_employee($data);
        $this->ajax_content(REQUEST_SUCCESS,'',$ret_data);
    }

    function get_available_timeslot(){
        $data = $this->input->post();
        if($data['employee_id'] == -1){
            $ret_data = $this->load->controller_model($this->moduleName)->get_no_preference_timeslot($data);
        }
        else{
            $ret_data = $this->load->controller_model($this->moduleName)->get_available_timeslot($data);
        }

        $this->ajax_content(REQUEST_SUCCESS,'',$ret_data);
    }

    function check_avail_appointment(){
        $data = $this->input->post();
        if($data['data']['staff_id'] == -1) {
            $this->ajax_content(REQUEST_SUCCESS);
        }
        else {
            $ret_data = $this->load->controller_model($this->moduleName)->check_avail_appointment($data['data']);
            if ($ret_data) {
                $this->ajax_content(REQUEST_SUCCESS);
            } else {
                $this->ajax_content(REQUEST_FAIL,'This employee is not available for this service at this time. Please choose another time');
            }
        }
    }

    function delete_pending_appointment(){
        $data = $this->input->post();
        $ret_data = $this->load->controller_model($this->moduleName)->delete_pending_appointment($data['pending_id']);
        if($ret_data['status'])
            $this->ajax_content(REQUEST_SUCCESS,'',$ret_data);
        else{
            $this->ajax_content(REQUEST_FAIL,'Error when delete pending appointment');
        }
    }

    function get_time_slot(){
        $date = $this->input->post();
        $ret_data = $this->load->controller_model($this->moduleName)->get_time_slot($date['date']);
        if($ret_data['status'])
            $this->ajax_content(REQUEST_SUCCESS,'',$ret_data);
        else{
            $this->ajax_content(REQUEST_FAIL,'Error when delete pending appointment');
        }
    }

    function get_branch_info(){
        $branch_id = $this->input->post("id");
        $ret_data = $this->load->controller_model($this->moduleName)->getCurrentBranch($branch_id);
        $this->ajax_content(REQUEST_SUCCESS,'',$ret_data);
    }


    function update_first_login($cid){
        $this->load->controller_model('customers')->update_first_login($cid);
    }

    function get_cancel_app($bid, $isLogged){
        $cancel_app = $this->load->controller_model($this->moduleName)->get_cancel_app($bid, $isLogged);
        return $cancel_app;
    }

    function check_valid_cancelapp($bid, $isLogged){
    //    $valid_time = $this->system_config->get('cancel_reschedule_time');
        $cancel_app = $this->load->controller_model($this->moduleName)->get_cancel_app($bid, $isLogged);
        if(empty($cancel_app))
            return false;
    //  $allow_time = $this->system_config->get('cancel_reschedule_time',array('login_as_type' => Permission_Value::BRANCH, 'branch_id' => $cancel_app[0]->branch_id));
        $allow_time = 2;
        $current_hour = get_database_date();
        $datetime1 = strtotime($current_hour);
        $datetime2 = strtotime($cancel_app[0]->start_time);

        $secs = $datetime2 - $datetime1;// == <seconds between the two times>
        $hours = $secs / 3600;
        if($hours >= $allow_time)
            return true;
        else
            return false;
    }

    function cancelapp(){
        $bid = $this->session->userdata('cancel_app_id');
        $this->session->set_userdata('cancel_app_id','');
        $update_status = $this->load->controller_model($this->moduleName)->cancel_app($bid);
        if($update_status)
            $this->ajax_content(REQUEST_SUCCESS,'','success');
        else
            $this->ajax_content(REQUEST_FAIL,'','failed');
    }

    function check_valid_editapp()
    {
        $rid = $this->input->post('aid');
        $isLogged = $this->input->post('isLogged');
        if (isset($rid) && $rid != "") {
            if (ctype_digit($rid)) {
                $is_valid = $this->check_valid_cancelapp($rid, $isLogged);
                $appdata["app_details"] = $this->get_cancel_app($rid, $isLogged);
                if (empty($appdata["app_details"]))
                    $this->ajax_content(REQUEST_FAIL, 'Booking not found', 'failed');
                else {
                    if (!$is_valid)
                        $this->ajax_content(REQUEST_FAIL, 'Cannot reschedule this booking', 'failed');
                    else {
                        $newdata = new stdClass;
                        $newdata->scalar = "";
                        $newdata->bid = $rid;
                        $newdata->branch_id = $appdata["app_details"][0]->branch_id;
                        $newdata->service_id = $appdata["app_details"][0]->service_id;
                        $newdata->staff_id = $appdata["app_details"][0]->staff_id;
                        $newdata->date_time = get_user_date($appdata["app_details"][0]->start_time, '', 'Y-m-d H:i');
                        $newdata->ctm_infomation = new stdClass;
                        $newdata->ctm_infomation->firstname = $appdata["app_details"][0]->cfirstname;
                        $newdata->ctm_infomation->lastname = $appdata["app_details"][0]->clastname;
                        $newdata->ctm_infomation->phone = $appdata["app_details"][0]->cnumber;
                        $newdata->ctm_infomation->email = $appdata["app_details"][0]->cemail;
                        $newdata->ctm_infomation->comment = $appdata["app_details"][0]->comment;

                        $currentUniqueId = uniqid('book_');
                        $newdata->currentUniqueId = $currentUniqueId;
                        $appdata["app_details"][0]->currentUniqueId = $currentUniqueId;

                        $this->session->set_userdata('currentUniqueId', $currentUniqueId);
                        $this->session->set_userdata('booking_data', $newdata);
                        $this->session->set_userdata('is_reschedule', $newdata);
                        $this->session->set_userdata('link_baseurl', base_url());
                        $this->session->set_userdata('ctm_information',$newdata->ctm_infomation);

                        $this->ajax_content(REQUEST_SUCCESS, '', $appdata["app_details"][0]);
                    }
                }
            }
        }
    }

    function resetBooking(){
        $this->session->set_userdata('booking_step','');
        $this->session->set_userdata('pending_appointment','');
        $this->session->set_userdata('booking_data','');
        $this->session->set_userdata('currentUniqueId','');
        $this->session->set_userdata('ctm_information','');
        $this->session->set_userdata('is_reschedule', '');
        $this->ajax_content(REQUEST_SUCCESS, '', array());
    }
}