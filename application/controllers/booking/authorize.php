<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authorize extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "authorize";
    }

    function index(){
        redirect(site_url('booking/home'));
    }

    function login_form(){
        $user = $this->user_check->checkLoggedIn(false);
        if($user){
            redirect(site_url('booking/book'));
        }
        $this->page('authorize/index',array());
    }

    function loadRegisterForm(){
        $html = $this->load->view('contents/booking/authorize/registerForm',array(),true);
        $this->ajax_content(REQUEST_SUCCESS,'',array('html' => $html));
        exit;
    }

    /**
     * Customer register at booking site
     * @throws Exception
     */
    function register(){
        $data = new stdClass();
        $data->first_name = $this->input->get_post('reg_name');
        $data->email = trim($this->input->get_post('email_res'));
        $data->password = md5(date('Y-m-d H:i:s'));
        $data->mobile_number = $this->input->get_post('mobilenum');
        $data->country_calling_code = $this->input->get_post('country_calling_code');
        $data->status = 2;
        $data->customer_type = CUSTOMER_TYPE('Member');

        // update detect phone number
//        $data->mobile_number = convert_phonenumber($data->mobile_number);

        // Case 1
        // Email is already exist in the database
        $customer_data1 = $this->load->table_model('customer')->get(array('email' => $data->email,'status' => CUSTOMER_STATUS(array("Active","Hold"))));
        if(count($customer_data1) > 0){
            echo json_encode(array('status' => false, 'message' => 'Your email already exists as one of our customers, please go back to the previous step to request for a verification email.'));
            exit;
        }

        // case 2 : phoneNumber is already exist in the database
        $customer_data = $this->load->table_model('customer')->get(array('mobile_number' => $data->mobile_number,'status' => CUSTOMER_STATUS(array("Active","Hold"))));
        $customer_data_w_cc = $this->load->table_model('customer')->get(array('mobile_number' => $data->country_calling_code.$data->mobile_number, 'status' => CUSTOMER_STATUS(array("Active","Hold"))));
        if(count($customer_data) > 0 || count($customer_data_w_cc) > 0){
            echo json_encode(array('status' => false, 'message' => 'Your mobile number already exists in our system, please check your email address as an existing customer or contact/visit us at one of our branches for more information.'));
            exit;
        }

        // case : process to send email
        $this->db->trans_start();
        $data->mobile_number = $data->country_calling_code.$data->mobile_number;
        $result = $this->load->controller_model('customers')->insert($data,Permission_Value::ADMIN);
        $customerData = $this->load->table_model('customer')->get(array('id'=>$result, 'status' => CUSTOMER_STATUS("Hold")))[0];

        // generate the one time password
        $otpId = $this->_generateOneTimePwd($customerData);

        // Check rollback
        if($otpId){
            $this->db->trans_complete();
        }
        else{
            $this->db->trans_rollback();
            throw new Exception('insert new customer information or generate one time password got problem');
            exit;
        }

        // send email to the customer
        $message = $this->send_email($customerData);
        if($message === 1){
            echo json_encode(array('status' => true, 'message' => 'You have successfully registered. Please confirm your email to activate your account'));exit;
        }else{
            echo json_encode(array('status' => false, 'message' => 'Can not send email')); exit;
        }
    }

    function send_email($customer){
        if(isset($customer) && $customer !== null) {
            $to = $customer->email;
            if (isset($to) && $to != "") {
                // get OTP to insert into the email.
                $temp = $this->db->get_where('customer_onetime_passwd',array('customer_id' => $customer->id))->row_array();
                if(!empty($temp)){
                    $customer->otp = $temp['one_time_pwd'];
                }
                // begin sending email
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "Please confirm your Email - Healing Touch";
                $body = $this->load->email_content('customer_confirm', $customer);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return 1;
                }
            }
        }
        return "The email can't be sent";
    }

    function confirm($code){
        $customer_data = $this->load->table_model('customer')->get(array('code'=>$code, 'status' => CUSTOMER_STATUS("Hold")));
        $data = new stdClass();
        if(count($customer_data)){
            $customer_data = $customer_data[0];
            $result = $this->load->table_model('customer')->update($customer_data->id,array('status' => CUSTOMER_STATUS('Active')));
            if($result){
                $data->message = 1;
                $user_session = $customer_data;
                $this->session->set_userdata('login',$user_session);
                $data->message = 1;
            }else{
                $data->message = "Oops. Something go wrong";
            }
        }else{
            $data->message = "No user code exists or this account has been activated";
        }
        $this->page('authorize/announcement',$data);
    }

    function login(){
        $data = array();
        $data['email'] = $this->input->post('_email');
        $data['password'] = md5($this->input->post('_password'));

        // CHECK OTP valid password
        $check = $this->db->get_where('customer_onetime_passwd', array(
            'customer_email' => $data['email'],
            'new_pwd' => 0));
        if($check->num_rows()){
            $this->ajax_content(REQUEST_SUCCESS,'', array('valid_email' => site_url('booking/authorize/verify_email?cid='.$check->row_array()['id'])));
            exit;
        }

        // check OTP valid phoneNumber
        $check = $this->db->get_where('customer_onetime_passwd', array(
            'customer_email' => $data['email'],
            'is_valid_mobile' => 0));
        if($check->num_rows()){
            $this->ajax_content(REQUEST_SUCCESS,'', array('valid_email' => site_url('booking/authorize/verifyMobileNum?cid='.$check->row_array()['id'])));
            exit;
        }


        $user = $this->load->table_model('customer')->get($data);

        if(!count($user)){
            unset($data['email']);
            $data['recovery_email'] = $this->input->post('_email');
            $user = $this->load->table_model('customer')->get($data);
        }

        if(count($user)){
            // VALID NEW PROCESS
            $checkNewProcess = $this->db->get_where('customer_onetime_passwd', array(
                'customer_id' => $user[0]->id))->row_array();
            if(empty($checkNewProcess)){
                $this->ajax_content(REQUEST_FAIL,'Please validate your account first.');
                exit;
            }

            $this->session->set_userdata('login',$user[0]);
            $this->session->set_userdata('is_customer_logged',1);
            $booking_data = $this->session->userdata('booking_data');
            if(empty($booking_data)){
                $booking_data = new stdClass();
            }
            $booking_data->is_login = 1;
            $booking_data->ctm_infomation = array(
                'firstname' => $user[0]->first_name,
                'lastname'  => $user[0]->last_name,
                'phone'     => $user[0]->mobile_number,
                'email'     => $user[0]->email,
                'comment'   => ''
            );
            $this->session->set_userdata('booking_data', $booking_data);
            $this->ajax_content(REQUEST_SUCCESS);
            //redirect(site_url('booking/book'));
        }else{
            $this->ajax_content(REQUEST_FAIL,'Incorrect Email or Password');
        }
    }

    function logout(){
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('booking_data');
        $this->session->unset_userdata('is_customer_logged');
        redirect(site_url('booking/book'));
    }

    function forgot_login_email(){
        $recovery_email = $this->input->post('email');
        $customer_model = $this->load->table_model('customer');
        $message = 0;
        if($recovery_email){
            $user = $customer_model->get(array('recovery_email'=>$recovery_email));
            if(count($user)){
                $result = $this->send_forgot_email($user[0]->recovery_email,$user[0]->email);
                if($result !== false){
                    $message = 1;
                }else{
                    $message = "Can not send email to customer";
                }
            }else{
                $message = "No such Email exists";
            }
        }
        $this->page('authorize/forgot_login_email',array('message'=>$message));
    }

    function forgot_id(){
        $mobile_number = $this->input->post('mobile_number');
        $customer_model = $this->load->table_model('customer');
        $message = 0;
        $result = -1;
        if($mobile_number){
            $user = $customer_model->get(array('mobile_number'=>$mobile_number));
            if(count($user)){
                if($user[0]->email == ''){
                    $result = 0;
                }
                else{
                    $result = 1;
                    $message = $user[0]->email;
                }
            }else{
                $message = "No such Mobile number exists";
                $result = 0;
            }
        }
        $this->page('authorize/forgot_id',array('result' => $result, 'message'=>$message));
    }

    function forgot(){
        $email = $this->input->post('email');
        $customer_model = $this->load->table_model('customer');
        $message = 0;

        $customer = $this->db->get_where('customer_onetime_passwd',array('customer_email' => $email))->row_array();
        if(!empty($customer)){
            if($email){
                $user = $customer_model->get(array('email'=>$email));
                if(count($user)){
                    $pass = substr(md5(rand(1,100)),0,8);
                    $result = $customer_model->update($user[0]->id,array('password' => md5($pass)));
                    if($result){
                        $result = $this->send_forgot_pass_email($user[0]->email,$pass, $user[0]);
                        if($result !== false){
                            $message = 1;
                        }else{
                            $message = "Can not send email to customer";
                        }
                    }
                    else{
                        $message = "Something wrong when changing your pass";
                    }
                }else{
                    $user = $customer_model->get(array('recovery_email'=>$email));
                    if(count($user)){
                        $pass = substr(md5(rand(1,100)),0,8);
                        $result = $customer_model->update($user[0]->id,array('password' => md5($pass)));
                        if($result){
                            $result = $this->send_forgot_pass_email($user[0]->email,$pass, $user[0]);
                            if($result !== false){
                                $message = 1;
                            }else{
                                $message = "Can not send email to customer";
                            }
                        }
                        else{
                            $message = "Something wrong when changing your pass";
                        }
                    }
                    else
                        $message = "No such Email exists";
                }
            }
        }
        else{
            $message = "Your email is not registered with an online account or it has not been verified, please verify or register.";
        }

        $this->page('authorize/forgot',array('message'=>$message));
    }

    function send_forgot_pass_email($user_email,$user_pass, $user){
        $data = new stdClass();
        $data->email = $user_email;
        $data->password = $user_pass;
        $data->id = $user->id;
        $data->code = $user->code;

        if(isset($user_email) && $user_email !== null) {
            $to = $user_email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "Your new password";
                $body = $this->load->email_content('customer_forgot_pass', $data);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return 1;
                }
            }
        }
        return "Can not send email";
    }

    function send_forgot_email($recovery_email,$login_email){
        $data = new stdClass();
        $data->recovery_email = $recovery_email;
        $data->login_email = $login_email;
        if(isset($recovery_email) && $recovery_email !== null) {
            $to = $recovery_email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "Healing Touch Spa - Your login email";
                $body = $this->load->email_content('customer_forgot_email', $data);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return 1;
                }
            }
        }
        return "Can not send email";
    }

    function send_password_on_registration($email,$password,$uid){
        $data = new stdClass();
        $data->email = $email;
        $data->password = $password;
        $data->userid = $uid;
        if(isset($data->email) && $data->email !== null) {
            $to = $data->email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "New Password from Healing Touch Online Booking Site";
                $body = $this->load->email_content('registration_forgot_pass', $data);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return true;
                }
                else{
                    return false;
                }
                //if(!$this->email->send()) {
                //    echo 'Message could not be sent.';
                //    echo 'Mailer Error: ' . $this->email->print_debugger();
                //}
            }
        }
    }

    function change_password(){
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $user_session = $this->session->userdata('login');
        if($old_password != false && $new_password != false && $user_session){
            $customer_model = $this->load->table_model('customer');
            $user = $customer_model->get(array('id'=>$user_session->id,'password'=>md5($old_password)));
            if(count($user)){
                $result = $customer_model->update($user[0]->id,array('password'=>md5($new_password)));
                $this->session->set_userdata($customer_model->get(array($user[0]->id))[0]);
                $this->ajax_content(REQUEST_SUCCESS,'Password has been changed successfully');
            }else{
                $this->ajax_content(REQUEST_FAIL,'Old password is wrong');
            }
        }else{
            $this->page('authorize/change_password',array('Missing information'));
        }
    }

    function update_password($id=""){
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $customer_model = $this->load->table_model('customer');

        $id = explode("_", $id);
        if(count($id) >= 2){
            $uid = $id[0];
            $old_pass = $id[1];
            if($old_password != false && $new_password != false){
                $user = $customer_model->get(array('id'=>$uid,'password'=>$old_password,'password_hash' => 'sent_to_mail'));
                if(count($user)){
                    $result = $customer_model->update($uid,array('password'=>md5($new_password),'password_hash' => NULL));
                    $this->ajax_content(REQUEST_SUCCESS,'Password has been changed successfully');
                }else{
                    $this->ajax_content(REQUEST_FAIL,'Old password is wrong');
                }
            }
            else{
                $user = $customer_model->get(array('id'=>$uid,'password'=>$old_pass,'password_hash' => 'sent_to_mail'));
                if(count($user)){
                    $this->page('authorize/update_password',array('oldpass' => $user[0]->password,'uid' => $user[0]->id));
                }
                else
                    echo "Expired link";        
            }
        }
        else{
            echo "Invalid link";
        }
    }

    function change_pin(){
        $user_session = $this->session->userdata('login');
        if($user_session){
            if(isset($_POST["new_pin"])){
                $customer_model = $this->load->table_model('customer');
                $user = $customer_model->get(array('id'=>$user_session->id));
                $new_pin = $this->input->post('new_pin');
                $result = $customer_model->update($user[0]->id,array('pin'=>($new_pin)));
                $this->session->set_userdata($customer_model->get(array($user[0]->id))[0]);
                $this->ajax_content(REQUEST_SUCCESS, "User's pin has been updated");
            }
            else
                $this->page('authorize/change_pin',array('Missing information'));
        }
    }

    function change_information(){
        $user = $this->user_check->checkLoggedIn(false);
        $user = $this->load->table_model('customer')->get(array('id'=>$user->id))[0];
        if(!$user){
            redirect(booking_url('authorize'));
        }

        $post_data = json_decode($this->input->post('data'));

        if($post_data){
            if(!$post_data->mobile_number){
                $this->ajax_content(REQUEST_FAIL,'Mobile number can not be empty.'); exit;
            }
            if(!$post_data->country_calling_code){
                $this->ajax_content(REQUEST_FAIL,'Calling country code can not be empty.'); exit;
            }

            $post_data->birthday = date('Y-m-d',strtotime($post_data->birthday));
            $post_data->mobile_number = $post_data->country_calling_code . $post_data->mobile_number;
            $result = $this->load->table_model('customer')->update($user->id,$post_data);
            $user = $this->load->table_model('customer')->get(array('id'=>$user->id));
            $this->session->set_userdata('login',$user[0]);
            if($result === true){
                $this->ajax_content(REQUEST_SUCCESS,"User's information has been changed successfully");
            }else{
                $this->ajax_content(REQUEST_FAIL,$result);
            }
        }
        else{
            $user->mobile_number_detect = detect_phonenumber($user->mobile_number);
            $this->page($this->moduleName.'/change_information',array('user'=>$user));
        }
    }

    function randomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function checkEmailRegistration(){
        $email = $this->input->get_post('email','true');
        $this->db->trans_start();
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            echo json_encode(array('status' => false, 'message' => 'Please input correct email format.'));
            exit;
        }

        // CHECK EMAIL EXIST OR NOT
        // , 'status' => 1
        $checking = $this->db->get_where('customer', array('email' => $email))->result_array();

        if(count($checking) == 0){
            echo json_encode(array('status' => false, 'message' => 'The email does not exist in our system, please click "HERE" below to register an account with us.'));
            exit;
        }

        // CHECK DUPLICATE CUSTOMER EMAIL ON SYSTEM
        if(count($checking) > 1){
            echo json_encode(array('status' => false, 'message' => 'Your email already exists in our system, please request to reset your password if you have forgotten or contact/visit us at one of our branches for more information.'));
            exit;
        }

        // CHECK EMAIL WAS EXIST OR NOT
        if(count($checking) == 1){
            // EXIST CUSTOMER
            $oneTimeCheck = $this->db->select('customer_onetime_passwd.*')
                                    ->join('customer_onetime_passwd','customer.id = customer_onetime_passwd.customer_id')
                                    ->where(array('customer_onetime_passwd.customer_email' => $email))
                                    ->get('customer')->row_array();
            if(!empty($oneTimeCheck)){
                // EXIST VERIFY INFORMATION
                if($oneTimeCheck['is_valid'] == 0) {
                    echo json_encode(array('status' => false, 'message' => 'Your email is not verified. Please check your email inbox and follow the instructions to verify your email.'));
                    exit;
                }

                if($oneTimeCheck['new_pwd'] == 0) {
                    echo json_encode(array('status' => false, 'message' => 'Your new Password has not created. Please check your email inbox and follow the instruction to create your new password.'));
                    exit;
                }

                if($oneTimeCheck['is_valid_mobile'] == 0) {
                    echo json_encode(array('status' => false, 'message' => 'You have verified your email and created your password but have missed the step to verify your mobile number, please login to verify your mobile number.'));
                    exit;
                }

                if($oneTimeCheck['is_valid'] == 1 && $oneTimeCheck['new_pwd'] == 1 && $oneTimeCheck['is_valid_mobile'] == 1){
                    echo json_encode(array('status' => false, 'message' => 'Your email and mobile number was verifed, please contact us or visit one of our branches for more information.'));
                    exit;
                }
            }
            else{
                // DOES NOT VERIFIED
            }
        }
        else{
            // DOES NOT EXIST CUSTOMER
        }


        // PROCESS ONETIME PASSWORD
        $oneTimePwd = $this->_generateOneTimePwd($checking[0]);
        $data = $this->db->select('customer_onetime_passwd.*, customer.first_name as customer_name')
                         ->join('customer', 'customer_onetime_passwd.customer_id = customer.id')
                         ->where(array('customer_onetime_passwd.id' => $oneTimePwd))
                         ->get('customer_onetime_passwd')->row_array();
        if(isset($data['customer_email']) && $data['customer_email'] !== null && $data['customer_email'] != '') {
            // GET BRANCHGROUP NAME IF LOGGED
//            if(isset($this->session->all_userdata()['staff'])){
//                $branchGroup = $this->session->all_userdata()['staff']['login']->branch_group_id;
//                $branchGroup = $this->select(array('branch_group' => array('name')))
//                                    ->from(array('branch_group' => array('table' => 'branch_group')))
//                                    ->where(array('id' => $branchGroup))->row_array();
//                $data['branch_group'] = $branchGroup['name'];
//            }

            $this->load->library('email');
            $this->email->clear(true);
            $data['branch_group'] = 'HealingTouchSpa';
            $body = $this->load->email_content('customer_onetime_password', $data);
            $this->email->to($data['customer_email']);
            $this->email->subject("Verify email at Healingtouchspa");
            $this->email->message($body);
            if ($this->email->send()) {
                echo json_encode(array('status' => true, 'message' => 'We just send an email to your inbox to verify your email, please check and follow the instructions.'));
            }
            else{
                $this->db->delete('customer_onetime_passwd', array('id' => $data['id']));
                $this->email->print_debugger();
                echo json_encode(array('status' => false, 'message' => 'Cant send one time password to your email. please contact us or visit one of our branches for more information.'));
            }
        }
        $this->db->trans_complete();
    }

     function _generateOneTimePwd($customer,$is_register = FALSE)
     {
         $customer = (array)$customer;
         if ($is_register == TRUE) {
             $is_register = 1;
         } else {
             $is_register = 0;
         }
         $oneTimePwd = strtoupper(substr(md5(strtotime('NOW')), 0, 6));
         $this->db->insert('customer_onetime_passwd', array(
             'customer_id' => $customer['id'],
             'customer_email' => $customer['email'],
             'one_time_pwd' => $oneTimePwd,
             'created_date' => date('Y-m-d H:i:s'),
             'is_valid' => $is_register
         ));

         return $this->db->insert_id();
     }

    function verify_email(){
        $data['cid'] = $this->input->get('cid',true);
        $data['uid'] = $this->input->get('uid',true);
        $data['otp'] = $this->input->get('otp',true);

        if(!empty($data['cid'])){
            $check['id'] = $data['cid'];
        }
        elseif(!empty($data['uid']) && !empty($data['otp'])){
            $check['customer_id'] = $data['uid'];
            $check['one_time_pwd'] = $data['otp'];
        }
        $checkOTP = $this->db->get_where('customer_onetime_passwd', $check)->row_array();

        $html['is_valid'] = false;
        $html['new_pwd'] = false;
        if(!count($checkOTP)){
            $html['is_valid'] = false;
            $html['message'] = 'OTP for this email does not exist, please check again or contact with us for more information.';
        }
        if($checkOTP['is_valid'] == 1){
            $html['is_valid'] = true;
            if($checkOTP['new_pwd']) {
                $html['new_pwd'] = true;
                $html['message'] = 'This email already confirmed and changed the OTP password. No need to reconfirm again.';
            }
            else{
                $html['new_pwd'] = false;
                $html['message'] = 'Your email has been verified.';
            }
        }
        else {
            $update = $this->db->update('customer_onetime_passwd', array('is_valid' => 1, 'valid_date' => date('Y-m-d H:i:s')), $check);
            $html['is_valid'] = true;
            $html['message'] = 'Your email has been verified.';
        }
        $html['id'] = $checkOTP['id'];

        $this->page('authorize/verify_email',$html);
    }

    function sendSmsOTP(){
        $customerID = $this->input->get_post('customer_id',true);
        if($customerID != '' && is_numeric($customerID)){
            $customerData = $this->db->get_where('customer', array('id' => $customerID))->row_array();
            if(!empty($customerData)) {
                $id = $this->_generateOneTimePwd($customerData);
                $opt = $this->db->get_where('customer_onetime_passwd',array('id' => $id))->row_array();
                $this->load->library('Smslib');
                $this->smslib->sendConfirmEmailOTP($customerData,$opt['one_time_pwd']);
                echo json_encode(array('status' => true, 'message' => 'OTP was sending to customer email.'));
                exit;
            }
            else{
                echo json_encode(array('status' => false, 'message' => 'The system can not find the customer with the id '.$customerID));
                exit;
            }
        }
        else{
            echo json_encode(array('status' => false, 'message' => 'Customer Id was empty'));
            exit;
        }

    }

    function validNewPwd(){
        $cop  = $this->input->get_post('cop',true);
        $pwd1 = $this->input->get_post('pwd1',true);
        $pwd2 = $this->input->get_post('pwd2',true);

        if($pwd1 == ''){
            echo json_encode(array('status' => FALSE, 'message' => 'New Password field should not be empty.'));
            exit;
        }
        if($pwd2 == ''){
            echo json_encode(array('status' => FALSE, 'message' => 'Retype New Password field should not be empty.'));
            exit;
        }
        if($pwd1 != $pwd2){
            echo json_encode(array('status' => FALSE, 'message' => 'Your retype password field does not match.'));
            exit;
        }
        if($cop == ''){
            echo json_encode(array('status' => FALSE, 'message' => 'Cant get the customer OTP id'));
            exit;
        }

        $this->db->update('customer_onetime_passwd', array('new_pwd' => 1), array('id' => $cop));
        $customer = $this->db->get_where('customer_onetime_passwd',array('id' => $cop))->row_array();
        $this->db->update('customer', array('password' => md5($pwd1)), array('id' => $customer['customer_id']));

        $this->ajax_content(REQUEST_SUCCESS,'', array('valid_email' => site_url('booking/authorize/verifyMobileNum?cid='.$cop)));
        exit;
    }

    function verifyMobileNum(){
        $cid  = $this->input->get_post('cid',true);
        $customerId = $this->db->get_where('customer_onetime_passwd',array('id' => $cid))->row_array();
        $customerData = $this->db->get_where('customer', array('id' => $customerId['customer_id']));
        $html['id'] = $cid;
        $html['customerData'] = $customerData;
        $html['message'] = 'New password updated.<br>Please verify your mobile number to finish.';
        $this->page('authorize/verify_mobile',$html);
    }

    function verifyMobileNumAction(){
        $cop = $this->input->get_post('cop',true);
        $mobileNumber    = $this->input->get_post('mobile_number',true);

        // SEARCH ON DATABASE
        $customerData = $this->db->get_where('customer_onetime_passwd', array('id' => $cop))->row_array();
        $customerData = $this->db->get_where('customer', array('id' => $customerData['customer_id']))->row_array();

        if(count($customerData)){
            // NO DUPLICATE
            if($customerData['mobile_number'] == '') {
                $this->db->update('customer_onetime_passwd', array('is_valid_mobile' => 1), array('id' => $cop));
                $this->db->update('customer', array('mobile_number' => convert_phonenumber($mobileNumber)), array('id' => $customerData['id']));
                $this->ajax_content(REQUEST_SUCCESS,'');
                exit;
            }
            else{
                if($customerData['mobile_number'] != $mobileNumber){
                    // IF MOBILE NUMBER INSERT IS DIFFIRENT WITH THE NUMBER IN DATABASE
                    $this->ajax_content(REQUEST_FAIL,'This email address is already registered with another mobile number, please provide us your previous mobile number to be verified',array('is_check_previous' => 1));
                    exit;
                }
                else{
                    // IF MOBILE NUMBER INSERT IS THE SAME WITH THE NUMBER IN DATABASE
                    $this->db->update('customer_onetime_passwd', array('is_valid_mobile' => 1), array('id' => $cop));
                    $this->ajax_content(REQUEST_SUCCESS,'');
                    exit;
                }
            }
        }
        else{
            // DUPLICATE
            $this->ajax_content(REQUEST_FAIL,'Can not find the customer information.');
            exit;
        }
    }

    function checkPreviousMobileAction(){
        $previous_number = $this->input->get_post('previous_number',true);
        $cop = $this->input->get_post('cop',true);
        if($previous_number == ''){
            $this->ajax_content(REQUEST_FAIL,'Can not get the previous number sended from the browser.'); exit;
        }
        if($cop == ''){
            $this->ajax_content(REQUEST_FAIL,'Can not get the OTP ID send form the browser.'); exit;
        }

        $id = $this->db->get_where('customer_onetime_passwd', array('id' => $cop))->row_array();
        $check = $this->db->get_where('customer',array('id' => $id['customer_id']))->row_array();
        if($check['mobile_number'] == $previous_number){
            $this->db->update('customer_onetime_passwd', array('is_valid_mobile' => 1), array('id' => $cop));
            $detectPhoneNumber = convert_phonenumber($previous_number);
            if($detectPhoneNumber){
                $this->db->update('customer',array('mobile_number' => $detectPhoneNumber,'customer_type' => 2,'status' => 1), array('id' => $check['id']));
                $this->ajax_content(REQUEST_SUCCESS,''); exit;
            }
            else{
                $this->ajax_content(REQUEST_FAIL, ''); exit;
            }
        }
        else{
            $this->ajax_content(REQUEST_FAIL,'Previous number does not match.'); exit;
        }
    }

    function finishVerifyMobileAction(){
        $selected_number = $this->input->get_post('selected_number',true);
        $cop = $this->input->get_post('cop',true);

        $selected_number = convert_phonenumber($selected_number);

        $this->db->update('customer_onetime_passwd', array('is_valid_mobile' => 1) , array('id' => $cop));
        $cust = $this->db->get_where('customer_onetime_passwd',array('id' => $cop))->row_array();
        $this->db->update('customer',array(
            'country_calling_code' => 65,
            'mobile_number' => $selected_number,
            'customer_type' => 2,
            'status' => 1
        ), array('id' => $cust['customer_id']));

        $this->ajax_content(REQUEST_SUCCESS,'', array('redirect' => base_url().'booking/book'));
    }

}