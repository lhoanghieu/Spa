<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "home";
        $this->pageCode = "home";
        $this->pageName = "Dashboard";
        $this->pageIcon = "fa-home";
    }

    public function index()
    {
        $data = new stdClass();
        $this->load->library('menu_component');
        $menu = new stdClass();
        $menu->topMenu = $this->menu_component->get_data(array('type' => MENU_TYPE('Staff')));
        $data->menus = $menu;
        $data->count_items      = count($this->load->table_model('item')->get());
        $data->count_categories = count($this->load->table_model('category')->get());
        $data->count_employees  = count($this->load->table_model('employee')->get());
        $data->count_customers  = count($this->load->table_model('customer')->get());
        $data->count_sales      = count($this->load->table_model('bill')->get(array('status' => BILL_STATUS('Complete'))));
        $credits                = $this->load->table_model('credit')->get();
        $total = 0;
        foreach($credits as $credit){
            $total += $credit->credit;
        }
        $data->count_credits = $total;
        $this->page($this->moduleName, $data);
    }
}