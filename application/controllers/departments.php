<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:53 PM
 */

class Departments extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "departments";
        $this->pageCode = "departments";
        $this->pageName = "Departments";
        $this->breadcrumbs[] = array('url' => 'departments', 'text' => 'Departments');
    }

}