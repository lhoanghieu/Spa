<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "categories";
        $this->pageCode = "categories";
        $this->pageName = "Categories";
        $this->breadcrumbs[] = array('url' => 'categories', 'text' => 'Categories');
    }

    function index(){
        $this->assets->loadLibrary('dhtmlx_tree');
        parent::index();
    }

    function getByID($id){
        return $this->ajax_content(REQUEST_SUCCESS, '', $this->load->table_model('category')->getByID($id));
    }

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForViewForm($id);
        if($id==0){
            $data->item->show_in_ui = $modelName->getBookingBranch($id,true);
            $data->item->show_price_in_ui = $modelName->getBookingBranch($id,true,true);
        }
        else{
            $data->item->show_in_ui = $modelName->getBookingBranch($id,false);
            $data->item->show_price_in_ui = $modelName->getBookingBranch($id,false,true);
        }
        $data = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

}