<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_Registration extends Booking_Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $this->load->view('contents/customer_registration/index');
    }

}