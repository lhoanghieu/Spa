<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Items extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "items";
        $this->pageCode = "items";
        $this->pageName = "Items";
        $this->breadcrumbs[] = array('url' => 'items', 'text' => 'Items');
    }

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $user = $this->session->userdata('login');
        $data = $modelName->getDataForViewForm($id);
        if($id==0){
            $data->item->show_in_ui = $modelName->getBookingBranch($id,true);
            $data->item->show_price_in_ui = $modelName->getBookingBranch($id,true,true);
            $data->item->last_appointment = $modelName->getItemLastAppointment($id,$user->branch_id);
        }
        else{
            $data->item->show_in_ui = $modelName->getBookingBranch($id);
            $data->item->show_price_in_ui = $modelName->getBookingBranch($id,false,true);
            $data->item->last_appointment = $modelName->getItemLastAppointment($id,$user->branch_id);
            $data->item->restrict_datetime = $modelName->getRestrictDatetime($id,$user->branch_id);
        }

        $data = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function suggestItem(){
        $keyword = $this->input->get('term');
        $modelName = $this->load->controller_model($this->moduleName);

        $suggest = $modelName->getItemSuggestion($keyword);

        echo json_encode($suggest);
    }


    function getItem($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $item = $modelName->getItem($id);
        if($item == null){
            $item = false;
        }
        $this->ajax_content(REQUEST_SUCCESS,"",$item);
    }

    function suggestService(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        $employee_id = $this->input->get_post('employee_id');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion($keyword,array('item.type' => array(ITEM_TYPE('Service'),ITEM_TYPE('Bundle'))));
            if(isset($employee_id) && $employee_id != '' && $employee_id > 0){
                $get_list_service = $this->load->controller_model($this->moduleName)->getListEmployeeService($suggest,$employee_id);
                $suggest = $get_list_service;
            }
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion($keyword,array('item.type' => array(ITEM_TYPE('Service'),ITEM_TYPE('Bundle'))));
        }
        echo json_encode($suggest);
    }
    function suggestProduct(){
        $keyword = $this->input->get_post('term');
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('item.type' => ITEM_TYPE('Product')));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('item.type' => ITEM_TYPE('Product')));
        }
        echo json_encode($suggest);
    }


    function suggestByCategory($category_id){
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion('',array('category_item.category_id'=>$category_id));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion('',array('category_item.category_id'=>$category_id));
        }
        $this->ajax_content(REQUEST_SUCCESS,'',$suggest);
    }

    function disable_item($id){
        try {
            $this->load->controller_model($this->moduleName)->disable_item($id);

            // GET OLD DATA FOR LOGGING
            $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);
            // END GET OLD DATA FOR LOGGING

            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Disable',
                    'object' => $oldRow->code,
                    'module' => 'staff/'.$this->moduleName));

            $this->ajax_content(REQUEST_SUCCESS,'The item has been disabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

    function enable_item($id){
        try {
            $this->load->controller_model($this->moduleName)->enable_item($id);

            // GET OLD DATA FOR LOGGING
            $oldRow = $this->load->controller_model($this->moduleName)->getItem($id);
            // END GET OLD DATA FOR LOGGING

            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Enable',
                    'object' => $oldRow->code,
                    'module' => 'staff/'.$this->moduleName));

            $this->ajax_content(REQUEST_SUCCESS,'The item has been enabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

}