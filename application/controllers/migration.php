<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Migration extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    function run($version = ""){
        $ret = true;
        $msg = "Nothing Run";
        $this->load->library('migration');
        if($version == ""){
            $ret = $this->migration->current();
            $msg = "Database was changed to current version";
        }elseif($version == 'last'){
            $ret = $this->migration->latest();
            $msg = "Database was changed to lastest version";
        }else{
            if(is_numeric($version)){
                $ret = $this->migration->version($version);
                $msg = "Database was changed to version {$version}";
            }
        }
        if(!$ret){
            $msg = $this->migration->error_string();
        }
        exit($msg);
    }
}