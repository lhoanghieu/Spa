<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:53 PM
 */

class Groups extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "groups";
        $this->pageCode = "groups";
        $this->pageName = "Permissions";
        $this->breadcrumbs[] = array('url' => 'groups', 'text' => 'Groups');

    }

    function getPermissionsForm($id){
        $data = $this->load->controller_model($this->moduleName)->getGroupPermission($id);
        $data->current_group_id = $id;
        $ret = new stdClass();
        $ret->content = $this->content($this->moduleName.'/permission',$data, true);
        $ret->option = array(
            'title' => "Permission"
        );
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }


    function edit_permissions(){
        if($this->user_permission->checkPermission('ep',$this->pageCode) == false){
            throw new Exception('You dont have permission to access this form');
        }

        $id = $this->input->post('id');
        $permissions = $this->input->post('permissions');
        $remove_permissions = $this->input->post('removePermissions');

        $modelName = $this->load->controller_model($this->moduleName);
        $modelName->edit_permissions($id,$permissions,$remove_permissions);
        return $this->ajax_content(REQUEST_SUCCESS, "Permissions has been saved.");
    }

    function get_report_permission_form($id){
        if($this->user_permission->checkPermission('ep',$this->pageCode) == false){
            throw new Exception('You dont have permission to access this form');
        }
        $data = $this->load->controller_model($this->moduleName)->get_report_permission_data($id);
        $ret = new stdClass();
        $ret->content = $this->content($this->moduleName.'/report_permission',$data, true);
        $ret->option = array(
            'title' => "Report Permission"
        );
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function edit_report_permission(){
        if($this->user_permission->checkPermission('ep',$this->pageCode) == false){
            throw new Exception('You dont have permission to access this form');
        }

        $id = $this->input->post('id');
        $report_permission = $this->input->post('report_permission');
        $this->load->controller_model($this->moduleName)->edit_report_permission($id,$report_permission);
        return $this->ajax_content(REQUEST_SUCCESS, "Report Permissions has been saved.");

    }

}