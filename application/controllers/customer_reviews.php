<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_Reviews extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "customer_reviews";
        $this->breadcrumbs[] = array('url' => 'customer_reviews', 'text' => 'customer_reviews');
    }

    function getData(){
        $data = $this->load->table_model('customer_reviews')->getData();
        foreach($data as $key => $item){
            $data[$key]['createdAt'] = get_user_date($item['createAt'], '', 'Y-m-d H:i:s');
        }
        echo json_encode(array('data' => $data));
    }

}