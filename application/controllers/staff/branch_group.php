<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch_group extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "branch_group";
        $this->pageCode = "branch_group";
        $this->pageName = "Branch Group";
    }

    function suggest(){
        /**
         * keyword : keyword to search in
         * isMinimal : display in minimal or full fields
         * isFull : display all at once or just some rows
         * isAll : Full permission display or limited permission
         */
        $branch_group_array = $this->load->table_model('employee')->get_branch_group($this->user_check->get('user_id'));
        if( $branch_group_array!=null) {
            $condition['branch_group.id'] =  $branch_group_array;
        }
        $keyword    = $this->input->get_post('term');
        $isMinimal  = to_b($this->input->get_post('minimal'));
        $isFull     = to_b($this->input->get_post('full'));
        $isAll      = to_b($this->input->get_post('all'));
        $isExtend   = to_b($this->input->get_post('extend'));
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword), $condition, $isFull,$isExtend, $isAll);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword), $condition, $isFull,$isExtend,$isAll);
        }
        echo json_encode($suggest);
    }
}