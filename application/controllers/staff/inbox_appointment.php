<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Inbox_Appointment extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "inbox_appointment";
        $this->pageCode = "inbox_appointment";
        $this->pageName = "Inbox Appointment";
        $this->breadcrumbs[] = array('url' => 'staff/inbox_appointment', 'text' => 'Inbox Appointment');
    }

    function getData(){
        $data = $this->load->controller_model($this->moduleName)->getData();
        echo json_encode(array('data' => $data));
    }

}