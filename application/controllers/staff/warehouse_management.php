<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Warehouse_Management extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "warehouse_management";
        $this->pageCode = "warehouse_management";
        $this->pageName = "Warehouse Management";
        $this->breadcrumbs[] = array('url' => 'staff/warehouse_management', 'text' => 'Warehouse Management');
    }

    function item_info($item_id, $warehouse_id = null){
        try{
            if($this->user_permission->checkPermission('e',$this->pageCode) == false){
                throw new Exception("You don't have this permission");
            }
            $data = $this->load->controller_model($this->moduleName)->get_item_info($item_id,$warehouse_id);
            $this->ajax_content(REQUEST_SUCCESS,'',$data);
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

    function edit(){
        try{
            $id = $this->input->post('id');
            if($this->user_permission->checkPermission('e',$this->pageCode) == false){
                throw new Exception("You don't have permission to interact with warehouse");
            }
            $fields = $this->input->post('field_post');
            $modelName = $this->load->controller_model($this->moduleName);
            //Edit a record
            if($id){
                $result = $modelName->update($id,$fields);
            }else{
                $result = $modelName->insert($fields);
            }

            $data = new stdClass();
            $data->option = array('close_modal' => 1);
            if(is_numeric($result) || $result === true){
                return $this->ajax_content(REQUEST_SUCCESS,'The record has been saved', $data);
            }else{
                return $this->ajax_content(REQUEST_FAIL, $result, $data);
            }
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

    function remove_quantity_form($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForQuantityViewForm($id);
        $data = $this->content($this->moduleName.'/remove_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary",
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function add_quantity_form($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForQuantityViewForm($id);
        $data = $this->content($this->moduleName.'/add_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }


}