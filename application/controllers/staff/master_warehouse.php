<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Master_Warehouse extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "master_warehouse";
        $this->pageCode = "master_warehouse";
        $this->pageName = "Master Warehouse";
        $this->breadcrumbs[] = array('url' => 'staff/warehouse', 'text' => 'WareHouse');
        }

    function remove_quantity_form($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForQuantityViewForm($id);
        $data = $this->content($this->moduleName.'/remove_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary",
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function add_quantity_form($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForQuantityViewForm($id);
        $data = $this->content($this->moduleName.'/add_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function getForm($id){
        $modelName      = $this->load->controller_model($this->moduleName);
        $data           = $modelName->getDataForViewForm($id);
        $content   = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'url'           => site_url($this->dir . '/spread_item/' . $id),
                'text'          => 'Save',
                'class'         => "btn-primary",
                'before'        => 'before_spread_item',
                'close_modal'   => 1
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        if(isset($data['ret_option'])){
            $option = array_merge_recursive($option,$data['ret_option']);
        }
        return $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$content, 'type' => 'dialog', 'option' => $option));
    }

    function spread_item($id){
        if($this->user_permission->checkPermission('e',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $data = $this->input->post('field_post');
        $this->load->controller_model($this->moduleName)->spread_item_to_warehouses($id,$data);
        return $this->ajax_content(REQUEST_SUCCESS,'Item was distributed succsesfully');
    }
}