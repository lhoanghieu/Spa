<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "branch";
        $this->pageCode = "branch";
        $this->pageName = "Branch";
    }




    function suggest(){
        /**
         * keyword : keyword to search in
         * isMinimal : display in minimal or full fields
         * isFull : display all at once or just some rows
         * isAll : Full permission display or limited permission
         */
        $branch_array = $this->load->table_model('employee')->get_branch($this->user_check->get('user_id'));
        if( $branch_array!=null) {
            $condition['branch.id'] =  $branch_array;
        }
        $keyword    = $this->input->get_post('term');
        $isMinimal  = to_b($this->input->get_post('minimal'));
        $isFull     = to_b($this->input->get_post('full'));
        $isAll      = to_b($this->input->get_post('all'));
        $isExtend   = to_b($this->input->get_post('extend'));
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword), $condition, $isFull,$isExtend, $isAll);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword), $condition, $isFull,$isExtend,$isAll);
        }
        echo json_encode($suggest);
    }

    function new_suggest_by_branch_group(){
        $keyword = $this->input->get_post('term');
        $branch_group_id = $this->input->get_post('branch_group_id');
        if($branch_group_id == 0)
            $branch_group_id = $this->load->table_model('employee')->get_branch_group($this->user_check->get('user_id'));
        if(!$branch_group_id){
            $branch_group_id = $this->user_check->get('branch_group_id');
        }
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword),array('branch_group_id' => $branch_group_id));
        }
        echo json_encode($suggest);
    }
}