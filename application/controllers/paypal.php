<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'third_party/PayPal-PHP-SDK/autoload.php';
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

class Paypal extends POS_Controller {
    function __construct(){
        parent::__construct();
    }

    function receivePaypalTransaction(){
        $params = $this->input->get();
        if(!empty($params) && isset($params['paymentId'])){
            $insert['deposit_payment_item_id'] = $params['did'];
            $insert['paypal_payment_id'] = $params['paymentId'];
            $insert['paypal_payment_token'] = $params['token'];
            $insert['paypal_payment_payer_id'] = $params['PayerID'];
            $insert['time_no'] = $params['tmn'];
            $insert['amount_payment'] = $params['amo'];

            $this->db->insert('deposit_paypal_return', $insert);
            
            $data = $this->db->select('remain_payment_times,total_remain_amount')
                             ->where(array('deposit_payment_item_id' => $params['did']))
                             ->get('deposit_payment_item_list')->row_array();
            // reduce the time to resend;

            if(!empty($data)) {
                $data['remain_payment_times'] = $data['remain_payment_times'] - 1;
                $data['total_remain_amount'] = $data['total_remain_amount'] - $params['amo'];
                if ($data['remain_payment_times'] == 0) {
                    $data['status'] = 0;
                }
                $this->db->update('deposit_payment_item_list',$data, array('deposit_payment_item_id' => $params['did']));
            }

        }

        redirect(base_url().'booking/book');
    }

    function SendEmailDepositPayment(){
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                PAYPAL_AUTHORIZE('ClientID'),
                PAYPAL_AUTHORIZE('ClientSecrect')
            )
        );

        $apiContext->setConfig(
            array(
                'log.LogEnabled' => true,
                'log.FileName' => 'PayPal.log',
                'log.LogLevel' => 'DEBUG'
            )
        );

        $data = $this->db->select('di.id as did,de.id,de.total_remain_amount,de.remain_payment_times,
                                   bill.code as bill_code,
                                   customer.code as customer_code,customer.first_name as customerName,customer.email,
                                   item.name as item_name,item.code as item_code,
                                   (di.payment_time - de.remain_payment_times + 1) as times')
                         ->join('bill', 'de.bill_id = bill.id','LEFT')
                         ->join('customer', 'de.customer_id = customer.id','LEFT')
                         ->join('deposit_payment_item di', 'de.deposit_payment_item_id = di.id','LEFT')
                         ->join('item', 'di.item_id = item.id')
                         ->where(array(
                             'customer.email !=' => '',
                             'de.remain_payment_times >' => 0,
                             'de.status' => 1
                         ))
                         ->where('DATEDIFF(DATE(NOW()),DATE(bill.created_date)) = (di.payment_time - de.remain_payment_times + 1) * 30')
                         ->get('deposit_payment_item_list de')->result_array();
        if(count($data) > 0){
            $this->load->library('email');
            foreach($data as $row){
                $displayAmount = (floatval($row['total_remain_amount'])/floatval($row['remain_payment_times']));

                $payer = new Payer();
                $payer->setPaymentMethod("paypal");
                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(base_url().'paypal/receivePaypalTransaction?did='.$row['did'].'&tmn='.($row['times'].'&amo='.$displayAmount))
                    ->setCancelUrl(base_url());

                $item = new Item();
                $item->setName($row['item_name'])
                     ->setCurrency('SGD')
                     ->setQuantity(1)
                     ->setSku($row['item_code'])
                     ->setPrice($displayAmount);
                $itemList = new ItemList();
                $itemList->setItems(array($item));
                $details = new Details();
                $details->setShipping(0)
                    ->setTax(0)
                    ->setSubtotal($displayAmount);
                $amount = new Amount();
                $amount->setCurrency("SGD")
                    ->setTotal($displayAmount)
                    ->setDetails($details);
                $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription("Payment for using deposit payment at bill code: ".$row['bill_code'].'. Remaining payment: '.$row['remain_payment_times']-1)
                    ->setInvoiceNumber(uniqid());
                $payment = new Payment();
                $payment->setIntent("sale")
                    ->setPayer($payer)
                    ->setRedirectUrls($redirectUrls)
                    ->setTransactions(array($transaction));
                try {
                    $payment->create($apiContext);
                } catch (Exception $ex) {}
                $row['approvalUrl'] = $payment->getApprovalLink();

                $this->email->clear(true);
                $body = $this->load->email_content('customer_notify_remain_payment', $row);
                //$this->email->to($row['email']);
                $this->email->to('testing.gento@gmail.com');
                $this->email->subject("Notify email deposit payment");
                $this->email->message($body);
                if ($this->email->send()) {
                }
                else{
                    echo $this->email->print_debugger();
                }
            }
        }
    }
}