<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Lead_time_max extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "lead_time_max";
        $this->pageCode = "lead_time_max";
        $this->pageName = "lead_time_max";
        $this->breadcrumbs[] = array('url' => 'lead_time_max', 'text' => 'lead_time_max');
    }
}