<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bills extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "bills";
        $this->pageCode = "bills";
        $this->pageName = "Bills";
        $this->breadcrumbs[] = array('url' => 'bills', 'text' => 'Bills');
    }

    function index(){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            echo "You dont have permission"; exit;
        }
        $data = new stdClass();
        $data->pageCode = $this->pageCode;
        $this->session->set_userdata('bill_search',array());
        $this->page($this->moduleName, $data);
    }

    function void(){
        if($this->user_permission->checkPermission('void',$this->pageCode) == false){
            return;
        }
        $ids = $this->input->post('ids');
        $comment = $this->input->post('comment');
        $comment = $comment?$comment:'';
        $modelName = $this->load->controller_model($this->moduleName);
        $result = $modelName->void($ids,$comment);
        if($result && !is_string($result)){
            // LOG VOID BILL
            $billcode = $this->load->controller_model('sales')->getBillCode($ids);
            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Void Bill',
                    'object' => $billcode,
                    'objectDetail' => 'Void Bill : '.$billcode,
                    'module' => 'staff/'.$this->moduleName));
            // END LOG

            $this->load->table_model('booking')->check_booking_by_bill_id($ids);
            $this->ajax_content(REQUEST_SUCCESS, 'Void bill successfully');
        }else{
            $this->ajax_content(REQUEST_FAIL,$result);
        }
    }

    function setSearchCondition($return = true){
        $start_date_raw = $this->input->post('start_date') . ' 00:00:00';
        $end_date_raw = $this->input->post('end_date') . ' 23:59:59';
        $customer_id = $this->input->post('customer_id');
        $employee_id = $this->input->post('employee_id');
        $item_id = $this->input->post('item_id');
        $payment_types = $this->input->post('payment_type_id');
        $code = $this->input->post('code');
        $status = $this->input->post('status');
        $start_date = get_database_date($start_date_raw);
        $end_date = get_database_date($end_date_raw);
        $searchCondition = array(
            "bill.created_date > '{$start_date}'",
            "bill.created_date < '{$end_date}'"
        );
        if($code && count($code)){
            $searchCondition['bill.code'] = $code;
        }
        if($customer_id && count($customer_id)){
            $searchCondition['bill.customer_id'] = $customer_id;
        }
        if($employee_id && count($employee_id)){
            $searchCondition['bill_employee.employee_id'] = $employee_id;
        }
        if($item_id && count($item_id)){
            $searchCondition['bill_item.item_id'] = $item_id;
        }
        $row_condition = "";
        if($payment_types && count($payment_types)){
            if(in_array(-1,$payment_types)){
                if(count($payment_types) != 1){
                    $row_condition = 'bill_payment.payment_id IN ('.implode(',',$payment_types).')';
                }
                $row_condition .= (strlen($row_condition)?' or ':'').'bill_payment.payment_id is null';
            }else{
                $row_condition = 'bill_payment.payment_id IN ('.implode(',',$payment_types).')';
            }
        }
        if(strlen($row_condition)){
            $searchCondition[] = '('.$row_condition.')';
        }

        if($status){
            $searchCondition['bill.status'] = $status;
        }

        $this->session->set_userdata('bill_search',$searchCondition);
        $this->session->set_userdata('bill_search_raw', array(
            'start_date' => $start_date_raw,
            'end_date'   => $end_date_raw,
        ));
        if($return)
            $this->ajax_content(REQUEST_SUCCESS);
    }

    function clearSearch(){
        $this->session->set_userdata('bill_search',array());
        $this->ajax_content(REQUEST_SUCCESS);
    }

    function getList($page=1){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            return;
        }
        $search = $this->session->userdata('bill_search');
        if($search == false){
            $search = array();
        }
        $data = $this->load->controller_model($this->moduleName)->getDataSet($page,$search);
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForViewForm($id);
        $data = $this->content($this->moduleName.'/edit_form',$data, true);
        $buttons = array(
            'success' => array(
                'text'      => 'Replace',
                'class'     => 'btn-primary',
                'onClick'   => 'before_replace'
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function getReportForm(){
        if($this->user_permission->checkPermission('v', 'reports') == false){
            $this->ajax_content(REQUEST_FAIL,"You dont have permission"); exit;
        }
        $this->setSearchCondition(false);
        $data = $this->load->controller_model($this->moduleName)->getDataForReportForm();
        $data = $this->content($this->moduleName.'/report_form',$data, true);
        $buttons = array(
            'success' => array(
                'text'      => 'Generate Report',
                'onClick'   => 'get_report_detail',
                'class'     => 'btn-primary'
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function getReportDetail($name, $format =""){
        if($this->user_permission->checkPermission('v', 'reports') == false){
            $this->ajax_content(REQUEST_FAIL,"You dont have permission"); exit;
        }
        $search_condition = $this->session->userdata('bill_search');
        $search_condition = array('search_condition' => json_encode($search_condition));
        $data = $this->session->userdata('bill_search_raw');
        $data_content = $this->load->controller_model('reports')->get_report_detail_data($name, $search_condition);

        $report = REPORT($name);
        $data['name'] = $report['name'];
        $data['key']  = $name;
        $data['branch_name'] = $this->user_check->get('branch_name');
        $data['post_back'] = $search_condition;
        if($format != ''){
            $this->export($name, $format, $data_content, $data);
        }else{
            $data['data_content'] = $this->content("reports/details/" . $name, $data_content, true);
            $this->page("reports/report_layout", $data, 'raw');
        }
    }

    function get_backdate_form($bill_id){
        if($this->user_permission->checkPermission('backdate', 'bills') == false){
            throw new Exception("You don't have backdate permission");
        }
        $data = $this->load->controller_model($this->moduleName)->getDataForBackDate($bill_id);
        $this->ajax_content(REQUEST_SUCCESS,'',array('content' => $this->content('bills/backdate_form',$data,true)));

    }

    function backdate_bill($bill_id){
        if($this->user_permission->checkPermission('backdate', 'bills') == false){
            throw new Exception("You don't have backdate permission");
        }
        $date_back_dated = $this->input->post('date');
        $return = $this->load->controller_model($this->moduleName)->backdate_bill($bill_id,$date_back_dated);
        if($return){
            // LOG BACKDATE BILL
            $billcode = $this->load->controller_model('sales')->getBillCode($bill_id);
            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Backdate Bill',
                    'object' => $billcode,
                    'objectDetail' => 'Backdate Bill : '.$billcode,
                    'module' => 'staff/'.$this->moduleName));
            // END LOG

            $this->ajax_content(REQUEST_SUCCESS,'The bill has been backdated');
        }

    }

}