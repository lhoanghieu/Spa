
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-pills sf-menu">
                <?php foreach($menus as $menu):?>
                    <li role="presentation"><a href="<?php echo booking_url('category')."?code={$menu->code}"?>"><?php echo $menu->name ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
</div>