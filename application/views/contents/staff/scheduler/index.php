<div class="break_line"></div>
<?php if(!$is_full_screen) :?>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-content nopadding">
                <form class="form-horizontal form-horizontal-mobiles" id="receipt_report" method="post" target="_blank">
                    <div class="form-group">
                        <label for="range" class="col-sm-3 col-md-3 col-lg-2 control-label">Employee</label>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <select id="employee_chosen_id" class="form-control" data-source="<?php echo site_url('employees/suggest_therapist?full=1&all=false') ?>" >
                                <option value="0">All</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="submit-btn" class="btn btn-primary submit_button btn-large">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="break_line"></div>
<div class="row">
    <div class="btn-group" style="position: relative">
        <button id="change_dp_mode" value="0" class="btn btn-dark-blue" onclick="change_dp_mode($(this).attr('id'))">
            View Reserve Scheduler
        </button>
        <button id="cancellation_records" class="btn btn-danger hide" onclick="popup_cancellation_records()">
            Cancellation Records
        </button>
<!--        <a href="--><?php //echo site_url('staff/scheduler/index/employee')?><!--" class="btn btn-primary">Employee</a>-->
<!--        <a href="--><?php //echo site_url('staff/scheduler/index/room')?><!--" class="btn btn-dark-red">Room</a>-->
        <button class="btn btn-info" onclick="exportCancelledApptsForm()">Export Cancelled Appts</button>
        <button class="btn btn-warning" onclick="create_branch_offline_form()">Branch Offline</button>
        <button class="btn btn-dark-green" onclick="refresh_scheduler()">Refresh</button>
        <button class="btn btn-green" onclick="export_PDF()">Export to PDF</button>
        <div id="notify_content"></div>
    </div>
</div>
<?php endif; ?>


<?php
$width = 1400 + (count($employees) > 10?(count($employees)-10)*180:0);
$url = $_SERVER['REQUEST_URI'];
$url = explode("/", $url);
if(isset($url[4]) && $url[4] == "employee" && isset($url[5]) && $url[5] > 0)
    $width = "1200px;";
?>

<?php if($is_full_screen) :?>
    <div class="row" style="width: <?php echo $width; ?>">
        <h1 style="text-align: center" class="staff_name"></h1>
    </div>
<?php endif; ?>

<div class="row" style="width:<?php echo $width; ?>px;
    height:<?= $is_full_screen?'100vh;':'600px;'?>;
    overflow-x: scroll;
    ">
    <div id="scheduler_here" class="dhx_cal_container col-md-12" style='width:100%; height:100%'>
        <div id="fullscreen_notify_content" style="position: absolute;top: 0px;left: 0px;font-size: 14px;"></div>
        <div class="dhx_cal_navline">
            <div class="dhx_cal_prev_button">&nbsp;</div>
            <div class="dhx_cal_next_button">&nbsp;</div>
            <div class="dhx_cal_today_button"></div>
            <div class="dhx_cal_tab unit_tab" name="unit_tab"></div>
<!--            <div class="dhx_cal_tab" name="grid_tab" style="left:120px !important;"></div>-->
            <div class="dhx_cal_date"></div>
            <div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
            <div class="btn btn-lg" style="color:#808080" onclick="full_screen()"><span class="glyphicon glyphicon-expand"></span></div>
            <div style="left: 50px; top: -10px;">
                <div class="input-group">
                    <input id="search_filter" type="text" class="form-control" style="width: 170px;" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" id="search-submit">Search</button>
                </span>
                </div>
            </div>
        </div>
        <div id="search_content" class="search_content" style="display: none;">
            <div class="">
                <table id="history_data" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width:12%;">Booking Date</th>
                        <th style="width:12% ;">Booking Code</th>
                        <th style="width:6%">Branch</th>
                        <th style="width:10%;">Cust Code</th>
                        <th style="width:13%">Service</th>
                        <th style="width:11%;">Start Time</th>
                        <th style="width:11%;">End Time</th>
                        <th style="width:16%;">Comment</th>
                        <th style="width:4%;">Remarks</th>
                        <th style="width:4%">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Booking Date</th>
                        <th>Booking Code</th>
                        <th>Branch</th>
                        <th>Cust Code</th>
                        <th>Service</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Comment</th>
                        <th>Remarks</th>
                        <th>Status</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="dhx_cal_header">
        </div>
        <div class="dhx_cal_data">
        </div>
    </div>
</div>
<div id="dropdown-order-sample" class="hidden">
    <div style="position: absolute;top: 2px;left: 8px;">
        <a title="Set offline/online" href="#" onclick="set_online_offline({0})">
            <i class="fa fa-clock-o"></i>
        </a>
    </div>
    <div class="btn-group" style="margin-right:5px;">
        <a  href="#" data-toggle="dropdown">
            <i class="fa fa-bars"></i>
        </a>
        <ul class="dropdown-menu pull-right" style="width: 150px; z-index: 9999;">
            <li>
                <div class="">
                    <div class="col-md-6">
                        <input min="1" value="1" type=number class="order-number form-control" />
                    </div>
                    <a class="btn col-md-3" href="#" onclick="reorder({0},this)">Ok</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<script>
    var bookings              = <?php echo json_encode($bookings) ?>;
    var employees             = <?php echo json_encode($employees)?>;
    var bookings_comment      = <?php echo json_encode($bookings_comment)?>;
    var employee_map          = <?php echo json_encode($employee_map)?>;
    var rooms                 = <?php echo json_encode($rooms)?>;
    var store_open_hour       = <?php echo json_encode($store_open_hour)?>;
    var current_employee_id   = <?php echo $employee_id?"'{$employee_id}'":'null'; ?>;
    var current_url           = "<?php echo current_url()?>";
    var date                  = <?php echo $this->input->get('date')?'"' . $this->input->get('date') . '"':'false' ?>;
    var branch_data           = <?php echo json_encode($branch_data) ?>;
    var marked_time           = "<?php echo get_database_date(); ?>";
    var is_full_screen        = <?php echo $this->input->get('is_full_screen')?'true':'false' ?>;
    var dp_mode               = <?php echo $this->input->get('dp_mode')?'true':'false' ?>;
</script>