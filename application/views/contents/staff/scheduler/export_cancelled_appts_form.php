<input type="hidden" name="id" value="<?php echo isset($branch_id)?$branch_id:'';?>" class="insert form-control form-inps" id="branchId"  />
<form class="form-horizontal">
    <div class="widget-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="startDate" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Start Date:</label>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <input name="startDate" value="" class="insert form-control form-inps" id="startDate"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="endDate" class="col-sm-3 col-md-3 col-lg-2 control-label required ">End Date:</label>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <input key="checkbox-relate" name="endDate" value="" class="insert form-control form-inps" id="endDate"  />
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6 pull-right">
                <button type="button" class="btn" onclick="getExportCancelledApptData();">Export</button>
                <button class="btn lightbox-close" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>