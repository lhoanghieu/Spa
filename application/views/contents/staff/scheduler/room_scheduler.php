<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-content nopadding">
                <form class="form-horizontal form-horizontal-mobiles" id="receipt_report" method="post" target="_blank">
                    <div class="form-group">
                        <label for="range" class="col-sm-3 col-md-3 col-lg-2 control-label">Room</label>
                        <div class="col-sm-6 col-md-6 col-lg-5">
                            <select id="room_chosen_id" class="form-control" data-source="<?php echo site_url('staff/rooms/suggest?full=1') ?>"  data-value="<?php echo isset($room_id)?$room_id:0?>">
                                <option value="0">All</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="submit-btn" class="btn btn-primary submit_button btn-large">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="break_line"></div>
<div class="row">
    <div class="btn-group">
        <a href="<?php echo site_url('staff/scheduler/index/employee')?>" class="btn btn-primary">Employee</a>
        <a href="<?php echo site_url('staff/scheduler/index/room')?>" class="btn btn-dark-red">Room</a>
        <button class="btn btn-warning" onclick="create_branch_offline_form()">
            Branch Offline
        </button>
        <button class="btn btn-dark-green" onclick="refresh_scheduler()">
            Refresh
        </button>
    </div>
</div>
<div class="row">
    <div id="scheduler_here" class="dhx_cal_container col-md-12" style='width:100%; height:600px;'>
        <div class="dhx_cal_navline">
            <div class="dhx_cal_prev_button">&nbsp;</div>
            <div class="dhx_cal_next_button">&nbsp;</div>
            <div class="dhx_cal_today_button"></div>
            <div class="dhx_cal_tab" name="unit_tab" style="right:280px;"></div>
            <div class="dhx_cal_tab" name="grid_tab" style="right:300px;"></div>
            <div class="dhx_cal_date"></div>
            <div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
            <div class="btn btn-lg" style="color:#808080" onclick="full_screen()"><span class="glyphicon glyphicon-expand"></span></div>
        </div>
        <div class="dhx_cal_header">
        </div>
        <div class="dhx_cal_data">
        </div>
    </div>
</div>

<script>
    var bookings          = <?php echo json_encode($bookings) ?>;
    var rooms             = <?php echo json_encode($rooms)?>;
    var employees         = <?php echo json_encode($employees)?>;
    var employee_map      = <?php echo json_encode($employee_map)?>;
    var store_open_hour   = <?php echo json_encode($store_open_hour)?>;
    var current_room_id   = <?php echo $room_id?"'{$room_id}'":'null'; ?>;
    var site_url          = "<?php echo site_url()?>";
    var date              = <?php echo $this->input->get('date')?'"' . $this->input->get('date') . '"':'false' ?>;
    var branch_data       = <?php echo json_encode($branch_data) ?>;
    var marked_time       = "<?php echo get_database_date(); ?>";
</script>