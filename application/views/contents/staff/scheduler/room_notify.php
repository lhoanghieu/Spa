<div id="not_available_rooms">
    <div id="room_notify">
        <div class="lefttext">Available Room(s)</div>
        <div id="toogle_room_notify" class="righttext">Show</div>
        <br clear="all">
    </div>
    <div id="room_notify_content">
        <?php
        foreach($data as $value) {
            if(count($value->room_list) != 0)
                echo '<div>'.$value->name.' - Available: '.count($value->room_list).'</div>';
        }
        ?>
    </div>
</div>

<script>
    $("#toogle_room_notify").on("click",function(){
        var text = $(this).text();
        if(text == "Show") $(this).text("Hide");
        if(text == "Hide") $(this).text("Show");
        $("#room_notify_content").slideToggle("slow");
    });
</script>