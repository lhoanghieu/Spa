<div class="break_line"></div>
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="btn-group pull-left">
            <a href="<?= staff_url('warehouse') ?>" class="btn btn-dark-red btn-sm">Manage Warehouse</a>
            <a href="<?= staff_url('master_warehouse')?>" class="btn btn-dark-blue btn-sm">Master Warehouse</a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="warehouse_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Warehouse" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="warehouse_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search warehouses" class="search ui-autocomplete-input" autocomplete="off" target="category_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="category_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="category_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo staff_url('warehouse') ?>/" id="warehouse_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id" >
                    <tr>
                        <th data-type="text-link" data-column="code" data-function="update_item" data-function-params="id">Code</th>
                        <th data-type="text-link" data-column="name" data-function="update_item" data-function-params="id">Name</th>
                        <th data-type="text" data-column="address">Address</th>
                        <th data-type="buttons" data-class='["fa fa-file-text"]'
                            data-function='["get_warehouse_distribution"]' data-function-params='["id"]' data-allow-key='[""]' data-align="center" data-tag='["<i>"]'>Items</th>
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>