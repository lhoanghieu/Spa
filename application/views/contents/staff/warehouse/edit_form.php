<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="code" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Code:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="code" value="<?php echo isset($item->code)?$item->code:'';?>" class="insert form-inps required" id="code"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class=" col-sm-3 col-md-3 col-lg-2 control-label required">Name:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="name" value="<?php echo isset($item->name)? $item->name:'';?>" class="insert form-inps required" id="name"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label">Address:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="address" value="<?php echo isset($item->address)? $item->address:'';?>" class="insert form-inps" id="address"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Group</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input id="branch_group_id" data-source="<?php echo staff_url('branch_group/suggest')?>" name="branch_group_id" value='<?php echo isset($item->branch_group_id)?json_encode($this->user_permission->trim_branch_group($item->branch_group_id)):'';?>' class="insert form-inps multi-select" id="branch_group_id" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch</label>
                    <div class="col-sm-9 col-md-9 col-lg-10" id="branch_id_container">
                        <input type="hidden" id="branch_id" name="branch" value='<?php echo isset($item->branch_id)?json_encode($this->user_permission->trim_branch($item->branch_id)):'';?>'>
                    </div>
                </div>
            </div>
        </div>

        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
    </form>
</div>