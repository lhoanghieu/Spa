<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Code:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="code" value="<?php echo isset($item->code)?$item->code:'';?>" class="insert form-inps required" id="code"  />			</div>
                </div>
                <div class="form-group">
                    <label for="last_name" class=" col-sm-3 col-md-3 col-lg-2 control-label required">Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="name" value="<?php echo isset($item->name)? $item->name:'';?>" class="insert form-inps required" id="name"  />			</div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label required">Number of bed:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="number" name="bed_quantity" value="<?php echo isset($item->bed_quantity)? $item->bed_quantity:1;?>" class="insert form-inps " id="bed_quantity"  />			</div>
                </div>
                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Room Type', 'class' => 'required'),
                    'control'   => array('type' => 'select','class' => 'required insert','value' => isset($item->room_type_id)?$item->room_type_id:'','id' => 'room_type_id','attribute' => array(
                        'data-source' => site_url('staff/room_type/suggest'),
                    ))
                )) ?>
            </div>
        </div>

        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
    </form>
</div>

<script>
</script>