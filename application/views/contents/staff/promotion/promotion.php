<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="room_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Room" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="room_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search promotion" class="search ui-autocomplete-input" autocomplete="off" target="room_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="room_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="room_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo site_url('staff/'.$pageCode) ?>/" id="room_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id" >
                    <tr>
                    <tr>
                        <th data-type="text-link" data-column="code" data-function="update_item" data-function-params="id">Code</th>
                        <th data-type="text" data-column="discount" data-function="update_item" data-function-params="id">Discount</th>
                        <th data-type="text" data-column="type" data-function="update_item" data-function-params="id">Discount Type</th>
                        <th data-type="text" data-column="count_of_used" data-function="update_item" data-function-params="id">Number of use</th>
                        <th data-type="text" data-column="date_from" data-function="update_item" data-function-params="id">Date From</th>
                        <th data-type="text" data-column="date_to" data-function="update_item" data-function-params="id">Date To</th>
                        <th data-type="text" data-column="time_from" data-function="update_item" data-function-params="id">Time From</th>
                        <th data-type="text" data-column="time_to" data-function="update_item" data-function-params="id">Time To</th>
                        <th data-type="text" data-column="created_date" data-function="update_item" data-function-params="id">Created Date</th>
                        <!-- <th data-type="text-button" data-button-text="Disable" data-else-button-text="Enable" data-function="disable_room" data-else-function="enable_room" data-function-params="id" data-function-allow-key="is_not_disable">&nbsp</th> -->
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>