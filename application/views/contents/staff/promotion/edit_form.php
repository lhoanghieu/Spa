<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Type:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <?php echo generate_control(PROMOTION_TYPE(),isset($item->type)?$item->type:'', 'select',array('class' => 'insert select_commission', 'id' => 'type'))?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Code:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="code" value="<?php echo isset($item->code)?$item->code:'';?>" class="insert form-inps required" id="code"  />			</div>
                </div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Discount:</label>            <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="discount" value="<?php echo isset($item->discount)?$item->discount:'';?>" class="insert form-inps required" id="discount"  />          </div>
                </div>
                <!-- <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Price:</label>            <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="price" value="<?php //echo isset($item->price)?$item->price:'';?>" class="insert form-inps required" id="price"  />          </div>
                </div> -->
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Number of use:</label>            <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="count_of_used" value="<?php echo isset($item->count_of_used)?$item->count_of_used:'';?>" class="insert form-inps required" id="count_of_used"  />          </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="date_from" name="date_from" class="datepicker insert form-inps" value="<?= isset($item->date_from)?$item->date_from:date('Y-m-d') ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="date_to" name="date_to" class="datepicker insert form-inps" value="<?= isset($item->date_to)?$item->date_to:date('Y-m-d') ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Time From</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="time_from" name="time_from" class="time-picker insert form-inps" value="<?= isset($item->time_from)?$item->time_from:'' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Time To</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="time_to" name="time_to" class="time-picker insert form-inps" value="<?= isset($item->time_to)?$item->time_to:'' ?>">
                    </div>
                </div>
                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Category'),
                    'control'   => array('type' => 'multiselect','class' => 'insert multi-select','value' => isset($item->categories)?convert_to_array($item->categories,'','id'):array(),'id' => 'category_id','attribute' => array(
                        'data-source' => site_url('categories/suggest?full=1')
                    ))
                )) ?>

                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Items'),
                    'control'   => array('type' => 'multiselect','class' => 'insert multi-select','value' => isset($item->items)?convert_to_array($item->items,'','id'):array(),'id' => 'item_id','attribute' => array(
                        'data-source' => site_url('items/suggest?full=1')
                    ))
                )) ?>

                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Branch'),
                    'control'   => array('type' => 'multiselect','class' => 'insert multi-select','value' => isset($item->branches)?convert_to_array($item->branches,'','id'):array(),'id' => 'branch_id','attribute' => array(
                        'data-source' => staff_url('branch/suggest?minimal=true')
                    ))
                )) ?>
            </div>
        </div>

        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
    </form>
</div>

<script>
</script>