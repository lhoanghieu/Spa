
<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <input type="hidden" class="insert" id="warehouse_id" value="<?php echo isset($warehouse_id)?$warehouse_id:0?>">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Item:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" class="autocomplete form-inps required" id="item_autocomplete" data-source="<?php echo site_url('items/suggestProduct')?>"  />
                        <input type="hidden" class="insert required" id="item_id" name="item_id">
                    </div>
                </div>

                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Quantity:</label>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-inps insert" id="quantity" name="quantity" />
                    </div>
                </div>
            </div>
    </form>
</div>