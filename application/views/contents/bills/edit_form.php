<div class="widget-content">
    <h4>Choose a bill you want to be replaced</h4>
    <input type="hidden" value="<?php echo $item->id ?>" id="id">
    <form  method="post" accept-charset="utf-8" class="form-horizontal">
        <table class="table table-bordered text-center table-hover">
            <thead>
            <tr>
                <th>&nbsp</th>
                <th>Date</th>
                <th>Code</th>
                <th>Cashier</th>
                <th>Void Date</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($void_bills as $bill): ?>
                <tr for="checkbox-<?php echo $bill->id?>">
                    <td><input type="radio" name="replace_bill_id" id="checkbox-<?php echo $bill->id?>" value="<?php echo $bill->id ?>"></td>
                    <td><?php echo get_user_date($bill->created_date)?></td>
                    <td><?php echo $bill->code?></td>
                    <td><?php echo to_full_name($employees[$bill->creator]->first_name,$employees[$bill->creator]->last_name)?></td>
                    <td><?php echo $bill->void_date?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </form>
    <?php if(isset($item->replaced)): ?>
    <small style="color:red">This bill was used to replace another bill. Please make sure you check it carefully.</small>
    <input type="hidden" id="replaced-hidden" value="1">
    <?php endif; ?>
</div>