<div class="widget-content">
    <h4>Choose a date you want to backdate</h4>
    <input type="hidden" value="<?php echo $bill->id ?>" id="id">
    <form  method="post" accept-charset="utf-8" class="form-horizontal">
        <div class="form-group">
            <label for="code" class="required wide col-sm-3 col-md-3 col-lg-2 control-label required wide">Date</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input minDate="<?php echo get_user_date($bill->min_date,'','Y-m-d H:i:s')?>" maxDate="<?php echo get_user_date($bill->max_date,'','Y-m-d H:i:s')?>"
                       datepicker="1" format="Y-m-d H:i:s" formatDate="Y-m-d H:i:s" id="date" name="date" value="<?php echo get_user_date($bill->created_date,'','Y-m-d H:i:s')?>"
                       class="time-picker form-control form-inps required"  />
            </div>
        </div>
    </form>
</div>
