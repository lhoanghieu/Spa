<h4>Choose A Report</h4>
<table class="table table-bordered text-center table-hover">
    <thead>
    <tr>
        <th></th>
        <th>Report Name</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($reports as $key=>$fields): ?>
        <tr>
            <td><input name="report_id" type="radio" value="<?php echo $key ?>"></td>
            <td><?php echo $fields['text'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>