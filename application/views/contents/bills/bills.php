<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-content nopadding">
                <form target="_blank" method="post" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
                    <div class="form-group">
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Date From</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input type="text" id="start_datepicker" name="start_date" class="datepicker" value="<?php echo isset($this->session->userdata('bills')->start_date) !=''? $this->session->userdata('start_date') : date('Y-m-d',time());?>">
                        </div>
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Date To</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input type="text" id="end_datepicker" name="end_date" class="datepicker" value="<?php echo isset($this->session->userdata('bills')->end_date)  !=''? $this->session->userdata('end_date') : date('Y-m-d',time());?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Code</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input style=""  type="text" name="code" id="code" size="30" placeholder="Type code name..." accesskey="c" class="ui-autocomplete-input multi-select" autocomplete="off">
                        </div>
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Customer</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input style="" type="text" name="customer" id="customer" size="30" placeholder="Type customer name..."
                                   class="ui-autocomplete-input multi-select" data-source="<?php echo site_url('customers')?>/suggest?minimal=0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Employees</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input style=""  type="text" name="employee" id="employee" size="30" placeholder="Type employee name..." accesskey="c" class="ui-autocomplete-input multi-select" autocomplete="off" data-source="<?php echo site_url('employees')?>/suggest">
                        </div>
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Payment Type</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input style="" type="text" name="payment_type" id="payment_type" size="30" placeholder="Type payment name..." accesskey="c" class="ui-autocomplete-input multi-select" autocomplete="off" data-source="<?php echo site_url('payments')?>/suggest">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 col-md-1 col-lg-2  control-label" for="range">Items</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <input style=""  type="text" name="item" id="item" size="30" placeholder="Type item name..." accesskey="c" class="ui-autocomplete-input multi-select" autocomplete="off" data-source="<?php echo site_url('items')?>/suggest">
                        </div>
                        <label class="col-sm-1 col-md-1 col-lg-2 control-label" for="range">Bill Status:</label>
                        <div class="col-sm-5 col-md-5 col-lg-3">
                            <?php foreach(BILL_STATUS(array(1, 3)) as $key=>$label): ?>
                                <input id="checkbox<?php echo $key?>" value="<?php echo $key ?>" name="bill_status[]" type="checkbox" checked/>
                                <label for="checkbox<?php echo $key?>" style="margin-right: 5px;"><?php echo $label?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn btn-primary submit_button btn-large" id="submit_search"  type="button"
                                name="submit_search">Submit</button>
                        <?php if($this->user_permission->checkPermission('v','reports')): ?>
                            <button class="btn btn-dark-red btn-large" id="choose_report"  type="button">Report</button>
                        <?php endif; ?>
                        <a class="btn btn-info" target="bill_list" btn-type="clear">Clear Search</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Bill" href="<?php echo site_url('sales') ?>">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="bill_list"></div>
                <span id="total_rows" class="label label-info"></span>

            </div>
            <div class="widget-content nopadding table_holder table-responsive" >
                <table data-url="<?php echo site_url('bills') ?>/" id="bill_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id" >
                    <tr>
                        <th data-type="text" data-column="code">Date</th>
                        <th data-type="text" data-column="created_date">Date</th>
                        <th data-type="text" data-column="payment_name">Payments</th>
                        <th data-type="text" data-column="total">Total</th>
                        <th data-type="text" data-column="customer_name">Customer</th>
                        <th data-type="text" data-column="status">Status</th>
                        <th data-type="text" data-column="description">Comment</th>
                        <th data-type="link" data-text="Detail" data-url="<?php echo site_url('sales/receipt') ?>/">Detail</th>
                        <th data-type="text-button" data-button-text="Void" data-function="void_btn" data-function-params="id" data-function-allow-key="is_active">&nbsp</th>
                        <th data-type="buttons" data-class='["fa fa-calendar"]'
                            data-function='["backdate_bill"]' data-function-params='["id"]' data-allow-key='["is_in_7_days"]' data-align="center" data-tag='["<i>"]'>Backdate</th>

                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>