<div class="text-center">
    <h3>Welcome to PHP Point Of Sale, click a module below to get started!</h3>

    <!-- Menu Buttons -->
    <ul class="quick-actions">
        <?php foreach($menus->topMenu[0] as $menu) : ?>

            <li class="">
                <a href="<?php echo site_url($menu->link) ?>">
                    <i class="icon fa fa-<?php echo $menu->link?>"></i>
                    <span class="hidden-minibar"><?php echo $menu->title?></span>
                </a>
            </li>

        <?php endforeach; ?>
    </ul>
    <!-- End of menus buttons -->
    <div class="row">
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="fa fa-signal"></i></span><h5> Statistics</h5></div>
            <div class="widget-content">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="site-stats">
                            <li><a href="items"><h3><i class="fa fa-shopping-cart"></i>  Total Items : <strong><?php echo $count_items ;?></strong></h3></a> </li>
                            <li><a href="categories"><h3> <i class="fa fa-inbox"></i>  Total categories  :  <strong><?php echo $count_categories;?></strong></h3></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="site-stats">
                            <li>  <a href="customers"><h3> <i class="fa fa-group"></i>  Total Customers  : <strong><?php echo $count_customers;?></strong></h3></a></li>
                            <li> <a href="employees"><h3> <i class="fa fa-user"></i>  Total Employees  :  <strong><?php echo $count_employees;?></strong></h3></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="site-stats">
                            <li> <a href="bills"><h3> <i class="fa fa-download"></i>  Total Sales  : <?php echo $count_sales ?> <strong></strong> </h3></a></li>
                            <li>  <a href="#"><h3> <i class="fa fa-credit-card"></i>  Total Credits  : <strong><?php echo $count_credits?></strong></h3></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>