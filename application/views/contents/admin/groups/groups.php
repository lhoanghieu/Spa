<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="group_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="group_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search customers" class="search ui-autocomplete-input" autocomplete="off" target="group_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="group_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="group_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo admin_url('groups') ?>/" id="group_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id">
                    <tr>
                        <th data-type="text-link" data-column="name" data-function="update_item" data-function-params="id">Role Name</th>
                        <th data-align="center" data-type="buttons" data-class='["glyphicon glyphicon-cog"]' data-function='["update_permission"]' data-function-params='["id"]'>Role Permission</th>
                        <th data-align="center" data-type="buttons" data-class='["glyphicon glyphicon-list-alt"]' data-function='["update_report_permission"]' data-function-params='["id"]'>Report Permission</th>
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var msgBoxImagePath = '<?= $this->assets->coreImageURL ?>';
    var url = "<?php echo site_url('groups') ?>";
    var success_update = "success_update_permissions";
    var fail_update    = "fail_update_permissions";

    var right = {};
    right['ep'] = <?= $this->user_permission->checkPermission('ep',$pageCode)==true?1:0 ?>;
</script>