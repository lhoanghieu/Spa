<div class="widget-content">
    <form  method="post" accept-charset="utf-8"  class="form-horizontal">
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:0 ?>"/>
        <div class="form-group">
            <label for="code" class="required wide col-sm-3 col-md-3 col-lg-2 control-label required wide">UPC/EAN/ISBN:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="code" type="text" name="code" value="<?= isset($item->code)?$item->code:""?>" size="8" class="insert form-control form-inps required"  />
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="required wide col-sm-3 col-md-3 col-lg-2 control-label required wide">Category Name:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="name" type="text" name="category_name" value="<?=isset($item->name)?$item->name:""?>" size="8" class="insert form-control form-inps required"  />
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 col-md-3 col-lg-2 control-label">Description:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <textarea name="description" cols="17" rows="5" id="description" class="insert form-textarea" ><?=isset($item->description)?$item->description:""?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Parent Category</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <select data-source="<?php echo admin_url('categories/suggest')?>?full=1" name="category_add" id="parent_id" class="insert">
                    <option value="0"> Select Category </option>
                </select>
            </div>
        </div>

        <?= Form_Generator::CREATE_FORM_GROUP(array(
            'label'     => array('text' => 'Items', 'class' => 'required'),
            'control'   => array('type' => 'multiselect','class' => 'required insert multi-select','value' => isset($item->item_list)?convert_to_array($item->item_list,'','id'):array(),'id' => 'item_id_list','attribute' => array(
                'data-source' => admin_url('items/suggest?full=1')
            ))
        )) ?>

        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Global</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="global" type="checkbox" name="name" value="1" class="insert" id="branch_group_id" <?php echo $item->global?'checked':''?> unchecked-value="0">
            </div>
        </div>

        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Group</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>" name="branch_group_id" value='<?php echo isset($item->branch_group_id)?json_encode($this->user_permission->trim_branch_group($item->branch_group_id)):'';?>' class="insert form-inps multi-select" id="branch_group_id" >
            </div>
        </div>

        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch</label>
            <div class="col-sm-9 col-md-9 col-lg-10" id="branch_id_container">
                <input type="hidden" id="branch_id" name="branch" value='<?php echo isset($item->branch_id)?json_encode($this->user_permission->trim_branch($item->branch_id)):'';?>'>
            </div>
        </div>
    </form>
</div>
<script>
    setTimeout(function(){
        var pid = '<?php echo isset($item->parent_id) ? $item->parent_id : 0; ?>';
        if(pid) {
            $('#parent_id').select2('val','<?php echo $item->parent_id; ?>');
        }
    }, 500);
</script>
