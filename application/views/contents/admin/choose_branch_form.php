<h4>Choose A Branch</h4>
<table class="table table-bordered text-center table-hover">
    <thead>
    <tr>
        <th>Branch name</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($branches as $key=>$fields): ?>
        <tr>
            <td><a href="<?php echo site_url("adapter/to_branch/{$key}/$url")?>" target="blank"><?php echo $fields->name ?></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>