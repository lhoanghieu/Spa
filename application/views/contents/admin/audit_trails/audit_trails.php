<div class="break_line"></div>
<div class="form-row">
    <button type="button" id="reload-table" class="btn btn-primary submit_button btn-large pull-right" style="margin-right: 52px; margin-bottom: 5px;">Reload</button>
</div>
<div class="row">
 <table id="audit-trails" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th style="width:0.1%;">ID</th>
        <th style="width:10% ;">User</th>
        <th style="width:10%">Create At</th>
        <th style="width:10%;">Module</th>
        <th style="width:12.5%;">Object</th>
        <th style="width:12.5%;">Action</th>
        <th style="width:49%;">Action Detail</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>User</th>
        <th>Create At</th>
        <th>Module</th>
        <th>Object</th>
        <th>Action</th>
        <th>Action Detail</th>
    </tr>
    </tfoot>

</table>
</div>
<script type="text/javascript">
    var admin_site_url = "<?php echo admin_url('audit_trails/getData') ?>";
</script>