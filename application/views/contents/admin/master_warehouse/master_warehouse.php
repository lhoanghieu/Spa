<div class="break_line"></div>
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="btn-group pull-left">
            <a href="<?= admin_url('warehouse') ?>" class="btn btn-dark-red btn-sm">Manage Warehouse</a>
            <a href="<?= admin_url('master_warehouse')?>" class="btn btn-dark-blue btn-sm">Master Warehouse</a>
        </div>
    </div>
    <div class="col-md-12">
        <input type="text" id="name" value="" data-suggest-url="<?= admin_url('items/suggestProduct') ?>" placeholder="Search warehouses" class="search ui-autocomplete-input" autocomplete="off" target="master_warehouse_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="master_warehouse_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="master_warehouse_list" btn-type="clear">clear search</a>
            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo admin_url('master_warehouse') ?>/" id="master_warehouse_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id" >
                    <tr>
                        <th data-type="text-link" data-column="code" data-function="update_item" data-function-params="id">Code</th>
                        <th data-type="text-link" data-column="name" data-function="update_item" data-function-params="id">Name</th>
                        <th data-type="text" data-column="master_quantity">Undistributed Quantity</th>
                        <th data-type="text" data-column="total_quantity">Total Quantity</th>
                        <?php if($this->user_permission->checkPermission('e','warehouse_management')): ?>
                        <th data-align="center" data-type="buttons" data-class='["fa fa-plus"]'     data-function='["add_quantity_form"]' data-function-params='["id"]'>Add Quantity</th>
                        <th data-align="center" data-type="buttons" data-class='["fa fa-minus"]'    data-function='["remove_quantity_form"]' data-function-params='["id"]'>Remove Quantity</th>
                        <?php endif; ?>
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>