<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="master_warehouse_form" class="form-horizontal">
        <div class="row">
            <input type="hidden" class="insert" id="id" value="<?php echo isset($id)?$id:0?>">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Item Remaining:</label>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-inps insert"  disabled id="master_quantity" name="quantity" value="<?php echo $product->master_quantity?>" />
                    </div>
                    <label class="required col-sm-3 col-md-3 col-lg-2 control-label ">Remove:</label>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-inps" id="item_change" />
                    </div>
                </div>
            </div>
    </form>
</div>