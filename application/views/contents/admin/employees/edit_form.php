<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">First Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="first_name" value="<?php echo isset($item->first_name)?$item->first_name:'';?>" class="insert form-inps required" id="first_name"  />			</div>
                </div>
                <div class="form-group">
                    <label for="last_name" class=" col-sm-3 col-md-3 col-lg-2 control-label ">Last Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="last_name" value="<?php echo isset($item->last_name)? $item->last_name:'';?>" class="insert form-inps" id="last_name"  />			</div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label required">E-Mail:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="email" value="<?php echo isset($item->email)? $item->email:'';?>" class="insert form-inps required email-validate" id="email"  />			</div>
                </div>
                <div class="form-group">
                    <label for="phone_number" class="col-sm-3 col-md-3 col-lg-2 control-label ">Phone Number:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="phone_number" value="<?php echo isset($item->phone_number)?$item->phone_number:'';?>" class="insert form-inps" id="phone_number"  />			</div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label ">Address 1:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="address" value="<?php echo isset($item->address)?$item->address:''; ?>" class="insert form-control form-inps" id="address"  />	</div>
                </div>

                <div class="form-group">
                    <label for="comments" class="col-sm-3 col-md-3 col-lg-2 control-label ">Comments:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <textarea name="comments" cols="17" rows="5"  id="comments" class="insert" ><?php echo isset($item->comments)?$item->comments:''; ?></textarea>	</div>
                </div>
            </div>
        </div>
        <div id="login_section">
            <input type="hidden" id="therapist_id" value="<?php echo implode(',',$this->load->table_model('department')->get_department_with_account_not_require()) ?>">
            <legend class="page-header text-info"> &nbsp; &nbsp; Employee Login Info</legend>
            <div class="form-group">
                <label for="username" class="col-sm-3 col-md-3 col-lg-2 control-label required">Username:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" name="username" value="<?php echo isset($item->username)?$item->username:''; ?>" id="username" class="insert form-control required"  />
                    <span id="confirmExist" class="confirmExist"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="<?php echo (isset($item->id)&&$item->id != null)?'':'required' ?> col-sm-3 col-md-3 col-lg-2 control-label">Password:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input for="password" type="password" name="password" value="" id="password" class="insert form-control cpassword-validate" target="cpassword"  />
                </div>
            </div>

            <div class="form-group">
                <label for="repeat_password" class="<?php echo (isset($item->id)&&$item->id != null)?'':'required' ?> col-sm-3 col-md-3 col-lg-2 control-label">Password Again:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input for="confirmpassword" type="password" name="cpassword" value="" id="cpassword" class="form-control cpassword" target="password" />
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="department" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Employee Role:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">

                <select id="department_id" name="department_id" class="insert drop-style get-list-manager">
                    <?php
                    foreach($departments as $department):
                        if(isset($item->department_id)&&$department->id==$item->department_id):
                            echo "<option value={$department->id} selected='selected'>{$department->name}</option>";
                        else:
                            echo "<option value={$department->id}>{$department->name}</option>";
                        endif;
                    endforeach;?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="department" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Commission Rule:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <?php echo generate_control($commissions,isset($item->commission_id) ? $item->commission_id: "", "select",array('class' => 'form-control insert', 'id' => 'commission_id', 'name' => 'commission_id'))?>
            </div>
        </div>

        <div class="form-group">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Employee Type:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <?php $user_level = $this->user_check->get('login_as_type'); $employee_types = EMPLOYEE_TYPE(); ?>
                <select class="form-inps insert" name="type" id="type">
                    <?php foreach($employee_types as $key=>$name): ?>
                        <option value="<?php echo $key?>"
                            <?php echo isset($item)&&$item->type==$key?'selected':''?>
                            <?php echo $user_level<$key?'disabled':''?>
                            ><?php echo $name?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Global</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="global" type="checkbox" name="name" value="1" class="insert" id="branch_group_id" <?php echo $item->global?'checked':''?> unchecked-value="0">
            </div>
        </div>
        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Group</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>" name="branch_group_id" value='<?php echo isset($item->branch_group_id)?json_encode($this->user_permission->trim_branch_group($item->branch_group_id)):'';?>' class="insert form-inps multi-select" id="branch_group_id" >
            </div>
        </div>
        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch</label>
            <div class="col-sm-9 col-md-9 col-lg-10" id="branch_id_container">
                <input type="hidden" id="branch_id" name="branch" value='<?php echo isset($item->branch_id)?json_encode($this->user_permission->trim_branch($item->branch_id)):'';?>'>
            </div>
        </div>
        <?php if(!isset($item->id)):
            ?>
        <div class="form-group">
            <label class="col-sm-3 col-md-3 col-lg-2 control-label">Employee Calendar</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                The employee's calendar has to be set in branch module. When successfully created this employee. Go to the branch you want to set up this employee's calendar
            </div>
        </div>
        <?php endif; ?>

        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
        <input id="current_user" name="current_user" value="<?php echo isset($item->username)?$item->username:''?>" type="hidden">
    </form>
</div>

<script>
    $(function(){
        $('#department_id').trigger('change');
    });
</script>