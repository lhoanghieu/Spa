<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/17/2014
 * Time: 2:14 PM
 */
$WORKING_DAYS = WORKING_DAYS(); ?>
<div class="widget-content">
    <input id="employee_id" value="<?php echo $id ?>" type="hidden">
    <table class="table table-bordered text-center">
        <thead>
        <tr>
            <th width="20px">&nbsp</th>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
            <th width="30px">
                <input id="header_date<?php echo $key ?>" header-id="<?php echo $key?>" type="checkbox" class="date-header"  <?php echo isset($item[$key])?'checked="true"':''?>style="margin-right:5px;">
                <label for="header_date<?php echo $key ?>"><?php echo $day?></label></th>
            <?php endforeach ?>
        </tr>
        </thead>
        <tbody id="commission_category_list">
        <tr id="start_time_row">
            <td>Start</td>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
                <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo isset($item[$key])?$item[$key]->start_time:$this->system_config->get('start_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_start"  /></td>
            <?php endforeach; ?>
        </tr>
        <tr id="end_time_row">
            <td>End</td>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
                <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo isset($item[$key])?$item[$key]->end_time:$this->system_config->get('end_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_end"  /></td>
            <?php endforeach; ?>
        </tr>
        </tbody>
    </table>
</div>
