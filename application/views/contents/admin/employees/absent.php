<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/24/2014
 * Time: 2:19 PM
 */
?>
<input type="hidden" name="id" value="<?php echo isset($id)?$id:'';?>" class="insert form-control form-inps" id="employee_id"  />
<form class="form-horizontal">
    <div class="widget-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="absent_date" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Date:</label>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <input name="absent_date" value="" class="insert form-control form-inps" id="absent_date"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="absent_from" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Time:</label>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input key="checkbox-relate" name="absent_from" value="" class="insert form-control form-inps" id="absent_from"  />
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input key="checkbox-relate" name="absent_to" value="" class="insert form-control form-inps" id="absent_to"  />
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input  type="checkbox" id="absent_whole_day"/> <label for="absent_whole_day">Whole day</label>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <div id="absent_add" class="btn btn-primary"><span class="fa fa-plus"></span></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group">
        <table id="Absent Dates" class="table table-bordered table-striped table-hover text-success text-center">
            <tr>
                <th>Delete</th>
                <th>Date</th>
                <th>From</th>
                <th>To</th>
            </tr>
            <tbody id="grid_absent">
            <?php foreach($absent_data as $row): ?>
            <tr>
                <td><span class="btn fa fa-trash-o" onclick="delete_absent_date(<?php echo $row->id ?>)"></span></td>
                <td><?php echo $row->date ?></td>
                <td><?php echo get_user_date($row->start_time,$this->system_config->get('timezone'),'Y-m-d H:i:s',true)?></td>
                <td><?php echo get_user_date($row->end_time,$this->system_config->get('timezone'),'Y-m-d H:i:s',true)?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</form>
<script>
</script>