<div class="widget-content">
    <form  method="post" accept-charset="utf-8" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:'0'; ?>"/>
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <div class="form-group">
            <div class="row">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" id="name" name="name" class="form-control insert" value="<?php echo isset($item->name)?$item->name:''; ?>"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="categories" class="col-sm-2 control-label">Categories</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control autocomplete" id="commission_category" data-source="<?php echo site_url('admin/categories/suggest') ?>"/>
                    <div class="widget-box align-top scrollable">
                        <div class="widget-content no-padding">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th width="60px">Delete</th>
                                    <th>Name</th>
                                    <th width="254px">Commission</th>
                                </tr>
                                </thead>
                                <tbody id="commission_category_list">
                                <?php foreach($item->commission_categories as $category_id => $commission_category): ?>
                                    <tr id="commission_category_<?php echo $category_id; ?>">
                                        <td><a class="delete-item"> <i class="fa fa-trash-o fa-2x"></i> </a></td>
                                        <td class="commission_category_name"><?php echo $commission_category['category_name']; ?></td>
                                        <td>
                                            <?php foreach($commission_category['rules'] as $rule): ?>
                                                <div class="clear commission_category_level">
                                                    <a class="delete-rule fleft" style="padding: 2px;"> <i class="fa fa-trash-o"></i> </a>
                                                    <input type="number" value="<?php echo $rule->from; ?>" class="commission_category_from form-control h-sm fleft no-padding" placeholder="From"/>
                                                    <input type="number" value="<?php echo $rule->to; ?>" class="commission_category_to form-control h-sm fleft no-padding" placeholder="To"/>
                                                    <input type="number" value="<?php echo $rule->value; ?>" class="commission_category_value form-control h-sm fleft no-padding" value="0"/>
                                                    <?php echo generate_control(COMMISSION_TYPE(),$rule->type, "select",array('class' => 'commission_category_type form-control w-auto h-sm fleft no-padding'))?>
                                                </div>
                                            <?php endforeach; ?>
                                            <a class="add-rule fright clear" style="padding: 2px;"> <i class="fa fa-plus"></i> </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                <tr id="commission_category_sample" style="display: none">
                                    <td><a class="delete-item"> <i class="fa fa-trash-o fa-2x"></i> </a></td>
                                    <td class="commission_category_name"> no name</td>
                                    <td>
                                        <a class="add-rule fright clear" style="padding: 2px;"> <i class="fa fa-plus"></i> </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="commission_category_level_sample" class="clear commission_category_level" style="display: none">
                        <a class="delete-rule fleft" style="padding: 2px;"> <i class="fa fa-trash-o"></i> </a>
                        <input type="number" class="commission_category_from form-control h-sm fleft no-padding" placeholder="From"/>
                        <input type="number" class="commission_category_to form-control h-sm fleft no-padding" placeholder="To"/>
                        <input type="number" class="commission_category_value form-control h-sm fleft no-padding" value="0"/>
                        <?php echo generate_control(COMMISSION_TYPE(),"", "select",array('class' => 'commission_category_type form-control w-auto h-sm fleft no-padding'))?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="items" class="col-sm-2 control-label">Add Items</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control autocomplete" id="commission_item" data-source="<?php echo site_url('items/suggest') ?>"/>
                    <br>
                    <p>Search :</p>
                    <input type="text" class="form-control autocomplete" id="commission_search_item" data-source="<?php echo site_url('admin/commission/suggest?id='.$item->id) ?>"/>
                    <div class="widget-box align-top">
                        <div class="widget-content no-padding scrollable" style="height:400px;overflow:auto;">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th style="widh:10%;">Delete</th>
                                    <th>Name</th>
                                    <th style="width:70%">Commission</th>
                                </tr>
                                </thead>
                                <tbody id="commission_item_list">
                                <?php foreach($item->commission_items as $commission_item): ?>
                                    <tr id="commission_item_<?php echo $commission_item->item_id; ?>" item-type="<?= strtolower(ITEM_TYPE($commission_item->item_type)) ?>" item-price="<?= $commission_item->item_price ?>">
                                        <td><a class="delete-item"> <i class="fa fa-trash-o fa-2x"></i> </a></td>
                                        <td class="commission_item_name"><?php echo $commission_item->item_name; ?></td>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td><input type="number" class="commission_item_value form-control h-sm fleft no-padding" value="<?php echo $commission_item->value; ?>"/></td>
                                                    <td>
                                                        <?php
                                                        if($commission_item->item_type != ITEM_TYPE('Bundle')){
                                                            echo generate_control(COMMISSION_TYPE(),$commission_item->type, "select",array('class' => 'commission_item_type form-control w-auto h-sm fleft no-padding'));
                                                        }else{
                                                            echo generate_control(COMMISSION_BUNDLE_TYPE(),$commission_item->type, "select",array('class' => 'commission_item_type_bundle form-control w-auto h-sm fleft no-padding'));
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php if($commission_item->item_type == ITEM_TYPE('Bundle')): ?>
                                                        <td>
                                                            <table class="commission_bundle_sub_item_list">
                                                                <tbody>
                                                                <?php foreach($commission_item->sub_item_commission as $sub_item_commission): ?>
                                                                    <tr id="commission_bundle_sub_item_<?php echo $sub_item_commission->id?>">
                                                                        <td class="commission_bundle_sub_item_name"><?= $sub_item_commission->name ?></td>
                                                                        <td class="padding-left"><input type="number" class="commission_bundle_sub_item_value form-control h-sm fleft no-padding" value="<?= $sub_item_commission->commission_value ?>" /></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    <?php endif; ?>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                <tr id="commission_item_sample" style="display: none" item-type="normal">
                                    <td><a class="delete-item"> <i class="fa fa-trash-o fa-2x"></i> </a></td>
                                    <td class="commission_item_name"></td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td> <input type="number" class="commission_item_value form-control h-sm fleft no-padding" value="0"/></td>
                                                <td> <?php echo generate_control(COMMISSION_TYPE(),"", "select",array('class' => 'commission_item_type form-control w-auto h-sm fleft no-padding'))?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="commission_bundle_item_sample" style="display: none" item-type="bundle">
                                    <td><a class="delete-item"> <i class="fa fa-trash-o fa-2x"></i> </a></td>
                                    <td class="commission_item_name"></td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td> <input type="number" class="commission_item_value form-control h-sm fleft no-padding" value="0"/></td>
                                                <td> <?php echo generate_control(COMMISSION_BUNDLE_TYPE(),"", "select",array('class' => 'commission_item_type_bundle form-control w-auto h-sm fleft no-padding'));?></td>
                                                <td>
                                                    <table class="commission_bundle_sub_item_list">
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="commission_sub_bundle_item_sample" style="display:none">
                                    <td class="commission_bundle_sub_item_name"></td>
                                    <td class="padding-left"><input type="number" class="commission_bundle_sub_item_value form-control h-sm fleft no-padding" value="0" /></td>
                                </tr>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
