<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="item_kit_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <div class="form-group">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Type:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <?php echo generate_control(ITEM_TYPE(),isset($item->type)?$item->type:'', 'select',array('class' => 'insert select_commission', 'id' => 'type'))?>
            </div>
        </div>
        <div class="form-group">
            <label for="code" class="col-sm-3 col-md-3 col-lg-2 control-label required">UPC/EAN/ISBN:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="code" value="<?php echo isset($item->code)?$item->code:'';?>" class="form-control form-inps required insert" id="code"  />
            </div>
        </div>
        <div class="form-group">
            <label for="item_name" class="col-sm-3 col-md-3 col-lg-2 control-label  required">Item Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="name" value="<?php echo isset($item->name)?$item->name:'';?>" class="insert form-control form-inps required" id="name"  />
            </div>
        </div>
        <div class="form-group">
            <label for="category" class="col-sm-3 col-md-3 col-lg-2 control-label  required wide">Category:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="category_id" name="category_id" class="insert multi-select" value='<?= $item->id&&isset($item->categories)?json_encode(convert_to_array($item->categories,'','id')):json_encode(array()) ?>' data-source="<?= site_url('categories/suggest') ?>?full=1">
            </div>
        </div>

        <div class="form-group" id="unit_price_display">
            <label for="unit_price" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Unit Price:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="price" value="<?php echo isset($item->price)?$item->price:'';?>" class="insert form-control form-inps required" id="price"  />
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-3 col-md-3 col-lg-2 control-label">Description:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <textarea name="description" cols="17" rows="5" id="description" class="insert form-textarea" ><?php echo isset($item->description)?$item->description:'';?></textarea>
            </div>
        </div>

        <div class="form-group no_fullcash">
            <label for="no_fullcash" class="col-sm-3 col-md-3 col-lg-2 control-label">No cash payment:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="checkbox" name="no_fullcash" id="no_fullcash" class="insert chbox" unchecked-value="0" <?php echo (isset($item->no_fullcash) && ($item->no_fullcash == 1) ? 'checked' : '');?> />
            </div>
        </div>
        <div class="form-group">
            <label for="promotion" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Promotion:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="number" min="0" max="50" name="promotion" value="<?php echo isset($item->promotion)&&!empty($item->promotion)?$item->promotion:'0'?>" id="promotion" class="insert form-control form-inps">
            </div>
        </div>

        <div  id="package" class="">
            <div class="form-group credit_value">
                <label for="credit_value" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Credit Value:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="credit_value" value="<?php echo isset($item->credit_value)?$item->credit_value:'';?>" class="insert form-control form-inps" id="credit_value"  />
                </div>
            </div>

            <div class="form-group expiry_length">
                <label for="Expire_length" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Expiry Length:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="expire_length" value="<?php echo isset($item->package->expire_length)?$item->package->expire_length:'';?>" class="insert form-control form-inps" id="expire_length"  />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category item relate</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select data-source="<?php echo admin_url('categories/suggest')?>?full=1" name="category_add" id="category_add">
                        <option value="0"> Select Category </option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label"></label>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <select name="item_id" id="item_id" style="margin-top: 5px;"></select>
                    <input type="button" class="btn" id="btn_add_all_items" value="Add all list items" style="margin-left: 15px;">
                </div>
            </div>

            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label">Add Single Item</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div style="clear: both">
                        <select id="item_kit_select" data-source="<?php echo site_url('admin/items/suggest') ?>" ></select>
                    </div>
                    <div class="widget-box align-top">
                        <div class="widget-title">
						<span class="icon">
							<i class="fa fa-th"></i>
						</span>
                            <h5>Add Item</h5>
                        </div>
                        <div class="widget-content no-padding">
                            <input type="hidden" id="item_kit_ids" class="insert" value='<?php
                            $json = array();
                            if(isset($item->package))
                                foreach($item->package->kit_items as $kit_item){
                                    $json[] = $kit_item->id;
                                }
                            echo json_encode($json);
                            ?>'/>
                            <table id="item_kit_items" class="table table-bordered table-striped table-hover text-success text-center">
                                <tr>
                                    <th>Delete</th>
                                    <th>Item name</th>
                                    <th>Unit price</th>
                                </tr>
                                <tbody id="grid_item">
                                <?php
                                $kitdataitem = isset($item->package) ? $item->package : '';
                                if($kitdataitem != ''):
                                    foreach($kitdataitem->kit_items as $kit_item){
                                        ?>
                                        <tr>
                                            <td><a onclick='return deleteItemKitRow(this, <?= $kit_item->id ?>);' ><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>
                                            <td class="modelname"><?php echo $kit_item->name;?></td>
                                            <td class="modelname"><?php echo $kit_item->price;?></td>
                                        </tr>
                                    <?php }
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="product" class="">
            <div class="form-group">
                <label for="unit_cost" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Unit Cost:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="unit_cost" value="<?php echo isset($item->product)?$item->product->unit_cost:'';?>" class="insert form-control form-inps" id="unit_cost"  />
                </div>
            </div>
        </div>

        <div  id="service" class="">
            <div class="form-group">
                <label for="duration" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Duration:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="duration" value="<?php echo isset($item->service)?$item->service->duration:'';?>" class="insert form-control form-inps" id="duration"  />
                </div>
            </div>
        </div>

        <div id="bundle" class="">
            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label"> Sub-Items: </label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" name="item_bundle_item" value="" class="form-control form-inps autocomplete" data-source="<?php echo site_url('items/suggest') ?>" id="item_bundle_item"  />
                    <div class="widget-box align-top">
                        <div class="widget-title">
                            <span class="icon">
                                <i class="fa fa-th"></i>
                            </span>
                            <h5>Add Sub Item</h5>
                        </div>
                        <div class="widget-content no-padding">
                            <input type="hidden" id="item_bundle_item_ids" class="insert" value='<?php
                            echo isset($item->bundle->sub_items)?json_encode(convert_to_array($item->bundle->sub_items,'','id',true)):"[]";
                            ?>'/>
                            <table class="table table-bordered table-striped table-hover text-success text-center">
                                <tr>
                                    <th>Delete</th>
                                    <th>Item Name</th>
                                    <th>Unit Price</th>
                                </tr>
                                <tbody id="grid_item_bundle">
                                <?php
                                if(isset($item->bundle->sub_items)):
                                    foreach($item->bundle->sub_items as $sub_item){
                                        ?>
                                        <tr>
                                            <td><a onclick='return deleteBundleSubItem(this, <?= $sub_item->id ?>);' ><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>
                                            <td class="modelname"><?php echo $sub_item->name;?></td>
                                            <td class="modelname"><?php echo $sub_item->price;?></td>
                                        </tr>
                                    <?php }
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Global</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="global" type="checkbox" name="name" value="1" class="insert" id="branch_group_id" <?php echo $item->global?'checked':''?> unchecked-value="0">
            </div>
        </div>
        <div class="form-group">
            <label for="branch_group_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Group</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>" name="branch_group_id" value='<?php echo isset($item->branch_group_id)?json_encode($this->user_permission->trim_branch_group($item->branch_group_id)):'';?>' class="insert form-inps multi-select" id="branch_group_id" >
            </div>
        </div>
        <div class="form-group">
            <label for="branch_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Branch</label>
            <div class="col-sm-9 col-md-9 col-lg-10" id="branch_id_container">
                <input type="hidden" id="branch_id" name="branch" value='<?php echo isset($item->branch_id)?json_encode($this->user_permission->trim_branch($item->branch_id)):'';?>'>
            </div>
        </div>
    </form>
</div>
<script type="application/javascript">
    $("#type").trigger('change');
    custom_form_render_page();
</script>
