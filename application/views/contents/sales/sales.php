<div id="sale-grid-big-wrapper" class="clearfix">
    <div class="clearfix" id="category_item_selection_wrapper" style="display: block;">
        <div class="">
            <div class="pagination hidden-print alternate text-center fg-toolbar ui-toolbar categories"></div>
            <div id="category_item_selection" class="row"> </div>
            <div class="pagination hidden-print alternate text-center fg-toolbar ui-toolbar categories">
            </div>
        </div>
    </div>

    <div style="display: block" id="show_hide_grid_wrapper">
        <img src="<?= $this->assets->releaseImage('show_hide.png') ?>" alt="show or hide item grid" />
        <a href="javascript: void(0);" class="btn btn-primary" id="show_grid" style="display: none;">Show Categories</a>
        <a href="javascript: void(0);" class="btn btn-primary" id="hide_grid" style="display: inline;">Hide Categories</a>
    </div>

</div>
<div id="register_container" class="sales clearfix">
    <!--Left small box-->
    <div id="dialog-modal" title="Confirm" style="display: none;">Are you want to delete this item ? </div>
    <div class="row">
        <input id="myError" type="hidden" value="">
        <div id="messageBox"></div>
        <div class="sale_register_leftbox col-md-9">
            <div class="row forms-area">
                <div class="col-md-8 no-padding">
                    <div class="input-append">
                        <form  method="post" accept-charset="utf-8" id="add_item_form" class="form-inline" autocomplete="off">
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="item" id="item_search" size="40" placeholder="Enter item name or scan barcode" class="search-input-item" autocomplete="off">
                            <a href="<?= site_url('items') ?>" class="btn btn-primary none new_item_btn" title="New Item">New Item</a>
                            <a href="#" id="retrieve_bill" class="btn btn-primary none" title="view hold bill">
                                <div class="small_button">Retrieve bill</div></a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <table id="register" class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width:25%">Item Name</th>
                            <th style="width:5%">Price</th>
                            <th style="width:5%" >Qty.</th>
                            <th style="width:20%">Choice staff</th>
                            <th style="width:5%">Subtotal</th>
                            <th style="width:5%">Discount</th>
                            <th style="width:5%">Discount Type</th>
                            <th style="width:30%">Package/Gift Card</th>
                            <th style="width:10%">Total</th>
                        </tr>
                        </thead>
                        <tbody id="grid_row_items">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row hidden-xs" id="recent_sale">
            </div>
        </div>

        <!-- Right small box  -->
        <div class="col-md-3 sale_register_rightbox" id="right_panel"></div>
    </div>
</div>
<input id="sale_edit_id" name="sale_edit_id" type="hidden" />
<div id="dialog" style="display:none"></div>


<script>
    var controller = 'sales/';
    right = {
        vc : <?php echo $this->user_permission->checkPermission('v','customers')?1:0?>,
        ec : <?php echo $this->user_permission->checkPermission('e','customers')?1:0?>
    };
</script>