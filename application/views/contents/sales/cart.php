<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/13/2014
 * Time: 5:03 PM
 */

if(count($this->cart->contents())) {
    foreach ($this->cart->contents() as $items){
        $options = $this->cart->product_options($items['rowid']);
        ?>
        <tr rowid="<?= $items['rowid'] ?>">
            <td>
                <div class="item_delete" rowid="<?= $items['rowid'] ?>"><i
                        class="fa fa-trash-o fa fa-2x text-error"></i></div>
            </td>
            <td class="item_name_heading"><?= $items['name']; ?></td>
            <td id="sales_price_<?= $items['rowid'] ?>"><input type="hidden" readonly id="price_<?= $items['rowid'] ?>"
                                                               name="price" value="<?= $items['price']; ?>"
                                                               class="itemChange"><?= $items['price']; ?></td>
            <td id="sales_quantity<?= $items['rowid'] ?>">
                <input type="number" value="<?= $items['qty']; ?>" class="input-small itemChange text" name="quantity">
            </td>
            <td class="sales_employee" name="choice_staff">
                <?php if($options['type'] != ITEM_TYPE('Bundle')): ?>
                <input rowid='<?= $items['rowid']; ?>' name="choice_staff" id="emp_list_<?= $items['rowid']; ?>"
                       class="emp_list_<?= $items['rowid'] ?> employee_list multi-select itemChange" value='<?= json_encode($options['choice_staff']) ?>'
                       data-source="<?= site_url('employees/suggest?minimal=1&full=1') ?>">
                <?php endif; ?>
            </td>
            <td><?= number_format((float)$items['price'] * (float)$items["qty"],2); ?></td>
            <td><input type="text" value="<?= $options['discount']; ?>" name="discount_value"
                       class="itemChange discountValue input-small"/></td>
            <td style="line-height: 0;font-size: 12px;">
                <input type="radio" value="1" <?= ($options['discount_type'] == 1) ? 'checked="checked"' : ""; ?>
                       name="discount_type_<?= $items['rowid'] ?>" field_name="discount_type"
                       class="itemChange discountType radioType"/>
                <label>%</label>
                <br/>
                <input type="radio" value="0" <?= ($options['discount_type'] == 0) ? 'checked="checked"' : ""; ?>
                       name="discount_type_<?= $items['rowid'] ?>" field_name="discount_type"
                       class="itemChange discountType radioType"/>
                <label>$</label>
            </td>
            <td>
                <?php if(count($options['credit_id'])): $i = 1;?>
                <?php foreach($options['credit_pair'] as $credit_id=>$credit_value): ?>
                    <div class="row">
                    <span class="fa fa-trash-o" style="cursor: pointer; color: #e74c3c" onclick="package_change('delete',this)"></span>
                    <select class="itemChange package" field_name="credit_id" style="width:70%">
                        <?php foreach ($options['packages'] as $package):
                            if(!in_array($package->id,$options['credit_id']) || $package->id == $credit_id):
                            ?>
                            <option value="<?= $package->id ?>" <?= $package->id == $credit_id ? 'selected' : '' ?>><?= $package->name ?></option>
                        <?php endif; endforeach; ?>
                    </select>
                    <span><?php echo $credit_value?></span>
                    <?php if($options['total'] > 0 && count($options['packages']) > $i && $i == count($options['credit_id'])): ?>
                        <span class="fa fa-plus" style="cursor: pointer; color: #e74c3c" onclick="package_change('add',this)"></span>
                    <?php else: ?>
                        <span class="" style="cursor: pointer; color: #e74c3c">&nbsp</span>
                    <?php endif; ?>
                    </div>
                <?php $i++; endforeach; ?>
                <?php else: ?>
                    <?php if($options['total'] > 0 && count($options['packages']) > 0): ?>
                    <span class="fa fa-plus" style="cursor: pointer; color: #e74c3c" onclick="package_change('add',this)"></span>
                    <?php endif;?>
                <?php endif; ?>
            </td>
            <input type="hidden" min="0" max="" class="itemChange input-small" id="credit_value" name="credit_value"
                       value="<?= $options['total_credit_value'] ?>">
            <td><?= number_format($options['total'],2); ?></td>
            <input style="display: none;" rel="<?= $items['rowid'] ?>" type="hidden" value="Edit item"
                   class="edit_item"/>
        </tr>
        <?php if($options['type'] == ITEM_TYPE('Bundle')):
            foreach($options['sub_items'] as $row):
            ?>
            <tr rowid="<?= $items['rowid'] ?>">
                <td class="blank_cell" colspan="4">&nbsp;</td>
                <td style="text-align:right"><?= $row->name ?></td>
                <td colspan="5" name="choice_staff" key="<?= $row->id ?>">
                    <input rowid='<?= $items['rowid']; ?>' name="choice_staff"
                       class="emp_list_<?= $items['rowid'] ?> employee_list multi-select itemChange" value='<?php
                        if(isset($options['choice_staff'][strval($row->id)])){
                            echo json_encode($options['choice_staff'][intval($row->id)]);
                        }else{
                            echo json_encode(array());
                        }
                       ?>'
                       data-source="<?= site_url('employees/suggest?minimal=1&full=1') ?>">
                </td>
            </tr>
        <?php endforeach;
            endif; ?>

    <?php }
    }
    else {
        ?>
        <tr style="text-align: center"><td colspan="12">
                <div class="text-center text-warning"> There are no items in the cart yet</div>
            </td>
        </tr>
    <?php
    }
?>
