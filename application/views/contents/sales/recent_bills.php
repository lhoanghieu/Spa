<h1 style="font-size: 20px;font-weight: bold;text-decoration: underline">All Recent Bills:</h1>
<?php
if($bills != null){
    ?>
    <table id="recent_sales" class="table table-hover">
        <tbody><tr>
            <th style="text-align: left;">Sale ID</th>
            <th style="text-align: left;">Date</th>
            <th style="text-align: left;">Payments</th>
            <th style="text-align: left;">Total</th>
            <th style="text-align: left;">Customer</th>
            <th align="center">Status</th>
            <th align="center">Comment</th>
            <th align="center">Bill Detail</th>
        </tr>
        <?php
        foreach($bills as $bill) {
        ?>
        <tr>
            <td class="left"><?= $bill->code;?></td>
            <td class="left"><?=  $bill->created_date;?></td>
            <td class="right"><?= implode(', ',$bill->payments);?></td>
            <td class="right"><?= to_currency($bill->total_price); ?></td>
            <td class="right"><?= isset($bill->customer_detail)?to_full_name($bill->customer_detail->first_name,$bill->customer_detail->last_name):""; ?></td>
            <td align="center"><label><?= BILL_STATUS($bill->status)?></label></td>
            <td align="center"><?= $bill->description; ?></td>
            <td align="center"><a href="<?= site_url('sales/receipt/'.$bill->id) ?>">Detail</a></td>
            <?php }; ?>
        </tr>
        </tbody>
    </table>
<?php }else{ ?>
    <table>
        <tr>
            <td>There are no bill to display</td>
        </tr>
    </table>
<?php } ?>