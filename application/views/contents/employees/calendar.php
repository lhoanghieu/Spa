<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/17/2014
 * Time: 2:14 PM
 */
$WORKING_DAYS = WORKING_DAYS(); ?>
<div class="widget-content">
    <input id="employee_id" value="<?php echo $id ?>" type="hidden">
    <table class="table table-bordered text-center">
        <thead>
        <tr>
            <th width="20px">&nbsp</th>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
            <th width="30px">
                <input onclick="custom_days(<?php echo $key ?>)" id="header_date<?php echo $key ?>" header-id="<?php echo $key?>" type="checkbox" class="date-header"  <?php echo isset($item[$key])?'checked="true"':''?>style="margin-right:5px;">
                <label for="header_date<?php echo $key ?>"><?php echo $day?></label></th>
            <?php endforeach ?>
        </tr>
        </thead>
        <tbody id="commission_category_list">
        <tr id="start_time_row">
            <td>Start</td>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
                <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo isset($item[$key])?$item[$key]->start_time:$this->system_config->get('start_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_start" <?php echo isset($item[$key])?'':'disabled'?> /></td>
            <?php endforeach; ?>
        </tr>
        <tr id="end_time_row">
            <td>End</td>
            <?php foreach($WORKING_DAYS as $key=>$day): ?>
                <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo isset($item[$key])?$item[$key]->end_time:$this->system_config->get('end_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_end" <?php echo isset($item[$key])?'':'disabled'?> /></td>
            <?php endforeach; ?>
        </tr>
        </tbody>
    </table>
    <input type="hidden" id="custom_string" name="custom_string" value="">
    <input type="hidden" id="db_custom_string" name="db_custom_string" value="<?php echo $custom_string; ?>">
    <input type="hidden" id="db_disabled_dates" name="db_disabled_dates" value="<?php echo $disabled_dates; ?>">
</div>

<div style="padding: 0px 0px 10px 15px;">Customize working days</div>
<div style="float: left">
    <div id="calendar"></div>
</div>
<div style="float: left;margin-left: 10px;">
    <div id="custom_area_note"></div>
    <div id="custom_area" style="line-height: 22px;padding-left: 10px;">
        <div>
            Selected day: <span id="currentday"></span>
        </div>
        <div style="padding: 3px 0px 3px 35px;">
            <input type="checkbox" name="offday" id="offday" value="1">&nbsp;Off Day
        </div>
        <div>
            Start <input type="text" name="custom_time_start" id="custom_time_start" value="">
        </div>
        <div style="padding: 10px 0px 10px 0px;">
            End&nbsp; <input type="text" name="custom_time_end" id="custom_time_end" value="">
        </div>
        <div style="padding: 10px 0px 0px 10px;">
            <button id="update">Update</button>
        </div>
    </div>
</div>
<div style="clear: both;"></div>

<style>
    .ui-custom-highlight .ui-state-default{
        background: #3498db;
        color: #ffffff !important;
    }

    .ui-offday-highlight .ui-state-default{
        background: grey !important;
        color: #000000 !important;
    }
</style>

<script type="text/javascript">
    $("#custom_area").hide();
    var customdates = [];
    var offdates = [];
    var disabled_dates = [];
    var dates = {};
    var default_month = '';
    var db_custom_string = '<?php echo $custom_string; ?>';
    init_customdays();    
    init_calendar();

    function init_calendar(){
        $("#calendar").datepicker({
            dateFormat: 'yy-mm-dd',
            //numberOfMonths: 3,
            defaultDate: default_month,
            onSelect: function(date){
                $("#custom_area_note").html('');
                $("#custom_area").show();
                selected_date = $(this).val();
                $("#currentday").html(selected_date);
                day_of_week = new Date(selected_date);
                day_of_week = day_of_week.getDay(selected_date);
                default_start = $("#"+day_of_week+"_start").val();
                default_end = $("#"+day_of_week+"_end").val();
                default_month = selected_date;

                if(dates[selected_date] != undefined){
                    $("#custom_time_start").val(dates[selected_date][0].start);
                    $("#custom_time_end").val(dates[selected_date][0].end);
                    $("#offday").val(dates[selected_date][0].status);
                    if(dates[selected_date][0].status == 0)
                        $('#offday').prop('checked', true);
                    else
                        $('#offday').prop('checked', false);
                }
                else{
                    $("#custom_time_start").val(default_start);
                    $("#custom_time_end").val(default_end);
                    $("#offday").val(1);
                    $('#offday').prop('checked', false);
                }
            },
            beforeShowDay: function(date){
                var y = date.getFullYear().toString(); // get full year
                var m = (date.getMonth() + 1).toString(); // get month.
                var d = date.getDate().toString(); // get Day
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                if(m.length == 1){ m = '0' + m; } // append zero(0) if single digit
                if(d.length == 1){ d = '0' + d; } // append zero(0) if single digit
                var currDate = y+'-'+m+'-'+d;
                var day = new Date(currDate);
                day = day.getDay(currDate);
                if(disabled_dates.indexOf(day) >= 0){
                    var index = disabled_dates.indexOf(day);
                    return [(day != disabled_dates[index])];
                }
                else{
                    if(offdates.indexOf(currDate) >= 0)
                        return [true, "ui-offday-highlight"];

                    if(customdates.indexOf(currDate) >= 0)
                        return [true, "ui-custom-highlight"];

                    return [true];
                }
            },
            onChangeMonthYear: function (year, month, inst) {
                activeMonth = month; // Store active month when month is changed.
            }
        });
    }

    $("#offday").click(function(event) {
        if ($(this).is(':checked'))
            current = 0;
        else
            current = 1;
        $(this).val(current);
    });

    $("#update").click(function(event) {
        // Validate time with javascript
        var check_time = $("#custom_time_start").val();
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(check_time);
        if(!isValid){
            alert('Invalid time');
            return false;
        }
        check_time = $("#custom_time_end").val();
        isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(check_time);
        if(!isValid){
            alert('Invalid time');
            return false;
        }
        
        dates[selected_date] = [{'start':$("#custom_time_start").val(),'end':$("#custom_time_end").val(),'status':$("#offday").val()}];
        $("#custom_area").hide();
        if($("#offday").val() == 0)
            offdates.push(selected_date);
        else{
            offdates = $.grep(offdates, function(value) {
                return value != selected_date;
            });
        }
        if($("#custom_time_start").val() == default_start && $("#custom_time_end").val() == default_end){
            $("#custom_area_note").html('');
            customdates = $.grep(customdates, function(value) {
                return value != selected_date;
            });

            if($("#offday").val() == 1){
                var count = 0;
                var i;
                for(i in dates) {
                    if (dates.hasOwnProperty(i)) {
                        count++;
                    }
                }
                if(count > 1)
                    delete dates[selected_date];
            }
        }
        else{
            if($("#offday").val() == 1)
                customdates.push(selected_date);
            else
                offdates.push(selected_date);
            $("#custom_area_note").html("Update successful");
        }

        custom_string(dates,selected_date);
        $("#calendar").datepicker('destroy');
        init_calendar();
        console.log('tracing: ');
        console.log($("#custom_string").val());
    });


    function custom_string(dates,selected_date){
        var time_string = "";
        $.each(dates, function(i,v){
            if(selected_date == i)
                time_string += selected_date+"_"+dates[selected_date][0].start+"_"+dates[selected_date][0].end+"_"+dates[selected_date][0].status+"|";
            else
                time_string += i+"_"+dates[i][0].start+"_"+dates[i][0].end+"_"+dates[i][0].status+"|";
        });
        $("#custom_string").val(time_string);
    }

    function custom_days(key){
        disabled_dates = [];
        for(var i=0;i<7;i++){
            if ($("#header_date"+i).is(':checked')) ;
            else
                disabled_dates.push(i);
        }
                
        $("#calendar").datepicker('destroy');
        init_calendar();
    }

    function init_customdays(){
        var custom_days = db_custom_string.split('|').slice(0,-1);
        $.each(custom_days,function(index, el) {
            var temp = custom_days[index].split('_');
            var check_day = new Date(temp[0]);
            check_day = check_day.getDay(temp[0]);
            var start = $("#"+check_day+"_start").val();
            var end = $("#"+check_day+"_end").val();
            if(start == temp[1] && end == temp[2] && temp[3] == 1) ;
            else{
                if(temp[3] == 0)
                    offdates.push(temp[0]);
                else
                    customdates.push(temp[0]);
                dates[temp[0]] = [{'start':temp[1],'end':temp[2],'status':temp[3]}];
            }
        });

        var db_disabled_dates = $("#db_disabled_dates").val().split(',').slice(0,-1);
        $.each(db_disabled_dates,function(index, el) {
            var temp = parseInt(db_disabled_dates[index]);
            disabled_dates.push(temp);
        });
    }
</script>