<div class="break_line"></div>
    <div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="employee_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="employee_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search employees" class="search ui-autocomplete-input" autocomplete="off" target="employee_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="employee_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="employee_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo site_url('employees') ?>/" id="employee_list" class="tablesorter table table-bordered  table-hover datatable"
                       data-limited="true" data-limited-key="employee_global,employee_permission"  data-limited-start-column="4">
                    <thead data-key="id">
                    <tr>
                        <th data-type="text-link" data-column="first_name" data-function="update_item" data-function-params="id"  data-function-limited="insert_to_branch">First Name</th>
                        <th data-type="text-link" data-column="last_name" data-function="update_item" data-function-params="id"  data-function-limited="insert_to_branch">Last name</th>
                        <th data-type="text" data-column="email">Email</th>
                        <th data-type="text" data-column="phone_number">Phone Number</th>
                        <th data-type="buttons" data-class='["fa fa-calendar"]'
                            data-function='["update_calendar"]' data-function-params='["id"]' data-allow-key='[""]' data-align="center" data-tag='["<i>"]'>Staff Hours</th>
                        <th data-type="buttons" data-class='["fa fa-lock"]'
                            data-function='["update_absent"]' data-function-params='["id"]' data-allow-key='[""]' data-align="center" data-tag='["<i>"]'>Away Dates</th>
                        <th data-type="buttons" data-class='["fa fa-sitemap"]'
                            data-function='["update_assigned_service"]' data-function-params='["id"]' data-allow-key='["is_therapist"]' data-align="center" data-tag='["<i>"]'>Services</th>
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<!--    <table id="employee_list2" class="tablesorter table table-bordered  table-hover">-->
<!--        <thead data-key="id">-->
<!--        <tr>-->
<!--            <th data-cell-id="last_name">Last Name</th>-->
<!--            <th data-cell-id="first_name">First name</th>-->
<!--            <th data-cell-id="email">Email</th>-->
<!--            <th data-cell-id="phone_number">Phone Number</th>-->
<!--        </tr></thead>-->
<!--        <tbody id="grid-rows">-->
<!--        </tbody>-->
<!--    </table>-->
</div>
<div id="dialog" style="display:none">
</div>

<div id="message" class="alert alert-success hide">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong id="message-content">Success!</strong>
</div>

<script>
    var first_name_null = "<?php echo "edit_employee_firstname_null" ?>";
    var employee_mail_null = "<?php echo "edit_employee_email_null" ?>";
    var user_null = "<?php echo "edit_user_null" ?>";
    var password_null = "<?php echo "edit_password_null" ?>";
    var confirm_password_null = "<?php echo "edit_confirm_password_null" ?>";
    var username_exists = "<?php echo "username_exist" ?>";
</script>