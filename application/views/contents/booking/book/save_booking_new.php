<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/26/2014
 * Time: 1:14 PM
 */

$this->load->third_party('MPDF57/mpdf.php');
$date_start = new DateTime($book->start_time);
$upper_code = strtoupper($book->code);
?>

<?php
date_default_timezone_set('Asia/Singapore');
$date_start = new DateTime($book->start_time);
$default_date_start = new DateTime($book->default_start_time);
$upper_code = strtoupper($book->code);
$text = "Healing Touch Appointment Confirmed";
$location = str_replace("#", "%23", $book->branch_address);
$description = $date_start->format('h:i A')." ".$date_start->format('M')." ".$date_start->format('d')." ".$date_start->format('Y')."%0A%0A".$book->customer_name."%0A".$book->branch_name."%0A".$book->service_name."%0A".$book->employee_name."%0AReference %23".$upper_code;
$code_img = 'http://pos.healingtouchspa.com/ulti/barcode?barcode='.$upper_code.'&text='.$upper_code.'&width=300&height=50';

$datetime = new DateTime($book->default_start_time);
$datetime_begin = $datetime->format('Ymd')."T".$datetime->format('His')."Z";
$interval = new DateInterval('PT'.$book->service_duration.'M');
$datetime->add($interval);
$datetime_end = $datetime->format('Ymd')."T".$datetime->format('His')."Z";

$link = 'https://www.google.com/calendar/render?action=TEMPLATE&text='.$text.'&dates='.$datetime_begin.'/'.$datetime_end.'&details='.$description.'&location='.$location.'&sf=true&output=xml';
?>

<table width="100%" height="100%" style="font-family: TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif;">
    <tr>
        <td width='60%' height='100%'>
            <div>
                <img width="50%" src="http://pos.healingtouchspa.com/assets/core/images/email_logo.png">
            </div>
            <div style="height: 15px;">&nbsp;</div>
            <p style="color: #525254;font-size: 17px;font-weight: bold;line-height: 35px;">Healing Touch Appointment Confirmed</p>
            <div style="height: 15px;">&nbsp;</div>
            <p style="font-weight: bold;color: #b91e22;font-size: 17px;line-height: 30px;">
                <?php echo $date_start->format('h:i A')?>, <?php echo $date_start->format('M')?> <?php echo $date_start->format('d')?>, <?php echo $date_start->format('Y')?>
            </p>
            <div style="height: 15px;">&nbsp;</div>
            <p style="color: #000;font-size: 12px;">This email serve as a confirmation.</p>
            <div style="height: 6px;">&nbsp;</div>
            <p style="color: #000;font-size: 12px;">Should you wish to re-schedule your appointment, please reply to this email or call our branch and speak to one of our customer service representatives</p>
            <div style="height: 9px;">&nbsp;</div>
            <p style="font-weight: bold;font-size: 12px;">Appointment Details</p>
            <div>
                <table style="line-height: 24px;">
                    <tr>
                        <td valign="top" style="font-size: 12px;">Appointment</td>
                        <td style="font-weight: bold;color: #000;font-size: 12px;">
                            <div><?php echo $book->customer_name; ?></div>
                            <div><?php echo $book->branch_name; ?></div>
                            <div><?php echo $book->service_name; ?></div>
                            <div><?php echo $book->service_description; ?></div>
                            <div>
                                <span>With</span> 
                                <?php 
                                if($book->no_preference == 1) 
                                    echo "(No Preference)";
                                else echo $book->employee_first_name;?>                                        
                            </div>
                            <div><span style="font-weight: normal">Reference #</span> <?php echo $upper_code ?></div>
                            <div style="height: 2px;">&nbsp;</div>
                            <div>
                                <img class="img_barcode" src="<?php echo "http://pos.healingtouchspa.com/ulti/barcode?barcode={$upper_code}&text={$upper_code}&width=300&height=50"; ?>"></img>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="120" valign="top" style="font-size: 12px;">Your comment</td>
                        <td style="font-size: 12px;"><?php echo $book->comment ?></td>
                    </tr>
                </table>
            </div>
        </td>
        <td width="20%">
            <div style="font-weight: bold;color: blue;font-size: 11px;" align="center">
                <a style="text-decoration: none;" target="_blank" href="<?php echo $link; ?>">
                    Add to Calendar
                </a>
            </div>
        </td>
        <td width="20%">
            <table style="border: 1px solid #f2f2f4">
                <tr>
                    <td style="background-color: #f2f2f4;height: auto;padding: 10px;font-weight: bold;" align="center">
                        <?php echo $date_start->format('M')?> <?php echo $date_start->format('Y')?>
                    </td>
                </tr>
                <tr>
                    <td style="height: auto;padding: 10px;font-weight: bold;font-size: 50px;" align="center">
                        <?php echo $date_start->format('d')?>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #f2f2f4;height: auto;padding: 10px;font-size: 11px;line-height: 24px;">
                        <div><?php echo $book->branch_name ?></div>
                        <div><?php echo $book->branch_address ?></div>
                        <div align="center">Tel: <?php echo $book->branch_phone ?></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
$htmlfull = ob_get_contents();
ob_end_clean();


$mpdf = new mPDF();
$mpdf->WriteHTML($htmlfull);
$mpdf->Output($book->code.'.pdf','D');

//exit;
?>