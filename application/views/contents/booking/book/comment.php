<div class="col-md-12">
    <div style="margin-top: 15px;"  class="link_booking" style="width: 100%;"><p style="float: left;">Please enter your personal details below.</p>
        <div class="btn-group" style="margin-left: 5px;float: left;">
<!--            --><?php //if($this->session->userdata('login') == '') echo '<a id="toggle_login_form" href="#" onclick="on_show_tooltip_login();">Already registered?</a>'; else echo ''; ?>
            <ul id="form-login-process" class="dropdown-menu pull-left" style="padding: 30px; width: 300px;">
                <li>
                    <form action="<?php echo site_url('booking/authorize/login')?>" method="post" id="login_form">
                        <input type="text" id="txt-email2" name="_email" placeholder="Enter your email" class="form-control mbm" required="required">
                        <input type="password" id="txt-password2" name="_password" placeholder="Enter your password" class="form-control mbm">
                        <p class="login-warning" colspan="2" style="color:red; line-height:20px">
                        </p>
                        <a style="float: left" href="<?php echo site_url('booking/authorize/forgot') ?>">
                            Forget password?&nbsp;<i class="fa fa-arrow-circle-right"></i>
                        </a>
                        <a style="float: left;margin-left: 28px;" href="<?php echo site_url('booking/authorize/forgot_login_email') ?>">
                            Forget email?&nbsp;<i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </form>
                    <button id="login_booking_system" onclick="login_booking_system();" class="btn pull-right green-color">Login</button>
                </li>
            </ul>
        </div>
        <div class="time"><h6 style="color: #000;">Slot held for</h6><div class="timeleft"> <span class="minsremaining">0:00</span></div></div>
    </div>
    <br>
    <?php
    $first_name = "";
    $last_name = "";
    $phone_number = "";
    $mobile_number = "";
    $email = ""; 
    if($this->session->userdata('login') != ''){
        $login_data = $this->session->userdata('login');
        if(isset($login_data->first_name))
            $first_name = $login_data->first_name;
        if(isset($login_data->last_name))
            $last_name = $login_data->last_name;
        if(isset($login_data->mobile_number))
            $mobile_number = $login_data->mobile_number;
        if($mobile_number == "")
            $mobile_number = $login_data->phone_number;
        if(isset($login_data->email))
            $email = $login_data->email;
    }
    ?>
    <h3 class="title-step">Enter your details</h3>
    <div id="check_fill_information" class="">

    </div>
    <div style="width: 100%; position: relative;">
        <div id="customer_infomation">
            <div class="container_item">
                <div class="name_form">Your name <span class="mandatory">*</span></div>
                <div class="input_form"><input value="<?php echo $first_name; ?>" id="ctm_firstname" class="form-control custom-input" type="text" placeholder="First"/></div>
                <div class="name_form" id="fix_float_input"></div>
                <div class="input_form"><input value="<?php echo $last_name; ?>" id="ctm_lastname" class="form-control custom-input" type="text" placeholder="Last"/></div>
            </div>
            <div  class="container_item">
                <div class="name_form">Contact number<span class="mandatory">*</span></div>
                <div class="input_form"><input value="<?php echo $mobile_number; ?>" id="ctm_phone" class="form-control custom-input" type="text" placeholder="65 5555 5555"></div>
            </div>
            <div  class="container_item">
                <div class="name_form">Email<span class="mandatory">*</span></div>
                <div class="input_form"><input value="<?php echo $email; ?>" id="ctm_email" class="form-control custom-input" type="text" placeholder="your@email.com"></div>
            </div>
            <div class="container_item">
                <div class="name_form">Comment</div>
                <div class="input_form"><textarea id="ctm_comment" placeholder="Comment..." id="your_comment" class="form-control custom-textarea"></textarea></div>
            </div>
            <div class="container_item">
                <div class="name_form">
                </div>
                <div class="input_form">
                    <div>
                        <button onclick="finish_enter_comment()" type="button" class="btn btn-primary button-color" style="margin-top:5px; margin-left: 0px;min-width:28px; font-size:15px">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>