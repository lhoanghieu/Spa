<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/26/2014
 * Time: 1:14 PM
 */

$this->load->third_party('MPDF57/mpdf.php');
$date_start = new DateTime($book->start_time);
$upper_code = strtoupper($book->code);
?>
    <div class="certificate">
        <div style="height: 40px;"></div>
        <table style="height: 319px; width:615px;">
            <tr style="width:615px">
                <td width="140px">
                </td>
                <td width="445px">
                    <table>
                        <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 80px;">Appointment details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="td1" valign="top"><p>Appointment</p></td>
                            <td class="td2">
                                <p>
                                    <b style="color: #f00"><?php echo $book->customer_name?></b><br>
                                    <b style="color: #f00"><?php echo $book->branch_name?></b><br>
                                    <b style="color: #f00"><?php echo $book->service_name?></b><br>
                                    With <b style="color: #f00"><?php echo $book->employee_name ?></b><br>
                                    <b style="color: #00f"><?php echo $date_start->format('Y-m-d')?> at
                                        <?php echo $date_start->format('H:i')?></b><br/>
                                    Reference #  <b style="color: #f00"><?php echo$upper_code?></b><br/>
                                    <img class="img_barcode" src="<?php echo site_url("ulti/barcode?barcode={$upper_code}&text={$upper_code}&width=300&height=50") ?>"></img>
                                </p>
                            </td>

                        </tr>
                        <tr class="tr_border_top">
                            <td class="td1" valign="top"><p>Your Comment</p></td>
                            <td class="td2">
                                <p><?php echo $book->comment ?></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <style>
        .certificate { background:#fff url(<?php echo $this->assets->releaseImage('certificate.jpg')?>) no-repeat left top; font-size: 11px; height: 300px; padding: 95px 30px 5px 30px; width: 619px; }
        .img_barcode{width:200px; height:25px;}
        .td1{width: 120px;}
        .td2{width: 250px;}
    </style>
<?php
$htmlfull = ob_get_contents();
ob_end_clean();


$mpdf = new mPDF();
$mpdf->WriteHTML($htmlfull);
$mpdf->Output($book->code.'.pdf','D');

//exit;
?>