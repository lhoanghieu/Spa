<div class="col-md-2" id="boder-right">

</div>
<div class="col-md-10" id="boder-left" style="padding-left:0;">
    <div class="show-staff">
        <?php if(isset($employees) && count($employees)) : ?>
            <h3 style="padding-left:15px;">Choose Therapist</h3>
            <div class="child-service" onclick="choose_staff(0)">
                <div class="title-service" style="font-weight: 600">
                    <i class="fa fa-users"></i> Show all Therapist
                </div>
            </div>
            <?php foreach($employees as $key=>$employee): ?>
                <div class="child-service" onclick="choose_staff(<?php echo $key?>)">
                    <div class="title-service text-black" style="font-weight: 600">
                        <i class="fa fa-user"></i> <?php echo ucfirst(to_full_name($employee->first_name,$employee->last_name))?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="show-staff"><h4 style="padding-left:15px;">No employee available for the service in this branch. Please choose another branch or service</h4></div>
        <?php endif; ?>
    </div>
</div>

<input name="staff-chosen" id="staff-chosen" type="hidden">
