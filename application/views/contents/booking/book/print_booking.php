<?php
$date_start = new DateTime($book->start_time);
$upper_code = strtoupper($book->code);
?>

<body onload="self.print()">
    <img src="<?php echo $this->assets->releaseImage('certificate.jpg')?>" style="position: absolute; width: 679px; height: 400px; margin: auto;">
    <div style="position: absolute; width: 679px; height: 400px; margin: auto;left: 35px;">
        <div class="certificate">
            <div style="height: 40px;"></div>
            <table style="height: 319px; width:615px; margin-top: 35px;">
                <tbody>
                <tr style="width:615px">
                    <td width="140px"></td>
                    <td width="445px">
                        <table>
                            <thead>
                            <tr>
                                <th colspan="3" style="padding-left: 80px;">Appointment details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td valign="top" class="td1"><p>Appointment</p></td>
                                <td class="td2"><p><b style="color: #f00"><?php echo $book->customer_name; ?></b><br>
                                        <b style="color: #f00"><?php echo $book->branch_name; ?></b><br>
                                        <b style="color: #f00"><?php echo $book->service_name ?></b><br>With <b
                                            style="color: #f00"><?php echo $book->employee_name?></b><br><b style="color: #00f"><?php echo $date_start->format('Y-m-d')?> at
                                            <?php echo $date_start->format('H:i')?></b><br>Reference # <b style="color: #f00"><?php echo $upper_code ?></b><br>
                                        <image width="200" height="25" src="<?php echo site_url("ulti/barcode?barcode={$upper_code}&text={$upper_code}&width=300&height=50") ?>"></image>
                                    </p>
                                </td>
                            </tr>
                            <tr class="tr_border_top">
                                <td valign="top" class="td1" style="width: 120px;"><p>Your Comment</p></td>
                                <td class="td2"><p><?php echo $book->comment ?></p></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>