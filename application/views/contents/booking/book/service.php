<div class="col-md-4" id="boder-right2">
    <h3 class="title-step">Choose Category</h3>
    <div class="items">
        <?php foreach($categories as $key=>$category): ?>
            <?php if(isset($category->items) && count($category->items)): ?>
                <div class="child-item sub-title-content" onclick="load_service(<?php echo $key ?>)" category-id="<?php echo $key?>">
                    <?php echo ucfirst(strtolower($category->name)); ?></div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div style="margin-top:5px; margin-left: 0px;min-width:28px; font-size:15px" class="btn btn-primary button-color" onclick="load_step(1);">Back</div>
</div>
<div class="col-md-8" id="boder-left2" style="padding-left:0;">
    <?php foreach($categories as $key=>$category): ?>
        <?php if(isset($category->items) && count($category->items)): ?>
            <div class="show-service hidden" category-id="<?php echo $key ?>">
                <h3 class="title-step" style="padding-left:26px;">Choose Service</h3>
                <?php foreach($category->items as $item): ?>
                    <div class="child-service service_id_<?php echo $item->id; ?>" onclick="choose_service(<?php echo $item->id?>,this)">
                        <div class="title-service sub-title-content">
                            <?php echo ucfirst($item->name)?>
                            <?php if(isset($item->is_price_show) && $item->is_price_show > 0) { ?>
                                <span style='color: red'>
                                    ($<?php echo ucfirst($item->price)?>)
                                </span>
                            <?php } ?>
                        </div>
                        <div class="description-service">
                            <?php echo $item->description ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
