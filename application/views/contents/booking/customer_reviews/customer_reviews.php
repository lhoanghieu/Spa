<div class="container">
    <div>
        <h2>Write a review <?php echo !empty($branchId)?' for '.$branchId['name']:''; ?></h2>
        <p>As a recent customer of <?php echo !empty($branchId)?$branchId['name']:'us'; ?> we invite you to share your experience with the other potential customers. Your review may be published on the internet and will help others make an informed decision</p>
    </div>
    <div class="panel customer-reviews-panel col-md-7" style="padding: 0px;">
        <div class="panel-heading customer-reviews-header">
            <h3 class="panel-title">Review for Healingtouchspa <?php echo !empty($branchId)?'- '.$branchId['name']:''; ?></h3>
        </div>
        <div class="panel-body customer-reviews-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="service-rating" class="col-sm-3 control-label">Service Rating</label>
                    <div class="col-sm-7">
                        <input id="review_rating" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="review_author_name" class="col-sm-3 control-label">Author name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="review_author_name" value="<?php echo !empty($customerInfo)?$customerInfo->first_name:''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="review_title" class="col-sm-3 control-label">Review Title</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="review_title">
                    </div>
                </div>
                <div class="form-group">
                    <label for="review_body" class="col-sm-3 control-label">Review Body</label>
                    <div class="col-sm-7">
                        <textarea type="text" class="form-control" id="review_body" rows="8"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label"></label>
                    <div class="col-sm-offset-2 col-sm-7">
                        <button type="button" class="btn btn-default" id="review_submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var customerId = '<?php echo !empty($customerInfo)?$customerInfo->id:''; ?>';
    var billId = '<?php echo !empty($billId)?$billId:''; ?>';
</script>
