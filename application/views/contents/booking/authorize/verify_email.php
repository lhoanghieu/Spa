<?php $user = $this->session->userdata('login'); ?>

<center>
    <div style="height: 120px; width: 300px; margin: 50px 0px 200px 0px;">
        <p style="color:#3c2313;font-size: 25px;font-weight: 500">Create Password</p>
        <div id="show_finish">
            <?php if(!$is_valid){ ?>
                <p class="login-warning" colspan="2" style="color: red"><?php echo $message; ?></p>
            <?php } else {
                    if(!$new_pwd){
            ?>
                <p class="login-warning" colspan="2" style="color: #3c2313"><?php echo $message; ?></p>
                <p>Please create your new password: </p>
                <p><span style="width: 50px;">New Password:</span> <input id="pwd1" type="password" class="form-control input"/></p>
                <p><span style="width: 50px;">Retype New Password:</span> <input id="pwd2" type="password" class="form-control input"/></p>
                <p class="login-warning" colspan="2" style="color:red"></p>
                <button id="verify_new_pwd" type="submit" class="btn pull-right">Confirm Password</button>
                <?php } else{ ?>
                 <p class="login-warning" colspan="2" style="color: red"><?php echo $message; ?></p>
                <?php } ?>
            <?php } ?>
            <input type="hidden" id="cop" value="<?php echo $id; ?>">
        </div>
    </div>
</center>
