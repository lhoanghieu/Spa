<div class="container" align="center">
    <div style="margin: 20px;">
        <div style="font-size: 25px;font-weight: 500;margin-bottom: 30px;">Update Account Details</div>
            <?php if(isset($user)){ ?>
                <div class="row">
                    <!-- <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">NRIC:</label>
                        <div class="col-md-6"><input value="" type="text" id="nric" name="nric" autocomplete="off" placeholder="NRIC" class="insert form-control mbm required"></div>
                    </div> -->
                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">First Name:</label>
                        <div class="col-md-6"><input value="<?php echo isset($user->first_name)?$user->first_name:''?>" type="text" id="first_name" name="first_name"  placeholder="First Name" class="insert form-control mbm required"></div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label">Last Name:</label>
                        <div class="col-md-6"><input value="<?php echo isset($user->last_name)?$user->last_name:''?>" type="text" id="last_name" name="last_name" placeholder="Last Name" class="insert form-control mbm"></div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label">Address:</label>
                        <div class="col-md-6"> <input value="<?php echo isset($user->address)?$user->address:''?>" type="text" id="address" name="address" placeholder="Address" class="insert form-control mbm"></div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">Mobile Number:</label>
                        <div class="col-md-2" style="padding-right: 0px;margin-right:0px;">
                            <input value="<?php echo isset($user->mobile_number_detect['countryCallingCode'])?$user->mobile_number_detect['countryCallingCode']:''?>" type="text" id="country_calling_code" name="country_calling_code" placeholder="Country Calling Code" class="insert form-control mbm required">
                        </div>
                        <div class="col-md-4">
                            <input value="<?php echo isset($user->mobile_number_detect['phoneNumber'])?$user->mobile_number_detect['phoneNumber']:''?>" type="text" id="mobile_number" name="mobile_number" placeholder="Mobile Number" class="insert form-control mbm required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">Primary Email:</label>
                        <div class="col-md-6"><input value="<?php echo isset($user->email)?$user->email:''?>" type="text" id="email" name="email" placeholder="Email" class="insert form-control mbm required"></div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label">Secondary Email:</label>
                        <div class="col-md-6"><input value="<?php echo isset($user->recovery_email)?$user->recovery_email:''?>" type="text" id="recovery_email" name="recovery_email" placeholder="Secondary Email" class="insert form-control mbm"></div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">DOB:</label>
                        <div class="col-md-6"><input value="<?php echo isset($user->birthday)?date('d-m-Y',strtotime($user->birthday)):''?>" type="text" id="birthday" name="birthday" placeholder="DOB" class="insert form-control mbm required"></div>
                    </div>


                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">Gender:</label>
                        <div class="col-md-6">
                            <select name="gender" id="gender" class="insert form-control mbm">>
                                <?php foreach(GENDER() as $key=>$value): ?>
                                    <option value="<?php echo $key?>" <?php echo isset($user->nric)&&$user->gender == $key?'selected':''?> ><?php echo $value?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-sm-5 col-md-5 col-lg-5 control-label required">&nbsp;</label>
                        <div class="col-md-6">
                            <button id="submit-btn" type="submit" class="btn pull-right">Submit</button>
                        </div>
                    </div>
                </div>                
            <?php } ?>
    </div>
</div>
