<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center>
                <div style="height: 120px; width: 300px; margin: 50px 0px 200px 0px;">
                    <p style="font-size: 25px;font-weight: 500">Forget Email</p>
                    <div id="show_finish">
                        <?php if($message != ""){
                            if($message == 1){
                                echo '<div style="color: blue;font-weight: bold;">Your login email has been sent to your secondary email</div>'; ?>
                                <script>
                                    setTimeout(function () {
                                        window.location.href = "<?php echo site_url('booking/book')?>";
                                    }, 2200);
                                </script>
                            <?php }
                            else{
                                echo '<div style="color: red;font-weight: bold;">'.$message.'</div>';
                            }
                        }?>
                        <form id="forgot_form" method="post" action="" class="dv-contact-wrapper">
                            <br>
                            <input type="email" class="form-control mbm" placeholder="Please enter your secondary email" required="required" name="email" id="form_email">
                            <p style="color:red" colspan="2" class="login-warning"></p>
                            <button class="btn pull-right" type="submit">Get email</button>
                            <input type="hidden" value="30c8e613ca520820edc1f6d2c9eda10316e182c2" name="_token" id="form__token">
                        </form>
                    </div>
                </div>
            </center>
        </div>
    </div>

    <script>
        var message = "<?php echo $message; ?>"
    </script>