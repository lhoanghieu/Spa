<table style="width: 100%; margin-left: 20px; margin-right: 20px;">
    <tr>
        <td colspan="2" style="font-size: 13px !important;">
            <div>Login to book faster, manage appointments, check appointment history and credits balance</div>
            <div>Register now!</div>
            <div>&nbsp;</div>
            <div id="validate_check" type="text" style="display: none;color: red"></div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                Name
            </div>
        </td>
        <td>
            <div class="input_form rightcol_reg">
                <input style="font-size: 13px; background-color: #EDD8B2; border-color:#3c2313; padding: 6px; width: 260px;" id="reg_name" class="form-control custom-input" type="text" placeholder="First name"/>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                Email
            </div>
        </td>
        <td>
            <div class="input_form rightcol_reg">
                <input style="font-size: 13px; background-color: #EDD8B2;border-color:#3c2313; padding: 6px; width: 260px;" id="email_res" class="form-control custom-input" type="text" placeholder="Email"/>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                Mobile Number
            </div>
        </td>
        <td>
            <div class="input_form rightcol_reg">
                <input style="font-size: 13px; background-color: #EDD8B2;border-color:#3c2313; padding: 6px; width: 90px;" id="callingCountryCode" class="form-control custom-input" type="number" placeholder="Country code"/>
                <input style="font-size: 13px; background-color: #EDD8B2;border-color:#3c2313; padding: 6px; width: 160px;" id="mobilenum" class="form-control custom-input" type="number" placeholder="Mobile number"/>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br>
            <input type="checkbox" name="agreement" id="agreement_reg" class="policy_checkbox">
            I have read and accepted your spa policy ( <a target="_blank" style="text-decoration: none;" href="http://www.healingtouchspa.com/policy">http://www.healingtouchspa.com/policy</a> )
        </td>
    </tr>
</table>