<div class="container" align="center">
    <div class="col-sm-9 col-md-6 col-lg-6">
        <div style="font-size: 16px;font-weight: 500;margin-bottom: 8px;">Please take a survey with some questions</div>
        <form method="post" accept-charset="utf-8" id="survey_form" name="survey_form" class="form-horizontal">
            <input id="customer_id" type="hidden" value="<?php echo $customer_id?>"/>
            <?php
            foreach($questions as $question):
                if(QUESTION_TYPE($question->type) == 'y-n'): ?>
                    <div class="question form-group"  name="<?php echo $question->id;?>">
                        <input type="hidden" value="<?php echo $question->id?>"/>
                        <div style="text-align: left">
                            <label class="control-label required key_question">
                                <?php echo $question->title;?>
                            </label>
                        </div>
                        <div style="text-align: left">
                            <input type="radio" name="<?php echo $question->id;?>" class="YN" value="1" question-type="y-n" <?= isset($answers)&&isset($answers[$question->id])&&$answers[$question->id]!="No"?"checked='checked'":""?>>
                            <label for="yes_<?php echo $question->id;?>">Yes</label>
                        </div>
                        <div style="text-align: left">
                            <input type="radio" name="<?php echo $question->id;?>" class="YN" value="0" <?= !isset($answers)|| !isset($answers[$question->id]) || $answers[$question->id]=="No"?"checked='checked'":""?>  question-type="y-n">
                            <label for="no_<?php echo $question->id;?>">No</label>
                        </div>
                        <div class="answermore <?= !isset($answers)|| !isset($answers[$question->id]) || $answers[$question->id]=="No"?"hidden":""?>" style="text-align: left">
                            <label>If yes please state here:</label>
                            <input type="text" question-id="<?= $question->id ?>" class="form-control form-inps comment insert-question" value="<?= isset($answers[$question->id])?$answers[$question->id]:'No' ?>"/>
                        </div>
                    </div>
                <?php
                elseif(QUESTION_TYPE($question->type)=='one-choice'):?>
                    <div class="question form-group"  name="<?php echo $question->id;?>">
                        <div style="text-align: left">
                            <label class="control-label required key_question">
                                <?php echo $question->title;?>
                            </label>
                        </div>
                        <div style="text-align: left;margin-top: 5px;">
                            <select question-id="<?= $question->id ?>">
                                <?php foreach($question->type_values as $value): ?>
                                    <option value='<?= $value ?>' <?= isset($answers[$question->id])&&$answers[$question->id] == $value?'selected':'' ?>><?= $value ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                <?php
                elseif(QUESTION_TYPE($question->type)=='multiple-choice'):?>
                    <div class="question form-group"  name="<?php echo $question->id;?>">
                        <label class="key_question" style="width:100%"><?php echo $question->title;?></label>
                        <div>
                            <select question-id="<?= $question->id ?>" class="insert drop-style get-list-manager insert-question multiple" multiple="multiple"  question-type="multiple-choice">
                                <?php foreach($question->type_values as $value): ?>
                                    <option value='<?= $value ?>' <?= isset($answers[$question->id])&&$answers[$question->id] == $value?'selected':'' ?>><?= $value ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                <?php endif;
            endforeach;
            ?>
            <div style="margin-top: 30px;clear: both;"></div>
            <div align="right">
                <input type="checkbox" name="agreement" id="agreement" class="policy_checkbox">
                I have read and accepted your spa policy ( <a target="_blank" style="text-decoration: none;" href="http://www.healingtouchspa.com/policy">http://www.healingtouchspa.com/policy</a> )
            </div>
            <div>
                <input type="button" name="check_firstlogin" value="OK" onclick="firsttime_login()">
            </div>
        </form>
    </div>
</div>
