<center>
    <div style="height: 120px; width: 300px; margin: 50px 0px 200px 0px;">
        <p style="color:#f5527d;font-size: 25px;font-weight: 500">Login</p>
        <form id="login_form_main" method="POST" action="<?php echo site_url('booking/authorize/login')?>" >
            <br>
            <input id="txt-email" name="_email" required="required" placeholder="email" class="form-control mbm" type="text">
            <input id="txt-password" name="_password" required="required" placeholder="password" class="form-control mbm" type="password">
            <p class="login-warning" colspan="2" style="color:red"></p>
            <a href="forgot" class="pull-left text-italic text-primary man">Request password?
                &nbsp;<i class="fa fa-arrow-circle-right"></i></a>
            <button type="submit" class="btn pull-right">Login</button>
        </form>
    </div>

</center>
