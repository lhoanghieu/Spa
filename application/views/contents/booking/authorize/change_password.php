<?php $user = $this->session->userdata('login'); ?>

<center>
    <div style="height: 120px; width: 300px; margin: 50px 0px 200px 0px;">
        <p style="font-size: 25px;font-weight: 500">Change password</p>
        <div id="show_finish">
            <?php if(is_null($user) == false){ ?>
                    <input type="password" id="form_password_current" name="password_current" required="required" autocomplete="off" placeholder="Current Password" class="form-control mbm"><br>
                    <input type="password" id="form_password_first" name="password_first" required="required" autocomplete="off" placeholder="Password" class="form-control mbm"><br>
                    <input type="password" id="form_password_second" name="password_second" required="required" placeholder="Re password" class="form-control mbm">
                    <p class="login-warning" colspan="2" style="color:red"></p>
                    <button id="submit-btn" type="submit" class="btn pull-right">Change password</button>
                </p>
            <?php } ?>
        </div>
    </div>
</center>
