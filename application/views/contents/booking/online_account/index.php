<div class="container">
    <div class="row-fluid"><h3><b>Transactions</b></h3></div>
    <hr class="hr-format"/>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Next Appointment</div>
                        <div class="panel-body">
                            <?php if(!empty($nextApp)){ ?>
                                <div>Service: <b><?php echo $nextApp['service']; ?></b></div>
                                <div>Datetime: <b><?php echo $nextApp['start_time']; ?></b></div>
                                <div>Branch: <b><?php echo $nextApp['branch_name']; ?></b> </div>
                                <div>Therapist: <b><?php echo $nextApp['therapist']; ?></b></div>
                            <?php } else { ?>
                                <div>No next Available appointment</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Customer Information</div>
                        <div class="panel-body">
                            <div>Code: <b><?php echo $data->code; ?></b></div>
                            <div>Name: <b><?php echo $data->first_name; ?></b></div>
                            <div>Mobile Number: <b><?php echo $data->mobile_number; ?></b></div>
                            <div>Email: <b><?php echo $data->email; ?></b></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="tabs tabs-style-bar">
                        <nav>
                            <ul>
                                <li class="tab-current">
                                    <a href="#section-booking" ><span>All Bookings</span></a>
                                </li>
                                <li class="">
                                    <a href="#section-credit-history"><span>Credits History</span></a>
                                </li>
<!--                                <li class="" style="opacity: 0"><a href="#section-review" ><span></span></a></li>-->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:10px">
                <div class="content-wrap">
                    <section id="section-booking" class="content-current">
                        <table id="section-booking-table" class="display" cellspacing="0" width="100%" style="border:1px solid #faebcc; margin-top:10px;">
                            <thead style="color: #8a6d3b; background-color: #fcf8e3;border-color: #faebcc;">
                            <tr>
                                <th style="width:20%;">Appointments Code</th>
                                <th style="width:10% ;">Date/Times</th>
                                <th style="width:10%">Therapists</th>
                                <th style="width:10%;">Branch</th>
                                <th style="width:12.5%;">Treatment</th>
                                <th style="width:12.5%;">Status</th>
                                <th style="width:0.1%">Reschedule/Cancel</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </section>
                    <section id="section-credit-history">
                    </section>
                    <section id="section-review" class="">
                        review section
                    </section>
                </div><!-- /content -->
            </div>
        </div>
    </div>
    <input name="branch-chosen" id="branch-chosen" type="hidden">
    <input name="service-chosen" id="service-chosen" type="hidden">
    <hr class="hr-format"/>
</div>
<script>
    var sectionBookingData = '<?php echo base_url(); ?>booking/online_account/loadSectionBookingData';
    var sectionCreditHistory = '<?php echo base_url(); ?>booking/online_account/loadSectionCreditHistoryData';
    var resetCustomerPin = '<?php echo base_url();?>customers/resetPassword/'+<?php echo $data->id; ?>;
</script>