<div class="container">
    <div class="page-title">SHOPPING CART</div>
    <div class="cart-content">
        <div class="table">
            <table>
                <thead>
                    <tr>
                        <th width="10%">&nbsp</th>
                        <th width="50%">Product Name</th>
                        <th width="10%">Unit Price</th>
                        <th width="10%">Qty</th>
                        <th width="10%">Subtotal</th>
                        <th width="10%">&nbsp</th>
                    </tr>
                </thead>
                <?php if(count($this->cart->contents())): ?>
                <tbody>
                        <?php foreach($this->cart->contents() as $row): ?>
                            <tr id="<?php echo $row['rowid']?>">
                                <td><img style="width:100%" src="<?php echo $this->assets->releaseProductImage($row['id'])?>"></td>
                                <td><?php echo $row['name']?></td>
                                <td><?php echo to_currency($row['price'])?></td>
                                <td><input min="1" type="number" value="<?php echo $row['qty']?>" style="width:60%" rowid="<?php echo $row['rowid']?>" class="qty"></td>
                                <td><?php echo to_currency($row['subtotal']) ?></td>
                                <td style="text-align: center">
                                    <a class="btn-remove btn-remove2" title="Remove item" href="#" onclick="delete_item('<?php echo $row['rowid']?>')"><span class="fa fa-trash-o fa-2x"></span></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align: right; font-weight: bold">Subtotal</td>
                        <td colspan="4" style="text-align: left" id="subtotal"><?php echo to_currency($this->cart->total()) ?></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <a id="continue-shopping" class="btn" href="<?php echo booking_url('home')?>">Continue Shopping</a>
                            <a id="clear-shopping" class="btn" href="#clear" onclick="clear_cart()">Clear Shopping Cart</a>
                        </td>
                    </tr>
                </tfoot>
                <?php else: ?>
                    <tbody>
                    <tr><td colspan="6" style="text-align: center">There is no item in your cart</td></tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div>
        <iframe src="http://52.77.36.54/spa/booking/book" width="100%" height="1000" scrolling="no"></iframe>
    </div>
</div>
