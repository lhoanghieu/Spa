
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="image-box col-md-4">
                    <?php
                    $data['images'] = array($item->code);
                    $this->load->view('image_slider',$data);
                    ?>
                </div>

                <div class="product-shop col-md-8">
                    <div class="product-name">
                        <h1><?php echo $item->name?></h1>
                    </div>
                    <p class="availability in-stock">Availability: <span></span></p>
                    <p class="availability-only">
                        <span title="Only 99999 left">Only <strong>0</strong> left</span>
                    </p>
                    <div class="price-box">
                    <span id="product-price-37" class="regular-price">
                        <span class="price"><?php echo to_currency($item->price) ?></span>
                    </span>
                    </div>
                    <div class="clear"></div>
                    <div class="short-description">
                        <div class="std"><p><strong>It is very noble</strong> activity to protect health and help people recover. Our main goal is to find the easiest way of treatment. <strong>We understand</strong> that human’s activity is also very dangerous because it could lead to unforeseeable consequences like some complications or even death. Our company states that our goods are of premium quality. There is nothing to worry about.</p></div>
                    </div>
                    <div class="add-to-box">
                        <div class="add-to-cart row">
                            <div class="qty-block col-md-12">
                                <div class="row" style="padding:5px;">
                                    <div for="qty" class="col-md-2" style="margin-top:10px;" >Qty:</div>
                                    <input type="text" class="input-text qty form-control col-md-2" title="Qty" value="0" maxlength="12" id="qty" name="qty">
                                    <div class="col-md-4">&nbsp</div>
                                    <div onclick="productAddToCartForm('<?php echo $item->code ?>')" class="btn btn-cart col-md-2">
                                        <span class="fa fa-shopping-cart"><span>&nbsp</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row">
                <div class="product-collateral">
                    <div class="box-collateral box-description">
                        <h1>Details<span class="toggle"></span></h1>
                        <div class="box-collateral-content">
                            <div class="std">
                                <p><strong>Health is one</strong> of most important things in our life. <em>We think that it is a real luck to have a strong health.</em> Our way of life doesn’t increase the physiological condition of our body. Alcohol, cigarettes, unhealthy food, stresses and other factors have a great influence on our health.  The human’s immune system is very uncertain thing because there is a countless quantity of different dangerous viruses and bacteria. From ancient times plague and other infectious diseases have been killing people without leaving them any chance to survive. All these diseases are even worse than wars by the quantity of deaths.</p><p><strong>It is very noble</strong> activity to protect health and help people recover. Our main goal is to find the easiest way of treatment. <strong>We understand</strong> that human’s activity is also very dangerous because it could lead to unforeseeable consequences like some complications or even death. Our company states that our goods are of premium quality. There is nothing to worry about.</p>		</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="block block-related first">
                <div class="block-title">
                    <strong><span>Related Products</span></strong>
                    <span class="toggle"></span>
                </div>
                <div class="block-content">
                    <ol id="block-related" class="mini-products-list">
                        <?php $count = count($items_related); $i = 0;
                        foreach($items_related as $item): ?>
                        <li class="item <?php echo $i==$count?"odd":"even"?>">
                            <div class="product">
                                <a class="product-image" title="Vitamin Shoppe" href="<?php echo booking_url('product?code='.$item->code)?>"><img alt="Vitamin Shoppe" src="<?php echo $this->assets->releaseProductImage($item->code) ?>"></a>
                                <p class="product-name"><a href="<?php echo booking_url('product?code='.$item->code)?>"><?php echo $item->name?></a></p>
                                <div class="product-details">
                                    <div class="price-box">
                                        <span id="product-price-108-related" class="regular-price">
                                            <span class="price"><?php echo to_currency($item->price)?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $i++; endforeach; ?>
                    </ol>
                </div>
            </div>
        </div>

    </div>
</div>