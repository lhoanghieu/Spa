<div class="break_line"></div>
<div class="pull-right">
    <div class="row">
        <div class="col-md-12 center" style="text-align: center;">
            <div class="btn-group  ">
                <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Branchs" id="new_item_btn">
                    <span class="visible-lg">New Branchs</span>
                    <i title="New Branchs" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
                <?php endif; ?>
                <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger disabled" title="Delete"><i class="fa fa-trash-o hidden-lg fa fa-2x tip-bottom" data-original-title="Delete"></i><span class="visible-lg">Delete</span></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="row ">
    <input type="text" id="name" value="" placeholder="Search branchs" class="search ui-autocomplete-input" autocomplete="off">
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="paging" class="pull-right"></div>
            <div class="widget-box">
                <div class="widget-title">
						<span class="icon">
							<i class="fa fa-th"></i>
						</span>
                    <h5 >List of Locations</h5>
                    <span title="<?= count($data) ?> total branchs" id="total_rows" class="label label-info tip-left"><?= count($data) ?></span>
                    <a id="clear_search" class="btn btn-info btn-sm clear-state pull-right">clear search</a>

                </div>
                <div class="widget-content no-padding table_holder table-responsive" >
                    <table  class="tablesorter table table-bordered  table-hover" id="sortable_table">
                        <thead>
                        <tr>
                            <th width="20"><input id="checkAll" class="chkbox" type="checkbox"></th>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                        </tr></thead>
                        <tbody id="grid-rows">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>