<div class="widget-content">
    <form  method="post" accept-charset="utf-8"  class="form-horizontal">
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:0 ?>"/>
        <div class="form-group">
            <label for="code" class="required wide col-sm-3 col-md-3 col-lg-2 control-label required wide">UPC/EAN/ISBN:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="code" type="text" name="code" value="<?= isset($item->code)?$item->code:""?>" size="8" class="insert form-control form-inps required"  />
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="required wide col-sm-3 col-md-3 col-lg-2 control-label required wide">Category Name:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="name" type="text" name="category_name" value="<?=isset($item->name)?$item->name:""?>" size="8" class="insert form-control form-inps required"  />
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 col-md-3 col-lg-2 control-label">Description:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <textarea name="description" cols="17" rows="5" id="description" class="insert form-textarea" ><?=isset($item->description)?$item->description:""?></textarea>
            </div>
        </div>

        <!-- <div class="form-group">
            <label for="show_price_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show Price In UI</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="show_price_in_ui" name="show_price_in_ui" data-source="" value='' class="insert form-inps multi-select"/>
            </div>
        </div> -->
        <div class="form-group">
            <label for="show_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show In UI</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="show_in_ui" name="show_in_ui" data-source="<?php echo admin_url('branch/suggest')?>" value='<?= isset($item->show_in_ui)?json_encode(convert_to_array($item->show_in_ui,'','id')):json_encode(array()) ?>' class="insert form-inps multi-select"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Parent Category</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <select data-source="<?php echo site_url('categories/suggest')?>?full=1" name="category_add" id="parent_id" class="insert">
                    <option value="0"> Select Category </option>
                </select>
            </div>
        </div>

        <?= Form_Generator::CREATE_FORM_GROUP(array(
            'label'     => array('text' => 'Items', 'class' => 'required'),
            'control'   => array('type' => 'multiselect','class' => 'required insert multi-select','value' => isset($item->item_list)?convert_to_array($item->item_list,'','id'):array(),'id' => 'item_id_list','attribute' => array(
                'data-source' => site_url('items/suggest?full=1')
            ))
        )) ?>

        <div class="form-group">
            <label for="taxable" class="col-sm-3 col-md-3 col-lg-2 control-label ">Active:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="checkbox" name="status" class="chbox" id="status" <?= (isset($item->status)&&$item->status)?"checked":"" ?>>
            </div>
        </div>
    </form>
</div>
<script>
    setTimeout(function(){
        var pid = '<?php echo isset($item->parent_id) ? $item->parent_id : 0; ?>';
        if(pid) {
            $('#parent_id').select2('val','<?php echo $item->parent_id; ?>');
        }
    }, 500);
</script>
