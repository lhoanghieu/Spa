<select id="package_credit_history" class="form-control">
    <option value="">Select Package...</option>
    <?php foreach($customer_credit as $credit): ?>
        <?php $credit->item->uniqid = uniqid(); ?>
        <option value="<?php echo $credit->item->uniqid ?>"><?php echo $credit->item->name; ?></option>
    <?php endforeach; ?>
</select>
<?php if(!empty($customer_credit)){ ?>
    <?php foreach($customer_credit as $credit){ ?>
        <div class="<?php echo $credit->item->uniqid; ?> credit_item hide" style="margin-top: 10px;">
            <div class="row" style="background-color: #3c2313; color: #edd8b2;">
                <div class="row" style="font-weight: bold;">
                    <?php if(isset($export) && $export != 1){ ?>
                        <div class="col-sm-1">Package</div>
                        <div class="col-sm-7"><?php echo (isset($credit->item->name)) ? $credit->item->name : "" ?></div>
                        <div class="col-sm-1">Credit</div>
                        <div class="col-sm-3"><?php echo $credit->credit; ?></div>
                    <?php } else { ?>
                        <div>Package: <?php echo (isset($credit->item->name)) ? $credit->item->name : "" ?> - Credit: <?php echo $credit->credit; ?></div>
                    <?php } ?>
                </div>
                <div class="row" style="font-weight: bold;">
                    <?php $i=1; foreach($credit->change_time_log as $item) :?>
                        <?php if($i==1) :?>
                            <?php if(isset($export) && $export != 1){ ?>
                                <div class="col-sm-1">From</div>
                                <div class="col-sm-3"><?php echo get_user_date($item->start_date,"","",true); ?></div>
                                <div class="col-sm-1">To</div>
                                <div class="col-sm-3"><?php echo get_user_date($item->end_date,"","",true); ?></div>
                            <?php } else { ?>
                                <div>From: <?php echo get_user_date($item->start_date,"","",true); ?> - To: <?php echo get_user_date($item->end_date,"","",true); ?></div>
                            <?php } ?>
                        <?php else :?>
                            <?php if(isset($export) && $export != 1){ ?>
                                <div class="col-sm-1">From</div>
                                <div class="col-sm-3"><?php echo get_user_date($item->start_date,"","",true); ?></div>
                                <div class="col-sm-1">To</div>
                                <div class="col-sm-3"><?php echo get_user_date($item->end_date,"","",true); ?></div>
                                <div class="col-sm-1">Edit</div>
                                <div class="col-sm-3"><?php echo get_user_date($item->log_time,"","",true); ?></div>
                            <?php } else { ?>
                                <div>From: <?php echo get_user_date($item->start_date,"","",true); ?> - To: <?php echo get_user_date($item->end_date,"","",true); ?> - Edit: <?php echo get_user_date($item->log_time,"","",true); ?></div>
                            <?php } ?>
                        <?php endif; ?>
                        <?php if(isset($export) && $export != 1){ ?>
                            <br>
                        <?php } ?>
                        <?php $i++; endforeach; ?>
                </div>
            </div>
            <div class="row">
                <table class="custom-table table table-bordered text-center">
                    <thead>
                    <tr>
                        <th style="width: 80px;">Date</th>
                        <th style="width: 80px;">Bill Code</th>
                        <th>Item</th>
                        <th style="width: 120px;">Branch</th>
                        <th style="width: 100px;">Cashier</th>
                        <th style="width: 100px;">Therapist</th>
                        <th style="width: 60px;">Amount</th>
                        <th style="width: 60px;">Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($credit->detail) > 0) {
                        $content = array();
                        $temp = array();
                        foreach($credit->detail as $detail){
                            $temp[$detail['dateUnix']][] = $detail;
                        }
                        krsort($temp);
                        $credit->detail = $temp;
                        $balance = $credit->credit;
                        $old_balance = $balance;

                        foreach ($credit->detail as $slot) {
                            foreach ($slot as $detail) {
                                $symbol = "";
                                if ($detail['type'] != 2) {
                                    if ($detail['type'] == 1) {
                                        $symbol = "+";
                                        $balance -= $detail['value'];
                                    } else {
                                        $symbol = "-";
                                        $balance += $detail['value'];
                                    }
                                    $employee_name = implode(', ', $detail['employee']);
                                } else {
                                    $employee_name = $detail['bill_code'];
                                    $detail['branch_name'] = $detail['bill_code'];
                                    $detail['cashier_name'] = $detail['bill_code'];
                                    if ($detail['value'] > 0) {
                                        $symbol = "+";
                                    }
                                    $balance -= $detail['value'];
                                }

                                $item_name = ($detail['item'] !== null) ? $detail['item']->name : "";

                                $content[] = array(
                                    'date' => $detail['date'],
                                    'bill_code' => $detail['bill_code'],
                                    'item_name' => $item_name,
                                    'branch_name' => $detail['branch_name'],
                                    'cashier_name' => $detail['cashier_name'],
                                    'employee_name' => $employee_name,
                                    'amount' => $symbol . number_format($detail['value'], 2),
                                    'balance' => number_format($old_balance, 2),
                                );
                                $old_balance = $balance;
                            }
                        } ?>
                        <?php foreach(array_reverse($content) as $row): ?>
                            <tr>
                                <td><?= $row['date'] ?></td>
                                <td><?= $row['bill_code'] ?></td>
                                <td><?= $row['item_name'] ?></td>
                                <td><?= $row['branch_name'] ?></td>
                                <td><?= $row['cashier_name'] ?></td>
                                <td><?= $row['employee_name'] ?></td>
                                <td><?= $row['amount'] ?></td>
                                <td><?= $row['balance'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else{
                        ?>
                        <tr><td colspan="6"><?php echo "You have no bill on system for this credit. May this credit is the old credit was imported from old system."; ?></td></tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } ?>
<?php } else {?>
    <div>You do not have any transaction currently.</div>
<?php } ?>

