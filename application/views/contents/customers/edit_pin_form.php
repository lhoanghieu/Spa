<div class="widget-content">
    <input type="hidden" id="id_customer" value="<?php echo isset($id)?$id:''; ?>"/>
    <form class="form-horizontal" method="post">
        <div class="row">
            <div class="col-sm-12">
                <?php
                if((! isset($hide_pin) || $hide_pin == false) &&  isset($id) && $id) : ?>
                <div class="form-group">
                    <label for="code" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Old Pin:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="password" name="opin"  class="insert form-inps" id="opin" />
                    </div>
                </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="code" class="required col-sm-3 col-md-3 col-lg-2 control-label ">New PIN:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="password" name="pin"  class="insert form-inps cpassword-validate" target="cpin" id="pin"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="code" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Confirm PIN:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="password" name="cpin" class="insert form-inps required cpassword" value="" id="cpin" target="pin" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>