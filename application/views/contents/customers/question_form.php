
    <form  method="post" accept-charset="utf-8" id="survey_form" name="survey_form" class="form-horizontal">
        <input id="customer_id" type="hidden" value="<?php echo $customer_id?>"/>
        <label>Please take a survey with some questions:</label>
        <?php
        foreach($questions as $question):
            if(QUESTION_TYPE($question->type) == 'y-n'):
            ?>
                <div class="question form-group"  name="<?php echo $question->id;?>">
                        <input type="hidden" value="<?php echo $question->id?>"/>
                        <label class="key_question" style="width:100%"><?php echo $question->title;?></label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input  type="radio"  name="<?php echo $question->id;?>" class="YN" value="1" question-type="y-n" <?= isset($answers)&&isset($answers[$question->id])&&$answers[$question->id]!="No"?"checked='checked'":""?>>
                            <label for="yes_<?php echo $question->id;?>">Yes</label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input  type="radio" name="<?php echo $question->id;?>"  class="YN" value="0" <?= !isset($answers)|| !isset($answers[$question->id]) || $answers[$question->id]=="No"?"checked='checked'":""?>  question-type="y-n">
                            <label for="no_<?php echo $question->id;?>">No</label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-lg-10 answermore <?= !isset($answers)|| !isset($answers[$question->id]) || $answers[$question->id]=="No"?"hidden":""?>">
                            <label>If yes please state here:</label>
                            <input type="text" question-id="<?= $question->id ?>" class="form-control form-inps comment insert-question" value="<?= isset($answers[$question->id])?$answers[$question->id]:'No' ?>"/>
                        </div>
                </div>
            <?php
            elseif(QUESTION_TYPE($question->type)=='one-choice'):?>
                <div class="question form-group"  name="<?php echo $question->id;?>">
                    <label class="key_question" style="width:100%" ><?php echo $question->title;?></label>

                    <div class="col-sm-9 col-md-9 col-lg-10" >
                        <select question-id="<?= $question->id ?>" class="insert drop-style get-list-manager insert-question" style="width:25px;" >
                            <?php foreach($question->type_values as $value): ?>
                                <option value='<?= $value ?>' <?= isset($answers[$question->id])&&$answers[$question->id] == $value?'selected':'' ?>><?= $value ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                </div>
            <?php
            elseif(QUESTION_TYPE($question->type)=='multiple-choice'):?>
                <div class="question form-group"  name="<?php echo $question->id;?>">
                    <label class="key_question" style="width:100%"><?php echo $question->title;?></label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select question-id="<?= $question->id ?>" class="insert drop-style get-list-manager insert-question multiple" multiple="multiple"  question-type="multiple-choice">
                            <?php foreach($question->type_values as $value): ?>
                                <option value='<?= $value ?>' <?= isset($answers[$question->id])&&$answers[$question->id] == $value?'selected':'' ?>><?= $value ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                </div>
            <?php endif;
        endforeach;
        ?>
    </form>