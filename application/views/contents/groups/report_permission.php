<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 13/5/2015
 * Time: 3:43 PM
 */
?>

<form id="report_permission_form">
    <input type="hidden" id="group_id" name="group_id" value="<?= isset($group_id) ? $group_id : '0' ?>">
    <ol id='tree' >
        <?php foreach ($all_report_permission as $parent_report_id => $parent_report_field): ?>
        <li class='expanded' data-value='<?= $parent_report_field['id'] ?>'><?= $parent_report_field['name'] ?>
            <ol>
                <?php foreach ($parent_report_field['child'] as $report): ?>
                    <li class="container"
                        data-name='report_permission_data[]' data-value='<?= $report['id'] ?>' <?= isset($group_report_permission) && in_array($report['id'], $group_report_permission) ? "data-checked='1'" : "" ?>
                        ><?= $report['name'] ?></li>
                <?php endforeach;?>
            </ol>
        </li>
        <?php endforeach; ?>
    </ol>

</form>
