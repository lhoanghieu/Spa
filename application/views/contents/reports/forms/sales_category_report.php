<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">`
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>

                <div class="col-sm-4 col-md-4 col-lg-4">
                    <input max_month="1" type="text" id="start_datepicker" name="start_date" class="report_date datepicker form-control" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>

                <div class="col-sm-4 col-md-4 col-lg-4">
                    <input  type="text" id="end_datepicker" name="end_date" class="report_date datepicker form-control" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>

            <?=
            Form_Generator::CREATE_FORM_GROUP(array(
                'label'     => array('text' => 'Category'),
                'control'   => array(
                    'type'  => 'input',
                    'id'    => 'category_id',
                    'class' => 'category_id',

                    'attribute' => array(
                        'class'         => 'multi-select',
                        'data-source'   => $this->config->item('current_system')=='staff'?site_url('categories/suggest?full=1'):admin_url('categories/suggest?full=1'),
                    )
                ),
            ));
            ?>
            <?php if($current_system == 'admin'){ ?>
                <?=
                Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Branch'),
                    'control'   => array(
                        'type'  => 'select',
                        'id'    => 'branch',
                        'attribute' => array(
                            'name'          => 'branch_id',
                            'data-source'   => admin_url('branch/suggest_by_branch_group'),
                            'default-source'=> array('0' => 'All')
                        )
                    ),
                ));
                ?>
            <?php } else {?>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo staff_url('branch/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>

            <?php } ?>
            <div class="form-group hidden" id="summary_section">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="summary">Summary</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input id="summary" name="summary" value="1" type="checkbox" checked>
                </div>
            </div>
          <!--
          <div class="form-group" id="summary_section">
               <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="summary">Summary</label>

              <div class="col-sm-9 col-md-9 col-lg-10">
                   <select class="js-example-basic-multiple" multiple="multiple" data-source='<?= site_url('categories/suggest?full=1')?>' data-value='["1","2"]' style="width:100%">
                   </select>
            </div>
           </div>-->
            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>

        </form>
    </div>
</div>


