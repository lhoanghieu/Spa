<!-- <?php
$input = $this->session->userdata('report');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input  max_month="1" type="text" id="start_datepicker" name="start_date" class="report_date datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input  type="text" id="end_datepicker" name="end_date" class="report_date datepicker" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Item</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select name="item_id" data-source="<?php echo site_url('items/suggest') ?>"></select>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>
        </form>
    </div>
</div> -->
<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="start_date" name="start_date" class="datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="end_date" name="end_date" class="datepicker" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Staff</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="employee_id" class="multi-select" id="employee_id" data-source="<?php echo admin_url('employees/suggest?minimal=false')?>">
                </div>
            </div>

            <div  class="form-group category_group" >
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="category_id" class="multi-select" id="category_id" data-source="<?php echo admin_url('categories/suggest?minimal=true')?>">
                </div>
            </div>

            <div  class="form-group item_group" >
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Item</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="item_id" class="multi-select" id="item_id" data-source="<?php echo admin_url('items/suggest?minimal=true')?>">
                </div>
            </div>

            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo admin_url('branch/suggest_by_branch_group')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>

            <?php } else {?>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo staff_url('branch/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>

            <?php } ?>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>
                <div id="condition_filter" class="col-sm-9 col-md-9 col-lg-10">
                    <input checked="checked" type="radio" name="type" value="detail" id="none-summary">&nbsp;<label for="none-summary">None</label><br>
                    <input type="radio" name="type" value="daily" id="daily-summary">&nbsp;<label for="daily-summary">Daily</label><br>
                    <input type="radio" name="type" value="monthly" id="monthly-summary">&nbsp;<label for="monthly-summary">Monthly</label><br>
                    <input type="radio" name="type" value="monthly-branch-total" id="monthly-branch-total">&nbsp;<label for="monthly-branch-total">Monthly Branch Total</label><br>
                    <input type="radio" name="type" value="item" id="item-summary">&nbsp;<label for="item-summary">Item Summary</label>
                </div>
            </div>
            <div id="condition_filter_summary" class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range"></label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="summary" value="1">Summary
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Submit </button>
            </div>
        </form>
    </div>
</div>