<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">

            <?=
            Form_Generator::CREATE_FORM_GROUP(array(
                'label'     => array('text' => 'Employee'),
                'control'   => array(
                    'type'  => 'input',
                    'id'    => 'employee',
                    'class' => 'employee',
                    'attribute' => array(
                        'class'         => 'multi-select',
                        'data-source'   => $this->config->item('current_system')=='staff'?site_url('employees/suggest?full=1'):admin_url('employees/suggest?full=1'),
                    )
                ),
            ));
            ?>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="start_date" name="start_date" class="datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="end_date" name="end_date" class="datepicker" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>
            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="ec_branch_group_id" class="branch" name="ec_branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>
                <div  class="form-group" >
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <input name="branch_id" id="ec_branch_id" data-source="<?php echo staff_url('branch/suggest?minimal=true')?>">
                    </div>
                </div>
            <?php } else {?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="ec_branch_group_id" class="branch" name="ec_branch_group_id" data-source="<?php echo staff_url('branch_group/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>
                <div  class="form-group" >
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <input name="branch_id" id="ec_branch_id" data-source="<?php echo staff_url('branch/suggest?minimal=true')?>">
                    </div>
                </div>
            <?php } ?>


            <?php if($current_system == 'admin'){ ?>
                <?=
                Form_Generator::CREATE_FORM_GROUP(array(
                    'label'     => array('text' => 'Departments'),
                    'control'   => array(
                        'type'  => 'select',
                        'id'    => 'departments',
                        'attribute' => array(
                            'name'          => 'department_id',
                            'data-source'   => admin_url('departments/suggest_by_department_group'),
                            'default-source'=> array('0' => 'All')
                        )
                    ),
                ));
                ?>
            <?php } ?>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="radio" name="summary_type" onclick="change_option(this)" value="none" id="none" checked="checked">&nbsp;<label for="none">None</label><br>
                    <input type="radio" name="summary_type" onclick="change_option(this)" value="daily" id="daily">&nbsp;<label for="daily">Daily</label><br>
                    <input type="radio" name="summary_type" onclick="change_option(this)" value="monthly" id="monthly">&nbsp;<label for="monthly">Monthly</label><br>
                    <input type="radio" name="summary_type" onclick="change_option(this)" value="monthly-branch-total" id="monthly-branch-total">&nbsp;<label for="monthly-branch-total">Monthly Branch Total</label><br>
                </div>
            </div>

            <div class="form-group" id="summary_checkbox">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range"></label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"  name="summary" value="1"> Summary
                        </label>
                    </div>
                </div>
            </div>
            <input type="hidden" value="<?php echo $current_system; ?>" id="current_system">

            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Submit </button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function change_option(classname){
        var self = $(classname);
        var current_val = self.val();
        if(current_val == "monthly-branch-total")
            $("#summary_checkbox").hide();
        else
            $("#summary_checkbox").show();
    }
</script>