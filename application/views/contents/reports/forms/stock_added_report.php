<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input max_month="1" style="width:15%" type="text" id="start_datepicker" name="start_date" class="report_date datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input  style="width:15%" type="text" id="end_datepicker" name="end_date" class="report_date datepicker" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Item</label>

                <div class="col-sm-2 col-md-2 col-lg-3">
                    <select data-source="<?php echo site_url('items/suggestProduct')?>" name="item_id">
                        <option value="0">All</option>
                    </select>
                </div>
            </div>

            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo admin_url('branch/suggest_by_branch_group')?>"></select>
                    </div>
                </div>
            <?php } else {?>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo staff_url('branch/suggest')?>">
                        </select>
                    </div>
                </div>

            <?php } ?>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Sort by</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select id="sortby" class="branch" name="sortby">
                        <option value="date">Date</option>
                        <option value="name">Items Name</option>
                        <option value="code">Items Code</option>
                    </select>
                </div>
            </div>

            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>
        </form>
    </div>
</div>
