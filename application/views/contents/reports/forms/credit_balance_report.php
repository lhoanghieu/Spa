<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="end_date" name="month_distance" class="datepicker" value="<?= isset($input->month_distance)?$input->month_distance:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select id="branch" class="branch" name="branch_group_id" data-source="
                    <?php echo $current_system == 'admin'?
                        admin_url('branch_group/suggest'):
                        staff_url('branch_group/suggest')?>">
                    </select>
                </div>
            </div>

            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>
        </form>
    </div>
</div>


