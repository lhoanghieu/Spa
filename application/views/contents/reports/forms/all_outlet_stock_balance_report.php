<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">At</label>

                <div class="col-sm-4 col-md-4 col-lg-5">
                    <input type="text" id="start_datepicker" name="start_date" class="report_date datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                    <input type="text" id="start_timepicker" name="start_time" class="time-picker" value="<?= get_user_date(get_database_date(),'','H:i') ?>">
                </div>
            </div>
            <div  class="form-group category_group" >
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="category_id" class="multi-select" id="category_id" data-source="<?php echo admin_url('categories/suggest?minimal=true')?>">
                </div>
            </div>
            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>">
                            <option value="0">All Groups</option>
                        </select>
                    </div>
                </div>
            <?php } else {?>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_group_id" data-source="<?php echo staff_url('branch_group/suggest')?>">
                        <option value="0">All Groups</option>
                        </select>
                    </div>
                </div>

            <?php } ?>

            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>
        </form>
    </div>
</div>
