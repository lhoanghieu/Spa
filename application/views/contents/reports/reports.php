<?php $current_system = $this->config->item('current_system') ?>
<div class="break_line"></div>
<div class="row report-listing">
    <div class="col-md-6  ">
        <div class="panel">
            <div class="panel-body">
                <div class="list-group parent-list">
                    <?php foreach($reports as $key=>$report): ?>
                    <a href="#" class="list-group-item" id="<?php echo $key; ?>"><i class="fa fa-categories"></i>  <?php echo $report['name']; ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="report_selection">
        <div class="panel">
            <div class="panel-body child-list">
                <h3 class="page-header text-info" style="margin-top: 5px;">&laquo; Reports: Make a selection</h3>
                <?php foreach($reports as $key=>$report): ?>
                    <div class="list-group <?php echo $key; ?> hidden">
                        <?php if($current_system == 'staff'): ?>
                        <?php foreach($report['child'] as $cKey => $child):  ?>
                            <?php if(!isset($child['system']) || $current_system == strtolower(MENU_TYPE($child['system']))  || strtolower(MENU_TYPE($child['system'])) == 'all'): ?>
                                <?php if(!isset($child['in_report_page']) || (isset($child['in_report_page']) && $child['in_report_page'])):?>
                            <a class="list-group-item" href="<?php echo site_url('reports/load_report_form/' . $cKey);?>" ><i class="fa fa-reports"></i> <?php echo $child['name']; ?></a>
                            <?php   endif;
                            endif;
                        endforeach; else:
                            foreach($report['child'] as $cKey => $child):  ?>
                                <?php if(!isset($child['system']) || $current_system == strtolower(MENU_TYPE($child['system']))  || strtolower(MENU_TYPE($child['system'])) == 'all'): ?>
                                    <a class="list-group-item" href="<?php echo admin_url('reports/load_report_form/' . $cKey);?>" ><i class="fa fa-reports"></i> <?php echo $child['name']; ?></a>
                                <?php endif;
                            endforeach;
                            ?>
                        <?php endif;?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>