
<?php foreach($credit_transaction as $customer_code => $credit_trans_fields):?>
    <div class="row" style="font-size: 12px; font-weight: bold;">
        <div class="col-sm-6 col-md-5 col-lg-3">Customer Name: <?php echo $credit_trans_fields[0]['customer_name'] ?> </div>
        <div class="col-sm-6 col-md-5 col-lg-3">Customer ID: <?php echo $customer_code?></div>
    </div>
    <?php foreach($credit_trans_fields as $package_fields): ?>
        <div style="margin-top: 10px; margin-left:25px;">
            <div class="row">
                <table class="table table-bordered table-striped table-hover data-table tablesorter">
                    <thead>
                    <tr>
                        <th style="width: 10%">Date</th>
                        <th style="width: 20%">Bill Code</th>
                        <th style="width: 20%">Item</th>
                        <th style="width: 10%">Redeem</th>
                        <th style="width: 10%">Balance</th>
                        <th style="width: 30%">Employees</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="color:red; font-weight: bold">
                        <td colspan="10"><?php echo $package_fields['package_name']?></td>
                    </tr>
                    <?php foreach($package_fields['rows'] as $row): ?>
                        <tr>
                            <td><?php echo $row['created_date'] ?></td>
                            <td><?php echo $row['code'] ?></td>
                            <td><?php echo $row['item_name'] ?></td>
                            <td><?php echo $row['after']>$row['before']?('+'.($row['after']-$row['before'])):$row['after']-$row['before'] ?></td>
                            <td><?php echo $row['after'] ?></td>
                            <td><?php echo $row['employee_name']?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="10"> <?php $change = $package_fields['summary']['after'] - $package_fields['summary']['before'];
                            echo "Package Points before: {$package_fields['summary']['before']} + Points: {$change} = Available Points: {$package_fields['summary']['after']}"?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endforeach; ?>
    <hr class="divider">
<?php endforeach; ?>