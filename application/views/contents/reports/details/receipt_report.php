<?php

$colspan_payment = count($payment_types);
$colpsan_others = 11;
?>

<table class="table table-bordered table-striped table-hover data-table tablesorter" id="sortable_table">
    <thead>
    <tr>
        <?php if($type == 'none'):?>
        <th>Ref no.</th>
        <th>Date</th>
        <th>Member ID</th>
        <th>Customer Name</th>
        <?php else: ?>
        <th>Date</th>
        <?php endif; ?>
        <?php foreach($payment_types as $payment_type):?>
        <th> <?= $payment_type->name ?></th>
        <?php endforeach ?>
        <th>Discount</th>
        <th>Voucher</th>
        <?php if($type == 'daily' || $type == 'monthly'): ?>
            <th>Redeem+</th>
            <th>Redeem-</th>
        <?php endif;?>
        <th>Redeem</th>
        <th>Sales</th>
        <th>GST</th>
        <th>Amount Due</th>
        <th>Grand</th>
    </tr>
    </thead>
    <?php if(isset($bills) && count($bills)): ?>
    <tbody>
    <?php foreach($bills as $key=>$bill):?>
    <tr>
        <?php if($type == 'none'):?>
            <td><?= $bill->code ?></td>
            <td><?= $bill->created_date?></td>
            <td><?= $bill->customer_code?></td>
            <td><?= to_full_name($bill->first_name,$bill->last_name)?></td>
        <?php else: ?>
            <td><?= $key ?></td>
        <?php endif; ?>
        <?php foreach($payment_types as $payment):?>
            <td><?= isset($bill->payments[$payment->name])?to_currency($bill->payments[$payment->name]->amount):to_currency(0) ?></td>
        <?php endforeach;?>
        <td><?= to_currency($bill->items_discount)?></td>
        <td><?= to_currency($bill->discount)?></td>
        <?php if($type == 'daily' || $type == 'monthly'): ?>
            <th><?= number_format($bill->redeem_add,2)?></th>
            <th><?= number_format($bill->redeem_minus,2)?></th>
        <?php endif;?>
        <td><?= number_format($bill->redeem,2)?></td>
        <td><?= to_currency($bill->sale_value)?></td>
        <td><?= to_currency($bill->gst)?></td>
        <td><?= to_currency($bill->amount_due)?></td>
        <td><?= to_currency($bill->grand)?></td>
    </tr>
    <?php endforeach?>
    <?php if(isset($branch_total)): ?>
    <?php foreach($branch_total as $row):?>
    <tr>
        <td colspan="<?= $type=='none'?4:1 ?>" style="text-align: right;"><?php echo $row->branch_name?></td>
        <?php foreach($payment_types as $payment):?>
        <td><span class="under"><?=$payment->name?></span><br/><?= isset($row->payments->{$payment->name})?to_currency($row->payments->{$payment->name}):to_currency(0) ?></td>
        <?php endforeach;?>
        <td><span class="under">Discount</span><br/><?php echo to_currency($row->items_discount, 2); ?></td>
        <td><span class="under">Voucher</span><br/><?php echo to_currency($row->discount, 2); ?></td>
        <?php if($type == 'daily' || $type == 'monthly'): ?>
        <td><span class="under">Redeem+</span><br/><?php echo number_format($row->redeem_add, 2); ?></td>
        <td><span class="under">Redeem-</span><br/><?php echo number_format($row->redeem_minus, 2); ?></td>
        <?php endif;?>
        <td><span class="under">Redeem</span><br/><?php echo number_format($row->redeem, 2); ?></td>
        <td><span class="under">Sales</span><br/><?php echo to_currency($row->sale_value, 2); ?></td>
        <td><span class="under">GST</span><br/><?php echo to_currency($row->gst, 2); ?></td>
        <td><span class="under">Amount Due</span><br/><?php echo to_currency($row->amount_due, 2); ?></td>
        <td><span class="under">Grand</span><br/><?php echo to_currency($row->grand, 2); ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif;?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="<?= $type=='none'?4:1 ?>" style="text-align: right;">Total</td>
        <?php foreach($payment_types as $payment):?>
            <td><span class="under"><?=$payment->name?></span><br/><?= isset($total->payments->{$payment->name})?to_currency($total->payments->{$payment->name}):to_currency(0) ?></td>
        <?php endforeach;?>
        <td><span class="under">Discount</span><br/><?php echo to_currency($total->items_discount, 2); ?></td>
        <td><span class="under">Voucher</span><br/><?php echo to_currency($total->discount, 2); ?></td>
        <?php if($type == 'daily' || $type == 'monthly'): ?>
            <td><span class="under">Redeem+</span><br/><?php echo number_format($total->redeem_add, 2); ?></td>
            <td><span class="under">Redeem-</span><br/><?php echo number_format($total->redeem_minus, 2); ?></td>
        <?php endif;?>
        <td><span class="under">Redeem</span><br/><?php echo number_format($total->redeem, 2); ?></td>
        <td><span class="under">Sales</span><br/><?php echo to_currency($total->sale_value, 2); ?></td>
        <td><span class="under">GST</span><br/><?php echo to_currency($total->gst, 2); ?></td>
        <td><span class="under">Amount Due</span><br/><?php echo to_currency($total->amount_due, 2); ?></td>
        <td><span class="under">Grand</span><br/><?php echo to_currency($total->grand, 2); ?></td>
    </tr>
    </tfoot>
    <?php else:?>
        <tbody>
        <tr><td colspan="<?php echo $colspan_payment + $colpsan_others ?>">There is no current bill</td></tr>
        </tbody>
    <?php endif; ?>
</table>