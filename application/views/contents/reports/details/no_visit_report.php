<table class="table table-bordered table-striped table-hover data-table">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile Number</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($customers as $customer_id=>$customer_fields):?>
        <tr>
            <td><?php echo $customer_fields->first_name ?></td>
            <td><?php echo $customer_fields->last_name ?></td>
            <td><?php echo $customer_fields->mobile_number ?></td>
            <td><?php echo $customer_fields->email ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>