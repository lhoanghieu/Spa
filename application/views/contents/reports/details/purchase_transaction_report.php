<table class="table table-bordered table-striped table-hover data-table tablesorter">
    <thead>
    <tr>
        <th style="width: 10%">Date</th>
        <th style="width: 5%">Bill Code</th>
        <th style="width: 15%">Item</th>
        <th style="width: 10%">Price</th>
        <th style="width: 10%">Quantity</th>
        <th style="width: 10%">Discount</th>
        <th style="width: 20%">Employee</th>
        <th style="width: 10%">Redeem</th>
        <th style="width: 10%">Redeem Package</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($bills as $customer_id => $customer_bill_fields): ?>
        <tr style="color:red; font-weight: bold; background-color: #ecf0f1">
            <td colspan="2"><?php echo $customer_bill_fields['customer_code'] ?></td>
            <td colspan="8"><?php echo $customer_bill_fields['customer_name'] ?></td>
        </tr>
        <?php foreach ($customer_bill_fields['rows'] as $bill_fields):
            $isFirstBillItem = true;
            $rowspan = count($bill_fields->bill_items);
            foreach($bill_fields->bill_items as $bill_items){
                $rowspan += isset($bill_items->credit_used_detail)?count($bill_items->credit_used_detail)-1:0;
            }
            ?>
            <tr>
                <td rowspan="<?php echo $rowspan?>"><?php echo $bill_fields->created_date ?></td>
                <td rowspan="<?php echo $rowspan?>"><?php echo $bill_fields->code ?></td>
                <?php foreach($bill_fields->bill_items as $bill_item):
                    $rowspan_item = isset($bill_item->credit_used_detail)?count($bill_item->credit_used_detail):1;
                    $isFirstCredit = true;
                    if(! $isFirstBillItem):?>
                        <tr>
                    <?php endif;?>
                    <td rowspan="<?php echo $rowspan_item?>"><?php echo $bill_item->item_detail->name?></td>
                    <td rowspan="<?php echo $rowspan_item?>"><?php echo to_currency($bill_item->price)?></td>
                    <td rowspan="<?php echo $rowspan_item?>"><?php echo $bill_item->quantity?></td>
                    <td rowspan="<?php echo $rowspan_item?>"><?php $discount = $bill_item->discount_value;
                        if($bill_item->discount_type == 1){
                            $discount = round($discount).'%';
                        }else{
                            $discount = to_currency($discount);
                        }
                        echo $discount?></td>
                    <td rowspan="<?php echo $rowspan_item?>"><?php echo $bill_item->employee_name?></td>
                    <?php if(isset($bill_item->credit_used_detail)):
                        foreach($bill_item->credit_used_detail as $credit_fields):
                            if(! $isFirstCredit):?>
                                <tr>
                            <?php endif;?>
                                    <td><?php echo -$credit_fields['credit_value'] ?></td>
                                    <td><?php echo $credit_fields['credit_name'] ?></td>
                            <?php if(! $isFirstCredit):?>
                                </tr>
                            <?php else: $isFirstCredit = false;
                            endif;?>
                    <?php endforeach; ?>
                    <?php elseif(isset($bill_item->credit_add_detail)): ?>
                            <td><?php echo $bill_item->credit_add_detail['credit_value'] ?></td>
                            <td><?php echo $bill_item->credit_add_detail['credit_name'] ?></td>
                    <?php endif; ?>
                    <?php if(! $isFirstBillItem):?>
                        </tr>
                    <?php else: $isFirstBillItem = false; endif;?>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>

