<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Bill Code</th>
        <th>Created Time</th>
        <th>Void Time</th>
        <th>Replaced by Bill Code</th>
        <th>Replaced Time</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($bills as $bill):?>
        <tr>
            <td><?php echo $bill->code ?></td>
            <td><?php echo $bill->created_date ?></td>
            <td><?php echo $bill->void_date ?></td>
            <td><?php echo $bill->replacing_bill->code ?></td>
            <td><?php echo $bill->replace_bill_date ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>