<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Backdated From</th>
        <th>Backdated To</th>
        <th>Backdated Time</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($bills as $bill_id=>$logs):?>
        <tr style="color:red; font-weight: bold"><td colspan="5"><?php echo $logs['code']?></td></tr>
        <?php foreach($logs['log'] as $log): ?>
            <tr>
            <td><?php echo $log['backdate_from']?></td>
            <td><?php echo $log['backdate_to']?></td>
            <td><?php echo $log['backdate_range']?></td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>