<table class="table table-bordered table-striped table-hover data-table tablesorter">
    <?php
    foreach($rows as $employee_code=>$values){ ?>

        <?php echo "<tr row-toggle='off'><td colspan='6' style='color: red;'><strong>".$values['customer_name']."</strong></td></tr>";
        ?>
        <tr>
            <td class="under" >Receipt Number</td>
            <td class="under" >Cashier Name</td>
            <td class="under" >Customer Name</td>
            <td class="under" >Member ID</td>
            <td class="under" >Item</td>
            <td class="under" >Sale Value</td>
        </tr>
        <?php
        foreach($values['rows'] as $item_key=>$item_values){
            echo "<tr>"
                . "<td class='percen5 row-data'>{$item_values->bill_code}</td>"
                . "<td class='percen5 row-data'>{$item_values->cashier_name}</td>"
                . "<td class='percen5 row-data'>".to_full_name($item_values->customer_first_name,$item_values->customer_last_name)."</td>"
                . "<td class='percen10 row-data'>".$item_values->member_id."</td>"
                . "<td class='percen5 row-data'>{$item_values->item_name}</td>"
                . "<td class='percen10 row-data'>".to_currency($item_values->sale_value)."</td>"
                . "</tr>";
        }

        echo "<tr class='sum_total_emp'>"
            . "<td colspan='5' style='text-align: right; padding-right: 2%;'>Total: </td>"
            . "<td>".to_currency($values['total'])."</td>"
            . "</tr>";
    } ?>


    <?php if(count($rows)>1):
        echo "<tr class='sum_total_emp'>"
            . "<td colspan='5' style='text-align: right; padding-right: 2%;'><strong>Total Sales : </strong></td>"
            . "<td><strong>".to_currency($total)."</strong></td>"
            . "</tr>";
    endif;
    if(!$rows):
        echo "<tr><td colspan='5' style='color: red;'>No data</td></tr>";
    endif;
    ?>
</table>


