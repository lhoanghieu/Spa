

<?php if($type == 'detail'):?>
    <h3 class="">Detailed Settlement Report</h3>
<?php foreach($redemption_detail as $branch_group_id=>$by_and_at): ?>
    <?php if(isset($by_and_at['by']) &&  count($by_and_at['by'])): ?>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width:25%" class="text-primary">Redemption by <?php echo $branch_group_data[$branch_group_id]->name ?></th>
                    <th style="width:10%">Branch Name</th>
                    <th style="width:20%">Date</th>
                    <th style="width:10%">Bill No</th>
                    <th style="width:25%">Item</th>
                    <th style="width:10%">Redemption</th>
                </tr>
                </thead>
            <tbody>
            <?php foreach($by_and_at['by'] as $row): ?>
            <tr>
                    <td>Customer <?php echo $row['customer_code'] . ' - ' . $row['customer_name']?></td>
                    <td><?php echo $row['branch_name']?></td>
                    <td><?php echo $row['created_date']?></td>
                    <td><?php echo $row['bill_code']?></td>
                    <td><?php echo $row['item_name']?></td>
                    <td><?php echo $row['redemption']?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
    <?php if(isset($by_and_at['at'])  && count($by_and_at['at'])): ?>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width:25%" class="text-danger">Redemption at <?php echo $branch_group_data[$branch_group_id]->name ?></th>
                <th style="width:10%">Branch Name</th>
                <th style="width:20%">Date</th>
                <th style="width:10%">Bill No</th>
                <th style="width:25%">Item</th>
                <th style="width:10%">Redemption</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($by_and_at['at'] as $row): ?>
                <tr>
                    <td>Customer <?php echo $row['customer_code'] . ' - ' . $row['customer_name']?></td>
                    <td><?php echo $row['branch_name']?></td>
                    <td><?php echo $row['created_date']?></td>
                    <td><?php echo $row['bill_code']?></td>
                    <td><?php echo $row['item_name']?></td>
                    <td><?php echo $row['redemption']?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
<?php endforeach;?>


<?php else:?>
    <h3 class="">Summary Report</h3>

    <table class="table table-bordered table-striped table-hover">
        <tbody>
            <?php foreach($redemption_summary as $branch_group_id => $row_by_at):
                if($row_by_at['by']!=0 || $row_by_at['at']!=0):
                ?>
                <tr>
                    <th style="width:30%">Redemption by <?php echo $branch_group_data[$branch_group_id]->name ?></td>
                    <td>&nbsp</td>
                </tr>
                <?php foreach($row_by_at['branch_by'] as $branch_name=>$branch_by): ?>
                    <tr>
                        <td style="text-align: right"><?php echo $branch_name ?></td>
                        <td style="text-align: left"><?php echo $branch_by?></td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <td style="text-align: right; font-weight: bold"><?php echo "Total" ?></td>
                        <td style="text-align: left"><?php echo $row_by_at['by']?></td>
                    </tr>
                <tr>
                    <th style="width:30%">Redemption at <?php echo $branch_group_data[$branch_group_id]->name ?></td>
                    <td>&nbsp</td>
                </tr>
                <?php foreach($row_by_at['branch_at'] as $branch_name=>$branch_at): ?>
                <tr>
                    <td style="text-align: right"><?php echo $branch_name ?></td>
                    <td style="text-align: left"><?php echo -$branch_at?></td>
                </tr>
                <?php endforeach; ?>
                    <tr>
                        <td style="text-align: right; font-weight: bold"><?php echo "Total" ?></td>
                        <td style="text-align: left"><?php echo -$row_by_at['at']?></td>
                    </tr>
                <tr>
                    <th style="width:30%">Settlement</td>
                    <td><?php echo $row_by_at['by'] - $row_by_at['at']?></td>
                </tr>
                <tr colspan="2">&nbsp</tr>
            <?php endif; endforeach ?>
        </tbody>
    </table>
<?php endif; ?>