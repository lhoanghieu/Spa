<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Item No</th>
        <th>Item Name</th>
        <th>Added</th>
        <th>Time</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($stock_items)):
        foreach($stock_items as $key_date=>$items_trans_log):?>
        <tr><td colspan="10" style="color:red;font-weight: bold"><?php echo $key_date?></td></tr>
            <?php foreach($items_trans_log as $item_id=>$item_trans_detail): ?>
                <?php foreach($item_trans_detail['transaction'] as $trans_row):?>
                    <tr>
                        <td><?php echo $item_trans_detail['item_code']?></td>
                        <td><?php echo $item_trans_detail['item_name']?></td>
                        <td><?php echo $trans_row['add']?></td>
                        <td><?php echo $trans_row['time']?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
    <?php endforeach; ?>

    <?php else: ?>
        <tr><td colspan="10">There is no record</td></tr>
    <?php endif; ?>
    </tbody>
</table>