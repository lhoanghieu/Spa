<div class="break_line"></div>
<div class="">
    <div class="row">
        <label id="message" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Store Configuration Information</h5>
                </div>

                <form action="<?php echo site_url('config/save')?>" method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Information</h4>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Name</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_name" name="branch_data[name]" class="form-inps form-control" value="<?php echo isset($branch_data)?$branch_data->name:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Address</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_address" name="branch_data[address]" class="form-inps form-control"  value="<?php echo isset($branch_data)?$branch_data->address:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Phone Number</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_phone_number" name="branch_data[phone_number]" class="form-inps form-control"  value="<?php echo isset($branch_data)?$branch_data->phone_number:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Email</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_email" name="branch_data[email]" class="form-inps form-control"  value="<?php echo isset($branch_data)?$branch_data->email:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Fax</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_fax" name="branch_data[fax]" class="form-inps form-control"  value="<?php echo isset($branch_data)?$branch_data->fax:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label">Branch Sale ID Prefix</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_fax" name="branch_data[sale_id_prefix]" class="form-inps form-control"  value="<?php echo isset($branch_data)?$branch_data->sale_id_prefix:'' ?>">
                        </div>
                    </div>
                    <hr class="divider">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Config</h4>
                    </div>
                    <?php foreach($data as $row) : ?>
                        <?= generate_form_control($row) ?>
                    <?php endforeach; ?>

                    <hr class="divider">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">SMS Configuration</h4>
                    </div>
                    <?php foreach($sms as $row) : ?>
                        <?= generate_form_control($row,false) ?>
                    <?php endforeach; ?>

                    <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                    <div class="form-actions">
                        <button name="submitf" value="Save" id="submitf" class="submit_button btn btn-primary float_right">Submit</button>
                    </div>
                    <?php endif; ?>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var controller = "config/";
</script>
