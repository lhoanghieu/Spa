
<?php if(count($breadcrumbs)): ?>
<ol class="breadcrumb booking-breadcrumb">
    <?php foreach($breadcrumbs as $key => $value): ?>
        <li><a href="<?php echo isset($value['url']) ? booking_url($value['url']) : ""; ?>"><?php echo $value['text']; ?></a></li>
    <?php endforeach; ?>
</ol>

<?php endif; ?>