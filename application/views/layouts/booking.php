<?php if(!isset($simple_mode) || (isset($simple_mode) && !$simple_mode)): ?>
<header>
    <div style="height: 4px; background: #593729;"></div>
        <div id="header"><?php echo $header?></div>

  <!--  <div style="position: relative; top: 0px;" class="nav-container">
        <?php echo $menu ?>
    </div>
    -->
</header>
<?php endif ?>
<div id="page-wrapper" class="page-wrapper">
    <div class="main-container" style="padding-top:30px; padding-bottom:30px; min-height:760px">
        <?php echo $content ?>
    </div>
</div>

<?php if(!isset($simple_mode) || (isset($simple_mode) && !$simple_mode)): ?>
<footer>
    <?php echo $footer ?>
</footer>
<?php endif ?>