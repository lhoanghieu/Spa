<?php
$user = $this->session->userdata('login');
?>

<div id="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 topbar-language">
                <a href="http://www.healingtouchspa.com/" target="_blank">
                    <img src="<?php echo $this->assets->releaseImage('logo.png')?>" style="">
                </a>
            </div>
            <div class="col-md-5 col-xs-12 topbar-member">
                <div style="float: right; padding-top: 20px;">
                <?php if($user){ ?>
                <div class="btn-group">
                    <a href="#" data-toggle="dropdown" class="btn-login-rigister" style="margin-right: 15px;" >
                        <?php echo $user->first_name ?>
                        <i class="fa fa-caret-down" style="padding: 0px 0px 0px 3px;margin-top: 5px;"></i>
                        <!-- <i class="fa fa-cog"></i> -->
                    </a>
                    <ul id="form-register" class="dropdown-menu pull-right" style="width: 220px; padding: 20px;">
                        <li id="info_update_form">
                            <a class="href" href="<?php echo booking_url('home') ?>">Main Menu</a>
                            <a class="href" href="<?php echo site_url();?>booking/online_account">Appointments/Reschedule</a>
                            <a class="href" href="<?php echo site_url();?>booking/authorize/change_information">Update Account Details</a>
                            <a class="href" href="<?php echo site_url();?>booking/authorize/change_password">Change Login Password</a>
                            <a title="Security pin to redeem credits" class="href" href="<?php echo site_url();?>booking/authorize/change_pin">Change Package Pin</a>
                            <a class="href" href="http://www.healingtouchspa.com/frequently-asked-questions/" target="_blank">FAQ</a>
                            <a id="logout_menu_item" class="href" href="<?php echo site_url();?>booking/authorize/logout">Logout</a>
                        </li>
                    </ul>
                </div>

                <?php }else{ ?>
                <div style="margin-left: 25px;float: left;" class="btn-group">
                    <a class="btn-login-rigister" style="margin-right: 30px;">
                        <img src="<?php echo $this->assets->releaseImage('login.png')?>" style="width: 15px;height: 15px; margin-left: -20px;" /><span class="text-header-rgt" onclick="getRegistrationForm()">Register</span>
                    </a>
                    <a class="btn-login-rigister" data-toggle="dropdown" href="#">
                        <img src="<?php echo $this->assets->releaseImage('login.png')?>" style="width: 15px;height: 15px; margin-left: -20px;" /><span class="text-header-rgt">Login</span>
                    </a>
                    <ul class="dropdown-menu pull-right custom-dropdown-menu" id="form-login">
                        <li>
                            <form id="login_form" method="post" action="<?php echo site_url('booking/authorize/login')?>">
                                <input type="text" required="required" class="form-control mbm" placeholder="Enter your email" name="_email" id="txt-email">
                                <input type="password" class="form-control mbm" placeholder="Enter your password" name="_password" id="txt-password">
                                <p style="color:red; line-height:20px" colspan="2" class="login-warning">
                                </p>
                                <a href="#" onclick="getRegistrationForm()" style="float: left;margin-left: 0px;">
                                    Register&nbsp;<i class="fa fa-arrow-circle-right"></i>
                                </a>
                                <a href="<?php echo site_url('booking/authorize/forgot') ?>" style="float: left;margin-left: 25px;">
                                    Forget Password&nbsp;<i class="fa fa-arrow-circle-right"></i>
                                </a>
<!--                                <a style="float: left;margin-left: 18px;" href="--><?php //echo site_url('booking/authorize/forgot_login_email') ?><!--">-->
<!--                                    Forget email?&nbsp;<i class="fa fa-arrow-circle-right"></i>-->
<!--                                </a>-->
                                <button class="btn pull-right green-color" type="submit">Login</button>
                            </form>
                        </li>
                    </ul>
                </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    var registerForm = '<?php echo site_url('booking/authorize/loadRegisterForm') ?>';
    var checking_email_url = '<?php echo site_url('booking/authorize/checkEmailRegistration'); ?>';
    var valid_new_pwd = '<?php echo site_url('booking/authorize/validNewPwd'); ?>';
    var valid_mobile_number = '<?php echo site_url('booking/authorize/verifyMobileNumAction'); ?>';
    var valid_previous_number = '<?php echo site_url('booking/authorize/checkPreviousMobileAction'); ?>';
    var finish_verify_mobile_number = '<?php echo site_url('booking/authorize/finishVerifyMobileAction'); ?>';
</script>