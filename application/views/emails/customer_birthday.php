<p>Dear <?php echo ucwords(strtolower($first_name)); ?></p>

<p>We are pleased to present you with a $[dollar value] E-Voucher (subject to min. $[dollar value] spending) to be redeemed in your birthday month.</p>

[QR Code]

<p>Have a great celebration!</p>

<p>
Terms & Conditions for E-Voucher Redemption
    <ul>
        <li>Present your NRIC/ID card to our staff along with this email voucher.</li>
        <li>This E-Voucher can be redeemed only on weekdays and non public holidays.</li>
        <li>This E-Voucher can only be redeemed in your birthday month.</li>
        <li>You may (optional) bring along someone to meet the minimum spending requirement.</li>
        <li>Discount is not applicable to other promotional offers.</li>
    </ul>
</p>
Thank you.<br>

<?php echo $branch_group_name; ?>

<p>
    <a href="<?php echo base_url().'booking/email_validation/unsubscribe/'.$id.'/'.md5($id.$code); ?>">Please click here if you wish to unsubscribe</a>
</p>
