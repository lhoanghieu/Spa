<?php
date_default_timezone_set('Asia/Singapore');
$date_start = new DateTime($book->start_time);
$default_date_start = new DateTime($book->default_start_time);
$upper_code = strtoupper($book->code);
$text = "Healing Touch Appointment Confirmed";
$location = str_replace("#", "%23", $book->branch_address);
$description = $date_start->format('h:i A')." ".$date_start->format('M')." ".$date_start->format('d')." ".$date_start->format('Y')."%0A%0A".$book->customer_name."%0A".$book->branch_name."%0A".$book->service_name."%0A".$book->employee_name."%0AReference %23".$upper_code;
$code_img = 'http://pos.healingtouchspa.com/ulti/barcode?barcode='.$upper_code.'&text='.$upper_code.'&width=300&height=50';

$datetime = new DateTime($book->default_start_time);
$datetime_begin = $datetime->format('Ymd')."T".$datetime->format('His')."Z";
$interval = new DateInterval('PT'.$book->service_duration.'M');
$datetime->add($interval);
$datetime_end = $datetime->format('Ymd')."T".$datetime->format('His')."Z";

$link = 'https://www.google.com/calendar/render?action=TEMPLATE&text='.$text.'&dates='.$datetime_begin.'/'.$datetime_end.'&details='.$description.'&location='.$location.'&sf=true&output=xml';
?>

<div style="width: 830px;font-family: TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif;">
    <table width="100%" height="100%">
        <tr>
            <td width='100%' height='100%'>
                <div style="height: 20px;"></div>
                <div style="width: 380px;float: left;margin-left: 56px;position: relative;">
                    <!-- <p style="color: #b4b4b6;font-size: 30px;font-weight: bold;line-height: 50px;">Healing Touch Spa</p> -->
                    <div>
                        <img width="360" src="http://pos.healingtouchspa.com/assets/core/images/email_logo.png">
                    </div>
                    <p style="color: #525254;font-size: 20px;font-weight: bold;line-height: 35px;">Healing Touch Appointment Confirmed</p>
                    <p style="font-weight: bold;color: #b91e22;font-size: 20px;line-height: 30px;">
                        <?php echo $date_start->format('h:i A')?>, <?php echo $date_start->format('M')?> <?php echo $date_start->format('d')?>, <?php echo $date_start->format('Y')?></p>
                    <p style="color: #000;font-size: 11px;">This email serve as a confirmation.</p>
                    <p style="color: #000;font-size: 11px;">Should you wish to re-schedule your appointment, please reply to this email or call our branch and speak to one of our customer service representatives</p>
                    <div style="height: 9px;"></div>
                    <p style="font-weight: bold;font-size: 13px;">Appointment Details</p>
                    <div>
                        <table style="line-height: 24px;">
                            <tr>
                                <td valign="top">Appointment</td>
                                <td style="font-weight: bold;color: #000;font-size: 14px;">
                                    <div><?php echo $book->customer_name; ?></div>
                                    <div><?php echo $book->branch_name; ?></div>
                                    <div><?php echo $book->service_name; ?></div>
                                    <div><?php echo $book->service_description; ?></div>
                                    <div>
                                        <span>With</span> 
                                        <?php 
                                        if($book->no_preference == 1) 
                                            echo "(No Preference)";
                                        else echo $book->employee_first_name;?>                                        
                                    </div>
                                    <div><span style="font-weight: normal">Reference #</span> <?php echo $upper_code ?></div>
                                    <div>
                                        <img class="img_barcode" src="<?php echo "http://pos.healingtouchspa.com/ulti/barcode?barcode={$upper_code}&text={$upper_code}&width=300&height=50"; ?>"></img>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" valign="top">Your comment</td>
                                <td><?php echo $book->comment ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="width: 170px;float: right;margin-right: 60px;margin-top: 100px;">
                    <div style="width: 170px;border: 1px solid #f2f2f4">
                        <div style="background-color: #f2f2f4;width: 170px;height: 37px;margin: 0px auto 0px auto;font-size: 20px;font-weight: bold;">
                            <div style="text-align: center;padding-top: 5px;">
                                <?php echo $date_start->format('M')?> <?php echo $date_start->format('Y')?>
                            </div>
                        </div>
                        <div style="height: 118px;text-align: center">
                            <div style="font-size: 80px;font-weight: bold;padding-top: 15px;"><?php echo $date_start->format('d')?></div>
                        </div>
                    </div>
                    <div style="height: 22px;"></div>
                    <div style="width: 170px;">
                        <div style="height: 9px;"></div>
                        <div style="width: 170px;margin-left: auto;margin-right: auto;background-color: #f2f2f4;font-weight: bold;text-align: center;line-height: 22px;font-size: 11px;">
                            <div style="padding: 5px 10px 0px 10px"><?php echo $book->branch_name ?></div>
                            <div style="padding: 0px 10px 0px 10px"><?php echo $book->branch_address ?></div>
                            <div align="center">Tel: <?php echo $book->branch_phone ?></div>
                        </div>
                        <div style="height: 9px;"></div>
                    </div>
                </div>
                <div style="width: 140px;float: right;margin-right: 10px;margin-top: 100px;font-size: 11px;">
                    <div align="center">
                        <div style="width: 105px;height: 27px;background-color: #fff;border: 2px solid #fff">
                            <div style="padding: 7px 0px 0px 7px;font-weight: bold;color: blue;">
                                <a style="text-decoration: none;" target="_blank" href="<?php echo $link; ?>">
                                    Add to Calendar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;height: 5px;"></div>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-style: italic;font-size: 12px;padding-left:61px;">Note: You have read and accepted our spa policy on <a target="_blank" style="color: blue;text-decoration: none;" href="http://www.healingtouchspa.com/policy">www.healingtouchspa.com/policy</a></td>
        </tr>
    </table>
    <div style="clear: both;height: 20px;"></div>
    <?php if($book->logged_user != "") {
        if($book->is_valid) { ?>
            <table>
                <tr>
                    <td style="font-style: italic;font-size: 12px;padding-left:61px;">
                        Please click on this link to <a target="_blank" style="color: blue;text-decoration: none;" href="<?php echo $book->baseurl; ?>booking/book?bid=<?php echo $book->id ?>">Cancel</a> or <a target="_blank" style="color: blue;text-decoration: none;" href="<?php echo $book->baseurl; ?>booking/book?rid=<?php echo $book->id ?>">Reschedule</a> your appointment
                    </td>
                </tr>
            </table>
            <div style="clear: both;height: 20px;"></div>
        <?php }
    } ?>
</div>