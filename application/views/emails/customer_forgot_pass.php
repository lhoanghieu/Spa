<p>
    Dear Dear Sir/Madam <?php echo $email; ?>,
</p>
<p>
    Your password has been reset to <b><?php echo $password ?></b><br/>
    You may click on the link <a style="text-decoration: none;" href="<?php echo base_url(); ?>booking/authorize/change_password"><?php echo base_url(); ?>booking/authorize/change_password</a> to change your password.
</p>
<p>
    Best Regards,<br/>
    Healing Touch
</p>
<p>
    <a href="<?php echo base_url().'booking/email_validation/unsubscribe/'.$id.'/'.md5($id.$code); ?>">Please click here if you wish to unsubscribe</a>
</p>