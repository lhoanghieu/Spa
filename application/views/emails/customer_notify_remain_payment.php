<div>
    Hello <?php echo isset($customerName)?$customerName:'' ?>,
    This is a email to test notify the deposit payment. please click on the button below to complete the payment.
    <br>
    <table border="0" cellpadding="10" cellspacing="0" align="center">
        <tr>
            <td align="center"></td>
        </tr>
        <tr>
            <td align="center">
                <a href="<?php echo isset($approvalUrl)?$approvalUrl:'#'; ?>" title="Pay with Paypal" ">
                    <img src="https://www.paypalobjects.com/webstatic/en_AU/i/buttons/btn_paywith_primary_l.png" alt="Pay with PayPal" />
                </a>
            </td>
        </tr>
    </table>
</div>
