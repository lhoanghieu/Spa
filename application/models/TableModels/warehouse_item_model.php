<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Warehouse_item_model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "warehouse_item";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array();
        $this->availableStatus = Status::Active;
    }

    public function get_warehouse($condition = array(),$user_level = '',$permission = array()){
        $warehouses = $this->load->table_model('warehouse')->get($condition,'','','',$user_level,$permission);
        $warehouse_id = 0;
        foreach($warehouses as $warehouse) {
            if ($warehouse->id) {
                if($warehouse_id){
                    if(is_array($warehouse_id)){
                        $warehouse_id[] = $warehouse->id;
                    }else{
                        $warehouse_id = array($warehouse_id,$warehouse->id);
                    }
                }else{
                    $warehouse_id = $warehouse->id;
                }
            }
        }
        return $warehouse_id;
    }

    public function get_warehouse_quantity($warehouse_id = 0,$item_id = array(), $throw_error = true){
        $condition = array();
        if(!$warehouse_id){
            $warehouse_id = $this->get_warehouse();
            if(!$warehouse_id){
                if($throw_error){
                    throw new Exception('No warehouse exists');
                }else{
                    return array();
                }
            }
            $condition['warehouse_id'] = $warehouse_id;
        }

        if($item_id){
            if(is_array($item_id)){
                $condition['item_id'] = $item_id;
            }else if(is_numeric($item_id)){
                $condition['item_id'] = $item_id;
            }
        }
        if(is_array($item_id)){
            $warehouse_item = $this->getTableMap('item_id','',$condition);
        }else{
            $warehouse_item = $this->get($condition);
            if(count($warehouse_item)){
                $warehouse_item = $warehouse_item[0];
            }
            else{
                $warehouse_item = null;
            }
        }
        return $warehouse_item;
    }


}


