<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Information_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "information";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = array();
        $this->tableType = "main";
    }

}


