<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/21/2014
 * Time: 2:48 PM
 */

class Module_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "module";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }
}