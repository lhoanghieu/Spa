<?php
class Giftcard_List_Detail_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "giftcard_list_detail";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function getData(){
        return $this->select(array(
            'select' => array(
                'item' => array('itemName' => 'name'),
                'giftcard_list_detail' => array('code', 'start_value', 'current_value'),
                'customer' => array('customerCode' => 'code', 'customerName' => 'first_name')
            ),
            'from' => array(
                'giftcard_list_detail' => array('table' => 'giftcard_list_detail'),
                'customer' => array('table' => 'customer', 'condition' => 'giftcard_list_detail.customer_id = customer.id', 'type' => 'LEFT'),
                'item' => array('table' => 'item', 'condition' => 'giftcard_list_detail.item_id = item.id', 'LEFT')
            ),
            'where' => array(

            )
        ))->result();
    }

    function getDetailData($giftcard_code){
         return $this->select(array(
            'select' => array(
                'bill' => array('billCode' => 'code'),
                'giftcard_list_detail_transaction' => array('before_trans', 'trans', 'after_trans', 'created_date'),
                'customer' => array('customerCode' => 'code', 'customerName' => 'first_name')
            ),
            'from' => array(
                'giftcard_list_detail_transaction' => array('table' => 'giftcard_list_detail_transaction'),
                'customer' => array('table' => 'customer', 'condition' => 'giftcard_list_detail_transaction.customer_id = customer.id', 'type' => 'LEFT'),
                'bill' => array('table' => 'bill', 'condition' => 'giftcard_list_detail_transaction.bill_id=bill.id', 'type' => 'LEFT')
            ),
            'where' => array(
                'giftcard_list_detail_transaction.giftcard_code' => $giftcard_code
            )
        ))->result();
    }
}