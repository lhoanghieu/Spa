<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Warehouse_model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "warehouse";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'code' => array(Constraints::Required, Constraints::Unique),
            'name' => array(Constraints::Required),
        );
        $this->availableStatus = Status::Active;
    }
}


