<?php

class Commission_Category_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "commission_category";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function generateBaseInfo($condition = array()){
        $info = parent::generateBaseInfo($condition);
        $info['select'] = array(
            $this->tableName => $this->fieldList,
            'category' => array('category_name' => 'name'),
            'commission' => array('commission_name' => 'name')
        );
        $info['from'] = array(
            $this->tableName => array('table' => $this->tableName),
            'category' => array('table' => 'category', 'condition' => "category.id = {$this->tableName}.category_id"),
            'commission' => array('table' => 'commission', 'condition' => "commission.id = {$this->tableName}.commission_id")
        );
        return $info;
    }
}