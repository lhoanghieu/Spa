<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Category_Item_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "category_item";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }


    function getCategoriesByItem($id)
    {
        $query = $this->db->get_where($this->tableName, array('item_id' => $id));
        $category_ids = $query->result();
        $result = array();
        foreach($category_ids as $category_id){
            $result[] = $this->load->table_model('category')->getByID($category_id->category_id);
        }
        return $result;
    }
}