<?php

/**
 * @author
 * @copyright 2014
 */

class Report_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "report";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'name' => array(Constraints::Required)
        );
        $this->availableStatus = Status::Active;
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array()){
        $reports = parent::get($condition, $order_by, $limit, $offset, $user_level, $permission);
        $group_map   = $this->select(array(
            'select' => array(
                'report_group'    => array('report_id'),
                'group'           => array('name','id'),
            ),
            'from'   => array(
                'report_group'    => array('table' => 'report_group'),
                'group'           => array('table' => 'group','condition' => 'group.id = report_group.group_id')
            ),
            'where'  => array('report_group.report_id' => convert_to_array($reports,'','id'))
        ))->result();
        $group_map = convert_to_array($group_map,'report_id','',false);
        foreach($reports as $report){
            $report->group_list = isset($group_map[$report->id])?$group_map[$report->id]:array();
        }
        return $reports;
    }


}