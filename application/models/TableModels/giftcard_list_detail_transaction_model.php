<?php
class Giftcard_List_Detail_Transaction_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "giftcard_list_detail_transaction";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}