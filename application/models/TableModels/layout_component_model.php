<?php

/**
 * @author Vũ Hoàng Huy
 * @copyright 2014
 */

class Layout_Component_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "layout_component";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function getComponent($LayoutID){
        $query = $this->db->get_where($this->tableName,array('LayoutID'  =>  $LayoutID));
        return $query->result();
    }

}
