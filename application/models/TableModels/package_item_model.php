<?php

class Package_Item_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "package_item";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}