<?php

class Audit_Trails_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "audit_trails";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

    function getData(){
        return $this->select(array(
            'select' => array(
                'audit_trails' => array('id','createAt', 'action', 'module', 'object', 'object_detail'),
                'employee'  => array('employee_user' => 'username')
            ),
            'from' => array(
                'audit_trails' => array('table' => 'audit_trails'),
                'employee' => array('table' => 'employee', 'condition' => 'audit_trails.employee_id = employee.id')
            ),
            'where' => array(

            )
        ))->result_array();
    }

}