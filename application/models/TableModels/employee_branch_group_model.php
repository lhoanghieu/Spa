<?php

/**
 * @author
 * @copyright 2014
 */

class Employee_Branch_Group_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "employee_branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function renderDataForInsert($data){
        $willInsertFields = parent::renderDataForInsert($data);
        if(!isset($willInsertFields['branch_group_id'])){
            if ($this->session->userdata('login') !== false) {
                $login = $this->session->userdata('login');
                $willInsertFields['branch_group_id'] = $login->branch_group_id;
            }
        }
        return $willInsertFields;
    }

    function check_employee($person_id, $branch_group_id = ""){
        $result = array();
        if($branch_group_id == ""){
            if ($this->session->userdata('login') !== false) {
                $login = $this->session->userdata('login');
                $branch_group_id = $login->branch_group_id;
            }
        }
        if($branch_group_id != ""){
            $result = $this->db->get_where($this->tableName, array('employee_id' => $person_id, 'branch_group_id' => $branch_group_id));
        }
        return ($result->num_rows() > 0) ? true : false;
    }
}
