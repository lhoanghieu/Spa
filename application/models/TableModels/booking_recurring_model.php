<?php

class Booking_Recurring_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "booking_recurring";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }

}

