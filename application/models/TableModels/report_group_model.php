<?php

/**
 * @author
 * @copyright 2014
 */

class Report_Group_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "report_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
        $this->fieldListRequirement = array();
    }


}