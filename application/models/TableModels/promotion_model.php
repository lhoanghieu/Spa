<?php
class Promotion_model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "promotion";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'code' => array(Constraints::Required, Constraints::Unique),
            'discount'  => array(Constraints::Required),
            'count_of_used'  => array(Constraints::Required),

        );
        $this->availableStatus = Status::Active;
    }

    function update($id,$values,$constraint_check,$log_data){
        $logged_user = $this->session->userdata('login');
        if(isset($logged_user->user_id))
            $values->updater = $logged_user->user_id;

        $result = parent::update($id,$values,$constraint_check,$log_data);

        if($result){
            $this->db->where('promotion_id', $id);
            $this->db->delete("promotion_detail");

            $this->insertPromotionDetail($id,$values);
        }

        return $result;
    }

    function insert($fields){
        $promotion_id = parent::insert($fields);

        if($promotion_id){
            $this->insertPromotionDetail($promotion_id,$fields);
        }

        return $promotion_id;
    }

    function insertPromotionDetail($id,$fields){
        if(!isset($fields->item_id))
            $items = array();
        else
            $items = $fields->item_id;

        if(!isset($fields->category_id))
            $categories = array();
        else
            $categories = $fields->category_id;

        if(!isset($fields->branch_id))
            $branches = array();
        else
            $branches = $fields->branch_id;


        $data = new stdClass();
        foreach($items as $item)
            foreach($categories as $category)
                foreach($branches as $branch){
                    $data->promotion_id = $id;
                    $data->item_id = $item;
                    $data->category_id = $category;
                    $data->branch_id = $branch;
                    $this->db->insert('promotion_detail', $data);
                }
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array(), $no_permission = false){
        $result = parent::get($condition,$order_by,$limit,$offset,$user_level,$permission,$no_permission);
        $promotion_type = PROMOTION_TYPE();

        foreach($result as $row){
            $row->type = $promotion_type[$row->type];
        }

        return $result;
    }
}


