<?php

/**
 * @author
 * @copyright 2014
 */

class Category_branch_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "category_branch";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}
