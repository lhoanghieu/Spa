<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Branch_Offline_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "branch_offline";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array();
        $this->availableStatus = Status::Active;
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array(), $no_permission = false)
    {
        if(! isset($condition['branch_id'])){
            $condition['branch_id'] = $this->user_check->get('branch_id');
        }
        return parent::get($condition,$order_by,$limit,$offset,$user_level,$permission,$no_permission);
    }
}


