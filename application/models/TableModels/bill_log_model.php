<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Bill_Log_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "bill_log";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connective";
        $this->fieldListRequirement = array();
        $this->availableStatus = null;
    }

}


