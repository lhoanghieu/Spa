<?php

class Bill_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "bill";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "main";
        $this->availableStatus = BILL_STATUS(array('Complete'));
    }

    function getSuspendedBills(){
        $bills = $this->select(
            array(
                'select'    => array(
                    $this->tableName => $this->fieldList,
                    'bill_branch'    => array('branch_id'),
                    'branch'         => array('name')
                ),
                'from'      => array(
                    $this->tableName => array('table' => $this->tableName),
                    'branch'         => array('table' => 'branch', 'condition' => 'branch.id = bill_branch.branch_id')
                ),
                'where'     => array('bill.status' => 2)
            )
        )->result();

        $customer_ids = convert_to_array($bills, '', 'customer_id');
        $customers = $this->load->table_model('customer')->getTableMap('id', '', array('id' => $customer_ids));
        foreach($bills as $bill){
            if(isset($customers[$bill->customer_id])){
                $bill->customer_name = to_full_name($customers[$bill->customer_id]->first_name,$customers[$bill->customer_id]->last_name);
            }
        }
        return $bills;
    }

    function getMaxBillCode($prefix,$user_level = '', $permission = array()){
        $bill_number = $this->getTotal(array("bill.code like '{$prefix}_%'", 'status' => BILL_STATUS(array('Complete','Void'))),$user_level,$permission);
        $bill_number++;
        $isDup = true;
        while($isDup){
            $check_bill_code = $this->get(array("bill.code" => $prefix.'_'.$bill_number));
            if(count($check_bill_code) == 0){
                $isDup = false;
            }else{
                $bill_number++;
            }
        }
        return $prefix.'_'.$bill_number;
    }

    function get_detail($condition = array(), $details = array(),$user_level = '', $permission = array()){
        $details_default = array(
            'employee' => false,
            'item'     => false,
            'customer' => false,
            'payment'  => false,
        );

        $details = array_merge($details_default,$details);

        $info = array(
            'select'    => array(
                'bill'          => array('id')
            ),
            'from'      => array(
                'bill' => array('table' => 'bill'),
                'bill_payment' => array('table' => 'bill_payment', 'condition' => 'bill.id = bill_payment.bill_id','type' => 'left'),
                'bill_item'    => array('table' => 'bill_item','condition' => 'bill_item.bill_id = bill.id', 'type' => 'left'),
                'bill_employee'=> array('table' => 'bill_employee', 'condition' => 'bill_employee.bill_item_id = bill_item.id', 'type' => 'left'),
                'customer'     => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id', 'type' => 'left'),
                'payment_type' => array('table' => 'payment_type', 'condition' => 'payment_type.id = bill_payment.payment_id','type' => 'left')
            ),
            'where' => array_merge($condition,array('bill.status' => $this->availableStatus)),
            'order' => 'bill.created_date desc',
            'group' => 'bill.id',
            'user_level'    => $user_level,
            'permission'    => $permission,
        );

        $bill_ids = $this->select($info)->result();
        $bill_ids = convert_to_array($bill_ids,'','id');

        $bills              = $this->getTableMap('id','',array('id' => $bill_ids),true,$user_level,$permission,'bill.created_date desc');
        $bills_items        = $this->load->table_model('bill_item')->getTableMap('bill_id','',array('bill_id' => $bill_ids),false);
        $bill_item_ids      = array();
        $item_ids = array();
        foreach($bills_items as $bill_id=>$bill_items){
            foreach($bill_items as $bill_item){
                $bill_item_ids[] = $bill_item->id;
                $item_ids[] = $bill_item->item_id;
            }
        }
        $customer_ids = array();
        foreach($bills as $bill){
            $customer_ids[] = $bill->customer_id;
        }
        $bills_items_credits    = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_ids),false);
        $bills_employees        = $this->load->table_model('bill_employee')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_ids),false);
        $bills_payments         = $this->load->table_model('bill_payment')->getTableMap('bill_id','',array('bill_id' => $bill_ids),false);

        $credits = $this->load->table_model('credit')->getTableMap('customer_id','',array('customer_id' => $customer_ids),false,Permission_Value::ADMIN);

        if($details['customer']){
            $customers  = $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids,'status' => ALL_STATUS()),true, Permission_Value::ADMIN);
        }
        if($details['item']) {
            $items      = $this->load->table_model('item')->getTableMap('id', '', array('id' => $item_ids,'status' => ALL_STATUS()));
        }
        if($details['payment']){
            $payments   = $this->load->table_model('payment_type')->getTableMap('id', '',array('status' => ALL_STATUS()));
        }
        if($details['employee']){
            $employees = $this->load->table_model('employee')->getTableMap('id','',array('status' => ALL_STATUS()),true,Permission_Value::ADMIN);
            foreach($bills_employees as $bill_item_id=>$bill_employees){
                foreach($bill_employees as $bill_employee){
                        $bill_employee->employee_detail = $employees[$bill_employee->employee_id];
                }
                $bill_employees[$bill_item_id] = $bill_employees;
            }
        }

        foreach($credits as $customer_id=>$credit_fields){
            $credits[$customer_id] = convert_to_array($credit_fields,'item_id');
        }

        foreach($bills_items as $bill_id=>$bill_items){
            foreach($bill_items as $bill_item){
                if($bill_item->credit_add && isset($credits[$bills[$bill_id]->customer_id]) && isset($credits[$bills[$bill_id]->customer_id][$bill_item->item_id])){
                    $bill_item->credit_add_id = $credits[$bills[$bill_id]->customer_id][$bill_item->item_id]->id;
                }
                $bill_item->credit_used = isset($bills_items_credits[$bill_item->id])?$bills_items_credits[$bill_item->id]:array();
                $bill_item->employees   = isset($bills_employees[$bill_item->id])?$bills_employees[$bill_item->id]:array();
                if($details['item']) {
                    $bill_item->item_detail = $items[$bill_item->item_id];
                }
            }
            $bills_items[$bill_id] = $bill_items;
        }
        foreach($bills_payments as $bill_id=>$bill_payments){
            foreach($bill_payments as $bill_payment){
                if($details['payment']){
                    $bill_payment->payment_detail = $payments[$bill_payment->payment_id];
                }
            }
            $bills_payments[$bill_id] = $bill_payments;
        }
        foreach($bills as $bill_id=>$bill_fields){
            if(!isset($bills_items[$bill_id])){
                unset($bills[$bill_id]);
                continue;
            }
            $bills[$bill_id]->bill_items    = $bills_items[$bill_id];
            $bills[$bill_id]->bill_payments = isset($bills_payments[$bill_id])?$bills_payments[$bill_id]:array();
            if($details['customer'] && isset($bill_fields->customer_id) && isset($customers[$bill_fields->customer_id])){
                $bills[$bill_id]->customer_detail = $customers[$bill_fields->customer_id];
            }
        }
        return $bills;
    }

    function delete_suspended_bill($condition){
        $this->delete($condition, 4, array(), true);
    }

    function updateEmailReviewBill($bill_id){
        if(!empty($bill_id)){
            $this->db->update('bill', array('email_review' => 1), array('id' => $bill_id));
            $billCode = $this->db->get_where('bill',array('id' => $bill_id))->row_array();
            $this->db->update('bill', array('email_review' => 1), array('code' => $billCode['code']));
        }
    }
}

