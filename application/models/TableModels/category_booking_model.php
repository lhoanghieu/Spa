<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Category_Booking_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "category_booking";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function Remove_Cat_Branch($cat_id, $branch = array()){
        $cat_b = $this->get(array('category_id' => $cat_id));
        $remove = array();
        foreach($cat_b as $key => $b){
            if(!in_array($b->branch_id,$branch)){
                $remove[] = $b->branch_id;
            }
            else{
                $key = array_search($b->branch_id,$branch);
                unset($branch[$key]);
            }
        }
        if(!empty($remove)){
            $this->delete(array('category_id' => $cat_id, 'branch_id' => $remove));
        }
        if(!empty($branch)){
            foreach($branch as $item){
                $this->insert(array('category_id' => $cat_id,'branch_id' => $item,'is_show' => 0, 'is_price_show' => 0));
            }
        }

    }
}