<?php

/**
 * @author
 * @copyright 2014
 */

class Employee_Scheduler_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "employee_scheduler";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function renderDataForInsert($data){
        $willInsertFields = parent::renderDataForInsert($data);
        if(!isset($willInsertFields['branch_id'])){
            if ($this->session->userdata('login') !== false) {
                $login = $this->session->userdata('login');
                $willInsertFields['branch_id'] = $login->branch_id;
            }
        }
        return $willInsertFields;
    }

    function check_employee($person_id, $branch_id){
        $result = array();
        if($branch_id == ""){
            if ($this->session->userdata('login') !== false) {
                $login = $this->session->userdata('login');
                $branch_id = $login->branch_id;
            }
        }
        if($branch_id != ""){
            $result = $this->db->get_where($this->tableName, array('employee_id' => $person_id, 'branch_id' => $branch_id));
        }
        return ($result->num_rows() > 0) ? true : false;
    }

    function update_employee_order($id,$department_id,$branch_id,$field_list,$constraint_check = true,$log_data = array()){
        $willInsertFields = parent::renderDataForUpdate($id, $field_list, $constraint_check);
        $ret = parent::update_database($this->tableName, $willInsertFields, array('employee_id' => $id,'department_id' => $department_id,'branch_id' => $branch_id));
        if($ret !== false){
            parent::save_log($id, 'update', $log_data);
        }
        return $ret;
    }


    function insert_to_employee_scheduler($employee_id,$department_id,$branch_id,$ordering){
        $ret = $this->insert(array('employee_id'=>$employee_id,'department_id' => $department_id,'branch_id' => $branch_id,'ordering' => $ordering));
        return $ret;
    }
}
