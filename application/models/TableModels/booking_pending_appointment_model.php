<?php

class Booking_Pending_Appointment_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "booking_pending_appointment";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }

    /**
     * GET LIST DATA OF BOOKING PENDING APPOINTMENT
     * @param array $filter
     * filter : list field checking
     * @return mixed
     */
    public function get_list_pending($filter = array()){
        $data = $this->select(array(
            'select' => array(
                'booking_pending_appointment' => array('id','start_time','staff_id','service_id','branch_id','date_time','duration','unique_id')
            ),
            'from'  => 'booking_pending_appointment',
            'where' => $filter
        ))->result();
        return $data;
    }

    /**
     * SAVE THE BOOKING PENDING APPOINTMENT INFORMATION TO DATABASE
     * @param array $data['id','start_time','staff_id','service_id','branch_id','date_time']
     * start_time : time begin to hold booking slot for customer
     * staff_id   : id of staff
     * service_id : id of service
     * branch_id  : id of branch
     * date_time  : the time that customer has choosen for booking appointment
     * @return array
     */
    public function insert_list_pending($data = array()){
        if(count($data) > 0){
            $this->db->trans_start();
            $data['unique_id'] = uniqid('pending_');
            $this->insert(array($data));
            $this->db->trans_complete();
            return array('status'=>TRUE,'data'=>$data['unique_id']);
        }
        else{
            return array('status'=>FALSE,'message'=>'Empty input parameters');
        }
    }

    public  function delete_list_pending($id){
        if($id != ''){
            $this->db->trans_start();
            $this->delete(array('unique_id' => $id));
            $this->db->trans_complete();
            return array('status'=>TRUE);
        }
        else{
            return array('status'=>FALSE,'message'=>'Empty input ID');
        }
    }

    public function check_pending_appointment($data){
        $is_double = 0;
        $list_pending = $this->get_list_pending(array('staff_id'=>$data['staff_id'],'branch_id'=>$data['branch_id']));
        $new_end = strtotime($data['date_time'])+(intval($data['duration']*60));
        $status = TRUE;

        foreach($list_pending as $item){
            $begin = strtotime($item->date_time);
            $end   = strtotime($item->date_time)+(intval($item->duration)*60);
            if(($begin < strtotime($data['date_time']) && strtotime($data['date_time']) < $end) || ($begin < $new_end  && $new_end < $end)){
                return FALSE;
            }
            if(strtotime($data['date_time']) < $begin  && $end < $new_end){
                return FALSE;
            }
        }

        if($status){
            $booked = $this->select(array(
                'select' => array('booking_service' => 'start_time','end_time'),
                'from'   => array(
                    'booking_service'   => array('table' => 'booking_service'),
                    'booking'           => array('table' => 'booking', 'condition' => 'booking_service.booking_id = booking.id')
                ),
                'where'  => 'booking.status IN (1,2) AND booking.branch_id = '.$data['branch_id'].' AND booking_service.employee_id='.$data['staff_id']
            ))->result();
            foreach($booked as $item){
                // check timezone and plus the difftime to check

                $begin = strtotime($item->start_time) + 28800;
                $end   = strtotime($item->end_time) + 28800;
                if(($begin < strtotime($data['date_time']) && strtotime($data['date_time']) < $end) || ($begin < $new_end  && $new_end < $end)){
                    return FALSE;
                }
                if(strtotime($data['date_time']) < $begin  && $end < $new_end){
                    return FALSE;
                }

                $block_start_time = get_user_date($item->start_time,'','Y-m-d H:i:s');
                $block_end_time = get_user_date($item->end_time,'','Y-m-d H:i:s');
                
                if(strtotime($data['date_time']) < strtotime($block_start_time)){
                    if(strtotime($new_end) > strtotime($block_start_time))
                        $is_double = 1;
                }
                if(strtotime($data['date_time']) == strtotime($block_start_time)){
                    $is_double = 1;
                }
                if(strtotime($data['date_time']) > strtotime($block_start_time)){
                    if(strtotime($data['date_time']) < strtotime($block_end_time))
                        $is_double = 1;
                }
                
                if($is_double) return $is_double;
            }
        }
        
        return $status;
    }

}

