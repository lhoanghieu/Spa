<?php

class Booking_Service_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "booking_service";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array(
        );
        $this->tableType = "connected";
    }

    function get_booking_range($params){
        $default_value = array(
            'start_time' => '1970-01-01 00:00:00',
            'end_time' => '2100-12-30 23:59:59',
            'condition'   => array(),
            'permission'  => array(
                'user_level' => '',
                'field'      => '',
            )
        );
        $params = $params + $default_value;
        $add_where = '';
        if(isset($params['permission']['field']['branch_id'])){
            $params['condition']['branch_id'] = $params['permission']['field']['branch_id'];
            $add_where = ' AND booking.branch_id = '.$params['permission']['field']['branch_id'];
        }
        if(isset($params['condition']['booking.id !'])){
            $add_where = ' AND booking.id != '.$params['condition']['booking.id !'];
        }
        $booked = 'SELECT booking_service.id,booking_service.employee_id, booking_service.room_id, booking.id as booking_id
                   FROM booking_service JOIN booking ON booking_service.booking_id = booking.id
                   WHERE 1 '.$add_where.'
                   AND booking.status IN ('.BOOKING_STATUS('Complete').','.BOOKING_STATUS('Hold').','.BOOKING_STATUS('Payment').','.BOOKING_STATUS('HoldBill').')
                   AND booking_service.room_id > 0
                   AND ((booking_service.start_time < "'.$params['start_time'].'" AND booking_service.end_time > "'.$params['start_time'].'")
                    OR (booking_service.start_time < "'.$params['end_time'].'" AND booking_service.end_time > "'.$params['end_time'].'")
                    OR (booking_service.start_time > "'.$params['start_time'].'" AND booking_service.end_time < "'.$params['end_time'].'")) ';
        $booked = $this->db->query($booked)->result();
        return $booked;
    }
}

