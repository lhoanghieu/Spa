<?php

/**
 * @author
 * @copyright 2014
 */

class Category_Branch_group_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "category_branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}
