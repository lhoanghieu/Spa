<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Item_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "item";
        $this->fieldList = $this->db->list_fields($this->tableName);

        $this->fieldListRequirement = (object) array(
            'name'          => array(Constraints::Required),
            'code'          => array(Constraints::Required, Constraints::Unique),
            'price'         => array(Constraints::Required)
        );
        $this->tableType = "main";
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array()){

        $items = parent::get($condition, $order_by, $limit, $offset, $user_level, $permission);
        $results = array();
        $item_ids = array();
        foreach($items as $item_id=>$item_fields){
            $item_ids[] = $item_fields->id;
        }
        /*----- Get Categories -----*/
        $categoriesMap = $this->select(array(
            'select' => array(
                'category_item' => array('item_id'),
                'category'      => array('id','name','code','description')
            ),
            'from'   => array(
                'category_item'             => array('table' => 'category_item'),
                'category'                  => array('table' => 'category', 'condition' => 'category.id = category_item.category_id'),
            ),
            'where'  => array('category_item.item_id' => $item_ids)
        ))->result();
        $categoriesMap = convert_to_array($categoriesMap,'item_id','',false);
        /*---- Get Product ------*/
        $product_map = $this->load->table_model('item_product')->getTableMap('item_id','',array('item_id' => $item_ids));
        /*--[]---------------------*/

        /*----- Get Service -----*/
        $serviceMap = $this->select(array(
            'select' => array(
                'item_service' => array('parent_item_id' => 'item_id','duration')
            ),
            'from'   => array(
                'item_service'          => array('table' => 'item_service'),
            ),
            'where'  => array('item_service.item_id' => $item_ids)
        ))->result();
        $roomMap = $this->select(array(
            'select' => array(
                'item_service'=> 'item_id',
                'room'        => array('id','name','code','bed_quantity')
            ),
            'from'   => array(
                'item_service'          => array('table' => 'item_service'),
                'category_item'         => array('table' => 'category_item', 'condition' => 'item_service.item_id = category_item.item_id','type' => 'left'),
                'room_type_category'    => array('table' => 'room_type_category', 'condition' => 'room_type_category.category_id = category_item.category_id', 'type' => 'left'),
                'room'                  => array('table' => 'room', 'condition' => 'room.room_type_id = room_type_category.room_type_id','type' => 'left')
            ),
            'where'  => array('item_service.item_id' => $item_ids)
        ))->result();
        $serviceMap = convert_to_array($serviceMap,'parent_item_id','');
        $roomMap = convert_to_array($roomMap,'item_id','',false);
        /*---- Get Package ----*/
        $packageMap = $this->select(array(
            'select' => array(
                'item_package' => array('parent_item_id' => 'item_id','expire_length'),
                'item'        => array('id','name','price','code','description','type')
            ),
            'from'   => array(
                'item_package'           => array('table' => 'item_package'),
                'package_item'          => array('table' => 'package_item', 'condition' => 'package_item.package_id = item_package.item_id'),
                'item'                  => array('table' => 'item', 'condition' => 'item.id = package_item.item_id')
            ),
            'where'  => array('item_package.item_id' => $item_ids)
        ))->result();
        $packageMap = convert_to_array($packageMap,'parent_item_id','',false);

        /*--- Get Bundle ----*/
        $bundleMap = $this->select(array(
            'select' => array(
                'item_bundle' => array('parent_item_id' => 'item_id'),
                'item'        => array('id','name','price','code','description','type'),
                'item_service'=> array('duration')
            ),
            'from'   => array(
                'item_bundle'           => array('table' => 'item_bundle'),
                'item_bundle_item'      => array('table' => 'item_bundle_item', 'condition' => 'item_bundle.item_id = item_bundle_item.item_id'),
                'item'                  => array('table' => 'item', 'condition' => 'item.id = item_bundle_item.sub_item_id'),
                'item_service'          => array('table' => 'item_service', 'condition' => 'item_service.item_id = item.id', 'type' => 'left')
            ),
            'where'  => array('item_bundle.item_id' => $item_ids)
        ))->result();
        $bundleMap = convert_to_array($bundleMap,'parent_item_id','',false);


        $em = $this->load->table_model('deposit_payment_item');
        foreach($items as $item){
            if(isset($categoriesMap[$item->id])){
                $item->categories = $categoriesMap[$item->id];
            }
            if(isset($product_map[$item->id])){
                $item->product = $product_map[$item->id];
            }
            if(isset($serviceMap[$item->id])){
                $item->service = (object)array(
                    'duration'          => $serviceMap[$item->id]->duration,
                    'rooms'             => isset($roomMap[$item->id])?$roomMap[$item->id]:array()
                );
            }
            if(isset($packageMap[$item->id])){
                $item->package = (object)array(
                    'expire_length'     => $packageMap[$item->id][0]->expire_length,
                    'kit_items'         => $packageMap[$item->id]
                );
            }
            if(isset($bundleMap[$item->id])){
                $item->bundle = (object)array(
                    'sub_items'         => convert_to_array($bundleMap[$item->id],'id')
                );
            }

            $deposit = $em->get(array('item_id' => $item->id));

            if(!empty($deposit)){
                $item->min_cash_payment = $deposit[0]->min_cash_payment;
                $item->payment_time = $deposit[0]->payment_time;
            }
            $results[] = $item;
        }
        return $results;
    }

    function getByCategory($code = ''){
        if($code == ''){
            $condition = array();
        }else{
            $condition =  array("category.code = '{$code}'");
        }
        $items = $this->select(array(
            'select' => array('item'=>array('id','name','code','price','description')),
            'from'   => array(
                'item'          => array('table'=>'item'),
                'category_item' => array('table'=>'category_item', 'condition' => 'item.id = category_item.item_id'),
                'category'      => array('table'=>'category', 'condition' => 'category.id = category_item.category_id')
            ),
            'where' => $condition)
        )->result();
        return $items;
    }

    function get_service_detail($condition = array()){
        $items = $this->get(array_merge(array('type' => ITEM_TYPE(array('Bundle','Service'))),$this->to_condition($condition,'item')));

        $item_id        = convert_to_array($items,'','id');
        $category_map   = $this->load->table_model('category_item')->getTableMap('item_id','',array('item_id' => $item_id),false);
        $bundle_map     = $this->load->table_model('item_bundle_item')->get(array('item_id' => $item_id));
        $sub_item_id    = convert_to_array($bundle_map,'','sub_item_id');
        $bundle_map     = convert_to_array($bundle_map,'item_id','',false);
        $sub_item_map   = $this->select(array(
            'select' => array(
                'item'          => array('id','name','code'),
                'item_service'  => array('duration'),
            ),
            'from'   => array(
                'item'          => array('table' => 'item'),
                'item_service'  => array('table' => 'item_service', 'condition' => 'item.id = item_service.item_id'),
            ),
            'where'  => array('item.id' => $sub_item_id),
        ))->result();
        $sub_item_map   = convert_to_array($sub_item_map,'id','');
        $service_map    = $this->load->table_model('item_service')->getTableMap('item_id','',array('item_id' => $item_id));

        foreach($items as $item){
            $item->category_id = $category_map[$item->id];
            if($item->type == ITEM_TYPE('Bundle')){
                $item->services = $bundle_map[$item->id];
                $duration = 0;
                foreach($item->services as $key=>$service){
                    if(isset($sub_item_map[$service->sub_item_id])){
                        $item->services[$key] = $sub_item_map[$service->sub_item_id];
                        $duration += $sub_item_map[$service->sub_item_id]->duration;
                    }
                    else{
                        unset($item->services[$key]);
                    }
                }
                $item->duration = $duration;
            }
            else if($item->type == ITEM_TYPE('Service')){
                if(isset($service_map[$item->id])){
                    $item->duration = $service_map[$item->id]->duration;
                }else{
                    unset($item);
                }
            }else{
                // Should not be this case
                unset($item);
            }
        }

        return $items;
    }

    function getMinimumDepostByItem($itemId){
        if(!$itemId){
            return array();
        }

        $item = $this->select(array(
                'select' => array(
                    'item' => array('id'),
                    'deposit_payment_item' => array('min_cash_payment')
                ),
                'from'   => array(
                    'item'          => array('table'=>'item'),
                    'deposit_payment_item' => array('table' => 'deposit_payment_item', 'condition' => 'item.id = deposit_payment_item.item_id')
                ),
                'where' => array(
                    'item.id' => $itemId
                ))
        )->result();

        if(count($item) > 0){
            return $item[0];
        }
        else{
            return array();
        }
    }

    function getSimpleData(){
        $result = $this->select(array(
            'select' => array(
                'item' => array('id','name')
            ),
            'from' => array(
                'item' => array('table' => 'item')
            ),
            'where' => array(
                'status' => 1,
                'type' => 1 // service item only
            )
        ))->result();
        $result = convert_to_array($result,'id');
        return $result;
    }
}
