<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Group_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = array(
            'name'  =>  array(Constraints::Required, Constraints::Unique)
        );
        $this->tableType = "main";
    }

    function getPermissions($group_id){
        $condition = array();
        if($this->user_check->get('login_as_type') < Permission_Value::BRANCH_GROUP){
            $condition['module.type'] = array(MENU_TYPE('Staff'),MENU_TYPE('All'));
        }

        $permissions = $this->select(
            array(
                'select'    =>  array(
                    'module_permission'                 => array('id'),
                    'module'                            => array('module_name' => 'name'),
                    'permission'                        => array('permission_name' => 'name')
                ),
                'from'      => array(
                    'module_permission'                     => array('table'    =>  'module_permission'),
                    'module'                                => array('table'    =>  'module', 'condition'   => 'module.id = module_permission.module_id'),
                    'permission'                            => array('table'    =>  'permission', 'condition'   => 'permission.code = module_permission.permission_code')
                ),
                'where'     => $condition,
                'user_level'=>1,
                'permission'=>array('group_id' => $group_id)
            )
        )->result();
        return convert_to_array($permissions, '', 'id');
    }

    function getFullPermissions(){
        $permissions = $this->select(
            array(
                'select'    =>  array(
                    'module_permission'                 => array('id','permission_code','module_permission_global' => 'global'),
                    'module'                            => array('module_name' => 'name'),
                    'permission'                        => array('permission_name' => 'name')
                ),
                'from'      => array(
                    'module_permission'                     => array('table'    =>  'module_permission'),
                    'module'                                => array('table'    =>  'module', 'condition'   => 'module.id = module_permission.module_id'),
                    'permission'                            => array('table'    =>  'permission', 'condition'   => 'permission.code = module_permission.permission_code')
                ),
                'order'     => 'permission.order asc',
                'user_level'=> 4,
                'group'     => 'module_permission.id'
            )
        )->result();
        //Render data
        $permissions = convert_to_array($permissions, 'module_name', '', false);
        return $permissions;
    }

    function getFullDetailPermissions($group_id = 0){
        $condition = array();
        if($this->user_check->get('login_as_type') < Permission_Value::BRANCH_GROUP){
            $condition['module.type'] = array(MENU_TYPE('Staff'),MENU_TYPE('All'));
        }

        if($group_id == 0){
            $permissions = $this->select(
                array(
                    'select'    =>  array(
                        'module_permission'                 => array('id','permission_code'),
                        'module'                            => array('module_name' => 'name'),
                        'permission'                        => array('permission_name' => 'name')
                    ),
                    'from'      => array(
                        'module_permission'                     => array('table'    =>  'module_permission'),
                        'module'                                => array('table'    =>  'module', 'condition'   => 'module.id = module_permission.module_id'),
                        'permission'                            => array('table'    =>  'permission', 'condition'   => 'permission.code = module_permission.permission_code')
                    ),
                    'where'     => $condition,
                    'order'     => 'permission.order asc',
                    'user_level'=>2,
                )
            )->result();
        }else{
            $condition['module_permission_group.group_id'] = $group_id;
            $permissions = $this->select(
                array(
                    'select'    =>  array(
                        'module_permission_branch'          => array('id'),
                        'module_permission'                 => array('permission_code'),
                        'module'                            => array('module_name' => 'name'),
                        'permission'                        => array('permission_name' => 'name')
                    ),
                    'from'      => array(
                        'module_permission'                     => array('table'    =>  'module_permission'),
                        'module'                                => array('table'    =>  'module', 'condition'   => 'module.id = module_permission.module_id'),
                        'permission'                            => array('table'    =>  'permission', 'condition'   => 'permission.code = module_permission.permission_code')
                    ),
                    'where'     => $condition,
                    'order'     => 'permission.order asc',
                    'user_level'=>2,
                )
            )->result();
        }

        //Render data
        $permissions = convert_to_array($permissions, 'module_name', '', false);
        return $permissions;
    }

    function getFullDetailUserPermissions(){
        $condition = array();
        if($this->user_check->get('login_as_type') < Permission_Value::BRANCH_GROUP){
            $condition['module.type'] = array(MENU_TYPE('Staff'),MENU_TYPE('All'));
        }
        $permissions = $this->select(
            array(
                'select'    =>  array(
                    'module_permission'                 => array('id','permission_code'),
                    'module'                            => array('module_name' => 'name'),
                    'permission'                        => array('permission_name' => 'name')
                ),
                'from'      => array(
                    'module_permission'                     => array('table'    =>  'module_permission'),
                    'module'                                => array('table'    =>  'module', 'condition'   => 'module.id = module_permission.module_id'),
                    'permission'                            => array('table'    =>  'permission', 'condition'   => 'permission.code = module_permission.permission_code')
                ),
                'where'     => $condition,
                'order'     => 'permission.order asc',
                'user_level'=> 2,
                'group'     => 'module_permission.id'
            )
        )->result();
        //Render data
        $permissions = convert_to_array($permissions, 'module_name', '', false);
        return $permissions;
    }

    function deletePermission($info){
        $result = parent::delete($this->tableName, $info['condition'],1,$info['permission'],'group_id');
        if($result != 1){
            return 0;
        }
        else{
            return 1;
        }
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array()){
        $groups = parent::get($condition, $order_by, $limit, $offset, $user_level, $permission);
        $report_map   = $this->select(array(
            'select' => array(
                'report_group'    => array('group_id'),
                'report'          => array('name','code','id'),
            ),
            'from'   => array(
                'report_group'    => array('table'  => 'report_group'),
                'report'           => array('table' => 'report','condition' => 'report.id = report_group.report_id')
            ),
            'where'  => array('report_group.group_id' => convert_to_array($groups,'','id'))
        ))->result();
        $report_map = convert_to_array($report_map,'group_id','',false);
        foreach($groups as $group){
            $group->report_list = isset($report_map[$group->id])?$report_map[$group->id]:array();
        }
        return $groups;
    }


}


