<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Bill_Item_Credit_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "bill_item_credit";
        $this->fieldList = $this->db->list_fields($this->tableName);

        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }

    function get_bill_item_credit($customer_id, $credit_ids, $package_ids, $order = 'created_date asc'){
        if(count($credit_ids) > 0 && count($package_ids) > 0){
            $credit_ids_string = implode(',', $credit_ids);
            $package_ids_string = implode(',', $package_ids);
            $info = array(
                'select' => array(
                    $this->tableName => array('credit_id', 'credit_value'),
                    'bill_item' => array('bill_item_id' => 'id', 'bill_id', 'item_id', 'credit_add', 'credit_add_id' => 'credit_id'),
                    'bill' => array('created_date', 'void_date', 'creator','bill_code' => 'code'),
                    'bill_branch' => array('branch_id')
                ),
                'from' => array(
                    'bill_item' => array('table' => 'bill_item'),
                    'bill' => array('table' => 'bill', 'condition' => "bill_item.bill_id = bill.id"),
                    $this->tableName => array('table' => $this->tableName, 'condition' => $this->tableName. '.bill_item_id = bill_item.id', 'type' => 'left'),
                    'bill_branch' => array('table' => 'bill_branch', 'condition' => 'bill.id = bill_branch.bill_id')
                ),
                'where' => array(
                    "(bill_item.item_id IN ({$package_ids_string}) OR {$this->tableName}.credit_id IN ({$credit_ids_string}))",
                    "bill.customer_id" => $customer_id,
                    "bill.status" => BILL_STATUS(array('Complete'))
                ),
                'user_level' => 4,
                'order' => $order
            );
            $query = $this->select($info);
            if($query !== null){
                return $query->result();
            }
        }
        return array();
    }
}