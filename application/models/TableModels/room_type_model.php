<?php

/**
 * @author
 * @copyright 2014
 */

class Room_Type_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "room_type";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'name' => array(Constraints::Required),
            'code' => array(Constraints::Required, Constraints::Unique),
        );
        $this->availableStatus = Status::Active;
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array()){
        $room_types = parent::get($condition, $order_by, $limit, $offset, $user_level, $permission);
        $category_map   = $this->select(array(
            'select' => array(
                'room_type_category' => array('room_type_id'),
                'category'           => array('name','code','id'),
            ),
            'from'   => array(
                'room_type_category' => array('table' => 'room_type_category'),
                'category'           => array('table' => 'category','condition' => 'category.id = room_type_category.category_id')
            ),
            'where'  => array('room_type_category.room_type_id' => convert_to_array($room_types,'','id'))
        ))->result();
        $category_map = convert_to_array($category_map,'room_type_id','',false);
        foreach($room_types as $room_type){
            $room_type->category_list = isset($category_map[$room_type->id])?$category_map[$room_type->id]:array();
        }
        return $room_types;
    }


}