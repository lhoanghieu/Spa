<?php
class Stock_Added_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date   = $data['end_date'];
        $item_id    = $data['item_id'];
        $sortby     = isset($data['sortby'])?$data['sortby']:'date';
        $condition = array();
        $item_condition = array();
        $branch_id = $data['branch_id'];
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        if($item_id){
            $condition['item_id'] = $item_id;
            $item_condition['id'] = $item_id;
        }

        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse(array(),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));

        $ret_data = array();
        $base_data = array();
        $items = $this->load->table_model('item')->getTableMap('id','',$item_condition);
        // Tran Minh Thao -- added --------- Begin
        $item_product = $this->load->table_model('item_product')->getTableMap('item_id','',$item_condition);
        $user = $this->select(array(
            'select' => array(
                'employee' => array('id','first_name','last_name')
            ),
            'from'   => array(
                'employee' => array('table' => 'employee')
            )
        ))->result_array();
        $new_user = array();
        foreach($user as $u){
            $new_user[$u['id']][] = $u;
        }
        // Tran Minh Thao -- added --------- End
        if($warehouse_id){
            $condition['warehouse_id']  = $warehouse_id;
            $condition[]                = "log_time < '$end_date'";
          //  $condition[]                = "log_time >'$start_date'";
          //  $condition['log_type']      = "import_pro";
            $log_data = $this->load->table_model('warehouse_item_log')->getTableMap('item_id','',$condition,false,null,null,'log_time asc');

            foreach($log_data as $item_id=>$rows){
                foreach($rows as $row){
                    if($row->log_time < $start_date){
                        $base_data[$item_id] = array('quantity' => $row->quantity);
                        unset($row);
                    }else {
                        break;
                    }
                }
                if(! isset($base_data[$item_id])){
                    $base_data[$item_id] = array('quantity' => 0);
                }
                foreach($rows as $row){
                    if($row->log_time >= $start_date && $row->log_time <= $end_date){
                        if($base_data[$item_id]['quantity'] < $row->quantity){
                            $key_date = get_user_date($row->log_time,'','',false);
                            if(! isset($ret_data[$key_date][$item_id])){
                                if(isset($items[$item_id])) {
                                    $ret_data[$key_date][$item_id] = array(
                                        'transaction' => array(),
                                        'item_name'   => $items[$item_id]->name,
                                        'item_code'   => $items[$item_id]->code
                                    );
                                }
                            }

                            /**
                             * Tran Minh Thao added - Begin
                             * Get price and unit cost of item from database
                             */
                            $item_cost = 0;
                            $item_price = 0;
                            if(isset($item_product[$item_id]))
                                $item_cost = $item_product[$item_id]->unit_cost;
                            if(isset($items[$item_id]))
                                $item_price = $items[$item_id]->price;
                            // Tran Minh Thao added - End
                            $ret_data[$key_date][$item_id]['transaction'][] = array(
                                'add'       => $row->quantity - $base_data[$item_id]['quantity'],
                                'item_cost' => $item_cost, // Tran Minh Thao added
                                'item_price'=> $item_price, // Tran Minh Thao added
                                'time'      => get_user_date($row->log_time,'','H:i'),
                                'user_name'      => isset($new_user[$row->log_user]) ? $new_user[$row->log_user][0]['first_name'] : ''
                            );

                            // Tran Minh Thao added - to get added quantity of item -- Begin
                            $ret_data[$key_date][$item_id]['item_quantity'] = $row->quantity;
                            // Tran Minh Thao add ---- End
                        }
                        $base_data[$item_id]['quantity'] = $row->quantity;
                    }
                }

            }
        }

        if($sortby == "name"){
            $new_ret_data = array();
            foreach($ret_data as $key => $value){
                usort($value, array($this, "cmp_obj_name"));
                $new_ret_data[$key] = $value;
            }
            $ret_data = $new_ret_data;
        }
        elseif($sortby == "code"){
            $new_ret_data = array();
            foreach($ret_data as $key => $value){
                usort($value, array($this, "cmp_obj_code"));
                $new_ret_data[$key] = $value;
            }
            $ret_data = $new_ret_data;
        }

        return array(
            'stock_items' => $ret_data
        );
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date   = $data['end_date'];
        $item_id    = $data['item_id'];
        $condition = array();
        $item_condition = array();
        if($item_id){
            $condition['item_id'] = $item_id;
            $item_condition['id'] = $item_id;
        }

        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse();
        $ret_data = array();
        $base_data = array();
        $items = $this->load->table_model('item')->getTableMap('id','',$item_condition);
        if($warehouse_id){
            $condition['warehouse_id']  = $warehouse_id;
            $condition[]                = "log_time < '$end_date'";
            $log_data = $this->load->table_model('warehouse_item_log')->getTableMap('item_id','',$condition,false,null,null,'log_time asc');
            foreach($log_data as $item_id=>$rows){
                foreach($rows as $row){
                    if($row->log_time < $start_date){
                        $base_data[$item_id] = array('quantity' => $row->quantity);
                        unset($row);
                    }else {
                        break;
                    }
                }
                if(! isset($base_data[$item_id])){
                    $base_data[$item_id] = array('quantity' => 0);
                }
                foreach($rows as $row){
                    if($row->log_time >= $start_date && $row->log_time <= $end_date){
                        if($base_data[$item_id]['quantity'] < $row->quantity){
                            $key_date = get_user_date($row->log_time,'','',false);
                            if(! isset($ret_data[$key_date][$item_id])){
                                $ret_data[$key_date][$item_id] = array(
                                    'transaction' => array(),
                                    'item_name'   => $items[$item_id]->name,
                                    'item_code'   => $items[$item_id]->code
                                );
                            }
                            $ret_data[$key_date][$item_id]['transaction'][] = array(
                                'add'       => $row->quantity - $base_data[$item_id]['quantity'],
                                'time'      => get_user_date($row->log_time,'','H:i')
                            );
                        }
                        $base_data[$item_id]['quantity'] = $row->quantity;
                    }
                }

            }
        }

        return array(
            'stock_items' => $ret_data
        );
    }

    /**
     * Tran Minh Thao edited - 29/07/2015
     */
    function get_original_data($data,$type = false){
        $stock_items = $data['stock_items'];
        $userdata = $this->session->userdata('login');

        $content = array();
        // if user logged as staff
        if($userdata->login_as_type == 1 OR $userdata->login_as_type == 2 ) {
            $content[] = array(
                array('export_text' => 'Item Code'),
                array('export_text' => 'Item Name'),
                array('export_text' => 'Added'),
                array('export_text' => 'Price'),
                array('export_text' => 'Total'),
                array('export_text' => 'Time'),
                array('export_text' => 'User Create')
            );
        }
        else {
            $content[] = array(
                array('export_text' => 'Item Code'),
                array('export_text' => 'Item Name'),
                array('export_text' => 'Added'),
                array('export_text' => 'Price'),
                array('export_text' => 'Cost'),
                array('export_text' => 'Total Cost'),
                array('export_text' => 'Time'),
                array('export_text' => 'User Create')
            );
        }

        if (count($stock_items)) {
            $grand_total_quantity = 0;
            $grand_total_price = 0;
            $grand_total_cost = 0;
            $grand_total_total_cost = 0;
            foreach($stock_items as $key_date=>$items_trans_log){
                $content[] = array(
                    array('export_text' =>  $key_date,
                        'style'=>'color:red;font-weight: bold',
                        'colspan'=>'10'),
                );
                $total_quantity = 0;
                $total_price = 0;
                $total_cost = 0;
                $total_total_cost = 0;
                foreach($items_trans_log as $item_id=>$item_trans_detail){
                    foreach($item_trans_detail['transaction'] as $trans_row){
                        if(isset($item_trans_detail['item_code'])) {
                            $total_quantity += $trans_row['add'];
                            $total_price += $trans_row['item_price'];
                            $total_cost += $trans_row['item_cost'];
                            // if user logged as staff
                            if($userdata->login_as_type == 1 OR $userdata->login_as_type == 2 ) {
                                $content[] = array(
                                    array('export_text' =>  $item_trans_detail['item_code']),
                                    array('export_text' => $item_trans_detail['item_name']),
                                    array('export_text' => $trans_row['add']),
                                    array('export_text' => $trans_row['item_price'],'export_type' => 'currency'),
                                    array('export_text' => $trans_row['item_price']*$trans_row['add'],'export_type' => 'currency'),
                                    array('export_text' => $trans_row['time']),
                                    array('export_text' => $trans_row['user_name'])
                                );
                            }
                            else {
                                $content[] = array(
                                    array('export_text' =>  $item_trans_detail['item_code']),
                                    array('export_text' => $item_trans_detail['item_name']),
                                    array('export_text' => $trans_row['add']),
                                    array('export_text' => $trans_row['item_price'],'export_type' => 'currency'),
                                    array('export_text' => $trans_row['item_cost'],'export_type' => 'currency'),
                                    array('export_text' => $trans_row['item_cost']*$trans_row['add'],'export_type' => 'currency'),
                                    array('export_text' => $trans_row['time']),
                                    array('export_text' => $trans_row['user_name'])
                                );
                                $total_total_cost += $trans_row['item_cost']*$trans_row['add'];
                            }
                        }
                    }
                }
                $content[] = array(
                    $this->to_export_text('Total',3,2),
                    $this->to_export_text($total_quantity,3,1,1),
                    $this->to_export_text($total_price,3,1,1,'currency'),
                    $this->to_export_text($total_cost,3,1,1,'currency'),
                    $this->to_export_text($total_total_cost,3,2,1,'currency')
                );

                $grand_total_quantity += $total_quantity;
                $grand_total_price += $total_price;
                $grand_total_cost += $total_cost;
                $grand_total_total_cost += $total_total_cost;
            }

            $content[] = array(
                $this->to_export_text('Grand Total',0,2),
                $this->to_export_text($grand_total_quantity,0,1,1),
                $this->to_export_text($grand_total_price,0,1,1,'currency'),
                $this->to_export_text($grand_total_cost,0,1,1,'currency'),
                $this->to_export_text($grand_total_total_cost,0,2,1,'currency')
            );
        }else{
            $content[] = array(array('export_text' =>  'There is no record',
                'colspan'=>'10'),
            );
        }
        return $content;
    }

    function cmp_obj_name($a, $b){
        return strcmp($a["item_name"], $b["item_name"]);
    }

    function cmp_obj_code($a, $b){
        return strcmp($a["item_code"], $b["item_code"]);
    }
}