<?php
class Interbranch_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

//    function admin_get_detail_data($post_data){
//        if(isset($post_data['summary'])){
//            $type = 'summary';
//        }else{
//            $type = 'detail';
//        }
//
//        $data = new stdClass();
//        $data->start_date = $post_data['start_date'];
//        $data->end_date = $post_data['end_date'];
//
//        $branch_group_id    = $post_data['branch_group_id'];
//        $branch_data        = $this->load->table_model('branch_group')->get_branch_and_branch_group($branch_group_id);
//        $branch_data        = convert_to_array($branch_data,'id');
//        $branch_id          = convert_to_array($branch_data,'','id');
//        $condition          = array(
//            "bill.created_date >= '{$data->start_date}'",
//            "bill.created_date <= '{$data->end_date}'",
//            "bill.status" => BILL_STATUS('Complete')
//        );
//
//        /* ----- Gather bill data and item data for each bill -----*/
//        $bills       = $this->select(array(
//            'select'    => array('bill' => '*',
//                'branch' => array('branch_id' => 'id', 'branch_name' => 'name' , 'branch_group_id'),
//                'branch_group' => array('branch_group_name' => 'name')),
//            'from'      => array(
//                'bill'          => array('table' => 'bill'),
//                'bill_branch'   => array('table' => 'bill_branch', 'condition' => 'bill_branch.bill_id = bill.id'),
//                'branch'        => array('table' => 'branch', 'condition' => 'bill_branch.branch_id = branch.id'),
//                'branch_group'  => array('table' => 'branch_group', 'condition' => 'branch_group.id = branch.branch_group_id'),
//            ),
//            'where'     => $condition,
//            'order'     => 'bill.created_date DESC',
//            'user_level' => Permission_Value::ADMIN
//        ))->result();
//        $bills = convert_to_array($bills,'id');
//
//        $bills_items_used_result = $this->select(array(
//            'select'    => array(
//                'bill_item'         => array('id','bill_id','item_id'),
//                'bill_item_credit'  => array('credit_used_id' => 'credit_id','credit_used_value' => 'credit_value'),
//                'item' => array('item_name'=>'name')
//            ),
//            'from'      => array(
//                'bill_item'         => array('table' => 'bill_item'),
//                'bill' => array('table' => 'bill', 'condition'=>'bill.id = bill_item.bill_id'),
//                'bill_item_credit'  => array('table' => 'bill_item_credit', 'condition' => array('bill_item.id = bill_item_credit.bill_item_id')),
//                'item' => array('table'=>'item', 'condition'=>'bill_item.item_id=item.id')
//            ),
//            'where'     => $condition,
//            'user_level' => Permission_Value::ADMIN
//        ))->result();
//
//        $bills_items_used = convert_to_array($bills_items_used_result,'bill_id','',false);
//        $credits = $this->select(array(
//            'select'    => array(
//                'credit'                => '*',
//                'credit_branch_group'   => array('branch_group_id')
//            ),
//            'from'      => array(
//                'credit'    => array('table' => 'credit'),
//                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit_branch_group.credit_id = credit.id')
//            ),
//            'no_permission' => true
//        ))->result();
//        $branch_group_data = $this->load->table_model('branch_group')->getTableMap('id');
//        $credit_data = convert_to_array($credits,'id');
//        /*----- Get Tracking Package SQL ---------------*/
//        $credit_track  = array();
//        foreach($credits as $row){
//            $credit_track[$row->id] = $row->branch_group_id;
//        }
//        /*-------------------------------------*/
//
//        $credits = convert_to_array($credits,'customer_id','',false);
//        foreach($credits as $key=>$credit){
//            $credits[$key] = convert_to_array($credit,'item_id');
//        }
//
//        $customers = $this->load->table_model('customer')->getTableMap('id');
//        foreach($bills_items_used as $key=>$bill_items){
//            if(isset($bills[$key])){
//                $bill_items = convert_to_array($bill_items,'credit_used_id','',false);
//                $total = 0;
//                foreach($bill_items as $credit_id=>$credit_used_values){
//                    foreach($credit_used_values as $value){
//                        if(isset($bills[$key]->item_name)){
//                            $bills[$key]->item_name .= ','.$value->item_name;
//                        }else{
//                            $bills[$key]->item_name = $value->item_name;
//                        }
//                        $total += $value->credit_used_value;
//                    }
//                    $bill_items[$credit_id] = $total;
//                }
//                $bills[$key]->credit_used = $bill_items;
//            }
//        }
//
//        $redemption_data = $this->_get_tracking_bills($bills,$credit_track);
//        $redemption_detail = array();
//        foreach($branch_id as $user_branch_id) {
//            $branch = $branch_data[$user_branch_id];
//            $branch_redemption = isset($redemption_data['branch_redemption'][$user_branch_id]) ? $redemption_data['branch_redemption'][$user_branch_id] : array();
//            /*----- Gather data for view ------*/
//
//            foreach ($branch_redemption as $branch_group_id => $bill_fields) {
//                if (!isset($redemption_detail[$branch_group_id])) {
//                    $redemption_detail[$branch_group_id] = array(
//                        'by' => array(),
//                        'at' => array());
//                }
//                foreach ($bill_fields as $bill_id => $bill_value) {
//                    $redemption = 0;
//                    foreach ($bill_value as $credit_id => $credit_value) {
//                        $redemption += $credit_value;
//                    }
//                    $detail_row = array(
//                        'redemption' => $redemption,
//                        'bill_code' => $bills[$bill_id]->code,
//                        'item_name' => $bills[$bill_id]->item_name,
//                        'customer_code' => $customers[$bills[$bill_id]->customer_id]->code,
//                        'customer_name' => to_full_name($customers[$bills[$bill_id]->customer_id]->first_name, $customers[$bills[$bill_id]->customer_id]->last_name),
//                        'branch_name' => $bills[$bill_id]->branch_name,
//                        'created_date' => get_user_date($bills[$bill_id]->created_date, '', BASE_DATE_FORMAT_LONG),
//                        'bill_id' => $bill_id
//                    );
//                    $redemption_detail[$branch_group_id]['by'][] = $detail_row;
//                }
//            }
//        }
//        foreach($redemption_data['branch_redemption'] as $branch_id => $branch_group){
//            if($branch_id == $user_branch_id){
//                continue;
//            }
//            foreach($branch_group as $branch_group_id => $bill_fields){
//                if(! isset($redemption_detail[$branch_group_id])){
//                    $redemption_detail[$branch_group_id] = array(
//                        'by' => array(),
//                        'at' => array());
//                }
//                foreach($bill_fields  as $bill_id => $bill_value){
//                    $redemption = 0;
//                    foreach($bill_value as $credit_id => $credit_value){
//                        if($credit_data[$credit_id]->branch_group_id == $branch->branch_group_id)
//                            $redemption += $credit_value;
//                    }
//                    if($redemption == 0){
//                        continue;
//                    }
//                    $detail_row = array(
//                        'redemption'    => $redemption,
//                        'bill_code'     => $bills[$bill_id]->code,
//                        'item_name'     => $bills[$bill_id]->item_name,
//                        'customer_code' => $customers[$bills[$bill_id]->customer_id]->code,
//                        'customer_name' => to_full_name($customers[$bills[$bill_id]->customer_id]->first_name,$customers[$bills[$bill_id]->customer_id]->last_name),
//                        'branch_name'   => $bills[$bill_id]->branch_name,
//                        'created_date'  => get_user_date($bills[$bill_id]->created_date,'',BASE_DATE_FORMAT_LONG),
//                        'bill_id'       => $bill_id
//                    );
//                    if(! isset($redemption_detail[$bills[$bill_id]->branch_group_id])){
//                        $redemption_detail[$bills[$bill_id]->branch_group_id] = array(
//                            'by' => array(),
//                            'at' => array(),
//                        );
//                    }
//                    $redemption_detail[$bills[$bill_id]->branch_group_id]['at'][] = $detail_row;
//                }
//            }
//        }
//
//        if($type ==  'detail'){
//
//            return array(
//                'start_date'        => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
//                'end_date'          => get_user_date($data->end_date,'',BASE_DATE_FORMAT_LONG),
//                'redemption_detail' => $redemption_detail,
//                'branch_group_data' => $branch_group_data,
//                'type'              => $type
//            );
//        }
//        $redemption_summary = array();
//        foreach($redemption_detail as $branch_group_id=>$rows_by_at){
//            $redemption_summary[$branch_group_id] = array();
//            $branch_by = array();
//            $branch_at = array();
//            $rows = $rows_by_at['by'];
//            $total = 0;
//            foreach($rows as $row){
//                $total += $row['redemption'];
//                if(isset($branch_by[$row['branch_name']])){
//                    $branch_by[$row['branch_name']] += $row['redemption'];
//                }else{
//                    $branch_by[$row['branch_name']] = $row['redemption'];
//                }
//            }
//            $redemption_summary[$branch_group_id]['by'] = $total;
//            $redemption_summary[$branch_group_id]['branch_by'] = $branch_by;
//            $total = 0;
//            $rows = $rows_by_at['at'];
//            foreach($rows as $row){
//                $total += $row['redemption'];
//                if(isset($branch_at[$row['branch_name']])){
//                    $branch_at[$row['branch_name']] += $row['redemption'];
//                }else{
//                    $branch_at[$row['branch_name']] = $row['redemption'];
//                }
//            }
//            $redemption_summary[$branch_group_id]['at'] = $total;
//            $redemption_summary[$branch_group_id]['branch_at'] = $branch_at;
//
//        }
//        return array(
//            'start_date'        => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
//            'end_date'          => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
//            'redemption_summary'=> $redemption_summary,
//            'branch_group_data' => $branch_group_data,
//            'type'              => $type
//        );
//    }


    function admin_get_detail_data($post_data){
        if(isset($post_data['summary'])){
            $type = 'summary';
        }else{
            $type = 'detail';
        }

        $data = new stdClass();
        $data->start_date = $post_data['start_date'];
        $data->end_date = $post_data['end_date'];

        $branch_group_id    = $post_data['branch_group_id'];
        $branch_data        = $this->load->table_model('branch_group')->get_branch_and_branch_group($branch_group_id);
        $branch_data        = convert_to_array($branch_data,'id');
        $branch_id          = convert_to_array($branch_data,'','id');
        $condition          = array(
            "bill.created_date >= '{$data->start_date}'",
            "bill.created_date <= '{$data->end_date}'",
            "bill.status" => BILL_STATUS('Complete'),
        );

        /* ----- Gather bill data and item data for each bill -----*/
        $bills       = $this->select(array(
            'select'    => array('bill' => '*',
                'branch' => array('branch_id' => 'id', 'branch_name' => 'name' , 'branch_group_id'),
                'branch_group' => array('branch_group_name' => 'name')),
            'from'      => array(
                'bill'          => array('table' => 'bill'),
                'bill_branch'   => array('table' => 'bill_branch', 'condition' => 'bill_branch.bill_id = bill.id'),
                'branch'        => array('table' => 'branch', 'condition' => 'bill_branch.branch_id = branch.id'),
                'branch_group'  => array('table' => 'branch_group', 'condition' => 'branch_group.id = branch.branch_group_id'),
            ),
            'where'     => $condition,
            'order'     => 'bill.created_date DESC',
            'user_level' => Permission_Value::ADMIN
        ))->result();
        $bills = convert_to_array($bills,'id');

        $bills_items_used_result = $this->select(array(
            'select'    => array(
                'bill_item'         => array('id','bill_id','item_id'),
                'bill_item_credit'  => array('credit_used_id' => 'credit_id','credit_used_value' => 'credit_value'),
                'item' => array('item_name'=>'name')
            ),
            'from'      => array(
                'bill_item'         => array('table' => 'bill_item'),
                'bill' => array('table' => 'bill', 'condition'=>'bill.id = bill_item.bill_id'),
                'bill_item_credit'  => array('table' => 'bill_item_credit', 'condition' => array('bill_item.id = bill_item_credit.bill_item_id')),
                'item' => array('table'=>'item', 'condition'=>'bill_item.item_id=item.id')
            ),
            'where'     => $condition,
            'user_level' => Permission_Value::ADMIN
        ))->result();

        $bills_items_used = convert_to_array($bills_items_used_result,'bill_id','',false);
        $credits = $this->select(array(
            'select'    => array(
                'credit'                => '*',
                'credit_branch_group'   => array('branch_group_id')
            ),
            'from'      => array(
                'credit'    => array('table' => 'credit'),
                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit_branch_group.credit_id = credit.id')
            ),
            'no_permission' => true
        ))->result();
        $branch_group_data = $this->load->table_model('branch_group')->getTableMap('id');
        $credit_data = convert_to_array($credits,'id');
        /*----- Get Tracking Package SQL ---------------*/
        $credit_track  = array();
        foreach($credits as $row){
            $credit_track[$row->id] = $row->branch_group_id;
        }
        /*-------------------------------------*/

        $credits = convert_to_array($credits,'customer_id','',false);
        foreach($credits as $key=>$credit){
            $credits[$key] = convert_to_array($credit,'item_id');
        }

        $customers = $this->load->table_model('customer')->getTableMap('id');

        foreach($bills_items_used as $key=>$bill_items){
            if(isset($bills[$key])){
                $bill_items = convert_to_array($bill_items,'credit_used_id','',false);
                foreach($bill_items as $credit_id=>$credit_used_values){
                    $total = 0;
                    foreach($credit_used_values as $value){
                        if(isset($bills[$key]->item_name)){
                            $bills[$key]->item_name .= ','.$value->item_name;
                        }else{
                            $bills[$key]->item_name = $value->item_name;
                        }
                        $total += $value->credit_used_value;
                    }
                    $bill_items[$credit_id] = $total;
                }
                $bills[$key]->credit_used = $bill_items;
            }
        }

        $redemption_data = $this->_get_tracking_bills($bills,$credit_track);

        $redemption_detail = array();
        foreach($branch_id as $user_branch_id) {
            $branch = $branch_data[$user_branch_id];
            $branch_redemption = isset($redemption_data['branch_redemption'][$user_branch_id]) ? $redemption_data['branch_redemption'][$user_branch_id] : array();
            /*----- Gather data for view ------*/

            foreach ($branch_redemption as $branch_group_id => $bill_fields) {
                if (!isset($redemption_detail[$branch_group_id])) {
                    $redemption_detail[$branch_group_id] = array(
                        'by' => array(),
                        'at' => array());
                }
                foreach ($bill_fields as $bill_id => $bill_value) {
                    $redemption = 0;
                    foreach ($bill_value as $credit_id => $credit_value) {
                        $redemption += $credit_value;
                    }
                    $detail_row = array(
                        'redemption' => $redemption,
                        'bill_code' => $bills[$bill_id]->code,
                        'item_name' => $bills[$bill_id]->item_name,
                        'customer_code' => $customers[$bills[$bill_id]->customer_id]->code,
                        'customer_name' => to_full_name($customers[$bills[$bill_id]->customer_id]->first_name, $customers[$bills[$bill_id]->customer_id]->last_name),
                        'branch_name' => $bills[$bill_id]->branch_name,
                        'created_date' => get_user_date($bills[$bill_id]->created_date, '', BASE_DATE_FORMAT_LONG),
                        'bill_id' => $bill_id
                    );
                    $redemption_detail[$branch_group_id]['by'][] = $detail_row;
                }
            }
        }
        foreach($redemption_data['branch_redemption'] as $branch_id => $branch_group){
            if($branch_id == $user_branch_id){
                continue;
            }
            foreach($branch_group as $branch_group_id => $bill_fields){
                if(! isset($redemption_detail[$branch_group_id])){
                    $redemption_detail[$branch_group_id] = array(
                        'by' => array(),
                        'at' => array());
                }
                foreach($bill_fields  as $bill_id => $bill_value){
                    $redemption = 0;
                    foreach($bill_value as $credit_id => $credit_value){
                        if($credit_data[$credit_id]->branch_group_id == $branch->branch_group_id)
                            $redemption += $credit_value;
                    }
                    if($redemption == 0){
                        continue;
                    }
                    $detail_row = array(
                        'redemption'    => $redemption,
                        'bill_code'     => $bills[$bill_id]->code,
                        'item_name'     => $bills[$bill_id]->item_name,
                        'customer_code' => $customers[$bills[$bill_id]->customer_id]->code,
                        'customer_name' => to_full_name($customers[$bills[$bill_id]->customer_id]->first_name,$customers[$bills[$bill_id]->customer_id]->last_name),
                        'branch_name'   => $bills[$bill_id]->branch_name,
                        'created_date'  => get_user_date($bills[$bill_id]->created_date,'',BASE_DATE_FORMAT_LONG),
                        'bill_id'       => $bill_id
                    );
                    if(! isset($redemption_detail[$bills[$bill_id]->branch_group_id])){
                        $redemption_detail[$bills[$bill_id]->branch_group_id] = array(
                            'by' => array(),
                            'at' => array(),
                        );
                    }
                    $redemption_detail[$bills[$bill_id]->branch_group_id]['at'][] = $detail_row;
                }
            }
        }

        if($type ==  'detail'){

            return array(
                'start_date'        => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
                'end_date'          => get_user_date($data->end_date,'',BASE_DATE_FORMAT_LONG),
                'redemption_detail' => $redemption_detail,
                'branch_group_data' => $branch_group_data,
                'type'              => $type
            );
        }
        $redemption_summary = array();
        foreach($redemption_detail as $branch_group_id=>$rows_by_at){
            $redemption_summary[$branch_group_id] = array();
            $branch_by = array();
            $branch_at = array();
            $rows = $rows_by_at['by'];
            $total = 0;
            foreach($rows as $row){
                $total += $row['redemption'];
                if(isset($branch_by[$row['branch_name']])){
                    $branch_by[$row['branch_name']] += $row['redemption'];
                }else{
                    $branch_by[$row['branch_name']] = $row['redemption'];
                }
            }
            $redemption_summary[$branch_group_id]['by'] = $total;
            $redemption_summary[$branch_group_id]['branch_by'] = $branch_by;
            $total = 0;
            $rows = $rows_by_at['at'];
            foreach($rows as $row){
                $total += $row['redemption'];
                if(isset($branch_at[$row['branch_name']])){
                    $branch_at[$row['branch_name']] += $row['redemption'];
                }else{
                    $branch_at[$row['branch_name']] = $row['redemption'];
                }
            }
            $redemption_summary[$branch_group_id]['at'] = $total;
            $redemption_summary[$branch_group_id]['branch_at'] = $branch_at;

        }
        return array(
            'start_date'        => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
            'end_date'          => get_user_date($data->start_date,'',BASE_DATE_FORMAT_LONG),
            'redemption_summary'=> $redemption_summary,
            'branch_group_data' => $branch_group_data,
            'type'              => $type
        );
    }



    /**
     * @param array $bills
     * @param array $credit_track
     *  credit_add : key-value pair credit added
     *  credit_used: key-value pair credit_used
     * @return array branch_redemption and each bill redemption
     */

    function _get_tracking_bills($bills, $credit_track = array()){

        $branch_redemption = array();
        foreach($bills as $bill_key=>$bill){
            if(! isset($branch_redemption[$bill->branch_id])){
                $branch_redemption[$bill->branch_id] = array();
            }
            $bill->redemtion = 0;
            if(isset($bill->credit_used))
                foreach($bill->credit_used as $credit_id=>$credit_value){
                    if($credit_track[$credit_id] != $bill->branch_group_id){
                        if(! isset($branch_redemption[$bill->branch_id][$credit_track[$credit_id]]))
                        {
                            $branch_redemption[$bill->branch_id][$credit_track[$credit_id]] = array();
                        }
                        if(! isset($branch_redemption[$bill->branch_id][$credit_track[$credit_id]][$bill_key])){
                            $credit_redemption[$bill_key] = array();
                        }
                        $branch_redemption[$bill->branch_id][$credit_track[$credit_id]][$bill_key][$credit_id] = $credit_value;
                    }
                }
        }

        return array(
            'branch_redemption' => $branch_redemption,
            'bills'             => $bills
        );
    }
    function get_original_data($data,$type = false){
        $content = array();
        $type = $data['type'];
        $branch_group_data = $data['branch_group_data'];
        if($type == 'detail'){
            $redemption_detail = $data['redemption_detail'];
            foreach($redemption_detail as $branch_group_id=>$by_and_at){
                if(isset($by_and_at['by']) &&  count($by_and_at['by'])){
                    $content[] = array(
                        array('export_text' => 'Redemption by '.$branch_group_data[$branch_group_id]->name,
                            'style'=>'width:25%',
                            'html_class'=>'text-primary'
                        ),
                        array('export_text' => 'Branch Name',
                            'style'=>'width:10%'),
                        array('export_text' => 'Date',
                            'style'=>'width:20%'),
                        array('export_text' => 'Bill No',
                            'style'=>'width:10%'),
                        array('export_text' => 'Item',
                            'style'=>'width:25%'),
                        array('export_text' => 'Redemption',
                            'style'=>'width:10%'),
                    );
                    foreach($by_and_at['by'] as $row){
                        $content[] = array(
                            array('export_text' => 'Customer '.$row['customer_code']  . ' - ' . $row['customer_name']),
                            array('export_text' => $row['branch_name']),
                            array('export_text' => $row['created_date']),
                            array('export_text' => $row['bill_code']),
                            array('export_text' => $row['item_name']),
                            array('export_text' => $row['redemption']),

                        );
                    }
                }
                if(isset($by_and_at['at']) &&  count($by_and_at['at'])){
                    $content[] = array(
                        array('export_text' => 'Redemption at '.$branch_group_data[$branch_group_id]->name,
                            'html_class'=>'text-danger',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Branch Name',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Date',
                         'style'=>'font-weight:bold'),
                        array('export_text' => 'Bill No',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Item',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Redemption',
                            'style'=>'font-weight:bold'),

                    );
                    foreach($by_and_at['at'] as $row){
                        $content[] = array(
                            array('export_text' => 'Customer '.$row['customer_code']  . ' - ' . $row['customer_name']),
                            array('export_text' => $row['branch_name']),
                            array('export_text' => $row['created_date']),
                            array('export_text' => $row['bill_code']),
                            array('export_text' => $row['item_name']),
                            array('export_text' => $row['redemption']),

                        );
                    }
                }
            }
        }else{
            $redemption_summary = $data['redemption_summary'];
            foreach($redemption_summary as $branch_group_id => $row_by_at){
                if($row_by_at['by']!=0 || $row_by_at['at']!=0){
                    $content[] = array(
                        array('export_text' => 'Redemption by '.$branch_group_data[$branch_group_id]->name,
                            'style'=>'width:30%'),
                        array('export_text' =>'')

                    );
                    foreach($row_by_at['branch_by'] as $branch_name=>$branch_by){
                        $content[] = array(
                            array('export_text' =>  $branch_name,
                                'style'=>'text-align: right'),
                            array('export_text' => $branch_by,
                                'style'=>'text-align: left'),
                           );
                    }
                    $content[] = array(
                        array('export_text' => 'Total',
                            'style'=>'text-align: right'),
                        array('export_text' =>$row_by_at['by'],
                            'style'=>'text-align: left'),

                    );

                    $content[] = array(
                        array('export_text' => 'Redemption at '.$branch_group_data[$branch_group_id]->name,
                            'style'=>'width:30%; font-weight:bold'),
                        array('export_text' =>'')
                    );
                    foreach($row_by_at['branch_at'] as $branch_name=>$branch_at){

                        $content[] = array(
                            array('export_text' =>  $branch_name,
                                'style'=>'text-align: right'),
                            array('export_text' =>'-'.$branch_at,
                                'style'=>'text-align: left')
                        );
                    }
                    $content[] = array(
                        array('export_text' => 'Total',
                            'style'=>'text-align: right'),
                        array('export_text' => '-'.$row_by_at['at'],
                            'style'=>'text-align: left'),

                    );
                    $content[] = array(
                        array('export_text' => 'Settlement',
                            'style'=>'font-weight: bold'),
                        array('export_text' => $row_by_at['by'] - $row_by_at['at'],
                            'style'=>'text-align: left'),
                        );
                }
            }
        }
        return $content;
    }
}