<?php
class Credit_Balance_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $month_distance=$data['month_distance'];
        $end_date = get_database_date(date('Y-m-d', strtotime($month_distance.' +1 days')));
      //  var_dump($end_date);
        $now = get_database_date();

        $branch_group_id = $data['branch_group_id'];

        if($branch_group_id == 0){
            $branch_group_id = array(1,2,3);
        }
        // GET BALANCE TODAY - CHECKED
        $todayBalance = $this->load->table_model('credit')->select(array(
            'select'        => array(
            'credit'        => array('credit_id' => 'id', 'customer_id','item_id','end_date','credit')
        ),
            'from'          => array(
            'credit'                => array('table' => 'credit'),
            'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id'),
            'customer'              => array('table' => 'customer', 'condition' => 'credit.customer_id = customer.id')
        ),
            'no_permission' => true,
            'where'         => array(
            'customer.status' => 1,
         //   'customer.id' => 6563,
            'credit_branch_group.branch_group_id' => $branch_group_id,
            'credit.end_date <' => '2200-01-01 00:00:00', // hardcode to get the current balance base on credit table
            'credit.end_date >' => $now
        ),
        ))->result_array();
        $temp = array();
        foreach($todayBalance as $item){
            $temp[$item['credit_id']] = $item;
        }
        $todayBalance = $temp; unset($temp);

//        $total = 0;
//        foreach($todayBalance as $item){
//            $total += $item['credit'];
//        }
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        // ADD CREDITS REDEEMED FROM THE SELECTED DATE COUNT TO TODAY && MINUS ALL CREDITS PURCHASED AT BRANCH GROUP SELECTED AT SELECTED DATE TO TODAY - CHECKED
        $redeemedCredit = $this->select(array(
            'select' => array(
                'bill' => array('customer_id'),
                'bill_item' => array('credit_add'),
                'IF(bill_item_credit.credit_value is NULL,0,bill_item_credit.credit_value) as credit_redeemed,
                 IF(bill_item_credit.credit_id is null, bill_item.credit_id, bill_item_credit.credit_id) as credit_id'
            ),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => array('bill.id = bill_item.bill_id'), 'type' => 'LEFT'),
                'bill_item_credit' => array('table' => 'bill_item_credit', 'condition' => array('bill_item.id = bill_item_credit.bill_item_id'), 'type' => 'LEFT'),
                'bill_branch_group' => array('table'=> 'bill_branch_group', 'condition' => array('bill.id = bill_branch_group.bill_id'), 'type' => 'LEFT')
            ),
            'where' => array(
            //    'bill.customer_id' => 6563,
                'bill_branch_group.branch_group_id' => $branch_group_id,
                'bill.status' => BILL_STATUS('Complete'),
                'bill.created_date >' => $end_date,
                'bill.created_date <' => '2200-01-01 00:00:00'
            ),
            'no_permission' => true,
            'having' => 'bill_item.credit_add > 0 OR credit_redeemed > 0'
        ))->result_array();

        $redeemedCreditNew = array();
        foreach($redeemedCredit as $item){
                if (isset($redeemedCreditNew[$item['credit_id']])) {
                    $redeemedCreditNew[$item['credit_id']]['credit_id'] = $item['credit_id'];
                    $redeemedCreditNew[$item['credit_id']]['credit_add'] += $item['credit_add'];
                    $redeemedCreditNew[$item['credit_id']]['credit_redeemed'] += $item['credit_redeemed'];
                } else {
                    $redeemedCreditNew[$item['credit_id']] = $item;
                }
        }
        unset($redeemedCredit);

        $listCredit = $this->select(array(
            'select' => array(
                'credit' => array('credit_id' => 'id','item_id','end_date')
            ),
            'from' => array(
                'credit' => array('table' => 'credit'),
                'credit_branch_group' => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id')
            ),
            'no_permission' => true,
            'where' => array(
                'credit_branch_group.branch_group_id' => $branch_group_id
            )
        ))->result_array();
        foreach($listCredit as $it){
            $temp[$it['credit_id']] = $it;
        }
        $listCredit = $temp; unset($temp);

        foreach($redeemedCreditNew as $item){
            if(isset($listCredit[$item['credit_id']])){
                $redeemedCreditNew[$item['credit_id']]['item_id'] = $listCredit[$item['credit_id']]['item_id'];
            }
            else{
                unset($redeemedCreditNew[$item['credit_id']]);
            }
        }

//        $total = 0;
//        echo '<pre>';
//        foreach($redeemedCreditNew as $item){
//            $total += $item['credit_add'];
//        }
//        echo '</pre>';
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        // MINUS THE REDEEMED BY CREDITS FROM OTHER BRANCH GROUP FROM SELECTED DATE TO TODAY - CHECKED
        $listBill = $this->select(array(
            'select'        => array(
                'bill'        => array('bill_code' => 'code','customer_id','created_date'),
                'bill_item_credit' => array('credit_id','credit_value')
            ),
            'from' => array(
                'bill'                  => array('table' => 'bill'),
                'bill_item'             => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id'),
                'bill_item_credit'      => array('table' => 'bill_item_credit', 'condition' => 'bill_item.id = bill_item_credit.bill_item_id'),
            ),
            'no_permission' => true,
            'where'         => array(
            //    'bill.customer_id' => 6563,
                'bill.status' => BILL_STATUS('Complete'),
                'bill.created_date >' => $end_date,
                'bill.created_date <' => '2200-01-01 00:00:00'
            ),
        ))->result_array();
        $creditRedeemed = array();
        foreach($listBill as $item){
            if(isset($listCredit[$item['credit_id']])){
                if(isset($creditRedeemed[$item['credit_id']]['credit_redeemed'])){
                    $creditRedeemed[$item['credit_id']]['credit_redeemed'] += $item['credit_value'];
                }
                else{
                    $creditRedeemed[$item['credit_id']]['credit_id'] = $item['credit_id'];
                    $creditRedeemed[$item['credit_id']]['customer_id'] = $item['customer_id'];
                    $creditRedeemed[$item['credit_id']]['item_id'] = $listCredit[$item['credit_id']]['item_id'];
                    $creditRedeemed[$item['credit_id']]['credit_redeemed'] = $item['credit_value'];
                }
            }
        }

//        $total = 0;
//        foreach($creditRedeemed as $item){
//            $total += $item['credit_redeemed'];
//        }
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        // ADD CREDIT WAS EXPIRED FROM SELECTED DATE TO TODAY -- CHECKED
        $expiredCredit = $this->load->table_model('credit')->select(array(
            'select'        => array(
                'credit'        => array('credit_id' => 'id', 'customer_id','item_id','end_date','credit'),
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit'),
                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id')
            ),
            'no_permission' => true,
            'where'         => array(
            //    'credit.customer_id' => 6563,
                'credit_branch_group.branch_group_id' => $branch_group_id,
                'credit.end_date <' => $now,
                'credit.end_date >' => $end_date
            )
        ))->result_array();

        $temp = array();
        foreach($expiredCredit as $item){
            $temp[$item['credit_id']] = $item;
        }
        $expiredCredit = $temp; unset($temp);

//        $total = 0;
//        foreach ($expiredCredit as $item){
//            $total += $item['credit'];
//        }
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        // CREDIT ADJUSTMENT LOG
        $creditAdjustment = $this->load->table_model('credit')->select(array(
            'select' => array(
                'credit' => array('customer_id', 'item_id'),
                'credit_adjustment_log' => array('credit_id', 'credit_change')
            ),
            'from' => array(
                'credit_adjustment_log' => array('table' => 'credit_adjustment_log'),
                'credit_branch_group' => array('table' => 'credit_branch_group', 'condition' => 'credit_adjustment_log.credit_id = credit_branch_group.credit_id', 'type' => 'LEFT'),
                'credit' => array('table' => 'credit', 'condition' => 'credit_adjustment_log.credit_id = credit.id', 'type' => 'LEFT'),
            ),
            'no_permission' => true,
            'where' => array(
                'credit_branch_group.branch_group_id' => $branch_group_id,
                'credit_adjustment_log.log_time >' => $end_date,
                'credit_adjustment_log.log_time <' => $now,
            //    'credit_id' => 7643
            )
        ))->result_array();

        $temp = array();
        foreach($creditAdjustment as $item){
            if(isset($temp[$item['credit_id']])){
                $temp[$item['credit_id']]['credit_change'] += $item['credit_change'];
            }
            else {
                $temp[$item['credit_id']] = $item;
            }
        }
        $creditAdjustment = $temp;

//        $total = 0;
//        foreach($creditAdjustment as $item){
//            $total += $item['credit_change'];
//        }
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        // BEGIN CALCULATOR
        $final = $todayBalance;
        foreach($redeemedCreditNew as $a){
            if(isset($final[$a['credit_id']])){
                $final[$a['credit_id']]['credit'] -= $a['credit_add'];
            }
            else{
                $final[$a['credit_id']] = array(
                    'customer_id' => $a['customer_id'],
                    'credit_id' => $a['credit_id'],
                    'item_id' => $a['item_id'],
                    'end_date' => '',
                    'credit' => -$a['credit_add']
                );
            }
        }

        foreach($creditRedeemed as $a){
            if(isset($final[$a['credit_id']])){
                $final[$a['credit_id']]['credit'] += $a['credit_redeemed'];
            }
            else{
                $final[$a['credit_id']] = array(
                    'customer_id' => $a['customer_id'],
                    'credit_id' => $a['credit_id'],
                    'item_id' => $a['item_id'],
                    'end_date' => '',
                    'credit' => $a['credit_redeemed']
                );
            }
        }

        foreach($expiredCredit as $a){
            if(isset($final[$a['credit_id']])){
                $final[$a['credit_id']]['credit'] += $a['credit'];
            }
            else{
                $final[$a['credit_id']] = array(
                    'customer_id' => $a['customer_id'],
                    'credit_id' => $a['credit_id'],
                    'item_id' => $a['item_id'],
                    'end_date' => '',
                    'credit' => $a['credit']
                );
            }
        }

        foreach($creditAdjustment as $a){
            if(isset($final[$a['credit_id']])){
                $final[$a['credit_id']]['credit'] -= $a['credit_change'];
            }
            else{
                $final[$a['credit_id']] = array(
                    'customer_id' => $a['customer_id'],
                    'credit_id' => $a['credit_id'],
                    'item_id' => $a['item_id'],
                    'end_date' => '',
                    'credit' => -$a['credit_change']
                );
            }
        }

//        $total = 0;
//        foreach($final as $item){
//            $total += $item['credit'];
//        }
//        echo '<pre>';
//        var_dump($total);
//        echo '</pre>';

        return array(
            'data' => $final
        );
    }

    function get_original_data($res,$type = false){
        $data = $res['data'];
        $total_credit=0;
        $content = array();
        $content[] = array(
            array('export_text' =>  'Customer Name'),
            array('export_text' => 'Client ID'),
            array('export_text' => 'Credit Name'),
            array('export_text' => 'Credit Balance'),
            array('export_text' => 'Mobile Number'),
            //    array('export_text' => 'Expiry Date'),
        );

        $customer = $this->select(array(
            'select' => array('customer' => array('id', 'customer_name' => 'first_name', 'customer_code' => 'code', 'customer_mobile' => 'mobile_number','status','customer_type')),
            'from' => array('customer' => array('table' => 'customer')),
            'no_permission' => true
        ))->result_array();
        foreach($customer as $cust){
            $temp[$cust['id']] = $cust;
        }
        $customer = $temp;

        $itemDetail = $this->select(array(
            'select' => array( 'item' => array('id', 'item_name' => 'name')),
            'from' => array('item' => array('table' => 'item')),
            'no_permission' => true
        ))->result_array();
        foreach($itemDetail as $item){
            $temp[$item['id']] = $item;
        }
        $itemDetail = $temp; unset($temp);
        $system = $this->config->item('current_system');
        $customerStatus = array(1,0);

        foreach($data as $item){
            if($system == 'admin'){
                $link = base_url().'admin/customers/credit_history_report/'.$item['customer_id'];
            }
            else{
                $link = base_url().'customers/credit_history_report/'.$item['customer_id'];
            }

            if($type == 'export'){
                $fill = $customer[$item['customer_id']]['customer_code'];
            }
            else{
                $fill = '<a target="_blank" href="'.$link.'">'.$customer[$item['customer_id']]['customer_code'].'</a>';
            }

        //    if(in_array($customer[$item['customer_id']]['status'], $customerStatus) && $customer[$item['customer_id']]['customer_type'] == 2){
                $tempa = round(floatval($item['credit']),2);
                $content[] = array(
                    array('export_text' => $customer[$item['customer_id']]['customer_name']),
                    array('export_text' => $fill),
                    array('export_text' => $itemDetail[$item['item_id']]['item_name']),
                    array('export_text' => number_format($tempa,2)),
                    array('export_text' => $customer[$item['customer_id']]['customer_mobile'])
                );
                $total_credit += floatval($tempa);
        //    }
        }

        $content[] = array(
            array('export_text' =>  'Total Credits',
                'style'=>'text-align: right; font-weight: bold'),
            array('export_text' => number_format($total_credit,2,'.',','), 2,
                'style'=>'font-weight: bold',
                'colspan'=>'10'),
        );
        return $content;
    }
}