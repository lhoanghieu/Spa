<?php
class No_visit_report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $branch_id = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch_group')->get_branch_group_by_branch_id($branch_id,true);
        $ret_data = array('customers' => array());

        $bills = $this->load->table_model('bill')->get(array(
            "created_date > '{$start_date}'",
            "created_date < '{$end_date}'"
        ),'','','',Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
        if(count($bills) != 0) {
            $customer_ids = convert_to_array($bills, '', 'customer_id');
            $where_condition = array('id NOT IN (' . implode(',', $customer_ids) . ')');

            $customers = $this->load->table_model('customer')->getTableMap('id', '', $where_condition, true, Permission_Value::BRANCH, array('branch_id' => $branch_id, 'branch_group_id' => $branch_group_id));

            foreach ($customers as $id => $customer) {
                if ($customer->customer_permission) {
                    $ret_data['customers'][$id] = $customer;
                }
            }
        }
        return $ret_data;
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        $ret_data = array('customers' => array());

        $bills = $this->load->table_model('bill')->get(array(
            "created_date > '{$start_date}'",
            "created_date < '{$end_date}'"
        ));
        $customer_ids = convert_to_array($bills,'','customer_id');
        $where_condition = array('id NOT IN ('.implode(',',$customer_ids).')');

        $customers = $this->load->table_model('customer')->getTableMap('id','',$where_condition);

        foreach($customers as $id=>$customer){
            if($customer->customer_permission){
                $ret_data['customers'][$id] = $customer;
            }
        }
        return $ret_data;
    }
/*
    function get_export_data($format, $data){
        $content = array();
        $content[] = array('First Name', 'Last Name', 'Mobile Number', 'Email');
        foreach($data['customers'] as $customer_id=>$customer_fields){
            $content[] = array(
                $customer_fields->first_name,
                $customer_fields->last_name,
                $customer_fields->mobile_number,
                $customer_fields->email,
            );
        }
        return $content;
    }
*/
    function get_original_data($data,$type = false){
        $content = array();
        $content[] = array(
            array('export_text' => 'First Name'),
            array('export_text' => 'Last Name'),
            array('export_text' => 'Mobile Number'),
            array('export_text' => 'Email'),
            );
        foreach($data['customers'] as $customer_id=>$customer_fields){
            $content[] = array(
                array('export_text' => $customer_fields->first_name),
                array('export_text' => $customer_fields->last_name),
                array('export_text' => $customer_fields->mobile_number),
                array('export_text' => $customer_fields->email),

            );
        }
        return $content;
    }
}