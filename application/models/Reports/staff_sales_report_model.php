<?php
class Staff_Sales_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function get_form_data(){
        $data = new stdClass();
        $employee_model = $this->load->table_model('employee');
        $data->employees = $employee_model->get();
        return $data;
    }
    function admin_get_detail_data($post_data){
        $start_date = $post_data['start_date'];
        $end_date = $post_data['end_date'];
        $item_id  = isset($post_data['item_id'])?$post_data['item_id']:null;
        $employee_id = isset($post_data['employee_id'])?$post_data['employee_id']:null;
        $category_id    = isset($post_data['category_id'])?$post_data['category_id']:null;

        $model = $this->load->table_model('bill');
        $branch_model = $this->load->table_model('branch');
        $config_model = $this->load->table_model('config');
        $employee_model = $this->load->table_model('employee');
        $timezone = $config_model->get(array('code' => 'timezone'));
        $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));

        $data = new stdClass();
        $user = $this->session->userdata('login');
        $data->start_date   = convert_date($start_date,$timezone[0]->value);
        $data->end_date     = convert_date($end_date,$timezone[0]->value);
        //$data->branch_name  = $branch_model->getByID($user->branch_id)->name;

        //$get_employees_condition = $post_data['employee_id']==0?array():array('employee.id' => $post_data['employee_id']);
        $get_employees_condition[] = "bill.created_date > '{$data->start_date}' AND bill.created_date < '{$data->end_date}'";
        if($item_id!=null) {
            $get_employees_condition['item.id'] = $item_id;
        }
        if($employee_id!=null) {
            $get_employees_condition['employee.id'] = $employee_id;
        }
        if($category_id){
            $category_item_id = $this->load->table_model('category_item')->getTableMap('','item_id',array('category_id' => $category_id));
            if(is_array($item_id)){
                $category_item_id = array_merge($category_item_id,$item_id);
            }
            $get_employees_condition['item.id'] = $category_item_id;
        }
        $bills = $model->select(array(
            'select' => array(
                'bill_item' => array('price','quantity','discount_type','discount_value','bill_item_id' => 'id' ),
                'item'      => array('item_name' => 'name', 'item_id' => 'id'),
                'employee'  => array('employee_id' => 'id', 'employee_first_name' => 'first_name', 'employee_last_name' => 'last_name'),
                'bill'      => array('customer_id','bill_code' => 'code', 'cashier_id' => 'creator'),
                'customer'  => array('customer_first_name' => 'first_name','customer_last_name' => 'last_name', 'member_id' => 'code')
            ),
            'from'   => array(
                'bill_employee' => array('table' => 'bill_employee'),
                'bill_item'     => array('table' => 'bill_item', 'condition' => 'bill_employee.bill_item_id = bill_item.id'),
                'bill'          => array('table' => 'bill','condition' => 'bill.id = bill_item.bill_id'),
                'item'          => array('table' => 'item', 'condition' => 'bill_item.item_id = item.id'),
                'employee'      => array('table' => 'employee', 'condition' => 'employee.id = bill_employee.employee_id'),
                'customer'      => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id','type' => 'left')
            ),
            'where'  => $get_employees_condition,
            'group'  => 'bill_item.item_id, bill_employee.employee_id',
            'order' => 'bill.code',
             'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)

        ))->result();

        $bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bills,'','bill_item_id')),false);
        $employee_ids = convert_to_array($bills, '', 'cashier_id');
        $cashiers = $employee_model->select(array(
            'from'  => array('employee' => array('table' => 'employee')),
            'where' => array('employee.id' => $employee_ids,'employee.status' => ALL_STATUS()),
            'user_level' => Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        ))->result();
        $cashiers = convert_to_array($cashiers,'id');
        foreach($bills as $bill){

            $bill->credit_value = 0;
            if(isset($bill_items_credits[$bill->bill_item_id])){
                foreach($bill_items_credits[$bill->bill_item_id] as $bill_item_credit){
                    $bill->credit_value += $bill_item_credit->credit_value;
                }
            }
            if($bill->cashier_id){
                $bill->cashier_name = to_full_name($cashiers[$bill->cashier_id]->first_name,$cashiers[$bill->cashier_id]->last_name);
            }
            $bill->sale_value = $bill->price * $bill->quantity - $bill->credit_value - calculate_discount($bill->discount_type,$bill->discount_value,$bill->price * $bill->quantity);
        }
        $bills = convert_to_array($bills,'employee_id', '', false);
        $temp = array();
        $total = 0;
        foreach($bills as $key=>$bill_items){
            $temp[$key] = array();
            $temp[$key]['rows'] = $bill_items;
            $temp[$key]['customer_name'] = to_full_name($bill_items[0]->employee_first_name,$bill_items[0]->employee_last_name);
            $temp[$key]['total']= 0;
            foreach($bill_items as $item){
                $temp[$key]['total'] += $item->sale_value;
            }
            $total += $temp[$key]['total'];
        }
        $bills = $temp;
        $data->rows = $bills;
        $data->total = $total;
        return $data;
    }

    //

    function get_detail_data($post_data){
        $start_date = $post_data['start_date'];
        $end_date = $post_data['end_date'];
        $item_id  = isset($post_data['item_id'])?$post_data['item_id']:null;
        $employee_id = isset($post_data['employee_id'])?$post_data['employee_id']:null;
        $model = $this->load->table_model('bill');
        $branch_model = $this->load->table_model('branch');
        $config_model = $this->load->table_model('config');
        $employee_model = $this->load->table_model('employee');
        $timezone = $config_model->get(array('code' => 'timezone'));

        $data = new stdClass();
        $user = $this->session->userdata('login');
        $data->start_date   = convert_date($start_date,$timezone[0]->value);
        $data->end_date     = convert_date($end_date,$timezone[0]->value);
        $data->branch_name  = $branch_model->getByID($user->branch_id)->name;

        //$get_employees_condition = $post_data['employee_id']==0?array():array('employee.id' => $post_data['employee_id']);
        $get_employees_condition[] = "bill.created_date > '{$data->start_date}' AND bill.created_date < '{$data->end_date}'";
        if($item_id!=null) {
            $get_employees_condition['item.id'] = $item_id;
        }
        if($employee_id!=null) {
            $get_employees_condition['employee.id'] = $employee_id;
        }
        $bills = $model->select(array(
            'select' => array(
                'bill_item' => array('price','quantity','discount_type','discount_value','bill_item_id' => 'id' ),
                'item'      => array('item_name' => 'name', 'item_id' => 'id'),
                'employee'  => array('employee_id' => 'id', 'employee_first_name' => 'first_name', 'employee_last_name' => 'last_name'),
                'bill'      => array('customer_id','bill_code' => 'code', 'cashier_id' => 'creator'),
                'customer'  => array('customer_first_name' => 'first_name','customer_last_name' => 'last_name', 'member_id' => 'code')
            ),
            'from'   => array(
                'bill_employee' => array('table' => 'bill_employee'),
                'bill_item'     => array('table' => 'bill_item', 'condition' => 'bill_employee.bill_item_id = bill_item.id'),
                'bill'          => array('table' => 'bill','condition' => 'bill.id = bill_item.bill_id'),
                'item'          => array('table' => 'item', 'condition' => 'bill_item.item_id = item.id'),
                'employee'      => array('table' => 'employee', 'condition' => 'employee.id = bill_employee.employee_id'),
                'customer'      => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id','type' => 'left')
            ),
            'where'  => $get_employees_condition,
            'group'  => 'bill_item.item_id, bill_employee.employee_id',
            'order' => 'bill.code'
        ))->result();

        $bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bills,'','bill_item_id')),false);
        $employee_ids = convert_to_array($bills, '', 'cashier_id');
        $cashiers = $employee_model->select(array(
            'from'  => array('employee' => array('table' => 'employee')),
            'where' => array('employee.id' => $employee_ids,'employee.status' => ALL_STATUS()),
            'user_level' => Permission_Value::ADMIN
        ))->result();
        $cashiers = convert_to_array($cashiers,'id');
        foreach($bills as $bill){

            $bill->credit_value = 0;
            if(isset($bill_items_credits[$bill->bill_item_id])){
                foreach($bill_items_credits[$bill->bill_item_id] as $bill_item_credit){
                    $bill->credit_value += $bill_item_credit->credit_value;
                }
            }
            if($bill->cashier_id){
                $bill->cashier_name = to_full_name($cashiers[$bill->cashier_id]->first_name,$cashiers[$bill->cashier_id]->last_name);
            }
            $bill->sale_value = $bill->price * $bill->quantity - $bill->credit_value - calculate_discount($bill->discount_type,$bill->discount_value,$bill->price * $bill->quantity);
        }
        $bills = convert_to_array($bills,'employee_id', '', false);
        $temp = array();
        $total = 0;
        foreach($bills as $key=>$bill_items){
            $temp[$key] = array();
            $temp[$key]['rows'] = $bill_items;
            $temp[$key]['customer_name'] = to_full_name($bill_items[0]->employee_first_name,$bill_items[0]->employee_last_name);
            $temp[$key]['total']= 0;
            foreach($bill_items as $item){
                $temp[$key]['total'] += $item->sale_value;
            }
            $total += $temp[$key]['total'];
        }
        $bills = $temp;
        $data->rows = $bills;
        $data->total = $total;
        return $data;
    }


    function get_original_data($data,$type = false){

        $content = array();
        foreach($data->rows as $employee_code=>$values){
            $content_row = array(
                array('export_text' => $values['customer_name'],
                    'style'=>'color: red',
                    'colspan'=>'6'),
               );
            $content[] = $content_row;
            $content_row = array(
                array('export_text' => 'Receipt Number',
                    'html_text' => 'Receipt Number',
                    'html_class' => 'under'),
                array('export_text' => 'Cashier Name',
                    'html_text' => 'Cashier Name',
                    'html_class' => 'under'),
                array('export_text' => 'Customer Name',
                    'html_text' => 'Customer Name',
                    'html_class' => 'under'),
                array('export_text' => 'Member ID',
                    'html_text' => 'Member ID',
                    'html_class' => 'under'),
                array('export_text' => 'Item',
                    'html_text' => 'Item',
                    'html_class' => 'under'),
                array('export_text' => 'Sale Value',
                    'html_text' => 'Sale Value',
                    'html_class' => 'under'),
                );
            $content[] = $content_row;
            foreach($values['rows'] as $item_key=>$item_values){
                $content_row = array(
                    array('export_text' =>  $item_values->bill_code),
                    array('export_text' => $item_values->cashier_name),
                    array('export_text' => to_full_name($item_values->customer_first_name,$item_values->customer_last_name)),
                    array('export_text' => $item_values->member_id),
                    array('export_text' => $item_values->item_name),
                    array('export_text' => $item_values->sale_value,
                        'export_type'=>'currency'),

                );
                $content[] = $content_row;
            }
            $content[] = array(
                array('export_text' => 'Total',
                    'colspan' => '5',
                    'style' =>'text-align: right; padding-right: 2%'),
                array('export_text' => $values['total'],
                    'export_type'=>'currency')
            );

        }
        if(count($data->rows)>1)
        {
            $content[] = array(
                array('export_text' => 'Total Sales',
                    'colspan' => '5',
                    'style' =>'text-align: right; padding-right: 2%; font-weight:bold'),
                array('export_text' => $data->total,
                    'export_type'=>'currency',
                'style' =>' font-weight:bold')
            );

        }
        return $content;
    }
}