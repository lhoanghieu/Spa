<?php
class All_Outlet_Stock_Balance_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $category_id        = isset($data['category_id'])?$data['category_id']:null;
        $start_date         = $data['start_date'];
        $branch_group_id    = $data['branch_group_id'];

        /*
        *   Tran Minh Thao -- added 2015/08/17 to select from all branch group
         */
        if($branch_group_id == 0) {
            $branch_group_id = array();
            $all_branch_group = $this->load->table_model('branch_group')->getTableMap('id','',array());
            foreach($all_branch_group as $branch_group)
                array_push($branch_group_id, $branch_group->id);
        }
        // ----- End ------------

        $branch_data        = $this->load->table_model('branch_group')->get_branch_and_branch_group($branch_group_id);
        $branch_data        = convert_to_array($branch_data,'id');
        $branch_id          = convert_to_array($branch_data,'','id');

        $condition = array();
        $item_condition = array();

        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse(array(),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
        $ret_data = array();
        if($warehouse_id){
            $condition['warehouse_id']  = $warehouse_id;
            $condition[]                = "log_time < '$start_date'";
            $log_data = $this->load->table_model('warehouse_item_log')->get($condition,'warehouse_item_log.key desc');
            foreach($log_data as $row){
                if(!isset($ret_data[$row->id])){
                    $ret_data[$row->id] = $row;
                }else{
                    continue;
                }
            }
        }

        $items = $this->load->table_model('item')->getTableMap('id','',$item_condition,true,Permission_Value::ADMIN);
        $ret_data_2 = array();
        if(count($ret_data)){
            foreach($ret_data as $row_id=>$row_values){
                if(!isset($items[$row_values->item_id])){
                    continue;
                }
                $item = $items[$row_values->item_id];
                if(isset($ret_data_2[$item->id])){
                    $ret_data_2[$item->id]->quantity += $row_values->quantity;
                }else{
                    $ret_data[$row_id]->item_name = $item->name;
                    $ret_data[$row_id]->item_code = $item->code;
                    $ret_data[$row_id]->item_price = $item->price;
                    $ret_data[$row_id]->log_time = get_user_date($row_values->log_time,'','',true);
                    $ret_data_2[$item->id] = $ret_data[$row_id];
                }

                if(is_array($warehouse_id)) {
                    foreach($warehouse_id as $warehouse) {
                        if($row_values->warehouse_id == $warehouse)
                            $ret_data_2[$item->id]->warehouse[$warehouse] = $row_values->quantity;
                    }
                }
                else
                    if($row_values->warehouse_id == $warehouse_id)
                        $ret_data_2[$item->id]->warehouse[$warehouse_id] = $row_values->quantity;
            }
        }

        $item_product_list = $this->load->table_model('item_product')->getTableMap('item_id','',array());

        foreach($ret_data_2 as $item){
            $item->item_unit_cost = $item_product_list[$item->item_id]->unit_cost;
            if(isset($item_product_list[$item->item_id]->master_quantity)) {
                $item->master_quantity = $item_product_list[$item->item_id]->master_quantity;
            }
            else{
                $item->master_quantity = 'ERROR';
            }
        }

        $category_condition = array();
        if(isset($category_id) && $category_id){
            $category_condition['category.id'] = $category_id;
        }
        $model = $this->load->table_model('category');
        $list_category = $model->select(array(
            'select'    =>  array(
                'category'    => array('category_name' => 'name','id' => 'id'),
                'category_item' => array('item_id')
            ),
            'from'      => array(
                'category_item' => array('table' => 'category_item'),
                'category' => array('table' => 'category','condition' => 'category_item.category_id = category.id', 'type' => 'LEFT')
            ),
            'where'     => $category_condition
        ))->result();

        foreach($ret_data_2 as $item) {
            foreach($list_category as $cat_item) {
                $check = 0;
                if ($item->item_id == $cat_item->item_id) {
                    $item->category = $cat_item->id;
                    $item->category_name = $cat_item->category_name;
                    $check = 1;
                    break;
                }
            }
            if($check == 0){
                unset($ret_data_2[$item->item_id]);
            }
        }

        $warehouse_condition = array('id' => $warehouse_id);
        $warehouse_list = $this->load->table_model('warehouse')->getTableMap('id','',$warehouse_condition,true,Permission_Value::ADMIN);
        return array(
            'stock_items' => $ret_data_2,
            'warehouse_list' => $warehouse_list
        );
    }

    function get_original_data($data,$type = false){
        $stock_items = $data['stock_items'];
        $warehouse_list = $data['warehouse_list'];
        $add_text = "";

        $content = array();
        $header = array(
            array('export_text' => 'Item Code'),
            array('export_text' => 'Item Name'),
            array('export_text' => 'Master Warehouse'),
            array('export_text' => 'Costs')
        );

        foreach($warehouse_list as $warehouse){
            array_push($header, array('export_text' =>$warehouse->name));
            array_push($header, array('export_text' =>'Costs'));
        }
        array_push($header, array('export_text' => 'Total Quantity'));
        array_push($header, array('export_text' => 'Total Costs'));
        $content[] = $header;

        $category = array();
        foreach($stock_items as $item){
            if(isset($item->category) && !in_array($item->category_name,$category)){
                $category[$item->category] = $item->category_name;
            }
        }
        if(count($stock_items)){
            foreach($category as $category_key => $item_category) {
                $content[] = array($this->to_export_text($item_category,1,20));
                $get_total = array();
                $get_costs = array();
                $master_quantity = 0;
                $grand_master_warehouse_costs = 0;
                $grand_quantity = 0;
                $grand_costs = 0;
                foreach ($stock_items as $warehouse_item_id => $warehouse_item_fields) {
                    if($category_key == $warehouse_item_fields->category){
                        $header = array(
                            array('export_text' => $warehouse_item_fields->item_code),
                            array('export_text' => $warehouse_item_fields->item_name),
                            array('export_text' => $warehouse_item_fields->master_quantity),
                            array('export_text' => $warehouse_item_fields->item_unit_cost,'export_type' => 'currency')
                        );
                        $master_quantity += $warehouse_item_fields->master_quantity;
                        $grand_master_warehouse_costs += ($warehouse_item_fields->master_quantity*$warehouse_item_fields->item_unit_cost);
                        $total_quantity = $warehouse_item_fields->master_quantity;
                        $total_cost = $warehouse_item_fields->master_quantity*$warehouse_item_fields->item_unit_cost;
                        foreach($warehouse_list as $warehouse){
                            if(isset($warehouse_item_fields->warehouse[$warehouse->id]))
                                $quantity = $warehouse_item_fields->warehouse[$warehouse->id];
                            else
                                $quantity = 0;

                            if(!isset($get_total[$warehouse->id]))
                                $get_total[$warehouse->id] = $quantity;
                            else
                                $get_total[$warehouse->id] += $quantity;

                            if(!isset($get_costs[$warehouse->id]))
                                $get_costs[$warehouse->id] = $warehouse_item_fields->item_unit_cost*$quantity;
                            else
                                $get_costs[$warehouse->id] += $warehouse_item_fields->item_unit_cost*$quantity;

                            array_push($header, array('export_text' => $quantity));
                            array_push($header, array('export_text' => $warehouse_item_fields->item_unit_cost,'export_type' => 'currency'));
                            $total_quantity += $quantity;
                            $total_cost += ($quantity*$warehouse_item_fields->item_unit_cost);
                        }
                        array_push($header, array('export_text' => $total_quantity));
                        array_push($header, array('export_text' => $total_cost,'export_type' => 'currency'));
                        $grand_quantity += $total_quantity;
                        $grand_costs += $total_cost;

                        $content[] = $header;
                    }
                }

                $total = array(
                            array('export_text' => 'Total','style'=>'font-weight: bold','colspan' => '2'),
                            array('export_text' => $master_quantity,'style'=>'font-weight: bold'),
                            array('export_text' => $grand_master_warehouse_costs,'style'=>'font-weight: bold','export_type' => 'currency')
                        );
                foreach($warehouse_list as $warehouse) {
                    if(isset($get_total[$warehouse->id])){
                        $temp = array('export_text' => $get_total[$warehouse->id],'style'=>'font-weight: bold');
                        array_push($total, $temp);
                        $temp = array('export_text' => $get_costs[$warehouse->id],'style'=>'font-weight: bold','export_type' => 'currency');
                        array_push($total, $temp);
                    }
                }
                $temp = array('export_text' => $grand_quantity,'style'=>'font-weight: bold');
                array_push($total, $temp);
                $temp = array('export_text' => $grand_costs,'style'=>'font-weight: bold','export_type' => 'currency');
                array_push($total, $temp);
                $content[] = $total;
            }
        }else{
            $content[] = array( array('export_text' => 'There is no record',
                'colspan'=>'8'),
            );
        }
        return $content;
    }
}