<?php
class Customer_Transaction_History_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($post_data){
        global $data;
        $data = new stdClass();
        $start_date = isset($post_data['start_date'])?$post_data['start_date']:null;
        $end_date = isset($post_data['start_date'])?$post_data['end_date']:null;
        $customer_id  = isset($post_data['customer_id'])?$post_data['customer_id']:null;
        $condition = array();
        if(!isset($post_data['all_time'])){
            $condition = array("bill.created_date > '{$start_date}'","bill.created_date < '{$end_date}'");
        }
        $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));

        if($customer_id!=null){
            $condition['bill.customer_id'] = $customer_id;
        }

        $condition[] = 'bill.customer_id > 0';

        $model = $this->load->table_model('bill');

        $bill_id = $this->select(array(
            'select' => array(
                'bill' => array('id')
            ),
            'from'   => array(
                'bill'          => array('table' => 'bill'),
            ),
            'where'     => $condition,
            'user_level'=> Permission_Value::ADMIN,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        ))->result();


        unset($condition['bill.customer_id']);
        $condition = array('bill.id' => convert_to_array($bill_id,'','id'));
        if(!is_array($branch_id)){
            $condition = array('bill.id' => convert_to_array($bill_id,'','id'),'bill_branch.branch_id' => $branch_id);
        }        

        $info = array(
            'select' => array(
                'bill'          => array('bill_code' => 'code', 'customer_id', 'status', 'created_date','amount_due','discount','creator'),
                'bill_branch'   => array('bill_branch_id' => 'branch_id'),
                'bill_item'     => array('bill_item_id'=>'id', 'bill_id', 'item_id', 'quantity', 'price', 'discount_type', 'discount_value', 'credit_add', 'credit_id'),
            ),
            'from'   => array(
                'bill'          => array('table' => 'bill'),
                'bill_branch'   => array('table' => 'bill_branch','condition' => 'bill_branch.bill_id = bill.id'),
                'bill_item'     => array('table' => 'bill_item','condition' => 'bill.id = bill_item.bill_id'),
            ),
            'where'     => $condition,
            'order'     => 'bill.created_date desc',
            'user_level' => Permission_Value::ADMIN
        );

        $results = $model->select($info)->result();
        $customer_id = array();
        $bill_item_id = array();
        $item_id = array();
        $credit_id = array();
        foreach ($results as $key => $value) {
            $customer_id[] = $value->customer_id;
            $bill_item_id[] = $value->bill_item_id;
            $item_id[] = $value->item_id;
            if($value->credit_id){
                $credit_id[] = $value->credit_id;
            }
        }

        $data->branch_map = $this->load->table_model('branch')->getTableMap('id');
        $data->customer_map = $this->load->table_model('customer')->getTableMap('id', '', array('id' =>  $customer_id));
        $data->employee_map = $this->load->table_model('employee')->getTableMap('id','',array('status' => ALL_STATUS()),true,Permission_Value::ADMIN);
        $data->bill_employee_map = $this->load->table_model('bill_employee')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_id),false);
        $data->bill_item_credit_map = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_id),false);
        $data->credit_adjustment_log_map = $this->load->table_model('credit_adjustment_log')->get_minify(array(
            'condition' => array('credit_id' => $credit_id),
            'order_by' => 'log_time desc',
        ));
        $data->credit_adjustment_log_map = convert_to_array($data->credit_adjustment_log_map,'credit_id','',false);

        foreach($data->bill_item_credit_map as $bill_item_id => $bill_item_credit_list){
            foreach($bill_item_credit_list as $bill_item_credit){
                $credit_id[] = $bill_item_credit->credit_id;
            }
        }
        $data->item_map = $this->load->table_model('item')->getTableMap('id', '', array(),true,Permission_Value::ADMIN);
        $data->credit_map = $this->load->table_model('credit')->getTableMap('id','',array('id' => $credit_id),true,Permission_Value::ADMIN);

        $results = $this->mapping_data($results,array(
            'first_function' => function(&$entity,$row_data){
            },
            'mapping_function'=> array(
                'bill_branch_id' => function(&$entity,$row_data){
                    if(!isset($entity)){
                        $entity = array();
                        $entity['detail'] =  $GLOBALS['data']->branch_map[$row_data->bill_branch_id];
                    }
                },
                'customer_id'    => function(&$entity,$row_data){
                    if(!isset($entity)){
                        $entity = array();
                        if(!empty($GLOBALS['data']->customer_map[$row_data->customer_id]))
                            $entity['detail'] = $GLOBALS['data']->customer_map[$row_data->customer_id];

                    }
                },
                'bill_item_id' => function(&$entity,&$row_data){
                    /* ----- Get Employee Map ----- */
                    $row_data->employee_id = array();
                    $row_data->employee_name = array();
                    if(isset($GLOBALS['data']->bill_employee_map[$row_data->bill_item_id])){
                        foreach($GLOBALS['data']->bill_employee_map[$row_data->bill_item_id] as $each_bill_employee){
                            $employee_map = isset($GLOBALS['data']->employee_map[$each_bill_employee->employee_id])?$GLOBALS['data']->employee_map[$each_bill_employee->employee_id]:null;
                            $row_data->employee_id[] = $each_bill_employee->employee_id;
                            if($employee_map) {
                                $row_data->employee_name[] = to_full_name($employee_map->first_name, $employee_map->last_name);
                            }
                        }
                    }
                    $row_data->employee_name = implode(', ', $row_data->employee_name);

                    /* ----- Get Credit Map ------*/
                    $credit_used_after = 0;
                    if(!empty($GLOBALS['data']->bill_item_credit_map[$row_data->bill_item_id])){
                        $credit_used = array();
                        foreach($GLOBALS['data']->bill_item_credit_map[$row_data->bill_item_id] as $item_credit_used){
                            if(isset($GLOBALS['data']->credit_adjustment_log_map[$item_credit_used->credit_id])){
                                $adjustment_log = &$GLOBALS['data']->credit_adjustment_log_map[$item_credit_used->credit_id];
                                foreach($adjustment_log as $id=>$each_log){
                                    if($each_log->log_time > $row_data->created_date){
                                        if(!empty($GLOBALS['data']->credit_map[$item_credit_used->credit_id])) {
                                            $GLOBALS['data']->credit_map[$item_credit_used->credit_id]->credit -= $each_log->credit_change;
                                            unset($GLOBALS['data']->credit_adjustment_log_map[$item_credit_used->credit_id][$id]);
                                        }
                                    }else{
                                        break;
                                    }
                                }
                            }
                            if(!empty($GLOBALS['data']->credit_map[$item_credit_used->credit_id])){
                                if(!empty($GLOBALS['data']->credit_map[$item_credit_used->credit_id]->item_id)){
                                    $credit_used[] = array(
                                        'credit_id'     => $item_credit_used->credit_id,
                                        'credit_value'  => $item_credit_used->credit_value,
                                        'item_name'     => $GLOBALS['data']->item_map[$GLOBALS['data']->credit_map[$item_credit_used->credit_id]->item_id]->name,
                                        'balance'       => $GLOBALS['data']->credit_map[$item_credit_used->credit_id]->credit
                                    );
                                    $GLOBALS['data']->credit_map[$item_credit_used->credit_id]->credit += $item_credit_used->credit_value;
                                    $credit_used_after += $item_credit_used->credit_value;
                                }
                            }
                        }
                        $row_data->credit_used = $credit_used;
                    }else if($row_data->credit_id){
                        if(isset($GLOBALS['data']->credit_adjustment_log_map[$row_data->credit_id])){
                            $adjustment_log = &$GLOBALS['data']->credit_adjustment_log_map[$row_data->credit_id];
                            foreach($adjustment_log as $id=>$each_log){
                                if($each_log->log_time > $row_data->created_date){
                                    if(!empty($GLOBALS['data']->credit_map[$row_data->credit_id])){
                                        if(!empty($GLOBALS['data']->credit_map[$row_data->credit_id]->credit)){
                                            $GLOBALS['data']->credit_map[$row_data->credit_id]->credit -= $each_log->credit_change;
                                            unset($GLOBALS['data']->credit_adjustment_log_map[$row_data->credit_id][$id]);
                                        }
                                    }
                                }else{
                                    break;
                                }
                            }
                        }
                        if(!empty($GLOBALS['data']->credit_map[$row_data->credit_id])){
                            if(!empty($GLOBALS['data']->credit_map[$row_data->credit_id]->item_id)){
                                if(!empty($GLOBALS['data']->item_map[$GLOBALS['data']->credit_map[$row_data->credit_id]->item_id])){
                                    $credit_add = array(
                                        'credit_id'     => $row_data->credit_id,
                                        'credit_value'  => $row_data->credit_add,
                                        'item_name'     => $GLOBALS['data']->item_map[$GLOBALS['data']->credit_map[$row_data->credit_id]->item_id]->name,
                                        'balance'       => $GLOBALS['data']->credit_map[$row_data->credit_id]->credit
                                    );
                                    $GLOBALS['data']->credit_map[$row_data->credit_id]->credit -= $row_data->credit_add;
                                    $row_data->credit_add = $credit_add;
                                }
                            }
                        }

                    }else{
                        $row_data->credit_used = null;
                        $row_data->credit_add = null;
                    }
                    /* --- Calculate Sales ----*/
                    $row_data->sale_value = $row_data->price * $row_data->quantity - $credit_used_after - calculate_discount($row_data->discount_type,$row_data->discount_value,$row_data->price * $row_data->quantity);
                    if(empty($GLOBALS['data']->item_map[$row_data->item_id]->code) && empty($GLOBALS['data']->item_map[$row_data->item_id]->code)){
                        $row_data->item_name = '';
                        $row_data->item_code = '';
                    }
                    else{
                        $row_data->item_name = $GLOBALS['data']->item_map[$row_data->item_id]->name;
                        $row_data->item_code = $GLOBALS['data']->item_map[$row_data->item_id]->code;
                    }
                    $row_data->creator = to_full_name($GLOBALS['data']->employee_map[$row_data->creator]->first_name,$GLOBALS['data']->employee_map[$row_data->creator]->last_name);
                    $row_data->created_date = get_user_date($row_data->created_date);
                }
            )
        ),true);
        $data->results = $results;
        return $data;

    }



    function get_original_data($data,$type = false){
        $content = array();
        $content[] = array(
            array('export_text' => 'Date',
                'style'=>'width: 10%'),
            array('export_text' => 'Item No',
                'style'=>'width: 5%'),
            array('export_text' => 'Item Name',
                'style'=>'width: 10%'),
            array('export_text' => 'Sales',
                'style'=>'width: 15%'),
            array('export_text' => '',
                'style'=>'width: 1%'),

        );
        if(count($data->results)!=0) {
            foreach ($data->results['rows'] as $branch_id => $branch_info) {
                $content[] = array(
                    $this->to_export_text($branch_info['detail']->name,1,10)
                );
                foreach ($branch_info['rows'] as $customer_id => $customer_info) {
                    if(!empty($customer_info) && !empty($customer_info['detail'])){
                        $content[] = array($this->to_export_text(to_full_name($customer_info['detail']->first_name, $customer_info['detail']->last_name)
                            . ' - ' . $customer_info['detail']->code,2,10));
                        foreach ($customer_info['rows'] as $bill_item_id => $bill_item_info) {
                            if(isset($bill_item_info->credit_used)){
                                $rows_span = count($bill_item_info->credit_used);
                            }else{
                                $rows_span = 1;
                            }
                            $content_row = array(
                                $this->to_export_text($bill_item_info->created_date,'','',$rows_span),
                                $this->to_export_text($bill_item_info->item_code,'','',$rows_span),
                                $this->to_export_text($bill_item_info->item_name,'','',$rows_span),
                                $this->to_export_text($bill_item_info->sale_value,'','',$rows_span,'currency'),
                            );
                            if(isset($bill_item_info->credit_used)){
                                $first = false;
                                foreach($bill_item_info->credit_used as $each_credit_used){
                                    if(!$first) {
                                        $content_row[] = $this->to_export_text('');
                                        $first = true;
                                        $content[] = $content_row;
                                    }
                                    else{
                                        $after_content = array();
                                        $after_content[] = $this->to_export_text('');
                                        $content[] = $after_content;
                                    }
                                }
                            }else if($bill_item_info->credit_add){
                                $content_row[] = $this->to_export_text('');
                                $content[] = $content_row;
                            }else{
                                $content_row[] = $this->to_export_text('','',1);
                                $content[] = $content_row;
                            }
                            //$content[] = $content_row;
                        }
                    }
                }
            }
        }
        return $content;
    }
}