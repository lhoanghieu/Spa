<?php
class Stock_Balance_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $category_id = isset($data['category_id'])?$data['category_id']:null;
        $start_date = $data['start_date'];
        $item_id    = $data['item_id'];
        $sortby     = isset($data['sortby'])?$data['sortby']:'date';
        $branch_id  = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        $condition = array();
        $item_condition = array();
        if($item_id != 0){
            $condition['item_id'] = $item_id;
            $item_condition['id'] = $item_id;
            $item_condition['status'] = array(1,2,0);
        }

        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse(array(),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
        $ret_data = array();
        if($warehouse_id){
            $condition['warehouse_id']  = $warehouse_id;
            $condition[]                = "log_time < '$start_date'";
            $log_data = $this->load->table_model('warehouse_item_log')->get($condition,'warehouse_item_log.key desc');
            foreach($log_data as $row){
                if(!isset($ret_data[$row->id])){
                    $ret_data[$row->id] = $row;
                }else{
                    continue;
                }
            }
        }
        $items = $this->load->table_model('item')->getTableMap('id','',$item_condition,true,Permission_Value::ADMIN);
        $ret_data_2 = array();
        if(count($ret_data)){
            foreach($ret_data as $row_id => $row_values){
                if(!isset($items[$row_values->item_id])){
                    continue;
                }
                $item = $items[$row_values->item_id];
                if(isset($ret_data_2[$item->id])){
                    $ret_data_2[$item->id]->quantity += $row_values->quantity;
                }else{
                    $ret_data[$row_id]->item_name = $item->name;
                    $ret_data[$row_id]->item_code = $item->code;
                    $ret_data[$row_id]->log_time = get_user_date($row_values->log_time,'','',true);
                    $ret_data_2[$item->id] = $ret_data[$row_id];
                }
            }
        }else if($item_id){
            $item = $items[$item_id];
            $ret_data_2[$item_id] = (object)array(
                'item_name' => $item->name,
                'item_code' => $item->code,
                'quantity'  => 0,
                'log_time'  => 'There is no transaction for this item'
            );
        }

        $item_product_list = $this->load->table_model('item_product')->getTableMap('item_id','',array());

        foreach($ret_data_2 as $item){
            if(isset($item_product_list[$item->item_id]->master_quantity)) {
                $item->master_quantity = $item_product_list[$item->item_id]->master_quantity;
            }
            else{
                $item->master_quantity = 'ERROR';
            }
        }

        $category_condition = array();
        if(isset($category_id) && $category_id){
            $category_condition['category.id'] = $category_id;
        }
        $model = $this->load->table_model('category');
        $list_category = $model->select(array(
            'select'    =>  array(
                'category'    => array('category_name' => 'name','id' => 'id'),
                'category_item' => array('item_id')
            ),
            'from'      => array(
                'category_item' => array('table' => 'category_item'),
                'category' => array('table' => 'category','condition' => 'category_item.category_id = category.id', 'type' => 'LEFT')
            ),
            'where'     => $category_condition
        ))->result();

        foreach($ret_data_2 as $item) {
            foreach($list_category as $cat_item) {
                $check = 0;
                if ($item->item_id == $cat_item->item_id) {
                    $item->category = $cat_item->id;
                    $item->category_name = $cat_item->category_name;
                    $check = 1;
                    break;
                }
            }
            if($check == 0){
                unset($ret_data_2[$item->item_id]);
            }
        }

        if($sortby == "name")
            usort($ret_data_2, array($this, "cmp_obj_name"));
        elseif($sortby == "code")
            usort($ret_data_2, array($this, "cmp_obj_code"));

        return array(
            'stock_items' => $ret_data_2
        );
    }

    function get_original_data($data,$type = false){
        $stock_items = $data['stock_items'];
        $content = array();
        $content[] = array(
            array('export_text' => 'Item Code'),
            array('export_text' => 'Item Name'),
            array('export_text' => 'Master Quantity'),
            array('export_text' =>'Quantity'),
            array('export_text' => 'Last Transaction')
        );

        $category = array();
        foreach($stock_items as $item){
            if(isset($item->category) && !in_array($item->category_name,$category)){
                $category[$item->category] = $item->category_name;
            }
        }
        if(count($stock_items)){
            $quantity_grandtotal = 0;
            $master_quantity_grandtotal = 0;
            foreach($category as $category_key => $item_category) {
                $content[] = array($this->to_export_text($item_category,1,5));
                $quantity_total = 0;
                $master_quantity_total = 0;
                foreach ($stock_items as $warehouse_item_id => $warehouse_item_fields) {
                    if($category_key == $warehouse_item_fields->category){
                        $content[] = array(
                            array('export_text' => $warehouse_item_fields->item_code),
                            array('export_text' => $warehouse_item_fields->item_name),
                            array('export_text' => $warehouse_item_fields->master_quantity),
                            array('export_text' => $warehouse_item_fields->quantity),
                            array('export_text' => $warehouse_item_fields->log_time)
                        );
                        $quantity_total += $warehouse_item_fields->quantity;
                        $master_quantity_total += $warehouse_item_fields->master_quantity;
                    }
                }
                $quantity_grandtotal += $quantity_total;
                $master_quantity_grandtotal += $master_quantity_total;

                $content[] = array(
                    $this->to_export_text('Total',3,2,1),
                    $this->to_export_text($master_quantity_total,3,1,1),
                    $this->to_export_text($quantity_total,3,2,1),
                    $this->to_export_text('')
                );
            }

            $content[] = array(
                $this->to_export_text('Grand Total',0,2,1),
                $this->to_export_text($master_quantity_grandtotal,0,1,1),
                $this->to_export_text($quantity_grandtotal,0,2,1),
                $this->to_export_text('')
            );
        }else{
            $content[] = array( array('export_text' => 'There is no record',
                'colspan'=>'10'),
                );
        }
        return $content;
    }

    function cmp_obj_name($a, $b){
        return strcmp($a->item_name, $b->item_name);
    }

    function cmp_obj_code($a, $b){
        return strcmp($a->item_code, $b->item_code);
    }
}