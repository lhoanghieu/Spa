<?php

class Admin_Customers_Controller_Model extends Admin_Controller_model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "customer";
        $this->suggestionSearch = array('code', 'first_name', 'last_name','nric','mobile_number','email');
        $this->suggestionDisplay = array('code', 'first_name', 'last_name', 'mobile_number');
    }

    function renderDataSet($data){
        $data = $this->load->controller_model('customers','staff')->renderDataSet($data);
        return $data;
    }
    function getCustomerPin($id){
        return $this->load->controller_model('customers','staff')->getCustomerPin($id);
    }
    function setCustomerPin($id, $pin, $opin){
        return $this->load->controller_model('customers','staff')->setCustomerPin($id,$pin,$opin);
    }
    function getDataForQuestionForm($id = 0){
        return $this->load->controller_model('customers','staff')->getDataForQuestionForm($id);
    }
    function editSurvey($id, $answers){
        return $this->load->controller_model('customers','staff')->editSurvey($id,$answers);
    }
    function insert_extend($values){
        return $this->load->controller_model('customers','staff')->insert($values);
    }
    function update_extend($id,$values){
        return $this->load->controller_model('customers','staff')->update($id,$values);
    }
    function getCustomerCreditHistory($id){
        return $this->load->controller_model('customers','staff')->getCustomerCreditHistory($id);
    }

    function get_credit_adjustment_data($customer_id){
        $customer_credits = $this->load->table_model('credit')->get(array('customer_id' => $customer_id));
        $items = $this->load->table_model('item')->getTableMap('id', '', array('id' => convert_to_array($customer_credits,'','item_id')));
        foreach($customer_credits as $item_id=>$credit_field){
            foreach($items as $key => $item){
                if($credit_field->item_id == $item->id){
                    $customer_credits[$item_id]->item_detail = $item;
                    break;
                }
            }
        }

        return array('customer_credits' => $customer_credits,'customer_id' => $customer_id);
    }

    function adjust_credit($credit_data){
        $user_id    = $this->user_check->get('user_id');
        $log_time   = get_database_date(null,null,true);

        $credit_model                       = $this->load->table_model('credit');
        $credit_adjustment_log_model        = $this->load->table_model('credit_adjustment_log');

        $this->db->trans_start();
        $customer_credits = $credit_model->getTableMap('id','',array('id' => array_keys($credit_data)));
        $processed_id = array();
        foreach($credit_data as $credit_id=>$credit_fields){
            $credit_record = $customer_credits[$credit_id];
            $credit_value = $credit_fields['credit'];
            $update_fields = array();
            if(isset($credit_fields['expiry'])){
                $update_fields['end_date'] =  get_database_date($credit_fields['expiry'] . ' 23:59:59');
            }
            if($credit_value != $credit_record->credit){
                if($credit_value < 0){
                    throw new Exception('No credit is allowed lower than zero');
                }
                $credit_change = $credit_value - $credit_record->credit;
                $update_fields['credit_original'] =  $credit_record->credit_original + $credit_change;
                $update_fields['credit'] = $credit_value;
                $credit_adjustment_log_model->insert(array(
                    'credit_id'             => $credit_id,
                    'credit_change'         => $credit_change,
                    'credit_value_before'   => $credit_record->credit,
                    'log_time'              => $log_time,
                    'creator'               => $user_id
                ));
            }
            $credit_model->update($credit_id,$update_fields);
            $processed_id[] = $credit_id;
        }
        $this->db->trans_complete();
        return true;
    }

    function get_credit_transfer_form_data($customer_id){
        $data = array();
        $customer = $this->load->table_model('customer')->getByID($customer_id);
        $credit   = $this->select(array(
            'select'    => array(
                'credit'    => array('credit'),
                'item'      => array('id','name')
            ),
            'from'      => array(
                'credit'    => array('table' => 'credit'),
                'item'      => array('table' => 'item','condition' => 'credit.item_id = item.id')
            ),
            'where'     => array(
                'credit.customer_id' => $customer_id
            )
        ))->result();

        $data['customer']           = $customer;
        $data['customer_credits']   = convert_to_array($credit,'id');

        $total_package = $this->load->table_model('item')->get(array('type' => ITEM_TYPE('Package')));

        $data['total_package'] = $total_package;
        return $data;
    }

    function transfer_credit($credit_data){
        if(!$credit_data['to_package_id'] || !$credit_data['from_package_id']) throw new Exception('Please choose a source and destination package');
        $this->db->trans_start();
        $credit_model   = $this->load->table_model('credit');
        $to_credit      = $credit_model->get(array('customer_id' => $credit_data['customer_id'], 'item_id' => $credit_data['to_package_id']));
        $from_credit    = $credit_model->get(array('customer_id' => $credit_data['customer_id'], 'item_id' => $credit_data['from_package_id']));
        if(! count($from_credit)) throw new Exception('Cant find the source credit.');
        $from_credit = $from_credit[0];
        if( $from_credit->item_id == $credit_data['to_package_id']) throw new Exception('The package you want to transfer to is the the same as source package');
        $change_credit_value = $credit_data['to_credit_value'];

        $credit_log_data = array(
            'from_credit' => array(
                'credit_id'            => $from_credit->id,
                'credit_value_before'  => $from_credit->credit,
                'credit_change'        => -$from_credit->credit,
            ),
        );
        $credit_model->update(
            $from_credit->id,
            array(
                'credit'            => 0
            )
        );
        if(count($to_credit)){
            /* ----- Case 1 : Having the package ------ */
            $to_credit = $to_credit[0];
            $will_update_field = array(
                'end_date'          => $from_credit->end_date,
                'credit'            => $change_credit_value + $to_credit->credit
            );
            $credit_log_data['to_credit'] = array(
                'credit_id'                    => $to_credit->id,
                'credit_value_before'          => $to_credit->credit,
                'credit_change'                => $change_credit_value,
                'credit_id_transfer'           => $from_credit->id
            );
            $credit_model->update($to_credit->id,$will_update_field);
            $to_credit_id = $to_credit->id;
        }else{
            /* ----- Case 2 : Not Having the package ------ */
            $will_insert_field = array(
                'start_date'        => $from_credit->start_date,
                'end_date'          => $from_credit->end_date,
                'item_id'           => $credit_data['to_package_id'],
                'customer_id'       => $credit_data['customer_id'],
                'credit'            => $change_credit_value,
                'credit_original'   => $change_credit_value
            );
            $to_credit_id = $credit_model->insert($will_insert_field);
            $credit_log_data['to_credit'] = array(
                'credit_id'                    => $to_credit_id,
                'credit_value_before'          => 0,
                'credit_change'                => $change_credit_value,
                'credit_id_transfer'           => $from_credit->id
            );
        }
        /*------- Insert Credit Branch Group ---------*/
        $branch_group_id = $credit_model->get_branch_group($from_credit->id);
        $branch_group_id = $branch_group_id[0];
        $check = $this->select(array(
            'select' => array('credit_branch_group' => 'branch_group_id'),
            'from'   => array('credit_branch_group' => array('table' => 'credit_branch_group')),
            'where'  => array(
                'credit_id' => $to_credit_id
            )
        ))->result();

        if(count($check) > 0){
            $this->db->update('credit_branch_group', array('branch_group_id' => $branch_group_id), array('credit_id' => $to_credit_id));
        }
        else{
            $this->db->insert('credit_branch_group',array('credit_id' => $to_credit_id,'branch_group_id' => $branch_group_id));
        }

        /*--------------------------------------------*/
        $log_time = get_database_date();
        $credit_log_data['from_credit']['credit_id_transfer'] = $to_credit_id;
        $credit_log_data['from_credit']['log_time']         = $log_time;
        $credit_log_data['to_credit']['log_time']           = $log_time;
        $this->load->table_model('credit_adjustment_log_model')->write_transfer_log($credit_log_data);
        $this->db->trans_complete();
        return true;
    }

    function validationCustomer($validFields){
        return $this->load->table_model('customer')->validationCustomer($validFields);
    }
}