<?php

class Admin_Room_Type_Controller_Model extends Admin_Controller_model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "room_type";
        $this->suggestionSearch = array('code', 'name');
        $this->suggestionDisplay = array('code', 'name');
    }

    function renderDataSet($data){
        return $this->load->controller_model($this->main_table,'staff')->renderDataSet($data);
    }

    function insert_extend($values){
        return $this->load->controller_model($this->main_table,'staff')->insert($values);
    }

    function update_extend($id,$values){
        $getChildCat = $this->load->controller_model('categories')->getChildCategory($values->category_id_list);

        if(!empty($getChildCat)){
            foreach($getChildCat as $item){
                $values->category_id_list[] = $item->id;
            }
        }

        return $this->load->controller_model($this->main_table,'staff')->update($id,$values);
    }

}