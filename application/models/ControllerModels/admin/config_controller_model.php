<?php
class Config_Controller_Model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "config";
    }


    function update($values, $branch_values = null, $company_values = null){
        $this->db->trans_start();
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields);
        }
        if($company_values){
            foreach($company_values as $company_info_id=>$company_info_value){
                $fields = new stdClass();
                $fields->value = $company_info_value;
                $this->load->table_model('information')->update($company_info_id, $fields);
            }
        }
        $this->db->trans_complete();
    }

    function getDataForView($user_level = 1){
        $model = $this->load->table_model('config');
        $data = $model->get(array());

        $sms = array();
        foreach($data as $key => $item){
            if($item->order >= 1000){
                $sms[] = $item;
                unset($data[$key]);
            }
        }

        if($user_level > 2){
            $company_data = $this->load->table_model('information')->get();
            $ret = array('data' => $data, 'company_data' => $company_data, 'sms' => $sms);
        }else{
            $branch_data = $this->load->table_model('branch')->getByID($this->user_check->get('branch_id'));
            $ret = array('data' => $data, 'branch_data' => $branch_data, 'sms' => $sms);
        }

        return (object)$ret;
    }


    function get_config_inherit_value($id, $permission_fields = array()){
        $code = $this->load->table_model('config')->get(array('id'=>$id),array('user_level' => Permission_Value::ADMIN));
        if(! count($code)){
            return null;
        }
        $code = $code[0]->code;
        if(!isset($permission_fields['user_level'])){
            $user_level = $this->user_check->get('login_as_type');
            if(! $user_level || $user_level >= Permission_Value::ADMIN){
                $user_level = Permission_Value::ADMIN - 1;
            }
            $user_level += 1;
            $permission_fields['user_level'] = $user_level;
        }

        return $this->system_config->get($code,$permission_fields);
    }

}