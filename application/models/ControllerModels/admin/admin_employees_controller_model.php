<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Admin_Employees_Controller_Model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "employee";
        $this->suggestionSearch = array('first_name', 'last_name', 'phone_number', 'email');
        $this->suggestionDisplay = array('first_name', 'last_name', 'phone_number', 'email');
        $this->minimalSuggestionDisplay = array('first_name', 'last_name');
    }

    function renderDataSet($data){
        $data = $this->load->controller_model('employees','staff')->renderDataSet($data);
        return $data;
    }

    function insert_extend($values){
        return $this->load->controller_model('employees','staff')->insert($values);
    }
    function update_extend($id,$values){
        return $this->load->controller_model('employees','staff')->update($id,$values);
    }

    function getDataForViewForm($id){
        $data = parent::getDataForViewForm($id);
        $data->departments = $this->load->table_model('department')->get();
        $data->commissions = $this->load->table_model('commission')->getTableMap('id', 'name');
        return $data;
    }

    function get_assigned_service_form_data($id){
        return $this->load->controller_model('employees','staff')->get_assigned_service_form_data($id);
    }

    function assign_service($employee_id,$service_id_list){
        return $this->load->controller_model('employees','staff')->assign_service($employee_id,$service_id_list);
    }


}