<?php

class Admin_Categories_Controller_model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "category";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name', 'description');
        $this->entity_status = array(Status::Active,Status::Disable);
    }

    function renderDataSet($data){
        return $this->load->controller_model('categories','staff')->renderDataSet($data);
    }

    function update_extend($id, $values,$constraint_check = true){
        return $this->load->controller_model('categories','staff')->update($id,$values,$constraint_check);
    }

    function insert_extend($values,$user_level = '', $permission = array(),$force_insert = false){
        $id = $this->load->controller_model('categories','staff')->insert($values,$user_level,$permission,$force_insert);
        if(!is_numeric($id)){
            $id = $this->db->query('SELECT MAX(id) as id FROM category')->row_array();
            $id = $id['id'];
        }
        return $id;
    }

    function getChildCategory($categories){
        if(!empty($categories)){
            return $this->select(array(
                'select' => array(
                    'category' => array('id')
                ),
                'from' => array(
                    'category' => array('table' => 'category')
                ),
                'where' => array(
                    'category.parent_id' => $categories
                )
            ))->result();
        }
        else{
            return array();
        }
    }
}