<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Admin_Rooms_Controller_model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "room";
        $this->suggestionSearch = array('name','code');
        $this->suggestionDisplay = array('name');
    }
    function getItem($id){
        return $this->load->table_model('room')->getByID($id);
    }
}