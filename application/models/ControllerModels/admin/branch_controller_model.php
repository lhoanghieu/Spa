<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Branch_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "branch";
        $this->suggestionSearch = array('name', 'address', 'phone_number', 'email');
        $this->suggestionDisplay = array('name', 'address', 'phone_number', 'email');
    }

    function update($id, $fields){
        $old_sale_id = $this->load->table_model('branch')->getByID($id);
        $old_sale_id = $old_sale_id->sale_id_prefix;

        $branch_data = $fields['branch_data'];
        $values = $fields['value'];
        $this->db->trans_start();
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields, array('user_level' => 2,'branch_id' => $id));
        }
        if($branch_data){
            $this->load->table_model('branch')->update($id,$branch_data);
            if($old_sale_id != $branch_data['sale_id_prefix']){
                $bill_model = $this->load->table_model('bill');
                $bills = $bill_model->get(array("code like '$old_sale_id%'"));
                foreach($bills as $bill){
                    $new_code = str_replace($old_sale_id,$branch_data['sale_id_prefix'],$bill->code);
                    $check = $bill_model->get(array("code" => $new_code));
                    if(count($check)){
                        throw new Exception('Can not change prefix because of duplicate bill code');
                    }
                    $bill_model->update($bill->id,array('code' => $new_code));
                }
            }

            if($this->user_permission->check_level(Permission_Value::BRANCH_GROUP)){
                if(isset($branch_data['employees'])){
                    foreach($branch_data['employees'] as $employee_id){
                        $is_exists = $this->load->table_model('employee_branch')->get(array('employee_id' => $employee_id, 'branch_id' => $id));
                        if(! count($is_exists)){
                            $this->load->table_model('employee_branch')->insert(array('employee_id' => $employee_id, 'branch_id' => $id));
                        }
                    }
                }
            }
        }
        $this->db->trans_complete();
        return true;
    }

    function insert($fields){
        $branch_data = $fields['branch_data'];
        $values = $fields['value'];
        $this->db->trans_start();
        $id = $this->load->table_model('branch')->insert($branch_data);
        if($this->user_permission->check_level(Permission_Value::BRANCH_GROUP)){
            if(isset($branch_data['employees'])){
                foreach($branch_data['employees'] as $employee_id){
                    $is_exists = $this->load->table_model('employee_branch')->get(array('employee_id' => $employee_id, 'branch_id' => $id));
                    if(! count($is_exists)){
                        $this->load->table_model('employee_branch')->insert(array('employee_id' => $employee_id, 'branch_id' => $id));
                    }
                }
            }
        }
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields, array('user_level' => 2,'branch_id' => $id));
        }
        $this->db->trans_complete();
        return $id;
    }

    function renderDataSet($data){
        $branch_group_ids = convert_to_array($data,'','branch_group_id');
        $branch_groups = $this->load->table_model('branch_group')->getTableMap('id','',array('id' => $branch_group_ids));
        $temp = array();
        foreach($data as $row){
            if(! in_array($row->branch_group_id,$this->user_check->get('branch_group_id'))){
                continue;
            }

            $row->branch_group_name = $branch_groups[$row->branch_group_id]->name;
            $temp[] = $row;
        }

        return $temp;
    }

    function getDataForViewFormExtend($data){
        if(! isset($data->item) && $data->item != ''){
            return array('branch_config' => $this->load->table_model('config')->get(array(),array('user_level' => 3)));
        }
        $item = $data->item;

        if($this->user_permission->check_level(Permission_Value::BRANCH_GROUP)){
            $item->employees = $this->select(array(
                'select' => array('employee' => '*'),
                'from'   => array(
                    'employee' => array('table' => 'employee'),
                    'employee_branch' => array('table' => 'employee_branch', 'condition' => 'employee_branch.employee_id = employee.id'),
                ),
                'where'  => array('employee.type = 2', 'employee_branch.branch_id' => $item->id),
                'user_level' => Permission_Value::ADMIN
            ))->result();
            $item->employees = convert_to_array($item->employees,'','id');
        }
        $ret = array(
            'branch_data'     => $item,
            'branch_config'   => $this->load->table_model('config')->get(array(),array('user_level' => 2, 'branch_id' => $item->id, 'branch_group_id' => $item->branch_group_id)),
        );
        return $ret;
    }

    function get_suitable_employee(){
        $employees = $this->load->table_model('employee')->get(array('type' => 2));
        $ret = array();
        foreach($employees as $employee){
            $ret[] = array('value' => $employee->id, 'label' => to_full_name($employee->first_name,$employee->last_name));
        }
        return $ret;
    }

    function get_permission_data($branch_id){
        $branch_data = $this->load->table_model('branch')->getByID($branch_id);
        if($branch_data){
            $branch_group = $this->load->table_model('branch_group')->getByID($branch_data->branch_group_id);
        }

        $available_permission = $this->select(array(
            'select' => array(
                'module_permission' => array('id','permission_code','global'),
                'module'     => array('module_name' => 'name'),
                'permission' => array('name')),
            'from'   => array(
                'module'                    => array('table' => 'module'),
                'module_permission'         => array('table' => 'module_permission', 'condition' => 'module.id = module_permission.module_id'),
                'permission'                => array('table' => 'permission','condition' => 'module_permission.permission_code = permission.code')
            ),
            'user_level' => Permission_Value::BRANCH_GROUP,
            'permission' => array(
                'branch_group_id' => $branch_group->id,
            )
        ))->result();
        $available_permission = convert_to_array($available_permission,'module_name','',false);

        $branch_permission = $this->select(array(
            'select' => array('module_permission' => array('id')),
            'from'   => array(
                'module_permission'                 => array('table' => 'module_permission'),
            ),
            'permission' => array(
                'branch_id' => $branch_id,
            ),
            'user_level' => Permission_Value::BRANCH
        ))->result();
        $branch_permission = convert_to_array($branch_permission,'','id');

        return array('available_permission' => $available_permission, 'branch_permission' => $branch_permission, 'branch_data' => $branch_data);
    }

    function edit_permissions($branch_id,$permissions,$remove_permissions){
        $model =  $this->load->table_model('module_permission');
        $this->db->trans_start();
        if(isset($remove_permissions) && $remove_permissions !== false && count($remove_permissions) > 0){
            $ret = $this->load->table_model('module_permission_branch')->delete(array('module_permission_id' => $remove_permissions, 'branch_id' => $branch_id));
        }
        if(isset($permissions) && $permissions !== false && count($permissions) > 0){
            foreach($permissions as $permission){
                $ret = $model->insert(array('id' => $permission),Permission_Value::BRANCH,array('branch_id' => $branch_id));
            }
        }
        $this->db->trans_complete();
        return true;
    }


}