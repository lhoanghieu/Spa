<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Lead_Time_Min_Controller_model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "lead_time_min";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

}