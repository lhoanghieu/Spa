<?php
class Config_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "config";
    }


    function update($values, $branch_values = null, $company_values = null){
        $old_sale_id = $this->system_config->get('sale_id_prefix');
        $this->db->trans_start();
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields);
        }
        if($branch_values){
            $this->load->table_model('branch')->update($this->user_check->get('branch_id'),$branch_values);
            if($old_sale_id != $branch_values['sale_id_prefix']){
                $bill_model = $this->load->table_model('bill');
                $bills = $bill_model->get(array("code like '$old_sale_id%'"));
                foreach($bills as $bill){
                    $new_code = str_replace($old_sale_id,$branch_values['sale_id_prefix'],$bill->code);
                    $check = $bill_model->get(array("code" => $new_code));
                    if(count($check)){
                        throw new Exception('Can not change prefix because of duplicate bill code');
                    }
                    $bill_model->update($bill->id,array('code' => $new_code));
                }
            }
        }
        $this->db->trans_complete();
    }

    function getDataForView($user_level = 1){
        $model = $this->load->table_model('config');
        $data = $model->get(array());

        $sms = array();
        foreach($data as $key => $item){
            if($item->order >= 1000){
                $sms[] = $item;
                unset($data[$key]);
            }
        }

        if($user_level > 2){
            $company_data = $this->load->table_model('information')->get();
            $ret = array('data' => $data, 'company_data' => $company_data, 'sms' => $sms);
        }else{
            $branch_data = $this->load->table_model('branch')->getByID($this->user_check->get('branch_id'));
            $ret = array('data' => $data, 'branch_data' => $branch_data, 'sms' => $sms);
        }

        return (object)$ret;
    }


    function get_config_inherit_value($id, $permission_fields = array()){
        $code = $this->load->table_model('config')->get(array('id'=>$id),array('user_level' => Permission_Value::ADMIN));
        if(! count($code)){
            return null;
        }
        $code = $code[0]->code;
        if(!isset($permission_fields['user_level'])){
            $user_level = $this->user_check->get('login_as_type');
            if(! $user_level || $user_level >= Permission_Value::ADMIN){
                $user_level = Permission_Value::ADMIN - 1;
            }
            $user_level += 1;
            $permission_fields['user_level'] = $user_level;
        }

        return $this->system_config->get($code,$permission_fields);
    }

    function get_branch_offline_data(){
        $rows = $this->load->table_model('branch_offline')->get(array(),'start_time');
        return $rows;
    }

    function change_offline($id,$data){
        $date = $data->date;
        if(!$data->offline_whole_day){
            $date_start = $date.' '.$data->offline_from;
            $date_end   = $date.' '.$data->offline_to;
        }else{
            $date_start = $date.' 00:00:00';
            $date_end   = $date.' 23:59:59';
        }
        $st = get_database_date($date_start);
        $et = get_database_date($date_end);
        $branch_offline_model = $this->load->table_model('branch_offline');
        $branch_offline = $branch_offline_model->get(array('branch_id' => $id,
                "((start_time < '{$st}' AND end_time > '{$st}')
                OR (start_time > '{$st}' AND start_time < '{$et}' AND end_time > '{$st}'))")
        );

        $this->db->trans_begin();
        $branch_offline_model->delete(array('id' => convert_to_array($branch_offline,'','id'),'branch_id' => $id));

        $result = $branch_offline_model->insert(array(
            'branch_id'     => $id,
            'start_time'    => $st,
            'end_time'      => $et,
        ));

        $this->db->trans_complete();

        $booked_date = $this->load->table_model('booking_service')->select(
            array(
                'select' => array('booking' => array('customer_id')),
                'from'  => array(
                    'booking' => array('table' => 'booking'),
                    'booking_service' => array('table' => 'booking_service', 'condition' => 'booking_service.booking_id = booking.id')
                ),
                'where' => array('booking.status' => '1',
            "((booking_service.start_time <= '{$date_start}' AND booking_service.end_time >= '{$date_end}')
            OR (booking_service.start_time <= '{$date_start}' AND booking_service.end_time >= '{$date_end}') OR (booking_service.start_time >= '{$date_start}' AND booking_service.end_time <= '{$date_end}'))"))
        )->result();

        if(count($booked_date)){
            $customers = $this->load->table_model('customer')->get(array('id'=>convert_to_array($booked_date,'','customer_id')));
            $customers = convert_to_array($customers,'id');
            $result = "This branch have some appointment on this time. Please report to the following customer: <br>";
            foreach($booked_date as $row){
                $customer = $customers[$row->customer_id];
                $result .= to_full_name($customer->first_name,$customer->last_name)." {$customer->mobile_number} on {$row->start_time} <br>";
            }
        }
        return $result;
    }

    function delete_offline($id){
        return $this->load->table_model('branch_offline')->delete(array('id'=>$id));
    }

}