<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 * Them cot category_id
 */

class Sales_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "bill";
    }

    function getCategories(){
        $row =$this->load->table_model('category');
        $data = $row->select(array(
            'select' => array('category' => array('name','id')),
            'from' => array(
                'category'      => array('table'=>'category'),
                'category_item' => array('table' => 'category_item', 'condition' => 'category_item.category_id = category.id'),
                'item'          => array('table' => 'item', 'condition' => 'item.id = category_item.item_id')
            ),
            'where' => array(
                'parent_id' => array(0)
            )
        ))->result();

        $data = convert_to_array($data,'id');
        $return_data = array();;
        foreach($data as $row){
            $object = new stdClass();
            $object->category_name = $row->name;
            $object->category_id = $row->id;

            $return_data[] = $object;
        }
        return $return_data;
    }

    private function calculate_promotion_time($customer_id, $item_id){
        $promotion_used_list = $this->load->table_model('item')->select(array(
            'select' => array('bill' => array('id','created_date'),'bill_item' => array('item_id','quantity')),
            'from'   => array(
                'bill'          => array('table' => 'bill'),
                'bill_item'     => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
            ),
            'where'  => array('bill.customer_id' => $customer_id, 'bill_item.item_id' => $item_id, 'bill.status' => BILL_STATUS(array('Complete','Invisible'))),
            'user_level' => Permission_Value::ADMIN,
         //   'group'      => 'bill.created_date desc'
        ))->result();
        $result = array();
        foreach($promotion_used_list as $row){
            if(!isset($result[$row->item_id])) $result[$row->item_id] = 0;
            $result[$row->item_id] += $row->quantity;
        }
        return $result;
    }

    function getChildCategories($categoryId){
        return $this->select(array(
            'select' => array(
                'category' => array('category_id' => 'id', 'category_name' => 'name')
            ),
            'from' => array(
                'category' => array('table' => 'category')
            ),
            'where' => array(
                'parent_id' => $categoryId
            )
        ))->result();
    }

    function getItems($category_id){
        $items_model = $this->load->table_model('item');

        $customers_items = array();
        $sale = $this->session->userdata('sale');

        $condition = array(
            'category_item.category_id' => $category_id,
            'item.status' => Status::Active
        );
        $customer = null;
        $items = $items_model->select(
            array(
                'select'    =>  array(
                    'item'  =>  array('id','name','type','promotion'),
                ),
                'from'      => array(
                    'item'            => array('table'    => 'item'),
                    'category_item'     => array('table'    => 'category_item', 'condition' => 'item.id = category_item.item_id')
                ),
                'where'     => $condition
            )
        )->result();

        $item_ids = convert_to_array($items, '' ,'id');
        if(isset($sale) && isset($sale->customer_id) && $sale->customer_id){
            $customers_model = $this->load->table_model('customer');
            $customer = $customers_model->get(array('id' => $sale->customer_id));
            $customer = count($customer)?$customer[0]:null;
            $customers_items = $customers_model->getPackagesItems($sale->customer_id);

            $promotion_used_list = $this->calculate_promotion_time($sale->customer_id,$item_ids);
        }
        $warehouse_item = $this->load->table_model('warehouse_item')->get_warehouse_quantity(null,$item_ids,false);
        $return_data = array();
        foreach($items as $row){
            $object = new stdClass();

            if(in_array($row->id, $customers_items)){
                $object->item_name = $row->name . ' (Credit Available)';
                $object->credit_avail = 1;
            }
            else{
                $object->item_name = $row->name;
                $object->credit_avail = 0;
            }

            if(((!isset($customer) && $row->promotion > 0)
                || ((!isset($customer) || $customer->customer_type != CUSTOMER_TYPE('Member'))) && $row->type == ITEM_TYPE('Package'))){
                $object->item_name = $row->name.' (Not Available)';
                $object->credit_avail = -1;
            }

            if(isset($customer) && $customer->nric){
                if($row->promotion && isset($promotion_used_list[$row->id]) && $promotion_used_list[$row->id] >= $row->promotion){
                    $object->item_name = $row->name.'<br>(Promotion limit exceed)';
                    $object->credit_avail = -1;
                }
            }
            if($row->type == ITEM_TYPE('Product') && (!isset($warehouse_item[$row->id]) || $warehouse_item[$row->id]->quantity <= 0)){
                $object->item_name = $row->name.' (Out of stock) ';
                $object->credit_avail = -1;
            }
            $object->item_id = $row->id;
            $return_data[] = $object;
        }
        return $return_data;
    }

    function insertItemToCart($item_id){
        $items_model = $this->load->table_model('item');

        /* ---- Check item avail ----*/
        $sale = $this->session->userdata('sale');
        $preBillData = $this->session->userdata('preBillData');
        $customer = null;
        $customers_model = $this->load->table_model('customer');
        if(isset($sale) && isset($sale->customer_id) && $sale->customer_id){
            $customer = $customers_model->get(array('id' => $sale->customer_id));
            $customer = count($customer)?$customer[0]:null;

            $promotion_used_list = $this->calculate_promotion_time($sale->customer_id,$item_id);
        }
        else if(isset($preBillData) && isset($preBillData['customer_id']) && $preBillData['customer_id']){
            $customer = $customers_model->get(array('id' => $preBillData['customer_id']));
            $customer = count($customer)?$customer[0]:null;
            $promotion_used_list = $this->calculate_promotion_time($preBillData['customer_id'],$item_id);
        }
        $row = $items_model->getByID($item_id);
        $warehouse_item = $this->load->table_model('warehouse_item')->get_warehouse_quantity(null,$item_id,false);
        if(((!isset($customer) && $row->promotion == 1)
            || ((!isset($customer) || $customer->customer_type != CUSTOMER_TYPE('Member'))) && $row->type == ITEM_TYPE('Package'))){
            return "This item is not available for this customer";
        }
        if(isset($customer) && $customer->nric){
            if($row->promotion && isset($promotion_used_list[$row->id]) && $promotion_used_list[$row->id] >= $row->promotion){
                return 'Promotion limit exceed';
            }
        }
        if($row->type == ITEM_TYPE('Product') && (!isset($warehouse_item) || $warehouse_item->quantity <= 0)){
                return 'This item was out of stock';
        }
        /* ---- End of check item avail -----*/

        /*----- Get Credits which avail for the item ----*/
        $credit_avail = array();
        $sale = $this->session->userdata('sale');
        $credit_chosen = 0;
        if(isset($sale) && isset($sale->customer_id) && $sale->customer_id){
            foreach($sale->customer_credits as $key=>$credit){
                if(in_array($item_id,$credit->items)){
                    $credit_avail[$key] = $credit;
                }
            }
        }
        $item = $items_model->getByID($item_id);
        $credit_id = array();
        $credit_value = 0;
        $credit_pair = array();
        /*-----------------------------------------------*/

        /*---- Process if it's a bundle item ------------*/
        $sub_items = array();
        if($item->type == ITEM_TYPE('Bundle')){
            $sub_items = $item->bundle->sub_items;
        }

        /*-----------------------------------------------*/

        $data = array(
            'id'        => $item->id,
            'name'      => $item->name,
            'qty'       => 1,
            'price'     => $item->price,
            'options'   => array(
                'choice_staff'          => array(),
                'discount_type'         => 1,
                'discount'              => 0,
                'total'                 => $item->price * 1,
                'type'                  => $item->type,
                'packages'              => $credit_avail,
                'credit_id'             => $credit_id,
                'total_credit_value'    => $credit_value,
                'credit_pair'           => $credit_pair,
                'sub_items'             => $sub_items,
                'no_fullcash'           => $item->no_fullcash
            )
        );

        if(isset($item->product))
        {
            $data['unit_cost'] = $item->product->unit_cost;
        }
        else if(isset($item->services)){
            $data['unit_cost'] = 0;
        }

        $this->cart->insert($data);

        if(isset($sale) && isset($sale->customer_credits)){
            foreach($sale->customer_credits as $credit){
                if($credit->id == $credit_chosen)
                    $credit->credit_after = $credit->credit_after - $credit_value;
            }

            $this->session->set_userdata('sale',$sale);
        }
        return true;
    }

    function updateItemToCart($rowid, $row_values){
        $old_data = $this->cart->product_options($rowid);
        $total = $row_values->price * $row_values->quantity;
        $discount_real_value = $row_values->discount_type==1?$total * $row_values->discount_value/100:$row_values->discount_value;

        $sale = $this->session->userdata('sale');
        $credit_id = isset($row_values->credit_id)?$row_values->credit_id:0;
        $total_credit_value = 0;
        $total_after_discount = ($total-$discount_real_value);
        $credit_pair = array();
        if(isset($sale) && isset($sale->customer_id) && $sale->customer_id){
            foreach($sale->customer_credits as $credit){
                if(isset($old_data['credit_pair'][$credit->id])){
                    $credit->credit_after += $old_data['credit_pair'][$credit->id];
                }
            }
            if(isset($row_values->credit_id)){
                foreach($row_values->credit_id as $credit_id){
                    foreach($sale->customer_credits as $credit){
                        if($credit->id == $credit_id){
                            $credit_value = ($total_credit_value+$credit->credit_after)>$total_after_discount?$total_after_discount-$total_credit_value:$credit->credit_after;
                            if($credit_value == 0){
                                return 1;
                            }
                            $total_credit_value += round($credit_value,2);
                            $credit_pair[$credit->id] = round($credit_value,2);
                        }
                    }
                }
            }else{
                $total_credit_value = 0;
                $credit_pair = array();
            }

        }

        $total = $total_after_discount - $total_credit_value;
        $row_fields = $this->cart->get_row($rowid);

        $item = $this->load->table_model('item')->getByID($row_fields['id']);
        if($item->type == ITEM_TYPE('Product')){
            $total_quantity = 0;
            foreach($this->cart->contents() as $row){
                if($row_fields['id'] == $row['id'] && $row_fields['rowid'] != $row['rowid']){
                    $total_quantity += $row['qty'];
                }
            }
            $total_quantity += $row_values->quantity;
            $warehouse_item = $this->load->table_model('warehouse_item')->get_warehouse_quantity(null,$row_fields['id']);
            $row_values->quantity = $warehouse_item->quantity < $total_quantity?$warehouse_item->quantity - ($total_quantity - $row_values->quantity):$row_values->quantity;
        }
        $choice_staff = array();
        if($old_data['type'] == ITEM_TYPE('Bundle')){
            foreach($row_values->choice_staff as $key=>$value){
                $choice_staff[intval($key)] = $value;
            }
        }else{
            $choice_staff = isset($row_values->choice_staff)?$row_values->choice_staff:array();
        }

        $data = array(
            'rowid'     => $rowid,
            'qty'       => $row_values->quantity,
            'price'     => $row_values->price,
            'options'   => array(
                'choice_staff'          => $choice_staff,
                'discount_type'         => $row_values->discount_type,
                'discount'              => $row_values->discount_value,
                'total'                 => $total,
                'type'                  => $old_data['type'],
                'packages'              => $old_data['packages'],
                'credit_id'             => isset($row_values->credit_id)?$row_values->credit_id:array(),
                'total_credit_value'    => $total_credit_value,
                'credit_pair'           => $credit_pair,
                'sub_items'             => $old_data['sub_items'],
                'no_fullcash'           => $item->no_fullcash
            )
        );
        $this->cart->update($data);

        $sale = $this->session->userdata('sale');
        if(isset($sale) && isset($sale->customer_credits)){
            foreach($credit_pair as $key=>$credit_used){
                if(isset($sale->customer_credits[$key])){
                    $sale->customer_credits[$key]->credit_after -= $credit_used;
                }
            }
            $this->session->set_userdata('sale',$sale);
        }
        return 1;
    }

    function customerChange(){
        foreach($this->cart->contents() as $row){
            $row_option = $this->cart->product_options($row['rowid']);
            $sale = $this->session->userdata('sale');
            $total = $row['price'] * $row['qty'];
            $discount_real_value = $row_option['discount_type']==1?$total * $row_option['discount']/100:$row_option['discount'];
            $credit_avail = array();

            $row_option['sub_items'] = isset($row_option['sub_items'])?$row_option['sub_items']:array();

            if(isset($sale->customer_id) && $sale->customer_id){
                foreach($sale->customer_credits as $key=>$credit){
                    if(in_array($row['id'],$credit->items)){
                        $credit_avail[$key] = $credit;
                    }
                }
                $sale = $this->session->userdata('sale');

                $total = $row['qty'] * $row['price'] - $discount_real_value;

                $data = array(
                    'rowid'     => $row['rowid'],
                    'qty'       => $row['qty'],
                    'price'     => $row['price'],
                    'options'   => array(
                        'choice_staff'  => $row_option['choice_staff'],
                        'discount_type' => $row_option['discount_type'],
                        'discount'      => $row_option['discount'],
                        'total'         => $total,
                        'type'          => $row_option['type'],
                        'packages'      => $credit_avail,
                        'credit_id'     => array(),
                        'credit_pair'   => array(),
                        'total_credit_value'  => 0,
                        'sub_items'     => $row_option['sub_items']
                    )
                );
                $this->session->set_userdata('sale',$sale);
            }
            else{
                $total = $row['qty'] * $row['price'] - $discount_real_value;

                $data = array(
                    'rowid'     => $row['rowid'],
                    'qty'       => $row['qty'],
                    'price'     => $row['price'],
                    'options'   => array(
                        'choice_staff'          => $row_option['choice_staff'],
                        'discount_type'         => $row_option['discount_type'],
                        'discount'              => $row_option['discount'],
                        'total'                 => $total,
                        'type'                  => $row_option['type'],
                        'packages'              => array(),
                        'credit_id'             => array(),
                        'credit_pair'           => array(),
                        'total_credit_value'    => 0,
                        'sub_items'              => $row_option['sub_items']
                    )
                );
            }
            $this->cart->update($data);
        }
        return 1;
    }

    function deleteItemFromCart($rowid){
        $row_option = $this->cart->product_options($rowid);
        $data = array(
            'rowid' =>  $rowid,
            'qty'   => 0
        );

        $this->cart->update($data);


        $sale = $this->session->userdata('sale');
        if($sale && isset($sale->customer_credits)){
            foreach($row_option['credit_pair'] as $key=>$credit_used){
                if(isset($sale->customer_credits[$key])){
                    $sale->customer_credits[$key]->credit_after += $credit_used;
                }
            }
            $this->session->set_userdata('sale',$sale);
        }
        return 1;
    }

    function getDataForCartView(){
        $employees_model = $this->load->table_model('employee');
        $data = new stdClass();
        $data->employees = $employees_model->get();
        return $data;
    }

    function getDataForRightPanel(){
        $customers_model = $this->load->table_model('customer');
        $data = new stdClass();
        $sale = $this->session->userdata('sale');
        if(isset($sale->customer_id)){
            $data->customer = $customers_model->getByID($sale->customer_id);
            $data->customer_credits = isset($sale->customer_credits)?$sale->customer_credits:array();
            $data->customer_giftcard = isset($sale->customer_giftcard)?$sale->customer_giftcard:array();
        }

        $data->cart_data    = $this->_cartData();
        $data->payment_data = $this->_paymentData($data->cart_data->total);


        foreach($data->payment_data->payments as $payment){
            if($payment->payment_type == ITEM_TYPE('Gift_Card')){
                if(isset($data->customer_giftcard[$payment->id])){
                    $check = $data->customer_giftcard[$payment->id]->giftcard_after - $payment->amount_paid;
                    if($check >= 0) {
                        $data->customer_giftcard[$payment->id]->giftcard_after = $data->customer_giftcard[$payment->id]->giftcard_after - $payment->amount_paid;
                        $data->error = '';
                    }
                    else{
                        $data->error = 'The gift card : '.$payment->id.' does not have enough value for redeem.';
                    }
                }
            }
        }

        $data->comment = isset($sale->comment)?$sale->comment:"";

        return $data;
    }

    function _cartData(){
        $config_model = $this->load->table_model('config');
        $cart_data = new stdClass();
        $sale  = $this->session->userdata('sale');
        /* ---- Prepare sales data ---- */
        $gst = $config_model->get(array('code' => 'gst'));
        $gst = count($gst)?$gst[0]->value:0;
        $cart_data->gst = $gst;
        $cart_data->currency = $this->config->item('currency');

        $cart_data->total_items = $this->cart->total_items();
        $cart_data->subtotal = 0;
        foreach ($this->cart->contents() as $items){
            $options = $this->cart->product_options($items['rowid']);
            $cart_data->subtotal += $options['total'];
        }

        $cart_data->voucher_discount = isset($sale->voucher)&&$sale->voucher>0&&$sale->voucher?$sale->voucher:0;
        $cart_data->gst_value = round($cart_data->gst/100 * ($cart_data->subtotal - $cart_data->voucher_discount),2);
        $cart_data->total = round($cart_data->subtotal + $cart_data->gst_value - $cart_data->voucher_discount,2);

        /*--- Check if there is any payment paid by cash -----*/
        $payments = isset($sale->payments)?$sale->payments: array();
        if(isset($sale->current_payment) ){
            $have_cash_payment = false;
            foreach($payments as $payment){
                if($payment->payment_type == $this->system_config->get('cash')){
                    $have_cash_payment = true;
                    break;
                }
            }
            if($have_cash_payment ||$sale->current_payment == $this->system_config->get('cash')){
                $cart_data->total = sale_total_round($cart_data->total);
            }

        }

        return $cart_data;
    }

    function _paymentData($total){
        $payment_types_model = $this->load->table_model('payment_type');

        $payment_data = new stdClass();
        $sale  = $this->session->userdata('sale');
        $payments = isset($sale->payments)?$sale->payments: array();
        $ids = array();
        foreach($payments as $payment){
            if($payment->payment_type == ITEM_TYPE('Gift_Card')){
                $gc_ids[] = $payment->id;
            }
            else {
                $ids[] = $payment->payment_type;
            }
        }
        $payments_detail = $payment_types_model->getTableMap('id','',array('id'=>$ids));

        $amount_due = $total;

        foreach($payments as $payment){
            if($payment->payment_type == ITEM_TYPE('Gift_Card') || $payment->payment_type == 6){
                $payment->name = $payment->id;
            }
            else {
                $payment->name = $payments_detail[$payment->payment_type]->name;
            }
            $amount_due = $amount_due - $payment->amount_paid;
        }

        if(isset($sale->customer_id) && !$this->load->table_model('customer')->is_guest($sale->customer_id)){
            $minimum_deposit = $this->_getMinimumDeposit();
            $payment_data->minimum_deposit  = ($minimum_deposit == '' ? false : $minimum_deposit);
        }
        else{
            if(isset($payment_data->minimum_deposit)){
                unset($payment_data->minimum_deposit);
            }
        }
        $payment_data->payment_types    = $payment_types_model->get();
        $payment_data->payments         = $payments;
        $payment_data->amount_due       = round($amount_due,2);

        return $payment_data;
    }

    function addPayment($payment){
        $sale_session = $this->session->userdata('sale');
        if(!$sale_session){
            $sale_session = new stdClass();
        }

        if(isset($payment->voucher) && $payment->voucher){
            $sale_session->voucher = $payment->voucher>0?$payment->voucher:0;

            /* ---- Prepare sales data ---- */
            $subtotal = 0;
            foreach ($this->cart->contents() as $items){
                $options = $this->cart->product_options($items['rowid']);
                $subtotal += $options['total'];
            }
            if($subtotal < $sale_session->voucher){
                $sale_session->voucher = $subtotal;
            }
        }

        if(isset($payment->promotion_code) && $payment->promotion_code){
            $sale_session->promotion_code = $payment->promotion_code;
        }

        if(isset($payment->amount_paid) && isset($payment->payment_type) && $payment->amount_paid != null && $payment->payment_type != null){
            $isDuplicate = false;
            $payments = isset($sale_session->payments)?$sale_session->payments: array();
            foreach($payments as $row){
                if($row->payment_type == $payment->payment_type){
                    $row->amount_paid += $payment->amount_paid;
                    $isDuplicate = true;
                    break;
                }
            }

            if($isDuplicate === false){
                $i = count($payments) + 1;
                $sale_session->payments[] = (object) array(
                    'id'            =>   $i,
                    'payment_type'  =>   $payment->payment_type,
                    'amount_paid'    =>   $payment->amount_paid
                );
            }
        }

        if(!empty($payment->giftcard_list_redeem) && !empty($payment->giftcard_values_redeem)){
            $isDuplicate = false;
            $payments = isset($sale_session->payments)?$sale_session->payments: array();
            foreach($payments as $row){
                if($row->id == $payment->giftcard_list_redeem){
                    $row->amount_paid += $payment->giftcard_values_redeem;
                    $isDuplicate = true;
                    break;
                }
            }
            if($isDuplicate === false) {
                $sale_session->payments[] = (object)array(
                    'id' => $payment->giftcard_list_redeem,
                    'payment_type'  =>  ITEM_TYPE('Gift_Card'),
                    'amount_paid' => floatval($payment->giftcard_values_redeem)
                );
            }
        }

        if(!empty($payment->promotion_code)){
            $cartData = $this->_cartData();
            $checkP = $this->db->get_where('promotion', array('code' => $payment->promotion_code))->row_array();
            if($checkP['type'] == PROMOTION_TYPE('Discount Amount')){
                $value = floatval($checkP['discount']);
            }
            else{
                $value = (floatval($checkP['discount']) * $cartData->total) / 100;
            }

            $isDuplicate = false;
            $payments = isset($sale_session->payments)?$sale_session->payments: array();
            foreach($payments as $row){
                if($row->id == $payment->promotion_code){
                    $row->amount_paid += $value;
                    $isDuplicate = true;
                    break;
                }
            }
            if($isDuplicate === false) {

                $sale_session->payments[] = (object)array(
                    'id' => $payment->promotion_code,
                    'payment_type'  =>  6,
                    'amount_paid' => floatval($value)
                );
            }
        }

        $this->session->set_userdata('sale',$sale_session);
        return 1;
    }

    function removePayment($id){
        $sale_session = $this->session->userdata('sale');

        $new_payments = array();
        foreach($sale_session->payments as $payment){
            if($payment->payment_type != ITEM_TYPE('Gift_Card')){
                if($payment->payment_type != $id){
                    $new_payments[] = $payment;
                }
            }
        }
        $sale_session->payments = $new_payments;

        $this->session->set_userdata('sale', $sale_session);
        return 1;
    }

    function checkDataBeforeSubmit(){
        $data = $this->session->userdata('sale');
        foreach($this->cart->contents() as $item){
            if(isset($item['options']['no_fullcash']) && $item['options']['no_fullcash'] == 1){
                if(isset($item['options']['credit_pair']) && count($item['options']['credit_pair']) == 0){
                    throw new Exception($item['name']." can not payment only by cash.");
                }
            }
        }
        if(!isset($data->customer_id) || $data->customer_id == 0){
            foreach($this->cart->contents() as $row){
                $options = $this->cart->product_options($row['rowid']);
                if($options['type'] == 2)
                {
                    throw new Exception("Only system customers can buy packages");
                }
            }
        }
        return 1;
    }

    function _getMinimumDeposit(){
        $items_data = $this->cart->contents();
        $minimumDeposit = '';
        if(count($items_data) > 0){
            $item_model = $this->load->table_model('item');
            foreach($items_data as $item){
                $temp = $item_model->getMinimumDepostByItem($item['id']);
                if(count($temp) > 0 && isset($temp->min_cash_payment)){
                    $minimumDeposit += floatval($temp->min_cash_payment);
                }
            }
        }
        else{
            return '';
        }
        return $minimumDeposit;
    }

    function insertBill($comment,$is_deposit = false){
        try{
            $user_data      = $this->session->userdata('login');
            $cart_data      = $this->_cartData();
            $sale_data      = $this->session->userdata('sale');

            if(!$this->session->userdata('sale')){
                throw new Exception('');
            }
            $items_data     = $this->cart->contents();

            $payment_data   = $this->_paymentData($cart_data->total);
            $customer_id_log = isset($sale_data->customer_id)?$sale_data->customer_id:'';
            $bill = new stdClass();
            $this->db->trans_begin();
            $bills_model = $this->load->table_model('bill');
            /* ---- Get Max Code ------ */
            $prefix     = $this->system_config->get('sale_id_prefix');
            $max_code   = $bills_model->getMaxBillCode($prefix);

            $bill_item_id = '';

            /* ---- First insert the bill ----- */
            $bill->code                     = $max_code;
            $bill->customer_id              = isset($sale_data->customer_id)?$sale_data->customer_id:null;
            $bill->description              = $comment?$comment:null;
            $bill->employee_id              = $user_data->user_id;
            $bill->discount                 = isset($cart_data->voucher_discount)&&$cart_data->voucher_discount?$cart_data->voucher_discount:0;
            $bill->gst                      = $cart_data->gst;
            $bill->total_price              = $cart_data->total;
            $bill->amount_due               = -$payment_data->amount_due;
            $bill->email_review             = 0;
            $bill->promotion_code           = isset($sale_data->promotion_code)?$sale_data->promotion_code:null;
//            if(isset($sale_data->bill_created_date)){
//                $bill->created_date = $sale_data->bill_created_date;
//            }
            $bill_id = $bills_model->insert($bill);

            /* ---- Then insert the bill_items ---- */
            $bills_items_model          = $this->load->table_model('bill_item');
            $bills_employees_model      = $this->load->table_model('bill_employee');
            $items_model                = $this->load->table_model('item');
            $bundle_item_model          = $this->load->table_model('item_bundle_item');
            $credits_model              = $this->load->table_model('credit');
            $bill_item_credit_model     = $this->load->table_model('bill_item_credit');
            $customer_model             = $this->load->table_model('customer');
            $warehouse_model            = $this->load->table_model('warehouse');
            $warehouse_item_model       = $this->load->table_model('warehouse_item_model');
            $item_package_model         = $this->load->table_model('item_package_model');
            $commission_item_map        = $this->load->table_model('commission_item')->getTableMap('commission_id', "", array(), false);
            $default_expire_month = $this->system_config->get('expire');

            $item_ids = convert_to_array($items_data, "", "id");
            $item_map       = $items_model->getTableMap('id', '', array('id' => $item_ids));
            $item_package_map = $item_package_model->getTableMap('item_id','',array('item_id' => $item_ids));

            $customer = $customer_model->getByID(isset($sale_data->customer_id)?$sale_data->customer_id:null);

            $sub_item_map   = $bundle_item_model->getTableMap('item_id', '', array('item_id' => $item_ids),false);
            foreach($sub_item_map as $item_id => $sub_item_list){
                $sub_item_map[$item_id] = convert_to_array($sub_item_list,'sub_item_id','');
            }
            if($customer){
                $promotion_used_list = $this->calculate_promotion_time($customer->id,$item_ids);
            }

            foreach($commission_item_map as $key => $values){
                $commission_item_map[$key] = convert_to_array($values, 'item_id');
            }

            $employee_map = $this->load->table_model('employee')->getTableMap();
            $warehouse_items = $warehouse_item_model->get_warehouse_quantity(null,$item_ids,false);

            foreach($items_data as $data){
                //Check if customer exists and package avail
                if((!$customer ||  $customer->customer_type != CUSTOMER_TYPE('Member')) && $item_map[$data['id']]->type == ITEM_TYPE('Package')){
                    throw new Exception('The current customer are not allow to buy '. $item_map[$data['id']]->name);
                }
                if((!$customer || !$customer->nric) &&  $item_map[$data['id']]->promotion){
                    throw new Exception('You are not allow to buy any promotion item since do not have nric');
                }
                if($customer && $customer->nric
                    && $item_map[$data['id']]->promotion
                    && isset($promotion_used_list[$data['id']])
                    && $promotion_used_list[$data['id']] >= $item_map[$data['id']]->promotion){
                    throw new Exception('Promotion limit exceed');
                }
                $options = $this->cart->product_options($data['rowid']);
                $item_info = $items_model->getByID($data['id']);


                $willInsertData = (object)array(
                    'bill_id'       =>  $bill_id,
                    'item_id'       =>  $data['id'],
                    'quantity'      =>  $data['qty'],
                    'price'         =>  $data['price'],
                    'discount_type' =>  isset($options['discount_type'])?$options['discount_type']:null,
                    'discount_value'=>  isset($options['discount'])&&$options['discount']?$options['discount']:0,
                    'unit_cost'     =>  isset($data['unit_cost'])?$data['unit_cost']:0
                );

                if($item_info->type == ITEM_TYPE('Package')){
                    $willInsertData->credit_add = $item_info->credit_value;
                }
                else if($item_info->type == ITEM_TYPE('Product')){
                    if(!$warehouse_items || !isset($warehouse_items[$data['id']])){
                        throw new Exception('The warehouse does not exists for product purchasing: '. $item_info->name);
                    }
                    $warehouse_item = $warehouse_items[$data['id']];
                    if(! count($warehouse_item)){
                        throw new Exception('The warehouse does not have this product: '. $item_info->name);
                    }
                    $qty_left = $warehouse_item->quantity;
                    if($qty_left < $data['qty']){
                        throw new Exception('The warehouse does not have any unit left for this product: '. $item_info->name);
                    }
                    $willInsertData->warehouse_id = $warehouse_item->warehouse_id;
                    $willInsertData->creadit_add = 0;
                }
                $credit_add_id = null;
                /* ----- Insert credit to customer if it is a package ----- */
                if($item_info->type == ITEM_TYPE('Package')){
                    //Insert credit for customers
                    $today =  convert_date(date('Y-m-d H:i:s'));
                    $package = $credits_model->get(array(
                        'customer_id' => $sale_data->customer_id,
                        'item_id'  => $data['id'],
                        'status'   => 1,
                        "end_date > '{$today}'"
                    ));
                    if(isset($item_package_map[$item_info->id]) && $item_package_map[$item_info->id]->expire_length){
                        $effectiveDate = date('Y-m-d 15:59:59', strtotime("+".$item_package_map[$item_info->id]->expire_length." months"));
                    }else{
                        $effectiveDate = date('Y-m-d 15:59:59', strtotime("+".$default_expire_month." months"));
                    }

                    if(count($package)){
                        $package = $package[0];
                        $willUpdateCredit = (object)array(
                            'customer_id'       => $package->customer_id,
                            'item_id'           => $package->item_id,
                            'start_date'        => $package->start_date,
                            'end_date'          => $effectiveDate,
                            'credit'            => $package->credit + ($item_info->credit_value * $data['qty']),
                            'credit_original'   => $package->credit_original + $item_info->credit_value,
                        );
                        $credits_model->update($package->id, $willUpdateCredit);
                        $credit_add_id = $package->id;
                    }else{
                        $wilInsertCredit = (object)array(
                            'customer_id'       => $bill->customer_id,
                            'item_id'           => $item_info->id,
                            'start_date'        => $today,
                            'end_date'          => $effectiveDate,
                            'credit'            => $item_info->credit_value * $data['qty'],
                            'credit_original'   => $item_info->credit_value,
                        );
                        $credit_add_id = $credits_model->insert($wilInsertCredit);
                    }
                }
                // Or if it is a product, minus the quantity of the product in the warehouse
                else if($item_info->type == ITEM_TYPE('Product')){
                    $warehouse_item_model->update($warehouse_item->id,array(
                        'quantity' => $warehouse_item->quantity - $data['qty'],
                    ),true,array('log_type' => 'sale'));
                    $warehouse_items[$data['id']]->quantity -= $data['qty'];
                }
                // Or if it is a gift card, add the giftcard value to customer
                else if($item_info->type == ITEM_TYPE('Gift_Card')){
                    $values = intval($item_map[$data['id']]->price);
                    $giftcardModel = $this->load->table_model('giftcard_list_detail');
                    do{
                        $code = 'GIFT-'.substr(md5(strtotime('NOW').rand(1,999999).$data['id']),0,10);
                    }
                    while(count($giftcardModel->get(array('code' => $code))));
                    // NOTICE THAT THE CUSTOMER CAN BUY MULTIPLE CODE HERE.
                    $insertData = new stdClass();
                    $insertData->code = $code;
                    $insertData->customer_id = $sale_data->customer_id;
                    $insertData->item_id = $data['id'];
                    $insertData->start_value = $values;
                    $insertData->current_value = $values;
                    $giftcardModel->insert($insertData);
                }

                if(!isset($willInsertData->credit_add)){
                    $willInsertData->creadit_add = 0;
                }
                $willInsertData->credit_id = $credit_add_id;
                $bill_item_id = $bills_items_model->insert($willInsertData);

                /*----- Insert to bill_items_credit -------*/
                foreach($options['credit_pair'] as $key=>$value){
                    $package = $credits_model->getByID($key,array(),Permission_Value::ADMIN);
                    if($package->credit - $value < 0){
                        throw new Exception ("Do not have enough credit on item: ".(isset($options['name'])?$options['name']: ''));
                    }
                    $willUpdateCredit = (object)array(
                        'credit'            => $package->credit - $value,
                    );
                    $credits_model->update($package->id, $willUpdateCredit);
                    $bill_item_credit_model->insert(array(
                        'bill_item_id'  => $bill_item_id,
                        'credit_id'     => $key,
                        'credit_value'  => $value
                    ));
                }

                /* ----- Then insert the bill employees ---- */
                if($options['type'] == ITEM_TYPE('Bundle')){
                    foreach($options['choice_staff'] as $sub_item_id => $staff_list_id){
                        foreach($staff_list_id as $staff_id){
                            $commission_id = 0;
                            $commission_type = 1;
                            if(isset($employee_map[$staff_id])){
                                $commission_id = $employee_map[$staff_id]->commission_id;
                            }

                            if(isset($commission_item_map[$commission_id]) && isset($commission_item_map[$commission_id][$data['id']])){
                                $commission_item = $commission_item_map[$commission_id][$data['id']];
                                $commission_value = $commission_item->value;
                            }
                            else{
                                $commission_value = 0;
                            }

                            if(isset($commission_item_map[$commission_id]) && isset($commission_item_map[$commission_id][$sub_item_id])){
                                $sub_commission_value = $commission_item_map[$commission_id][$sub_item_id]->value;
                            }
                            else{
                                $sub_commission_value = 0;
                            }

                            $willInsertStaff = (object)array(
                                'bill_item_id'          => $bill_item_id,
                                'employee_id'           => $staff_id,
                                'commission_id'         => $commission_id,
                                'commission_type'       => $commission_type,
                                'commission_value'      => $commission_value,
                                'sub_item_id'           => $sub_item_id,
                                'sub_commission_value'  => $sub_commission_value,
                            );

                            $bills_employees_model->insert($willInsertStaff);
                        }
                    }
                }
                else{
                    foreach($options['choice_staff'] as $staff_id){
                        $commission_id = 0;
                        $commission_type = 1;
                        $commission_value = 0;
                        if(isset($employee_map[$staff_id])){
                            $commission_id = $employee_map[$staff_id]->commission_id;
                        }
                        if($commission_id != 0 && isset($commission_item_map[$commission_id][$data['id']])){
                            $commission_item = $commission_item_map[$commission_id][$data['id']];
                            $commission_type = $commission_item->type;
                            $commission_value = $commission_item->value;
                        }

                        $willInsertStaff = (object)array(
                            'bill_item_id'      => $bill_item_id,
                            'employee_id'       => $staff_id,
                            'commission_id'     => $commission_id,
                            'commission_type'   => $commission_type,
                            'commission_value'  => $commission_value
                        );
                        $bills_employees_model->insert($willInsertStaff);
                    }
                }
            }

            // CASE BUNDLE
            foreach($items_data as $data) {
                if($data['options']['type'] == ITEM_TYPE('Bundle')){
                    $sub_item_ids = array();
                    foreach($data['options']['sub_items'] as $sub_item){
                        $sub_item_ids[] = $sub_item->id;
                    }
                    foreach($data['options']['sub_items'] as $sub_item){
                        //Check if customer exists and package avail
                        if((!$customer ||  $customer->customer_type != CUSTOMER_TYPE('Member')) && isset($sub_item_map[$data['id']][$sub_item->id]->type) == ITEM_TYPE('Package') && $sub_item_map[$data['id']][$sub_item->id]->type == ITEM_TYPE('Package')){
                            throw new Exception('The current customer are not allow to buy '. $sub_item_map[$data['id']][$sub_item->id]->name);
                        }
                        if((!$customer || !$customer->nric)){
                            throw new Exception('You are not allow to buy ' .(isset($sub_item_map[$data['id']][$sub_item->id]->name) ? $sub_item_map[$data['id']][$sub_item->id]->name : ''). ' item since do not have nric');
                        }
                        $options = $this->cart->product_options($data['rowid']);
                        $item_info = $items_model->getByID($sub_item->id);
                        $willInsertData = (object)array(
                            'bill_id'       =>  $bill_id,
                            'item_id'       =>  $sub_item->id,
                            'quantity'      =>  $data['qty'],
                            'price'         =>  $sub_item->price,
                            'discount_type' =>  isset($options['discount_type'])?$options['discount_type']:null,
                            'discount_value'=>  isset($options['discount'])&&$options['discount']?$options['discount']:0,
                            'unit_cost'     =>  isset($data['unit_cost'])?$data['unit_cost']:0
                        );
                        $warehouse_items = $warehouse_item_model->get_warehouse_quantity(null,$sub_item_ids,false);
                        if($item_info->type == ITEM_TYPE('Package')){
                            $willInsertData->credit_add = $item_info->credit_value;
                        }
                        else if($item_info->type == ITEM_TYPE('Product')){
                            if(!$warehouse_items || !isset($warehouse_items[$sub_item->id])){
                                throw new Exception('The warehouse does not exists for product purchasing: '. $item_info->name);
                            }
                            $warehouse_item = $warehouse_items[$sub_item->id];
                            if(! count($warehouse_item)){
                                throw new Exception('The warehouse does not have this product: '. $item_info->name);
                            }
                            $qty_left = $warehouse_item->quantity;
                            if($qty_left < $data['qty']){
                                throw new Exception('The warehouse does not have any unit left for this product: '. $item_info->name);
                            }
                            $willInsertData->warehouse_id = $warehouse_item->warehouse_id;
                            $willInsertData->creadit_add = 0;
                        }

                        $credit_add_id = null;
                        /* ----- Insert credit to customer if it is a package ----- */
                        if($item_info->type == ITEM_TYPE('Package')){
                            //Insert credit for customers
                            $today =  convert_date(date('Y-m-d H:i:s'));
                            $package = $credits_model->get(array(
                                'customer_id' => $sale_data->customer_id,
                                'item_id'  => $sub_item->id,
                                'status'   => 1,
                                "end_date > '{$today}'"
                            ));

                            $item_package_map = $item_package_model->getTableMap('item_id','',array('item_id' => $item_info->id));
                            if(isset($item_package_map[$item_info->id]) && $item_package_map[$item_info->id]->expire_length){
                                $effectiveDate = date('Y-m-d 15:59:59', strtotime("+".$item_package_map[$item_info->id]->expire_length." months"));
                            }else{
                                $effectiveDate = date('Y-m-d 15:59:59', strtotime("+".$default_expire_month." months"));
                            }
                            if(count($package)){
                                $package = $package[0];
                                $willUpdateCredit = (object)array(
                                    'customer_id'       => $package->customer_id,
                                    'item_id'           => $package->item_id,
                                    'start_date'        => $package->start_date,
                                    'end_date'          => $effectiveDate,
                                    'credit'            => $package->credit + $item_info->credit_value,
                                    'credit_original'   => $package->credit_original + $item_info->credit_value,
                                );
                                $credits_model->update($package->id, $willUpdateCredit);
                                $credit_add_id = $package->id;
                            }else{
                                $wilInsertCredit = (object)array(
                                    'customer_id'       => $bill->customer_id,
                                    'item_id'           => $item_info->id,
                                    'start_date'        => $today,
                                    'end_date'          => $effectiveDate,
                                    'credit'            => $item_info->credit_value,
                                    'credit_original'   => $item_info->credit_value,
                                );
                                $credit_add_id = $credits_model->insert($wilInsertCredit);
                            }
                        }
                        // Or if it is a product, minus the quantity of the product in the warehouse
                        else if($item_info->type == ITEM_TYPE('Product')){
                            $warehouse_item_model->update($warehouse_item->id,array(
                                'quantity' => $warehouse_item->quantity - $data['qty'],
                            ),true,array('log_type' => 'sale'));
                            $warehouse_items[$sub_item->id]->quantity -= $data['qty'];
                        }

                        $willInsertData->credit_id = $credit_add_id;

                        if(!isset($willInsertData->credit_add)){
                            $willInsertData->credit_add = 0;
                        }
                        $willInsertData->belong_bundle = 1;
                        $bill_item_id = $bills_items_model->insert($willInsertData);
                        /*----- Insert to bill_items_credit -------*/

                        foreach($options['credit_pair'] as $key=>$value){
                            $package = $credits_model->getByID($key,array(),Permission_Value::ADMIN);
                            if($package->credit - $value < 0){
                                throw new Exception ("Do not have enough credit on item: ".$options['name']);
                            }
                            $willUpdateCredit = (object)array(
                                'credit'            => $package->credit - $value,
                            );
                            $credits_model->update($package->id, $willUpdateCredit);
                            $bill_item_credit_model->insert(array(
                                'bill_item_id'  => $bill_item_id,
                                'credit_id'     => $key,
                                'credit_value'  => $value
                            ));
                        }
                    }
                }
            }

            /* ----- Finally, insert bill payments ---- */
            $bills_payments_model = $this->load->table_model('bill_payment');
            $giftcard_list_detail = $this->load->table_model('giftcard_list_detail');
            $giftcard_list_detail_transaction = $this->load->table_model('giftcard_list_detail_transaction');
            foreach($payment_data->payments as $payment){
                if($payment->payment_type == ITEM_TYPE('Gift_Card')){
                    $giftcard = $this->load->table_model('giftcard_list_detail')->get(array('code' => $payment->id))[0];
                    $transactionInsert = (object)array(
                        'bill_id' => $bill_id,
                        'giftcard_code' => $payment->id,
                        'customer_id' => $customer->id,
                        'before_trans' => $giftcard->current_value,
                        'trans' => $payment->amount_paid,
                        'after_trans' => floatval($giftcard->current_value) - floatval($payment->amount_paid)
                    );
                    $giftcard_list_detail_transaction->insert($transactionInsert);
                    $this->db->update('giftcard_list_detail', array('current_value' => $transactionInsert->after_trans), array('code' => $payment->id));
                }
                else {
                    if($payment->payment_type == 6){ // PROMOTION CODE
                        $count_of_used = $this->db->get_where('promotion', array('code' => $payment->id))->row_array()['count_of_used'];
                        $count_of_used--;
                        $this->db->update('promotion', array('count_of_used' => $count_of_used), array('code' => $payment->id));
                    }
                    $willInsertPayment = (object)array(
                        'bill_id' => $bill_id,
                        'payment_id' => $payment->payment_type,
                        'amount' => $payment->amount_paid
                    );
                    $bills_payments_model->insert($willInsertPayment);
                }
            }

            /* ----- insert if using deposit ----- */
            if($is_deposit){
                $deposit_payment_item = $this->load->table_model('deposit_payment_item');
                $deposit_payment_item_list = $this->load->table_model('deposit_payment_item_list');
                $itemList = $this->cart->contents();
                $numDeposit = 0;
                $remainAmount = floatval($cart_data->total) - floatval($sale_data->payments[0]->amount_paid);
                foreach($itemList as $key => $item){
                    $checkDeposit = $deposit_payment_item->get(array('item_id' => $item['id']));
                    if(!empty($checkDeposit)){
                        $numDeposit++;
                        $itemList[$key]['depositItem'] = $checkDeposit[0];
                    }
                }
                $averageRemainAmount = $remainAmount/$numDeposit;
                foreach($itemList as $key => $item){
                    if(isset($item['depositItem'])){
                        $depositList = (object)array(
                            'bill_id' => $bill_id,
                            'customer_id' =>$customer->id,
                            'deposit_payment_item_id' => $item['depositItem']->id,
                            'remain_payment_times' => $item['depositItem']->payment_time,
                            'total_remain_amount' => $averageRemainAmount,
                            'status' => 1
                        );
                        $deposit_payment_item_list->insert($depositList);
                    }
                }
            }
        }
        catch(Exception $e){
            $this->db->trans_rollback();
            return $e->getMessage();
        }
        $this->db->trans_commit();
        return $bill_id;
    }

    function suspendSale(){
        $bills_items_model = $this->load->table_model('bill_item');
        $bills_employees_model = $this->load->table_model('bill_employee');
        $bill_item_credit_model = $this->load->table_model('bill_item_credit');
        $items_model = $this->load->table_model('item');
        $config_model = $this->load->table_model('config');
        $bills_model = $this->load->table_model('bill');
        $user_data      = $this->session->userdata('login');
        $cart_data      = $this->_cartData();
        $sale_data      = $this->session->userdata('sale');
        $items_data     = $this->cart->contents();
        $payment_data   = $this->_paymentData($cart_data->total);

        $bill = new stdClass();

        $this->db->trans_start();
        /* ---- First insert the bill ----- */
        if(isset($sale_data->bill_id)){
            $bill->id = $sale_data->bill_id;
        }
        if(isset($sale_data->bill_created_date)){
            $bill->created_date = $sale_data->bill_created_date;
        }
        $bill->customer_id              = isset($sale_data->customer_id)?$sale_data->customer_id:null;
        $bill->branch_id                = $user_data->branch_id;
        $bill->description              = isset($sale_data->comment)?$sale_data->comment:null;
        $bill->employee_id              = $user_data->user_id;
        $bill->discount                 = isset($cart_data->voucher_discount)&&$cart_data->voucher_discount?$cart_data->voucher_discount:0;
        $bill->gst                      = $cart_data->gst;
        $bill->total_price              = $cart_data->total;
        $bill->amount_due               = -$payment_data->amount_due;
        $bill->status                   = BILL_STATUS('Hold');

        $bill_id = $bills_model->insert($bill,null,null,true);
        /* ---- Then insert the bill_items ---- */

        $expire_months = $this->system_config->get('expire');

        foreach($items_data as $data){
            $options = $this->cart->product_options($data['rowid']);
            $item_info = $items_model->getByID($data['id']);
            $willInsertData = (object)array(
                'bill_id'       =>  $bill_id,
                'item_id'       =>  $data['id'],
                'quantity'      =>  $data['qty'],
                'price'         =>  $data['price'],
                'discount_type' =>  isset($options['discount_type'])?$options['discount_type']:null,
                'discount_value'=>  isset($options['discount'])&&$options['discount']?$options['discount']:0,
            );
            if($item_info->type == ITEM_TYPE('Package')){
                $willInsertData->credit_add = $item_info->credit_value;
            }
            $bill_item_id = $bills_items_model->insert($willInsertData);

            /*----- Insert to bill_items_credit -------*/
            foreach($options['credit_pair'] as $key=>$value){
                $bill_item_credit_model->insert(array(
                    'bill_item_id'  => $bill_item_id,
                    'credit_id'     => $key,
                    'credit_value'  => $value
                ));
            }

            /* ----- Then insert the bill employees ---- */
            if($item_info->type == ITEM_TYPE('Bundle')){
                foreach($options['choice_staff'] as $sub_item_id=>$staff_list_id){
                    foreach($staff_list_id as $staff_id){
                        $willInsertStaff = (object)array(
                            'bill_item_id'      => $bill_item_id,
                            'employee_id'       => $staff_id,
                            'sub_item_id'       => $sub_item_id
                        );
                        $bills_employees_model->insert($willInsertStaff);
                    }
                }
            }else{
                foreach($options['choice_staff'] as $staff_id){
                    $willInsertStaff = (object)array(
                        'bill_item_id'      => $bill_item_id,
                        'employee_id'       => $staff_id
                    );
                    $bills_employees_model->insert($willInsertStaff);
                }
            }

        }

        /* ----- Finally, insert bill payments ---- */
        $bills_payments_model = $this->load->table_model('bill_payment');

        foreach($payment_data->payments as $payment){
            $willInsertPayment = (object)array(
                'bill_id'       =>  $bill_id,
                'payment_id'    =>  $payment->payment_type,
                'amount'        =>  $payment->amount_paid
            );
            $bills_payments_model->insert($willInsertPayment);
        }

        $this->db->trans_complete();
        return $bill_id;
    }

    function getDataForBill($bill_id,$status = 1){
        $data = new stdClass();
        $bills_model            = $this->load->table_model('bill');
        $bills_items_model      = $this->load->table_model('bill_item');
        $bills_payments_model   = $this->load->table_model('bill_payment');
        $bills_employees_model  = $this->load->table_model('bill_employee');
        $payment_types_model    = $this->load->table_model('payment_type');
        $items_model            = $this->load->table_model('item');
        $branchs_model          = $this->load->table_model('branch');
        $customers_model        = $this->load->table_model('customer');
        $employees_model        = $this->load->table_model('employee');
        $credits_model          = $this->load->table_model('credit');
        $bill_item_credit_model = $this->load->table_model('bill_item_credit');
        $bill           = $bills_model->get(array('id' => $bill_id,'status' => $status))[0];
        $bill_items     = $bills_items_model->get(array('bill_id' => $bill_id));
        $bill_payments  = $bills_payments_model->get(array('bill_id' => $bill_id));
        $bill_items_credits = $bill_item_credit_model->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bill_items,'','id')),false);
        $item_ids = array();
        foreach($bill_items_credits as $key=>$row){
            $bill_items_credits[$key] = convert_to_array($row,'credit_id','credit_value',true);
        }
        /* --- Get Items Detail and Employee Working on Item---- */
        $group_ids = array();
        $group_bill_item_ids = array();
        foreach($bill_items as $item){
            $group_ids[] = $item->item_id;
            $group_bill_item_ids[] = $item->id;
        }
        $items = $items_model->getTableMap('id', '', array('id' => $group_ids,'item.status' => array(1,4)));

        $employees = $bills_employees_model->get(array('bill_item_id' => $group_bill_item_ids));
        $cashier = $employees_model->select(array(
            'select'    => array('employee'=>array('first_name','last_name')),
            'from'      => 'employee',
            'where'     =>array('id' => $bill->creator),
            'user_level'=>Permission_Value::ADMIN,
        ))->result();
        $cashier = $cashier[0];

        $credit_used_before = array();
        $credit_used = array();
        $credit_add_before = array();
        $credit_add = array();

        if($bill->customer_id){
            $credits = $credits_model->getTableMap('item_id','',array('customer_id'=>$bill->customer_id),true,Permission_Value::ADMIN,array());

            $bills_after = $bills_model->get(
                array('customer_id'=>$bill->customer_id,"created_date > '{$bill->created_date}'",'status'=>BILL_STATUS('Complete'))
                ,'','','',Permission_Value::ADMIN,array());

            $bills_items_after = $bills_items_model->select(array(
                'select'=> array('bill_item' => '*'),
                'from'  =>
                    array('bill_item' => array('table'=>'bill_item'),
                          'bill' => array('table'=>'bill','condition'=>'bill.id = bill_item.bill_id','type' => 'left')),
                'where' => array('bill.id'=>convert_to_array($bills_after,'','id'),'status' => BILL_STATUS('Complete')),
                'user_level' => Permission_Value::ADMIN,
                'permission' => array()
            ))->result();

            $credit_adjustment = $this->load->table_model('credit_adjustment_log')->get_detail(array(
                'credit_adjustment_log.credit_id' => convert_to_array($credits,'','id'),
                "credit_adjustment_log.log_time > '{$bill->created_date}'"
            ));

            $items_after = $items_model->getTableMap('id', '', array('id' => convert_to_array($bills_items_after,'','item_id')),true,Permission_Value::ADMIN,array());

            $bills_items_credits_after = $bill_item_credit_model->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bills_items_after,'','id')),false);
            foreach($bills_items_credits_after as $key=>$row){
                $bills_items_credits_after[$key] = convert_to_array($row,'credit_id','credit_value',true);
            }
            $bills_items_after = convert_to_array($bills_items_after, 'bill_id','',false);

            foreach($bills_after as $bill_after){
                $bill_items_after = $bills_items_after[$bill_after->id];
                foreach($bill_items_after as $bill_item_after){
                    if($items_after[$bill_item_after->item_id]->type == ITEM_TYPE('Package')){
                        if(!isset($credit_add_before[$credits[$bill_item_after->item_id]->id])){
                            $credit_add_before[$credits[$bill_item_after->item_id]->id] = $bill_item_after->credit_add;
                        }else{
                            $credit_add_before[$credits[$bill_item_after->item_id]->id] += $bill_item_after->credit_add;
                        }
                    }else if(isset($bills_items_credits_after[$bill_item_after->id])){
                        $item_credits = $bills_items_credits_after[$bill_item_after->id];
                        foreach($item_credits as $credit_id => $credit_value){
                            if(!isset($credit_used_before[$credit_id])){
                                $credit_used_before[$credit_id] = $credit_value;
                            }else{
                                $credit_used_before[$credit_id] += $credit_value;
                            }
                        }
                    }
                }
            }

            foreach($credit_adjustment as $row){
                if($row->credit_change > 0){
                    if(isset($credit_add_before[$row->id])){
                        $credit_add_before[$row->id] +=  $row->credit_change;
                    }else{
                        $credit_add_before[$row->id] = $row->credit_change;
                    }
                }else{
                    if(isset($credit_used_before[$row->id])){
                        $credit_used_before[$row->id] +=  -$row->credit_change;
                    }else{
                        $credit_used_before[$row->id] = -$row->credit_change;
                    }
                }
            }
            $credit_data = $credits_model->select(array(
                'select' => array(
                    'credit' => array('id','credit','end_date'),
                    'item'   => array('item_name' => 'name'),
                    'branch_group' => array('branch_group_name' => 'name')
                ),
                'from'   => array(
                    'credit'    => array('table' => 'credit'),
                    'item'                  => array('table' => 'item', 'condition' => 'credit.item_id = item.id'),
                    'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit_branch_group.credit_id = credit.id'),
                    'branch_group'          => array('table' => 'branch_group', 'condition' => 'branch_group.id = credit_branch_group.branch_group_id')
                ),
                'group'         => 'credit.id',
                'where'         => array('customer_id' => $bill->customer_id),
                'user_level'    => Permission_Value::ADMIN
            ))->result();
            foreach($credit_data as $row){
                $row->item_name = $row->item_name . " ({$row->branch_group_name})";
            }
            $credit_data = convert_to_array($credit_data,'id','',true);
        }

        //Ready data for the bill
        for($i = 0; $i < count($bill_items); $i++){
            $bill_items[$i]->detail = $items[$bill_items[$i]->item_id];
            if(isset($bill_items_credits[$bill_items[$i]->id])){
                $bill_items[$i]->package_detail = array();
                $bill_items[$i]->current_credit = array();
                $item_credits = $bill_items_credits[$bill_items[$i]->id];
                foreach($item_credits as $credit_id=>$credit_value){
                    $add_before = isset($credit_add_before[$credit_id])
                        ?$credit_add_before[$credit_id]:0;
                    $used_before = isset($credit_used_before[$credit_id])
                        ?$credit_used_before[$credit_id]:0;
                    $bill_items[$i]->package_detail[$credit_id] = $credits_model->getCreditPackageDetail(array('credit.id'=>$credit_id));

                    $bill_items[$i]->current_credit[$credit_id] = $bill_items[$i]->package_detail[$credit_id]->credit - $add_before +  $used_before;
                    if($bill->status == BILL_STATUS('Void')){
                        $credit_used[$credit_id] = 0;
                    }else{
                        $credit_used[$credit_id] = isset($credit_used[$credit_id])?
                            $credit_used[$credit_id] + $credit_value:
                            $credit_value;
                    }
                }
            }else if($bill_items[$i]->detail->type==2){
                $add_before = 0; $used_before = 0;
                if(isset($credits[$bill_items[$i]->item_id])){
                    $credit = $credits[$bill_items[$i]->item_id];
                    $add_before = isset($credit_add_before[$credit->id])?$credit_add_before[$credit->id]:0;
                    $used_before = isset($credit_used_before[$credit->id])?$credit_used_before[$credit->id]:0;
                }
                $bill_items[$i]->package_detail = $credits_model->getCreditPackageDetail(array('credit.customer_id'=>$bill->customer_id,'credit.item_id'=>$bill_items[$i]->item_id), false);

                $bill_items[$i]->current_credit = isset($bill_items[$i]->package_detail)?$bill_items[$i]->package_detail->credit - $add_before + $used_before:0;

                if($bill->status == BILL_STATUS('Void')){
                    if(isset($credits[$bill_items[$i]->item_id])){
                        $credit_add[$credits[$bill_items[$i]->item_id]->id] = 0;
                    }
                }else{
                    if(isset($credits[$bill_items[$i]->item_id])){
                        if(!isset($credit_add[$credits[$bill_items[$i]->item_id]->id])){
                            $credit_add[$credits[$bill_items[$i]->item_id]->id] = $bill_items[$i]->credit_add;
                        }else{
                            $credit_add[$credits[$bill_items[$i]->item_id]->id] -= $bill_items[$i]->credit_add;
                        }
                    }
                }

            }
            $bill_items[$i]->employees   = array();

            /*---------- Get Employee -----*/
            foreach($employees as $employee){
                if($employee->bill_item_id == $bill_items[$i]->id){
                    $bill_items[$i]->employees[] = $employee;
                }
            }
            if(count($bill_items[$i]->employees)){
                $group_ids = array();
                if($items[$bill_items[$i]->item_id]->type == ITEM_TYPE('Bundle')){
                    foreach($bill_items[$i]->employees as $employee){
                        $group_ids[$employee->sub_item_id][] = $employee->employee_id;
                    }
                    $bill_items[$i]->employees = array();
                    foreach($group_ids as $sub_item_id=>$employee_ids){
                        $bill_items[$i]->employees[$sub_item_id] = $employees_model->get(array('id' => $employee_ids));
                        $item_ids[] = $sub_item_id;
                    }
                }else{
                    foreach($bill_items[$i]->employees as $employee){
                        $group_ids[] = $employee->employee_id;
                    }
                    $bill_items[$i]->employees = $employees_model->get(array('id'=>$group_ids));
                }

            }
            /*------------------------------*/
            $bill_items[$i]->credit_value = 0;
            /*--- Caculate total credit value used ---- */
            if(isset($bill_items_credits[$bill_items[$i]->id] ))
                foreach($bill_items_credits[$bill_items[$i]->id] as $credit_id => $credit_value){
                    $bill_items[$i]->credit_value += $credit_value;
                }
        }

        $data->credit_used = $credit_used;
        $data->credit_add = $credit_add;
        $data->credit_used_before = $credit_used_before;
        $data->credit_add_before = $credit_add_before;
        /* --- Get Payments Detail ---- */
        $group_ids = array();
        foreach($bill_payments as $payment){
            $group_ids[] = $payment->payment_id;
        }
        $payments_types = $payment_types_model->get(array('id'=>$group_ids));
        foreach($bill_payments as $bill_payment){
            foreach($payments_types as $payment){
                if($payment->id == $bill_payment->payment_id)
                    $bill_payment->detail = $payment;
            }
        }

        $giftcard_data = $this->load->table_model('giftcard_list_detail_transaction')
                                    ->get(array('bill_id' => $bill_id));


        $data->bill                 = $bill;
        $data->bill_items           = $bill_items;
        $data->bill_payments        = $bill_payments;
        $data->bill_items_credits   = $bill_items_credits;
        $data->credits              = isset($credits)?$credits:array();
        $data->cashier              = $cashier;
        $data->credit_data          = isset($credit_data)?$credit_data:array();
        $data->item_map             = $this->load->table_model('item')->getTableMap('id','',array('id' => $item_ids));
        $data->giftcard_data        = $giftcard_data;
        /* --- Get Branch Information ---- */
        $data->branch_info                  = $branchs_model->select(
            array(
                'select'    =>  array(
                    'branch'  => array('name','address','phone_number'),
                    'branch_group'  => array('branch_group_name'=>'name')
                ),
                'from'      => array(
                    'bill'                  => array('table'    => 'bill'),
                    'branch'                => array('table'    => 'branch', 'condition' => 'branch.id = bill_branch.branch_id'),
                    'branch_group'          => array('table'    => 'branch_group', 'condition' => 'branch.branch_group_id = branch_group.id')
                ),
                'where'     => array('bill.id'  =>  $bill->id)
            )
        )->result()[0];
        $data->branch_info->company_name    = $this->system_config->get('com_name');
        /* --- Get Customer Information ---- */

        if($bill->customer_id)
            $data->customer = $customers_model->getByID($bill->customer_id);

        return $data;

    }

    function getDataForRecentBills(){
        $bills_model = $this->load->table_model('bill');
        $customers_model = $this->load->table_model('customer');
        $employees_model = $this->load->table_model('employee');
        $bills_payments_model = $this->load->table_model('bill_payment');
        $payment_types_model = $this->load->table_model('payment_type');
        $bills_items_model = $this->load->table_model('bill_item');
        $bill_item_credit_model = $this->load->table_model('bill_item_credit');
        $sale = $this->session->userdata('sale');
        $status = $bills_model->getAvailableStatus();
        $status[] = BILL_STATUS('Void');
        if(isset($sale) && isset($sale->customer_id) && $sale->customer_id)
        {
            $bills = $bills_model->get(array('customer_id' => $sale->customer_id,'status' => $status),'created_date desc',10,0);

        }else{
            $bills = $bills_model->get(array('status' => $status),'created_date desc',10,0);
        }

        $customer_ids = array();
        $bills_ids = array();
        foreach($bills as $bill){
            $customer_ids[] = $bill->customer_id;
            $bills_ids[] = $bill->id;
        }

        $customers_map = $customers_model->getTableMap('id', '', array('id' => $customer_ids));
        $bill_payment_map = $bills_payments_model->getTableMap('bill_id', "payment_id", array('bill_id' => $bills_ids), false);
        $payment_type_map = $payment_types_model->getTableMap();
        $bill_item_map = $bills_items_model->get(array('bill_id' => $bills_ids));
        $bill_item_credit_map = $bill_item_credit_model->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bill_item_map,'','id')),false,Permission_Value::ADMIN,array());
        $bill_item_map = convert_to_array($bill_item_map,'bill_id','',false);
        foreach($bills as $bill){
            $bill->payments = array();
            $total = 0;
            if(isset($customers_map[$bill->customer_id])){
                $bill->customer_detail = $customers_map[$bill->customer_id];
            }

            $payments = isset($bill_payment_map[$bill->id])?$bill_payment_map[$bill->id]:array();
            foreach($payments as $payment){
                $bill->payments[] = $payment_type_map[$payment]->name;
            }
            $bill_items = isset($bill_item_map[$bill->id])?$bill_item_map[$bill->id]:array();
            foreach($bill_items as $bill_item){
                if($bill_item->belong_bundle == 0) {
                    $bill_item_key = $bill_item->id;
                    $credit_value = 0;
                    if (isset($bill_item_credit_map[$bill_item_key])) {
                        if (!in_array('Credit', $bill->payments)) {
                            $bill->payments[] = 'Credit';
                        }

                        foreach ($bill_item_credit_map[$bill_item_key] as $bill_item_credit) {
                            $credit_value += $bill_item_credit->credit_value;
                        }
                    }
                    $total += $bill_item->quantity * $bill_item->price - $credit_value;
                    $discount = calculate_discount($bill_item->discount_type, $bill_item->discount_value, $bill_item->quantity * $bill_item->price);
                    $total -= $discount;
                }
            }
            $bill->total_price = ($total - $bill->discount - $bill->amount_due)>0?$total-$bill->discount-$bill->amount_due:0;
            $bill->total_price += $bill->total_price * ($bill->gst/100);
            $bill->created_date = get_user_date($bill->created_date);
        }

        $data = new stdClass();
        $data->bills = $bills;
        return $data;
    }

    function setCustomerSale($customer_id,$customer_pin){
        $customer_model = $this->load->table_model('customer');
        $check = $customer_model->get(array('id' => $customer_id));
        if(count($check) == 0){
            throw new Exception("User doesn't exists. Please contact to administrator");
        }
        if(!$customer_pin) {
            if (isset($this->config->item('permissionValue')['customer']) == true && ($check[0]->customer_global != 1 && $check[0]->customer_permission == 0)) {
            //        return 'need_mobile_number';
            }
        }
        if($customer_pin && $customer_pin != $check[0]->mobile_number){
            throw new Exception("Mobile Number is incorrect");
        }
        return 1;
    }

    function setCustomer($customer_id){
        $customer_model = $this->load->table_model('customer');
        $check = $customer_model->get(array('id' => $customer_id));
        if(count($check) == 0){
            throw new Exception("User doesn't exists. Please contact to administrator");
        }
        $sale = $this->session->userdata('sale');
        if(is_object($sale)){
            $sale->customer_id = $customer_id;
        }else{
            $sale = new stdClass();
            $sale->customer_id = $customer_id;
        }
        $customer_credits = convert_to_array($customer_model->getCreditDetail($customer_id),'id');
        foreach($customer_credits as $credit){
            $credit->credit_after = $credit->credit;
        }
        $customer_giftcard = convert_to_array($customer_model->getGiftCardDetail($customer_id),'code');
        foreach($customer_giftcard as $giftcard){
            $giftcard->giftcard_after = $giftcard->current_value;
        }
        $sale->customer_credits = $customer_credits;
        $sale->customer_giftcard = $customer_giftcard;
        $this->session->set_userdata('sale',$sale);
        return 1;
    }

    function setCustomerScheduler($customer_id,$customer_pin){
        return 1;
    /*    $customer_model = $this->load->table_model('customer');
        $check = $customer_model->get(array('id' => $customer_id));

        if(count($check) == 0){
            throw new Exception("User doesn't exists. Please contact to administrator");
        }

        if(!$customer_pin){
            if(isset($this->config->item('permissionValue')['customer']) == true && ($check[0]->customer_global != 1 && $check[0]->customer_permission == 0)){
                return 'need_mobile_number';
            }
        }

        if($customer_pin && $customer_pin != $check[0]->mobile_number){
            throw new Exception("Mobile Number is incorrect");
        }
        return 1;
    */
    }

    function detachCustomer(){
        $sale = $this->session->userdata('sale');
        if(is_object($sale)){
            if($sale->customer_id){
                unset($sale->customer_id);
                unset($sale->customer_credits);
            }
            else{
                return "There is no current customer be chosen. Please refesh the page"; exit;
            }
        }else{
            return "There is no current customer be chosen. Please refesh the page"; exit;
        }
        $this->session->set_userdata('sale',$sale);
    }

    function getSuspendedBills(){
        $user = $this->session->userdata('login');
        $bills_model = $this->load->table_model('bill');
        $data = new stdClass();

        $data->bills = $bills_model->getSuspendedBills();

        return $data;
    }

    function deleteBillDirectly($id){
        $bills_model = $this->load->table_model('bill');
        $bills_model->delete_suspended_bill(array('id' => $id, 'status' => BILL_STATUS('Hold')));
        return 1;
    }

    function retrieveBill($id){
        $this->session->unset_userdata('sale');
        $this->cart->destroy();
        $data = $this->getDataForBill($id,BILL_STATUS('Hold'));
        $customer_model = $this->load->table_model('customer');
        $sale = new stdClass();

        /* Set Customer and Payment Session */
        $sale->customer_id = isset($data->customer)?$data->customer->id:null;
        $sale->bill_id = $data->bill->id;
        $sale->bill_created_date = $data->bill->created_date;
        $payments = array();
        $i = 1;

        foreach($data->bill_payments as $payment){
            $payments[] = (object) array(
                'id'            => $id,
                'payment_type'  => $payment->payment_id,
                'amount_paid'   => $payment->amount,
            );
            $i++;
        }
        $sale->payments = $payments;
        $sale->voucher  = isset($data->bill->discount)&&$data->bill->discount?$data->bill->discount:0;

        /* Set Cart Sessions */
        $customer_credits = $customer_model->getCreditDetail($data->bill->customer_id);
        $customer_credits = convert_to_array($customer_credits,'id');
        foreach($data->bill_items as $item){
            $credit_pair = array();
            $item->credit_value = 0;
            $item->credit_id = array();
            if(isset($data->bill_items_credits[$item->id])){
                foreach($data->bill_items_credits[$item->id] as $credit_id=>$credit_value){
                    $credit_pair[$credit_id] = $credit_value;
                    $item->credit_value += $credit_value;
                    $item->credit_id[] = $credit_id;
                }
            }
            $credit_avail = array();
            foreach($customer_credits as $credit){
                if(in_array($item->item_id,$credit->items)){
                    $credit_avail[] = $credit;
                }
            }

            $total = $item->price * $item->quantity;
            $discount_real_value = $item->discount_type==1?$total * $item->discount_value/100:$item->discount_value;
            $sub_items = array();
            if($item->detail->type == ITEM_TYPE('Bundle')){
                $choice_staff = array();
                foreach($item->employees as $sub_item_id=>$employees){
                    $choice_staff[$sub_item_id] = convert_to_array($employees,'','id');
                }
                $sub_items = $item->detail->bundle->sub_items;
            }else{
                $choice_staff = convert_to_array($item->employees,'','id');
            }
            $cart_item = array(
                'id'   => $item->detail->id,
                'name'      => $item->detail->name,
                'qty'       => $item->quantity,
                'price'     => $item->detail->price,
                'options'   => array(
                    'choice_staff'          => $choice_staff,
                    'discount_type'         => $item->discount_type?$item->discount_type:0,
                    'discount'              => $item->discount_value?$item->discount_value:0,
                    'total'                 => $total - $item->credit_value - $discount_real_value,
                    'type'                  => $item->detail->type,
                    'packages'              => $credit_avail,
                    'credit_id'             => $item->credit_id,
                    'total_credit_value'    => $item->credit_value,
                    'credit_pair'           => $credit_pair,
                    'sub_items'             => $sub_items
                )
            );
            $this->cart->insert($cart_item);
        }

        $a = $this->cart->contents();
        foreach($customer_credits as &$credit){
            $credit->credit_after = $credit->credit;
            foreach($this->cart->contents() as $item){
                $options = $this->cart->product_options($item['rowid']);
                if(in_array($credit->id,$options['credit_id'])){
                    $credit->credit_after = $credit->credit_after - $options['credit_pair'][$credit->id];
                }
            }
        }
        $sale->customer_credits = $customer_credits;
        $this->session->set_userdata('sale', $sale);
        return 1;
    }

    function checkPin($pin = false){
        if($pin){
            $sale = $this->session->userdata('sale');
            $number = $this->load->table_model('customer')->get(array('id' => $sale->customer_id,'pin' => $pin));
            if(count($number)){
                return 1;
            }else{
                return "Incorrect Pin";
            }
        }else{
            foreach($this->cart->contents() as $row){
                $row_option = $this->cart->product_options($row['rowid']);
                if(count($row_option['credit_pair'])){
                    return 0;
                }
            }
            return 1;
        }
    }

    function package_change($rowid,$package_id,$row_values,$type){
        $cart_row_options = $this->cart->product_options($rowid);
        $row = $this->cart->get_row($rowid);
        $sale = $this->session->userdata('sale');
        $row_values['credit_id'] = isset($row_values['credit_id'])?$row_values['credit_id']:array();
        if($type == 'delete'){
            $row_values['credit_id'] = array_diff($row_values['credit_id'], array($package_id));
        }else{
            $credit_chosen = 0;
            if(isset($sale) && isset($sale->customer_id) && $sale->customer_id){
                foreach($sale->customer_credits as $key=>$credit){
                    if(in_array($row['id'],$credit->items)){
                        if($credit->credit_after != 0 && !in_array($key,$row_values['credit_id'])){
                            $row_values['credit_id'][] = $key;
                            break;
                        }
                    }
                }
            }
        }
        $this->updateItemToCart($rowid,(object)$row_values);
    }

    function getBillCode($id){
        return $this->select(array(
            'select' => array(
                'bill' => array('code')
            ),
            'from' => array(
                'bill' => array('table' => 'bill')
            ),
            'where' => array(
                'id' => $id
            )
        ))->row_array()['code'];
    }

    function getBillData($id){
        return $this->select(array(
            'select' => array(
                'bill' => array('description','discount'),
                'customer' => array('customer' => 'code'),
                'bill_item' => array('quantity','discount_type','discount_value'),
                'item' => array('item_name' => 'name'),
            ),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id=bill_item.bill_id', 'LEFT'),
                'item' => array('table' => 'item', 'condition' => 'bill_item.item_id=item.id', 'LEFT'),
                'customer' => array('table' => 'customer', 'condition' => 'bill.customer_id=customer.id', 'LEFT')
            ),
            'where' => array(
                'bill.id' => $id
            )
        ))->result_array();
    }

    function validPromotionCode($promotionCode){
        $data = $this->db->get_where('promotion',array('code' => $promotionCode));
        if($data->num_rows() == 0){
            return array('result' => FALSE, 'message' => 'Promotion code does not exist.');
        }
        else{
            $data = $data->row_array();
            if($data['count_of_used'] <= 0){
                return array('result' => FALSE, 'message' => 'This Promotion Code has been used up.');
            }
            $timezone = $this->system_config->get('timezone');
            $dateCurrent = get_user_date(gmdate('Y-m-d H:i:s', strtotime('NOW')),$timezone,'Y-m-d');
            if(strtotime($dateCurrent) < strtotime($data['date_from']) ||
               strtotime($dateCurrent) > strtotime($data['date_to'])){
                return array('result' => FALSE, 'message' => 'This Promotion Code only take effect from '.$data['date_from'].' to '.$data['date_to']);
            }
            $dateCurrent = get_user_date(gmdate('Y-m-d H:i:s', strtotime('NOW')),$timezone,'H:i');
            if(strtotime($dateCurrent) < strtotime($data['time_from']) ||
               strtotime($dateCurrent) > strtotime($data['time_to'])){
                return array('result' => FALSE, 'message' => 'This Promotion Code only take effect from '.$data['time_from'].' to '.$data['time_to']);
            }
        }
        return array('result' => TRUE, 'message' => '');
    }
}