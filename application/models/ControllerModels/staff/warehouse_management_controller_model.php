<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Warehouse_Management_Controller_model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "warehouse_item";
        $this->suggestionSearch = array('item.name','item.code');
        $this->suggestionDisplay = array('name');
    }

    function getSuggestion($keyword, $condition = array(), $is_full = false)
    {
        $suggestion_array = array();
        $limit = $this->config->item('item_per_page');
        $condition['warehouse_item.warehouse_id'] = $this->load->table_model('warehouse_item')->get_warehouse();

        if(count($this->suggestionSearch) > 0 && count($this->suggestionDisplay) > 0){
            $fields = "";
            if($keyword != ""){
                foreach($this->suggestionSearch as $field){
                    if($fields != ""){
                        $fields .= " OR ";
                    }
                    if(strpos($field, '.') === false){
                        $field = $this->main_table . "." . $field;
                    }
                    $fields .= $field . " LIKE '%{$keyword}%'";
                }
            }
            if(strlen($fields)){
                $condition[] = '('.$fields.')';
            }

            if($this->entity_status){
                $condition[$this->main_table.'.status'] = $this->entity_status;
            }
            if($is_full){
                $limit = "";
            }
            $suggestions = $this->select(array(
                'select' => array(
                    'warehouse_item'   => array('id'),
                    'item'              => array('code','name')
                ),
                'from'   => array(
                    'warehouse_item' => array('table' => 'warehouse_item'),
                    'item'           => array('table' => 'item', 'condition' => 'warehouse_item.item_id = item.id')
                ),
                'where' => $condition,
                'limit' => $limit,
            ))->result();
            $total = $this->load->table_model($this->main_table)->num_rows(array(
                'select' => array('item' => array('code','name')),
                'from'   => array(
                    'warehouse_item' => array('table' => 'warehouse_item'),
                    'item'           => array('table' => 'item', 'condition' => 'warehouse_item.item_id = item.id')
                ),
                'where' => $condition,
                'limit' => $limit,
            ));
            $num_row = count($suggestions);
            if($num_row > 0){
                foreach($suggestions as $suggestion){
                    $string = "";
                    if((isset($suggestion->{$this->main_table.'_permission'})&&$suggestion->{$this->main_table.'_permission'}) || $this->noPermissionDisplay == null){
                        foreach($this->suggestionDisplay as $displayField){
                            if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                                if($string != ""){
                                    $string .= " ";
                                }
                                $string .= $suggestion->{$displayField};
                            }
                        }
                    }else{
                        foreach($this->noPermissionDisplay as $displayField){
                            if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                                if($string != ""){
                                    $string .= " ";
                                }
                                $string .= $suggestion->{$displayField};
                            }
                        }
                    }

                    $row = array('label' => $string, 'value' => $suggestion->{$this->suggestionKey});
                    $suggestion_array[] = $row;
                }
                if($total > $num_row){
                    $more = $total-$num_row;
                    $row = array('label' => "And $more other row(s)...", 'value' => "disabled");
                    $suggestion_array[] = $row;
                }
            }else{
                $row = array('label' => "No result", 'value' => "disabled");
                $suggestion_array[] = $row;
            }
        }
        $suggestion_array = array_merge($suggestion_array,$this->getSuggestionExtend());
        return $suggestion_array;
    }

    function getDataSet($page = 1, $id = ""){
        $modelName = $this->load->table_model($this->main_table);

        $warehouses = $this->load->table_model('warehouse')->get();
        $warehouse_id = 0;
        foreach($warehouses as $warehouse){
            if($warehouse->warehouse_branch_id){
                $warehouse_id = $warehouse->id;
                break;
            }
        }

        $data = new stdClass();
        // get limit row number and start row index
        $limit = $this->config->item('item_per_page');
        $offset= ($page-1) * $limit;
        if($id === ""){
            $data->dataset = $modelName->get(array('warehouse_id' => $warehouse_id, 'status' => Status::Active),"created_date desc,updated_date desc", $limit,$offset);
            $data->dataset = $modelName->select(array(
                'select' => array('warehouse_item' => array('id','item_id','created_date','updated_date','quantity')),
                'from'   => array('warehouse_item' => array('table' => 'warehouse_item'),'item' => array('table' => 'item', 'condition' => 'item.id = warehouse_item.item_id')),
                'where'  => array('warehouse_item.warehouse_id' => $warehouse_id, 'warehouse_item.status' => Status::Active),
                'order'  => "created_date desc,updated_date desc",
                'limit'  => $limit,
                'start' => $offset
            ))->result();
        }else{
            $data->dataset = array($modelName->getByID($id));
        }
        $data->dataset = $this->renderDataSet($data->dataset);
        $data->total_rows = $modelName->num_rows(array(
            'select' => array('warehouse_item' => array('id','item_id','created_date','updated_date')),
            'from'   => array('warehouse_item' => array('table' => 'warehouse_item'),'item' => array('table' => 'item', 'condition' => 'item.id = warehouse_item.item_id')),
            'where'  => array('warehouse_item.warehouse_id' => $warehouse_id, 'warehouse_item.status' => Status::Active),
        ));
        //generate Paging information
        $data->page = new stdClass();
        $data->page->current_page = $page;
        $data->page->total_page   = ceil($data->total_rows/$limit);
        $data->page->item_per_page = $limit;
        $data->dataset_option = $this->getDataSetOption();
        return $data;
    }

    function renderDataSet($data){
        $item_ids = convert_to_array($data,'','item_id');

        $item_map = $this->load->table_model('item')->getTableMap('id','',array('id' => $item_ids,'status' => array(Status::Active,Status::Disable)));
        foreach($data as $row){
            if(isset($item_map[$row->item_id])) {
                $row->item_name = $item_map[$row->item_id]->name;
                $row->item_code = $item_map[$row->item_id]->code;
                if (!isset($row->updated_date)) {
                    $row->updated_date = (isset($row->created_date)) ? get_user_date($row->created_date, '', '', true) : "";
                } else {
                    $row->updated_date = (isset($row->updated_date)) ? get_user_date($row->updated_date, '', '', true) : "";
                }
            }
        }

        return $data;
    }

    function getDataForViewForm($id){
        $warehouses = $this->load->table_model('warehouse')->get();
        $warehouse_id = 0;
        foreach($warehouses as $warehouse){
            if($warehouse->warehouse_branch_id){
                $warehouse_id = $warehouse->id;
                break;
            }
        }
        $data = array('warehouse_id' => $warehouse_id);
        return $data;
    }

    function get_item_info($item_id, $warehouse_id = null){
        $ret = array(
            'quantity' => 0
        );

        if(! $warehouse_id){
            $warehouses = $this->load->table_model('warehouse')->get();
            $warehouse_id = 0;
            foreach($warehouses as $warehouse){
                if($warehouse->warehouse_branch_id){
                    $warehouse_id = $warehouse->id;
                    break;
                }
            }
        }

        $warehouse_item = $this->load->table_model('warehouse_item')->get(array('warehouse_id' => $warehouse_id,'item_id' => $item_id));

        if($warehouse_item){
            $ret['quantity'] = $warehouse_item[0]->quantity;
        }
        return $ret;
    }

    function insert($data){
        if(!$data['warehouse_id']){
            throw new Exception('Undefined Warehouse');
        }
        if(!$data['item_id']){
            throw new Exception('No item id is selected');
        }
        $is_exists = $this->load->table_model('warehouse_item')->get(array('warehouse_id' => $data['warehouse_id'],'item_id' => $data['item_id']));
        $log_data = array('log_type' => 'import_pro');
        if(count($is_exists)){
            $result = $this->load->table_model('warehouse_item')->update($is_exists[0]->id,$data,true,$log_data);
        }else{
            $result = $this->load->table_model('warehouse_item')->insert($data,"", array(), false,$log_data);
        }
        return $result;
    }

    function update($id,$data){
        $is_exists = $this->load->table_model('warehouse_item')->getByID($id);
        if(count($is_exists)){
            $log_data = array();
            if($data['log_data']){
                $log_data = $data['log_data'];
            }
            $result = $this->load->table_model('warehouse_item')->update($is_exists->id,$data,true,$log_data);
        }else{
            throw new Exception('The current item is not exists');
        }
        return $result;
    }

    function getDataForQuantityViewForm($id){
        return $this->load->table_model('warehouse_item')->getByID($id);
    }

}