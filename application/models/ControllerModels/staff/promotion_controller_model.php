<?php

class Promotion_Controller_model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "promotion";
        $this->suggestionSearch = array('name','code');
        $this->suggestionDisplay = array('name');
        $this->entity_status = array(Status::Active,Status::Disable);
    }
    function getItem($id){
        return $this->load->table_model('promotion')->getByID($id);
    }

    function getPromotion($id){
        $data = $this->select(array(
            'select' => array('*'),
            'from'   => array(
                'promotion' => array('table' => 'promotion')
            ),
            'where'  => array(
                'id' => $id
            )
        ))->result();
        if(isset($data[0]))
            return $data[0];
        else
            return $data;
    }

    function getPromotionDetail($id,$data){
        $items = $this->select(array(
            'select'    => array(
                'item'              => array('id','name','code')
            ),
            'from'      => array(
                'promotion_detail'  => array('table' => 'promotion_detail'),
                'item'              => array('table' => 'item', 'condition' => 'item.id = promotion_detail.item_id')
            ),
            'where'     => array('promotion_detail.promotion_id' => $id)
        ))->result();

        $categories = $this->select(array(
            'select'    => array(
                'category'          => array('id','name','code')
            ),
            'from'      => array(
                'promotion_detail'  => array('table' => 'promotion_detail'),
                'category'              => array('table' => 'category', 'condition' => 'category.id = promotion_detail.category_id')
            ),
            'where'     => array('promotion_detail.promotion_id' => $id)
        ))->result();

        $branches = $this->select(array(
            'select'    => array(
                'branch'              => array('id','name')
            ),
            'from'      => array(
                'promotion_detail'  => array('table' => 'promotion_detail'),
                'branch'              => array('table' => 'branch', 'condition' => 'branch.id = promotion_detail.branch_id')
            ),
            'where'     => array('promotion_detail.promotion_id' => $id)
        ))->result();

        if(!empty($categories))
            $data->categories = $categories;
        if(!empty($items))
            $data->items =  $items;
        if(!empty($branches))
            $data->branches = $branches;

        return $data;

    }


    function renderDataSet($data){
        return $data;
    }
}