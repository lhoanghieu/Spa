<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Items_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "item";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name', 'description');
        $this->entity_status = array(Status::Active,Status::Disable);
    }

    function getItemSuggestion($keyword){
        $items = $this->load->table_model('item')->get(array('type' => 1));
        $suggestion_array = array();
        foreach($items as $item){
            $string = (strlen($item->code)>0?$item->code.' ':'').
                (strlen($item->name)>0?$item->name.',':'').
                (strlen($item->description)>0?$item->description:'');

            if (strpos(strtolower($string), strtolower($keyword)) !== false) {
                $row = array('label' => $string, 'value' => $item->id);
                $suggestion_array[] = $row;
            }
        }
        return $suggestion_array;
    }

    function getListEmployeeService($suggest,$employee_id){
        $new_suggest = array();
        foreach($suggest as $item){
            $check = $this->select(array(
                'select' => array(
                    'employee_service' => array('id')
                ),
                'from' => array(
                    'employee_service' => array('table' => 'employee_service')
                ),
                'where' => array(
                    'employee_service.employee_id' => $employee_id,
                    'employee_service.item_id' => $item['value']
                )
            ))->result();
            if(count($check) > 0){
                $new_suggest[] = $item;
            }
        }
        return $new_suggest;
    }

    function getSuggestion($keyword, $condition = array())
    {
        $suggestion_array = array();
        if(count($this->suggestionSearch) > 0 && count($this->suggestionDisplay) > 0){
            $fields = "";
            if($keyword != ""){
                foreach($this->suggestionSearch as $field){
                    if($fields != ""){
                        $fields .= " OR ";
                    }
                    if(strpos($field, '.') === false){
                        $field = $this->main_table . "." . $field;
                    }
                    $fields .= $field . " LIKE '%{$keyword}%'";
                }
            }
            $condition[] = strlen($fields)?('('.$fields.')'):'';
//            $suggestions = $this->load->table_model($this->main_table)->get($condition);
            $suggestions = $this->select(array(
                'select' => array('item' => '*'),
                'from'   => array(
                    'item' => array('table' => 'item'),
                    'category_item' => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id', 'type' => 'left')
                ),
                'where' => $condition,
                'group' => 'item.id'
            ))->result();
            if(count($suggestions) > 0){
                foreach($suggestions as $suggestion){
                    $string = "";
                    foreach($this->suggestionDisplay as $displayField){
                        if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                            if($string != ""){
                                $string .= " ";
                            }
                            $string .= $suggestion->{$displayField};
                        }
                    }
                    $row = array('label' => $string, 'value' => $suggestion->{$this->suggestionKey});
                    $suggestion_array[] = $row;
                }
            }
        }
        $suggestion_array = array_merge($suggestion_array,$this->getSuggestionExtend());
        return $suggestion_array;
    }

    function getItem($id){
        $data = $this->load->table_model('item')->getByID($id,'','',array('status'=>$this->entity_status));
        return $data;
    }

    function getItembyType($type){
        $item_list = $this->select(array(
            'select' => array('item' => '*'),
            'from'   => array(
                'item' => array('table' => 'item')
            ),
            'where' => array('type' => $type),
            'group' => 'item.id'
        ))->result();
        return $item_list;
    }

    function _validationData($values){
        if(!empty($values['minimum_deposit']) && (!is_numeric($values['minimum_deposit']) && $values['minimum_deposit'] != '')){
            throw new Exception('The minimum deposit accept only number.');
        }
        if(!empty($values['remain_payment_time']) && (!is_numeric($values['remain_payment_time']) && $values['remain_payment_time'] != '')){
            throw new Exception('The Remain Payment Time accept only number.');
        }
    }

    function update($id, $values){
        $this->_validationData($values);
        $values = (object)$values;
        $this->db->trans_start();
        parent::update($id, $values);

        switch($values->type){
            case ITEM_TYPE('Product'):
                $this->insert_product_data($id,$values);
                break;
            case ITEM_TYPE('Service'):
                $this->insert_service_data($id,$values);
                break;
            case ITEM_TYPE('Package'):
                $this->insert_package_data($id,$values);
                break;
            case ITEM_TYPE('Bundle'):
                $this->insert_bundle_data($id,$values);
                break;
            case ITEM_TYPE('Gift_Card'):
                $this->insert_giftcard_data($id,$values);
                break;
        }
        $this->insert_category_data($id,$values);
        $this->insert_deposit_payment_data($id,$values);
        $this->update_booking_item_data($id,$values);
        $this->update_last_appointment_item_data($id,$values);
        if($this->db->trans_complete()){
            return true;
        }
        return false;
    }

    function insert($values){
        $this->_validationData($values);
        $values = (object)$values;
        $this->db->trans_start();
        $id = parent::insert($values);

        switch($values->type){
            case ITEM_TYPE('Product'):
                $this->insert_product_data($id,$values);
                break;
            case ITEM_TYPE('Service'):
                $this->insert_service_data($id,$values);
                break;
            case ITEM_TYPE('Package'):
                $this->insert_package_data($id,$values);
                break;
            case ITEM_TYPE('Bundle'):
                $this->insert_bundle_data($id,$values);
                break;
            case ITEM_TYPE('Gift_Card'):
                break;
        }
        $this->insert_category_data($id,$values);
        $this->insert_deposit_payment_data($id,$values);
        $this->insert_booking_item_data($id,$values);
        $this->insert_last_appointment_item_data($id,$values);

        if($this->db->trans_complete()){
            return $id;
        }
        return false;
    }

    function insert_product_data($id,$values){
        $item_product_model = $this->load->table_model('item_product');

        $product = $item_product_model->get(array('item_id'=>$id));
        if(! count($product)){
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->master_quantity = 0;
            $willInsert->unit_cost = $values->unit_cost;
            $item_product_model->insert($willInsert);
        }
        else{
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->unit_cost = $values->unit_cost;
            $item_product_model->update($id,$willInsert);
        }
    }

    function insert_service_data($id,$values){
        $item_service_model = $this->load->table_model('item_service');

        $service = $item_service_model->get(array('item_id'=>$id));
        if(count($service)){
            $service = $service[0];
            $willInsert = new stdClass();
            $willInsert->duration = $values->duration;
            $item_service_model->update($service->id,$willInsert);
        }
        else{
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->duration = $values->duration;
            $willInsert->status = 1;
            $item_service_model->insert($willInsert);
        }
    }

    function generateGiftCardItem($id,$values){
        $numItems = intval($values->giftcard_quantity);
        $values   = floatval($values->giftcard_values);

        $giftcardModel = $this->load->table_model('giftcard_list_detail');
        for($i=1 ; $i<=$numItems ; $i++){
            $code = substr(md5(strtotime('NOW').$i),0,15);
            if(!count($giftcardModel->get(array('code' => $code)))){
                $insertData = new stdClass();
                $insertData->code = substr(md5(strtotime('NOW').$i),0,15);
                $insertData->item_id = $id;
                $insertData->start_value = $values;
                $insertData->current_value = $values;
                $giftcardModel->insert($insertData);
            }
        }
    }

    function insert_package_data($id,$values){
        $packages_items_model = $this->load->table_model('package_item');

        $item_package_model = $this->load->table_model('item_package');
        $package = $item_package_model->get(array('item_id'=>$id));
        if(count($package)){
            $package = $package[0];
            $willInsert = new stdClass();
            $willInsert->expire_length =  isset($values->expire_length)&&$values->expire_length?$values->expire_length:null;
            $item_package_model->update($package->id,$willInsert);
        }
        else{
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->expire_length =  isset($values->expire_length)&&$values->expire_length?$values->expire_length:null;
            $willInsert->status = 1;
            $item_package_model->insert($willInsert);
        }
        $packages_items_model->delete(array('package_id' => $id));
        $item_ids = isset($values->item_kit_ids)?$values->item_kit_ids:array();
        foreach($item_ids as $item_id){
            $item_kit = new stdClass();
            $item_kit->package_id = $id;
            $item_kit->item_id = $item_id;
            $packages_items_model->insert($item_kit);
        }
    }

    function insert_bundle_data($id,$values){
        $bundle_item_model = $this->load->table_model('item_bundle_item');
        $bundle_model = $this->load->table_model('item_bundle');
        $bundle = $bundle_model->get(array('item_id'=>$id));
        if(count($bundle)){

        }else{
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $bundle_model->insert($willInsert);
        }
        $bundle_item_model->delete(array('item_id' => $id));
        $bundle_item_ids = isset($values->item_bundle_item_ids)?$values->item_bundle_item_ids:array();
        foreach($bundle_item_ids as $item_id){
            $sub_item = new stdClass();
            $sub_item->item_id = $id;
            $sub_item->sub_item_id = $item_id;
            $bundle_item_model->insert($sub_item);
        }
    }

    function insert_category_data($id,$values){
        $categories_items_model = $this->load->table_model('category_item');
        $categories_items_model->delete(array('item_id' => $id));
        $category_ids = isset($values->category_id)?$values->category_id:array();
        foreach($category_ids as $category_id){
            $category = new stdClass();
            $category->category_id = $category_id;
            $category->item_id     = $id;
            $categories_items_model->insert($category);
        }
    }

    function insert_booking_item_data($id,$values){
        $booking_items_model = $this->load->table_model('item_booking');
        $current_system = $this->config->item('current_system');
        $branch_list = $this->select(array(
            'select' => array('branch' => '*'),
            'from'   => array(
                'branch' => array('table' => 'branch')
            ),
            'where' => array('status' => 1)
        ))->result();

        $show_in_ui = array();
        if(isset($values->show_in_ui)){
            foreach($values->show_in_ui as $value)
                array_push($show_in_ui, $value);
        }
        $show_price_in_ui = array();
        if(isset($values->show_price_in_ui)){
            foreach($values->show_price_in_ui as $value)
                array_push($show_price_in_ui, $value);
        }

        $booking_item = new stdClass();
        foreach($branch_list as $branch){
            $booking_items_model->delete(array('branch_id' => $branch->id, 'item_id' => $id));
            $item = $this->select(array(
                        'select' => array('item_booking' => '*'),
                        'from'   => array(
                            'item_booking' => array('table' => 'item_booking')
                        ),
                        'where' => array('branch_id' => $branch->id,'item_id' => $id)
                    ))->result();
            if(!count($item)){
                $booking_item->branch_id   = $branch->id;
                $booking_item->item_id     = $id;
                if($current_system == "admin"){
                    $booking_item->is_show = 0;
                    $booking_item->is_price_show = 0;
                }
                else{
                    if(in_array($branch->id, $show_in_ui)) {
                        $booking_item->is_show = 1;
                        $booking_item->restrict_from_date = (isset($value->restrict_from_date) ? $value->restrict_from_date : null );
                        $booking_item->restrict_to_date = (isset($value->restrict_to_date) ? $value->restrict_to_date : null );
                        $booking_item->restrict_from_time = (isset($value->restrict_from_time) ? $value->restrict_from_time : null );
                        $booking_item->restrict_to_time = (isset($value->restrict_to_time) ? $value->restrict_to_time : null );
                    }
                    else {
                        $booking_item->is_show = 0;
                    }

                    if(in_array($branch->id, $show_price_in_ui))
                        $booking_item->is_price_show = 1;
                    else
                        $booking_item->is_price_show = 0;

                }
                $booking_items_model->insert($booking_item);
            }
        }
    }

    function update_booking_item_data($id,$values){
        $current_system = $this->config->item('current_system');
        if($current_system == "staff"){
            $booking_items_model = $this->load->table_model('item_booking');
            $current_system = $this->config->item('current_system');
            $booking_items_model->delete(array('item_id' => $id));
            $branch_list = $this->select(array(
                'select' => array('branch' => '*'),
                'from'   => array(
                    'branch' => array('table' => 'branch')
                ),
                'where' => array('status' => 1)
            ))->result();

            $show_in_ui = array();
            if(isset($values->show_in_ui)){
                foreach($values->show_in_ui as $value)
                    array_push($show_in_ui, $value);
            }
            $show_price_in_ui = array();
            if(isset($values->show_price_in_ui)){
                foreach($values->show_price_in_ui as $value)
                    array_push($show_price_in_ui, $value);
            }

            $booking_item = new stdClass();
            foreach($branch_list as $branch){
                $item = $this->db->select('*')
                                 ->where(array('branch_id' => $branch->id, 'item_id' => $id))
                                 ->get('item_booking')->row_array();
                if(!count($item)){
                    $booking_item->branch_id   = $branch->id;
                    $booking_item->item_id     = $id;

                    if(in_array($branch->id, $show_in_ui)) {
                        $booking_item->is_show = 1;
                        $booking_item->restrict_from_date = (isset($values->restrict_start_date) ? $values->restrict_start_date : null );
                        $booking_item->restrict_to_date = (isset($values->restrict_end_date) ? $values->restrict_end_date : null );
                        $booking_item->restrict_from_time = (isset($values->restrict_start_time) ? $values->restrict_start_time : null );
                        $booking_item->restrict_to_time = (isset($values->restrict_end_time) ? $values->restrict_end_time : null );
                    }
                    else {
                        $booking_item->is_show = 0;
                    }

                    if(in_array($branch->id, $show_price_in_ui))
                        $booking_item->is_price_show = 1;
                    else
                        $booking_item->is_price_show = 0;

                    $booking_item->show_date_from = $values->show_date_from;
                    $booking_item->show_time_from = $values->show_time_from;
                    $booking_item->show_date_to = $values->show_date_to;
                    $booking_item->show_time_to = $values->show_time_to;
                    $booking_item->show_date_loop = $values->show_date_loop;

                    $booking_items_model->insert($booking_item);
                }
            }
        }
    }

    function getBookingBranch($id,$getall=false,$is_price=false){
        if($getall){
            $branch_list = $this->select(array(
                'select' => array('branch' => '*'),
                'from'   => array(
                    'branch' => array('table' => 'branch')
                ),
                'where' => array('status' => 1)
            ))->result();
            $show_in_ui = array();
            foreach($branch_list as $branch){
                $temp = new stdClass();
                $temp->id = $branch->id;
                array_push($show_in_ui, $temp);
            }
            return $show_in_ui;
        }

        $item_booking_branch = $this->select(array(
                            'select' => array('item_booking' => '*'),
                            'from'   => array(
                                'item_booking' => array('table' => 'item_booking')
                            ),
                            'where' => array('item_id' => $id)
                        ))->result();
        $show_in_ui = array();
        foreach($item_booking_branch as $value){
            if($is_price){
                if($value->is_price_show){
                    $temp = new stdClass();
                    $temp->id = $value->branch_id;
                    array_push($show_in_ui, $temp);
                }
            }
            else{
                if($value->is_show){
                    $temp = new stdClass();
                    $temp->id = $value->branch_id;
                    array_push($show_in_ui, $temp);
                }
            }
        }
        return $show_in_ui;
    }


    function getItemBooking($item_id, $branch_id){
        $item_booking = $this->select(array(
            'select' => array(
                'item_booking' => '*',
            ),
            'from' => array(
                'item_booking' => array('table' => 'item_booking'),
            ),
            'where' => array(
                'item_id' => $item_id,
                'branch_id' => $branch_id,
            )
        ))->result();

        return $item_booking;
    }


    function getItemLastAppointment($id,$branch_id){
        $last_appointment = $this->select(array(
            'select' => array(
                'config_branch' => array('value'),
                'config' => array('default_inherite' => 'value')
            ),
            'from' => array(
                'config_branch' => array('table' => 'config_branch'),
                'config'        => array('table' => 'config', 'condition' => 'config_branch.config_id = config.id')
            ),
            'where' => array(
                'config_branch.branch_id' => $branch_id,
                'config.code' => 'last_app_time'
            )
        ))->result();
        if($last_appointment[0]->value == 'inherit'){
            $last_appointment = $last_appointment[0]->default_inherite;
        }
        else{
            $last_appointment = $last_appointment[0]->value;
        }
        
        $sql = $this->select(array(
                            'select' => array('item_last_appointment' => '*'),
                            'from'   => array(
                                'item_last_appointment' => array('table' => 'item_last_appointment')
                            ),
                            'where' => array('item_id' => $id,'branch_id' => $branch_id)
                        ))->result();
        if(count($sql)){
            $item_last_appointment = $sql[0]->last_appointment;
            if($item_last_appointment == "inherit")
                $item_last_appointment = $last_appointment;
        }
        else
            $item_last_appointment = $last_appointment;

        $data = array("branch_last_appointment" => $last_appointment,"item_last_appointment" => $item_last_appointment);
        
        return $data;
    }

    function getRestrictDatetime($id,$branch_id){
        $restrict = $this->select(array(
            'select' => array('item_booking' => '*'),
            'from'   => array(
                'item_booking' => array('table' => 'item_booking')
            ),
            'where' => array('branch_id' => $branch_id, 'item_id' => $id)
        ))->result();
        return (!empty($restrict))? $restrict[0]: array();
    }

    function insert_last_appointment_item_data($id,$values){        
        $item_last_appointment_model = $this->load->table_model('item_last_appointment');
        $current_system = $this->config->item('current_system');
        $branch_list = $this->select(array(
            'select' => array('branch' => '*'),
            'from'   => array(
                'branch' => array('table' => 'branch')
            ),
            'where' => array('status' => 1)
        ))->result();

        $item_last_appointment = new stdClass();
        foreach($branch_list as $branch){
            $data = $this->getItemLastAppointment($id,$branch->id);
            $item_last_appointment->item_id = $id;
            $item_last_appointment->branch_id = $branch->id;
            if(!isset($values->last_appointment))
                $values->last_appointment = "inherit";
            $item_last_appointment->last_appointment = $values->last_appointment;
            if($data["branch_last_appointment"] == $values->last_appointment)
                $item_last_appointment->last_appointment = "inherit";
            $item_last_appointment_model->insert($item_last_appointment);
        }
    }

    function update_last_appointment_item_data($id,$values){
        $item_last_appointment_model = $this->load->table_model('item_last_appointment');
        $current_system = $this->config->item('current_system');
        if($current_system != "admin"){
            $user = $this->session->userdata('login');
            $data = $this->getItemLastAppointment($id,$user->branch_id);

            $item_last_appointment = new stdClass();
            $item_last_appointment->item_id = $id;
            $item_last_appointment->branch_id = $user->branch_id;
            if(!isset($values->last_appointment))
                $values->last_appointment = "inherit";
            $item_last_appointment->last_appointment = $values->last_appointment;
            if($data["branch_last_appointment"] == $values->last_appointment)
                $item_last_appointment->last_appointment = "inherit";
            $item_last_appointment_model->delete(array('item_id' => $id,'branch_id' => $user->branch_id));
            $item_last_appointment_model->insert($item_last_appointment);
        }
    }

    function renderDataSet($data){
        foreach($data as $row){
            if($row->status != Status::Disable){
                $row->is_not_disable = true;
            }
        }
        return $data;
    }

    function disable_item($id){
        $item = $this->load->table_model('item')->getByID($id,'','',array('status' => $this->entity_status));
        if(!isset($item)){
            throw new Exception("Can't find the item");
        }
        $item->status = Status::Disable;
        $result = $this->load->table_model('item')->update($id,$item);
        if(!$result) {
            throw new Exception("Can't disable the item");
        }
    }

    function enable_item($id){
        $item = $this->load->table_model('item')->getByID($id,'','',array('status' => $this->entity_status));
        if(!isset($item)){
            throw new Exception("Can't find the item");
        }
        $item->status = Status::Active;
        $result = $this->load->table_model('item')->update($id,$item);
        if(!$result) {
            throw new Exception("Can't enable item");
        }
    }

    function insert_deposit_payment_data($id, $values){
        $em = $this->load->table_model('deposit_payment_item');
        $check = $em->get(array('item_id' => $id));
        if(count($check) > 0){
            if(isset($values->minimum_deposit) && isset($values->remain_payment_time)){
                $em->update($check[0]->id,array('min_cash_payment' => $values->minimum_deposit,
                                                'payment_time' => $values->remain_payment_time));
            }
        }
        else{
            $em->insert(array('item_id' => $id,
                              'min_cash_payment' => $values->minimum_deposit,
                              'payment_time' => $values->remain_payment_time,
                              'amount_payment' => 0));
        }
    }

}