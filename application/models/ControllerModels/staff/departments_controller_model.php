<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Departments_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "department";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function edit_permissions($id,$permissions){
        $department_permission_model = $this->load->table_model('department_permission');
        $this->db->trans_start();
        $department_permission_model->delete(array('department_id' => $id));
        foreach($permissions as $permission){
            $department_permission_model->insert(array('department_id' => $id, 'module_permission_id' => $permission));
        }
        $this->db->trans_complete();
    }

    function insert($values){
        if(is_array($values))
            $values = (object)$values;
        $this->db->trans_start();
        $id = $this->load->table_model('department')->insert($values);
        $group_ids = $values->group_ids;
        foreach($group_ids as $group_id){
            $this->load->table_model('department_group')->insert(array('group_id'=>$group_id, 'department_id' => $id));
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
           return 'Insert error';
        }else{
            return true;
        }

    }

    function update($id, $values){
        if(is_array($values))
            $values = (object)$values;
        $this->db->trans_start();
        $this->load->table_model('department')->update($id,$values);
        $group_ids = $values->group_ids;
        $this->load->table_model('department_group')->delete(array('department_id' => $id));
        foreach($group_ids as $group_id){
            $this->load->table_model('department_group')->insert(array('group_id'=>$group_id, 'department_id' => $id));
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return 'Update Error';
        }else{
            return true;
        }
    }

    function getDataForViewForm($id){
        $modelName = $this->load->table_model('department');
        $data = parent::getDataForViewForm($id);
        if(isset($data->item)){
            $data->item->groups = $modelName->select(array(
                'select'    =>  'group_id',
                'from'      => array('department_group' => array('table' => 'department_group')),
                'where'     => array('department_id' => $data->item->id),
            ))->result();
            $data->item->groups = convert_to_array($data->item->groups,'','group_id');

        }
        $data->groups = $this->load->table_model('group')->get();
        return $data;
    }

}