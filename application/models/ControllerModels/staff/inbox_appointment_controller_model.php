<?php

class Inbox_Appointment_Controller_Model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getData(){
        $currentUserDate = get_user_date('NOW','','Y-m-d H:i:s');
        $config_model = $this->load->table_model('config');
        $timezone = $config_model->get(array('code' => 'timezone'))[0]->value;
        $currentServerDate = date('Y-m-d', strtotime(get_database_date($currentUserDate,$timezone)));
        $data = array();
        $data[0] = $this->select(array(
            'select' => array(
                'booking' => array('bookingDate' => 'created_date','code','is_new'),
                'booking_service' => array('start_time','end_time'),
                'customer' => array('first_name', 'last_name', 'mobile_number', 'email'),
                'item' => array('itemName' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service','condition' => 'booking.id = booking_service.booking_id','LEFT'),
                'customer' => array('table' => 'customer', 'condition' => 'booking.customer_id = customer.id','LEFT'),
                'item' => array('table' => 'item','condition' => 'booking_service.service_id = item.id','LEFT')
            ),
            'where' => [
                'booking.branch_id' => $this->session->all_userdata()['staff']['login']->branch_id,
                'booking.status' => array(
                    BOOKING_STATUS('Complete'),
                    BOOKING_STATUS('Payment'),
                    BOOKING_STATUS('HoldBill')
                ),
                'booking.type'   => array(BOOKING_TYPE('Online')),
                'DATE(booking_service.start_time)' => $currentServerDate
            ]
        ))->result();

        $data[1] = $this->select(array(
            'select' => array(
                'booking' => array('bookingDate' => 'created_date','code','is_new'),
                'booking_service' => array('start_time','end_time'),
                'customer_temp' => array('first_name', 'last_name', 'mobile_number', 'email'),
                'item' => array('itemName' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service','condition' => 'booking.id = booking_service.booking_id','LEFT'),
                'customer_temp' => array('table' => 'customer_temp', 'condition' => 'booking.customer_id = customer_temp.id','LEFT'),
                'item' => array('table' => 'item','condition' => 'booking_service.service_id = item.id','LEFT')
            ),
            'where' => [
                'booking.branch_id' => $this->session->all_userdata()['staff']['login']->branch_id,
                'booking.status' => array(
                    BOOKING_STATUS('Hold')
                ),
                'booking.type'   => array(BOOKING_TYPE('Online')),
                'DATE(booking_service.start_time)' => $currentServerDate
            ]
        ))->result();
        $result = array_merge($data[0],$data[1]);
        if(count($result) > 0){
            foreach($result as $key => $item){
                $result[$key]->start_time = get_user_date($item->start_time,'','Y-m-d H:i');
                $result[$key]->end_time = get_user_date($item->end_time,'','Y-m-d H:i');
                $result[$key]->bookingDate = get_user_date($item->bookingDate,'','Y-m-d H:i:s');
                $result[$key]->name = $item->first_name.' '.$item->last_name;
                unset($result[$key]->first_name);
                unset($result[$key]->last_name);
            }
            return $result;
        }
        else{
            return $result;
        }
    }
}