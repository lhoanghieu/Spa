<?php

class Room_Type_Controller_model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "room_type";
        $this->suggestionSearch = array('code', 'name');
        $this->suggestionDisplay = array('code', 'name');
    }

    function renderDataSet($data){
        foreach($data as $each_category){
            $each_category->category_list_string = implode(', ',convert_to_array($each_category->category_list,'','name'));
        }
        return $data;
    }

    function update($id, $values,$constraint_check = true){
        $values = (array)$values;
        $this->db->trans_start();
        parent::update($id,$values,$constraint_check);
        $room_type_category_model = $this->load->table_model('room_type_category');
        $current_category_id = $room_type_category_model->getTableMap('','category_id',array('room_type_id' => $id));
        $room_type_category_model->delete(array('room_type_id' => $id,'category_id' => $current_category_id));
        if(isset($values['category_id_list'])){
            foreach(t_array($values['category_id_list']) as $category_id){
                $room_type_category_model->insert(array('category_id' => $category_id, 'room_type_id' => $id));
            }
        }
        $this->db->trans_complete();
        return true;
    }

    function insert($values,$user_level = '', $permission = array(),$force_insert = false){
        $values = (array)$values;
        $this->db->trans_start();
        $id = parent::insert($values,$user_level,$permission,$force_insert);
        $room_type_category_model = $this->load->table_model('room_type_category');
        if(isset($values['category_id_list'])){
            foreach(t_array($values['category_id_list']) as $category_id){
                $room_type_category_model->insert(array('category_id' => $category_id, 'room_type_id' => $id));
            }
        }
        $this->db->trans_complete();
        return $id;
    }

}