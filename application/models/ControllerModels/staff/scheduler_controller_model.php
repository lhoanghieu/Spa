<?php

class Scheduler_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "employee_scheduler";
        $this->suggestionSearch = array();
        $this->suggestionDisplay = array();
    }

    function getDataForCalendar($type, $condition = array(), $dp_mode = 0)
    {
        $data = array();
        $data = array_merge($data, $this->getBookingData($type, $condition, $dp_mode));
        $start_time = $this->system_config->get('start_time');
        $end_time = $this->system_config->get('end_time');
        $store_open_hour = array();
        //Store open and close
        //Get start date
        $temp_H = date('H', strtotime($start_time));
        $temp_M = date('i', strtotime($start_time));
        $store_open_hour[] = $temp_H + ($temp_M / 60);
        //Get end time
        $temp_H = date('H', strtotime($end_time));
        $temp_M = date('i', strtotime($end_time));
        $store_open_hour[] = $temp_H + ($temp_M / 60);
        $data['store_open_hour'] = $store_open_hour;
        return $data;
    }

    private function _getBookingDataEmployee($type, $condition = array(), $dp_mode = 0)
    {
        if (isset($condition['filter_date'])) {
            if($type == 'all'){

            }
            else if($type == 'employee'){
                if(date('D',strtotime($condition['filter_date'])) == 'Sun'){
                    $condition['filter_date'] = date('Y-m-d', strtotime($condition['filter_date']) - (3600*24));
                }
                $condition['filter_date'] = date('Y-m-d', strtotime('monday this week',strtotime($condition['filter_date'])));
            }
            $selected_date = $condition['filter_date'];
        }
        if ($dp_mode == 0) {
            $status_arr = array(BOOKING_STATUS('Complete'), BOOKING_STATUS('Hold'), BOOKING_STATUS('Payment'), BOOKING_STATUS('HoldBill'));
        } else if ($dp_mode == 1) {
            $status_arr = array(BOOKING_STATUS('Reserve'));
        }
        $condition = array_merge($condition,
            array('booking' => array('branch_id' => $this->user_check->get('branch_id'), 'status' => $status_arr))
        );
        if ($type == 'all') {
            $bookings = $this->load->table_model('booking')->get_detail_scheduler($condition);
        } else if ($type == 'employee') {
            $bookings = $this->load->table_model('booking')->get_detail_for_employee($condition);
        }
        $temp = array();
        $room_ids = array();
        $service_ids = array();
        $customer_ids = array();
        $bundle_ids = array();
        foreach ($bookings as $booking_fields) {
            if ($booking_fields->bundle_id) {
                $bundle_ids[] = $booking_fields->bundle_id;
            }
            if (isset($booking_fields->services)) {
                foreach ($booking_fields->services as $service) {
                    $service->start_time = get_user_date($service->start_time, '', 'Y-m-d H:i:s');
                    $service->end_time = get_user_date($service->end_time, '', 'Y-m-d H:i:s');
                    $room_ids[] = $service->room_id;
                    $service_ids[] = $service->service_id;
                }
            } else {
                $booking_fields->start_time = get_user_date($booking_fields->start_time, '', 'Y-m-d H:i:s');
                $booking_fields->end_time = get_user_date($booking_fields->end_time, '', 'Y-m-d H:i:s');
                $room_ids[] = $booking_fields->room_id;
                $service_ids[] = $booking_fields->service_id;
            }
            if ($booking_fields->status == BOOKING_STATUS('Hold') || $booking_fields->status == BOOKING_STATUS('Reserve')) {
                $customer_temp_ids[] = $booking_fields->customer_id;
            }
            if ($booking_fields->status != BOOKING_STATUS('Hold')) {
                $customer_ids[] = $booking_fields->customer_id;
            }
            $temp[$booking_fields->id] = $booking_fields;
        }

        $bookings = $temp;
        $branch_id = $this->user_check->get('branch_id');
        $employees = $this->select(array(
            'select' => array(
                'employee' => array('id', 'first_name', 'last_name', 'ordering', 'department_id'),
                'employee_branch' => array('branch_id')
            ),
            'from' => array(
                'employee' => array('table' => 'employee'),
            ),
            'where' => array(
                'department_id' => $this->load->table_model('department')->get_department_serve_booking()
            ),
            'order' => "ordering asc"
        ))->result();

        /*
        Added by Tran Minh Thao -- Begin
        Reupdate employee's ordering by employee scheduler table
        1. Get all ordering of this branch base on user login's branch
         */
        $employee_scheduler = $this->select(array(
            'select' => array(
                'employee_scheduler' => array('employee_id', 'branch_id', 'department_id', 'ordering')
            ),
            'from' => array(
                'employee_scheduler' => array('table' => 'employee_scheduler'),
            ),
            'where' => array(
                'branch_id = ' . $branch_id
            ),
            'order' => "ordering asc"
        ))->result();

        // 2. Reupdate employee's ordering by employee scheduer table
        for ($i = 0; $i < count($employees); $i++) {
            foreach ($employee_scheduler as $key => $value) {
                if (($value->employee_id == $employees[$i]->id) && $value->branch_id == $branch_id && ($value->department_id == $employees[$i]->department_id))
                    $employees[$i]->ordering = $value->ordering;
            }
        }

        /*
        Added by Tran Minh Thao -- End
         */

        $employees = convert_to_array($employees, 'id');
        $rooms = $this->load->table_model('room')->getTableMap('id', '');
        $services = $this->load->table_model('item')->getTableMap('id', '', array('id' => $service_ids));
        $bundle_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $bundle_ids));
        $customers = $this->load->table_model('customer')->getTableMap('id', '', array('id' => $customer_ids));

        if (!empty($customer_temp_ids)) {
            $customers_temp_id = $this->load->table_model('customer_temp')->getTableMap('id', '', array('id' => $customer_temp_ids));
        }

        $employees_absent = $this->load->table_model('employee_absent')->getTableMap('employee_id', '', array('employee_id' => convert_to_array($employees, '', 'id')), false);
        $employees_calendar = $this->load->table_model('employee_calendar')->getTableMap('employee_id', '', array('employee_id' => convert_to_array($employees, '', 'id'), 'branch_id' => $this->user_check->get('branch_id')), false);
        if ($type == 'employee') {
            $dayofweek = date('w', strtotime($condition["filter_date"]));
            $temp_date = new DateTime($condition["filter_date"]);
            $temp_date->modify('-' . ($dayofweek) . ' day');
            $filter_date = $temp_date->format('Y-m-d');
        } else
            $filter_date = $condition["filter_date"];
        foreach ($employees as $key => $employee) {
            $employees[$key]->full_name = to_full_name($employee->first_name, $employee->last_name);
            //Ready block calendar date data
            $blocked_calendar_date = array(
                0 => array(), 1 => array(), 2 => array(), 3 => array(),
                4 => array(), 5 => array(), 6 => array(), 7 => array(),
            );
            if (isset($employees_calendar[$key])) {
                foreach ($employees_calendar[$key] as $fields) {
                    $search_date = $filter_date;
                    // Get optional working day and hours of employees if exist
                    if ($type == 'employee') {
                        $temp_date = new DateTime($search_date);
                        $temp_date->modify('+' . ($fields->day_id) . ' day');
                        $search_date = $temp_date->format('Y-m-d');
                    }
                    $employee_calendar_customize = $this->select(array(
                        'select' => array('employee_calendar_customize' => '*'),
                        'from' => array(
                            'employee_calendar_customize' => array('table' => 'employee_calendar_customize')
                        ),
                        'where' => array(
                            'employee_calendar_customize.employee_id' => $fields->employee_id,
                            'employee_calendar_customize.branch_id' => $fields->branch_id,
                            'employee_calendar_customize.date' => $search_date
                        )
                    ))->result();
                    if (count($employee_calendar_customize)) {
                        $fields->start_time = $employee_calendar_customize[0]->start_time;
                        $fields->end_time = $employee_calendar_customize[0]->end_time;
                    }

                    //Get start date
                    $blocked_calendar_date[$fields->day_id][] = 0;
                    $begin_date_time = new DateTime('1970-01-01 00:00:00');
                    //Get end date
                    $start_time = new DateTime('1970-01-01 ' . $fields->start_time);
                    $diff = $start_time->diff($begin_date_time);
                    $blocked_calendar_date[$fields->day_id][] = ($diff->h ? $diff->h : 1) * 60 + $diff->i;
                    //Get end time
                    $start_time = new DateTime('1970-01-01 ' . $fields->end_time);
                    $diff = $start_time->diff($begin_date_time);
                    $blocked_calendar_date[$fields->day_id][] = ($diff->h ? $diff->h : 1) * 60 + $diff->i;
                    $end_date_time = new DateTime('1970-01-01 23:59:59');
                    $diff = $end_date_time->diff($begin_date_time);
                    $blocked_calendar_date[$fields->day_id][] = ($diff->h ? $diff->h : 1) * 60 + $diff->i;

                    if (count($employee_calendar_customize)) {
                        if ($employee_calendar_customize[0]->working != 1)
                            $blocked_calendar_date[$fields->day_id] = array();
                    }
                }
            }

            $employees[$key]->blocked_calendar_date = $blocked_calendar_date;
            //Ready absent date data
            $blocked_absent_date = array();
            if (isset($employees_absent[$key])) {
                foreach ($employees_absent[$key] as $fields) {
                    $absent_day = array();
                    $absent_day['id'] = $fields->id;
                    $absent_day['start_time'] = get_user_date($fields->start_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
                    $absent_day['end_time'] = get_user_date($fields->end_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
                    $absent_day['type'] = array('code' => $fields->type, 'label' => ABSENT_TYPE($fields->type));
                    $absent_day['description'] = isset($fields->description) ? $fields->description : '';
                    $blocked_absent_date[] = $absent_day;
                }
            }
            $employees[$key]->blocked_absent_date = $blocked_absent_date;
        }
        foreach ($customers as $key => $customer) {
            $customers[$key]->full_name = to_full_name($customer->first_name, $customer->last_name);
        }

        if (!empty($customers_temp_id)) {
            foreach ($customers_temp_id as $key => $customer) {
                $customers_temp_id[$key]->full_name = to_full_name($customer->first_name, $customer->last_name);
            }
        }

        $branch_offline_data = $this->load->table_model('branch_offline')->get();
        foreach ($branch_offline_data as &$row) {
            $row->start_time = get_user_date($row->start_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
            $row->end_time = get_user_date($row->end_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
        }
        foreach ($bookings as $booking_id => $booking_fields) {
            $booking_fields->created_date = get_user_date($booking_fields->created_date, $this->system_config->get('timezone'), 'Y-m-d H:i:s');
        }
        if (isset($condition['id'])) {
            $booking = $bookings[$condition['id']];
            if (isset($booking->services)) {
                foreach ($booking->services as $service) {

                    $service->room_detail = $rooms[$service->room_id];
                    $service->employee_detail = $employees[$service->employee_id];
                    $service->service_detail = $services[$service->service_id];
                    $service->section_id = $employees[$service->employee_id]->id;
                }
            } else {
                $booking->room_detail = $rooms[$booking->room_id];
                $booking->employee_detail = $employees[$booking->employee_id];
                $booking->service_detail = $services[$booking->service_id];
                $booking->section_id = $employees[$booking->employee_id]->id;
            }
            if ($booking->status != 2) {
                $bookings[$booking->id]->customer_detail = $customers[$booking->customer_id];
            } else {
                $booking[$booking->id]->customer_detail = $customers_temp_id[$booking->customer_id];
            }
            if (isset($booking->bundle_id)) {
                $bookings[$booking->id]->bundle_detail = $bundle_map[$booking->bundle_id];
            }
            return $booking;
        } else {
            foreach ($bookings as $booking_key => $booking_field) {
                if (isset($booking_field->services)) {
                    $bookings[$booking_key]->bundle_detail = $bundle_map[$booking_field->bundle_id];
                    foreach ($booking_field->services as $service) {
                        $service->room_detail = $rooms[$service->room_id];
                        $service->employee_detail = $employees[$service->employee_id];
                        $service->service_detail = $services[$service->service_id];
                        $service->section_id = $employees[$service->employee_id]->id;
                    }
                } else {
                    if (!isset($employees[$booking_field->employee_id])) {
                        unset($bookings[$booking_key]);
                        continue;
                    }
                    $temp_room = $rooms;
                    unset($temp_room[$booking_field->room_id]->services);
                    $bookings[$booking_key]->room_detail = isset($temp_room[$booking_field->room_id]) ? $temp_room[$booking_field->room_id] : '';
                    $bookings[$booking_key]->employee_detail = $employees[$booking_field->employee_id];
                    $bookings[$booking_key]->service_detail = $services[$booking_field->service_id];
                    $bookings[$booking_key]->section_id = $employees[$booking_field->employee_id]->id;
                }
                if ((intval($booking_field->status) == BOOKING_STATUS('Reserve'))) {
                    if ($booking_field->old_status == 1) {
                        $bookings[$booking_key]->customer_detail = isset($customers[$booking_field->customer_id]) ? $customers[$booking_field->customer_id] : '';
                    } else if ($booking_field->old_status == 2) {
                        if ($booking_field->type == 1) {
                            $bookings[$booking_key]->customer_detail = isset($customers_temp_id[$booking_field->customer_id]) ? $customers_temp_id[$booking_field->customer_id] : '';
                        } else if ($booking_field->type == 2) {
                            $bookings[$booking_key]->customer_detail = isset($customers[$booking_field->customer_id]) ? $customers[$booking_field->customer_id] : '';
                        }
                    } else {
                        if ((intval($booking_field->status) != BOOKING_STATUS('Hold'))) {
                            $bookings[$booking_key]->customer_detail = isset($customers[$booking_field->customer_id]) ? $customers[$booking_field->customer_id] : '';
                        } else {
                            $bookings[$booking_key]->customer_detail = isset($customers_temp_id[$booking_field->customer_id]) ? $customers_temp_id[$booking_field->customer_id] : '';
                        }
                    }
                } else if ((intval($booking_field->status) != BOOKING_STATUS('Hold'))) {
                    $bookings[$booking_key]->customer_detail = isset($customers[$booking_field->customer_id]) ? $customers[$booking_field->customer_id] : '';
                } else {
                    $bookings[$booking_key]->customer_detail = isset($customers_temp_id[$booking_field->customer_id]) ? $customers_temp_id[$booking_field->customer_id] : '';
                }
            }

            $employee_map = $employees;
            $employees = convert_to_array($employees, 'ordering');

            $booking_comment_condition['booking.status'] = array(BOOKING_STATUS('Comment'), BOOKING_STATUS('Away'), BOOKING_STATUS('Block'), BOOKING_STATUS('Offline'));
            $booking_comment_condition['booking.branch_id'] = $branch_id;
            $booking_comment_condition['DATE(booking_service.start_time) <'] = $selected_date;
            $booking_comment_condition['DATE(booking_service.end_time) >'] = $selected_date;
            if (!empty($condition['booking_service']['employee_id'])) {
                $booking_comment_condition['booking_service.employee_id'] = $condition['booking_service']['employee_id'];
            }

            if ($type == 'all') {

            } else if ($type == 'employee') {
                $check_time = strtotime($selected_date);
                $booking_comment_condition['DATE(booking_service.start_time) <'] = date('Y-m-d', strtotime('this Sunday', $check_time));
                $booking_comment_condition['DATE(booking_service.end_time) >'] = date('Y-m-d', strtotime('this Monday', $check_time));
            }
            $booking_comment = $this->select(array(
                'select' => array(
                    'booking' => array('booking_id' => 'id', 'code', 'text' => 'comment', 'status', 'created_date'),
                    'booking_service' => array('section_id' => 'employee_id', 'start_time', 'end_time')
                ),
                'from' => array(
                    'booking' => array('table' => 'booking'),
                    'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id=booking_service.booking_id')
                ),
                'where' => $booking_comment_condition
            ))->result();
            $employees_compare = convert_to_array($employee_map, 'id');
            foreach ($booking_comment as $key => $item) {
                if (isset($employees_compare[$item->section_id])) {
                    $item->start_date = get_user_date($item->start_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
                    $item->end_date = get_user_date($item->end_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
                } else {
                    unset($booking_comment[$key]);
                }
            }
            $booking_comment = array_values($booking_comment);
            return array('bookings' => $bookings, 'bookings_comment' => $booking_comment, 'employee_map' => $employee_map, 'employees' => $employees, 'rooms' => $rooms, 'branch_data' => array('branch_offline_data' => $branch_offline_data));
        }
    }

    private function _getBookingDataRoom($condition = array())
    {
        $condition = array_merge($condition,
            array('booking' => array(
                'branch_id' => $this->user_check->get('branch_id'), 'status' => array(BOOKING_STATUS('Complete'), BOOKING_STATUS('Hold'))
            ))
        );
        $bookings = $this->load->table_model('booking')->get_detail($condition);
        $temp = array();
        $room_ids = array();
        $service_ids = array();
        $customer_ids = array();
        foreach ($bookings as $booking_fields) {
            $booking_fields->start_time = get_user_date($booking_fields->start_time, $this->system_config->get('timezone'), 'Y-m-d H:i:s');
            $booking_fields->end_time = get_user_date($booking_fields->end_time, $this->system_config->get('timezone'), 'Y-m-d H:i:s');
            $temp[$booking_fields->id] = $booking_fields;
            $room_ids[] = $booking_fields->room_id;
            $service_ids[] = $booking_fields->service_id;
            $customer_ids[] = $booking_fields->customer_id;
        }
        $bookings = $temp;
        $employees = $this->load->table_model('employee')->select(array(
            'select' => array(
                'employee' => array('id', 'first_name', 'last_name'),
                'employee_branch' => array('branch_id')
            ),
            'from' => array(
                'employee' => array('table' => 'employee'),
            ),
            'where' => array(
                'department_id' => $this->load->table_model('department')->get_department_serve_booking()
            )
        ))->result();
        $employees = convert_to_array($employees, 'id');
        $rooms = $this->load->table_model('room')->getTableMap('id', '', array());
        $services = $this->load->table_model('item')->getTableMap('id', '', array('id' => $service_ids));
        $customers = $this->load->table_model('customer')->getTableMap('id', '', array('id' => $customer_ids));

        foreach ($employees as $key => $employee) {
            $employees[$key]->full_name = to_full_name($employee->first_name, $employee->last_name);
        }
        foreach ($customers as $key => $customer) {
            $customers[$key]->full_name = to_full_name($customer->first_name, $customer->last_name);
        }

        $branch_offline_data = $this->load->table_model('branch_offline')->get();
        foreach ($branch_offline_data as &$row) {
            $row->start_time = get_user_date($row->start_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
            $row->end_time = get_user_date($row->end_time, $this->system_config->get('timezone'), DATABASE_DATE_FORMAT . ' ' . DATABASE_TIME_FORMAT);
        }
        foreach ($bookings as $booking_id => $booking_fields) {
            $booking_fields->created_date = get_user_date($booking_fields->created_date, $this->system_config->get('timezone'), 'Y-m-d H:i:s');
        }
        if (isset($condition['id'])) {
            $booking = $bookings[$condition['id']];
            $booking->room_detail = $rooms[$booking->room_id];
            $booking->employee_detail = $employees[$booking->employee_id];
            $booking->service_detail = $services[$booking->service_id];
            $booking->customer_detail = $customers[$booking->customer_id];
            $booking->section_id = $employees[$booking->employee_id]->id;
            return $booking;
        } else {
            foreach ($bookings as $booking_key => $booking_field) {
                $bookings[$booking_key]->room_detail = $rooms[$booking_field->room_id];
                $bookings[$booking_key]->employee_detail = $employees[$booking_field->employee_id];
                $bookings[$booking_key]->service_detail = $services[$booking_field->service_id];
                $bookings[$booking_key]->customer_detail = $customers[$booking_field->customer_id];
                $bookings[$booking_key]->section_id = $employees[$booking_field->employee_id]->id;
            }
            $employee_map = $employees;
            return array('bookings' => $bookings, 'employee_map' => $employee_map, 'employees' => $employees, 'rooms' => $rooms, 'branch_data' => array('branch_offline_data' => $branch_offline_data));
        }
    }

    function getBookingData($type = "all", $condition = array(), $dp_mode = 0)
    {
        if ($type == "all" || $type == "employee") {
            return $this->_getBookingDataEmployee($type, $condition, $dp_mode);
        } else if ($type == "room") {
            return $this->_getBookingDataRoom($condition);
        }
    }

    function _getAvailEmployee($start_date_time, $service_id, $old_data = array(), $permission = null)
    {
        if (!$permission) {
            $permission = array(
                'user_level' => '',
                'field' => array()
            );
        }

        $timezone = $this->system_config->get('timezone', $permission);

        if (is_object($old_data)) {
            $old_data = (array)$old_data;
        }
        $condition = array();
        if (isset($old_data['id'])) {
            $condition = array("booking.id != '{$old_data['id']}'");
        }

        $service_duration = $this->load->table_model('item_service')->get(array('item_id' => $service_id));
        $service_duration = count($service_duration) ? $service_duration[0]->duration : 0;

        $end_date_time = $start_date_time + $service_duration * 60;
        $dayWeek = date('w', $start_date_time);
        $start_time = date('H:i', $start_date_time);
        $end_time = date('H:i', $end_date_time);

        $st = date('Y-m-d H:i:s', $start_date_time);
        $et = date('Y-m-d H:i:s', $end_date_time);

        $branch_id = isset($permission['field']['branch_id']) ? $permission['field']['branch_id'] : $this->user_check->get('branch_id');


        $employee_with_service_avail_id = $this->load->table_model('employee')->get_employee_serve_service($service_id, $branch_id);
        $employee_list = $this->select(array(
            'select' => array('employee' => '*'),
            'from' => array(
                'employee' => array('table' => 'employee'),
                'employee_calendar' => array('table' => 'employee_calendar', 'condition' => 'employee.id = employee_calendar.employee_id')
            ),
            'where' => array(
                "department_id" => $this->load->table_model('department')->get_department_serve_booking(),//$this->system_config->get('therapist',$permission)),
                'employee_calendar.day_id' => $dayWeek,
                "employee_calendar.start_time <= '{$start_time}'",
                "employee_calendar.end_time >= '{$end_time}'",
                'employee_calendar.branch_id' => $branch_id,
                'employee.id' => $employee_with_service_avail_id
            ),
            'group' => 'employee.id',
        ))->result();

        $employee_list = convert_to_array($employee_list, 'id');
        $st = get_database_date($st, $timezone);
        $et = get_database_date($et, $timezone);

        /* ---- Check if in branch working time ----*/
        $blocked_date = $this->load->table_model('branch_offline')->get_minify(array(
            'condition' => array("((start_time < '{$st}' AND end_time > '{$st}')
            OR (start_time > '{$st}' AND start_time < '{$et}' AND end_time > '{$st}'))",
                'branch_id' => isset($permission['field']['branch_id']) ? $permission['field']['branch_id'] : null
            ),
        ));
        if (count($blocked_date)) {
            return array();
        }

        /* ---- End of check if in branch working time ----*/

        $booked = $this->load->table_model('booking_service')->get_booking_range(array(
                'start_time' => $st,
                'end_time' => $et,
                'condition' => array_merge($condition, array('employee_id' => convert_to_array($employee_list, '', 'id'))),
                'permission' => $permission
            )
        );
        $booked = convert_to_array($booked, 'employee_id', '', false);

        $start_time = get_database_date(date('Y-m-d H:i:s', $start_date_time + 1), $timezone);
        $end_time = get_database_date(date('Y-m-d H:i:s', $end_date_time - 1), $timezone);
        $b_time = get_database_date(date('Y-m-d', $start_date_time), $timezone);
        $absent = 'SELECT booking.id, booking_service.employee_id, booking_service.start_time, booking_service.end_time, booking.status,booking.branch_id
                    FROM booking JOIN booking_service ON booking.id = booking_service.booking_id
                    WHERE booking.branch_id = ' . $branch_id . '
                    AND booking.status IN (6 ,7,2, 1, 8,9,4,5)
                    AND ((booking_service.start_time < "' . $start_time . '" AND booking_service.end_time > "' . $start_time . '")
                      OR (booking_service.start_time < "' . $end_time . '" AND booking_service.end_time > "' . $end_time . '")
                      OR (booking_service.start_time > "' . $start_time . '" AND booking_service.end_time < "' . $end_time . '"))';
        $absent = $this->db->query($absent)->result();

        $absent = convert_to_array($absent, 'employee_id', false);
        $return_array = array();

        foreach ($employee_list as $key => $employee) {
            if (!isset($booked[$key]) && !isset($absent[$key])) {
                // Search to check if employee not assigned to branch
                $employee_branch_search = $this->select(array(
                    'select' => array('employee_branch' => '*'),
                    'from' => array(
                        'employee_branch' => array('table' => 'employee_branch')
                    ),
                    'where' => array(
                        'employee_branch.employee_id' => $employee->id
                    )
                ))->result();

                if (count($employee_branch_search)) {
                    $employee->name = to_full_name($employee->first_name, $employee->last_name);
                    $return_array[$key] = $employee;
                }
            }
        }
        return $return_array;
    }

    function _getAvailEmployeeWithoutCheck($start_date_time, $service_id, $old_data = array(), $permission = null)
    {
        if (!$permission) {
            $permission = array(
                'user_level' => '',
                'field' => array()
            );
        }
        $timezone = $this->system_config->get('timezone', $permission);

        if (is_object($old_data)) {
            $old_data = (array)$old_data;
        }
        $condition = array();
        if (isset($old_data['id'])) {
            $condition = array("booking.id != '{$old_data['id']}'");
        }

        $service_duration = $this->load->table_model('item_service')->get(array('item_id' => $service_id));
        $service_duration = count($service_duration) ? $service_duration[0]->duration : 0;

        $end_date_time = $start_date_time + $service_duration * 60;

        $st = date('Y-m-d H:i:s', $start_date_time);
        $et = date('Y-m-d H:i:s', $end_date_time);

        $branch_id = isset($permission['field']['branch_id']) ? $permission['field']['branch_id'] : $this->user_check->get('branch_id');
        $employee_with_service_avail_id = $this->load->table_model('employee')->get_employee_serve_service($service_id, $branch_id);
        $employee_list = $this->select(array(
            'select' => array('employee' => '*'),
            'from' => array(
                'employee' => array('table' => 'employee'),
                'employee_calendar' => array('table' => 'employee_calendar', 'condition' => 'employee.id = employee_calendar.employee_id')
            ),
            'where' => array(
                //     "department_id"            => $this->system_config->get(array('therapist','beautician'),$permission),
                'employee_calendar.branch_id' => $branch_id,
                'employee.id' => $employee_with_service_avail_id
            ),
            'group' => 'employee.id',
        ))->result();

        $employee_list = convert_to_array($employee_list, 'id');

        $st = get_database_date($st, $timezone);
        $et = get_database_date($et, $timezone);

        $booked = $this->load->table_model('booking_service')->get_booking_range(array(
                'start_time' => $st,
                'end_time' => $et,
                'condition' => array_merge($condition, array('employee_id' => convert_to_array($employee_list, '', 'id'))),
                'permission' => $permission
            )
        );

        $booked = convert_to_array($booked, 'employee_id', '', false);
        $absent = $this->load->table_model('employee_absent')->get_absent_range(
            array(
                'start_time' => $st,
                'end_time' => $et,
                'condition' => array('employee_id' => convert_to_array($employee_list, '', 'id')),
                'permission' => $permission
            )
        );

        $absent = convert_to_array($absent, 'employee_id', false);
        $return_array = array();
        if (isset($booked[$old_data['employee_id']])) {
            unset($booked[$old_data['employee_id']]);
        }

        foreach ($employee_list as $key => $employee) {
            if (!isset($booked[$key]) && !isset($absent[$key])) {
                $employee->name = to_full_name($employee->first_name, $employee->last_name);
                $return_array[$key] = $employee;
            }
        }
        return $return_array;
    }

    function setRoomtoBook($booking_id, $room_id)
    {
        if (!empty($booking_id) && !empty($room_id)) {
            $id = $this->select(array(
                'select' => array(
                    'booking_service' => array('id')
                ),
                'from' => array(
                    'booking_service' => array('table' => 'booking_service')
                ),
                'where' => array(
                    'booking_id' => $booking_id
                )
            ))->result()[0];
            $this->load->table_model('booking_service')->update(array('id' => intval($id->id)), array('room_id' => intval($room_id)));
        }
    }

    public function _getAvailRoom($start_date_time, $service_id, $old_data = array(),$permission = null){
        if(!empty($this->session->all_userdata()['staff'])){
            $sessionData = $this->session->all_userdata()['staff'];
            if(!empty($sessionData) && !empty($sessionData['login'])){
                $branch_id = $sessionData['login']->branch_id;
            }
        }

        $session_data = $this->session->userdata('booking_data');
        $currentUniqueId = $this->session->userdata('currentUniqueId');
        $isBooking = false;
        if (!empty($session_data[$currentUniqueId]) && $session_data[$currentUniqueId]->branch_id > 0) {
            $branch_id = $session_data[$currentUniqueId]->branch_id;
            $isBooking = true;
        }
        if (!$permission) {
            $permission = array(
                'user_level' => '',
                'field' => array()
            );
        }
        $timezone = $this->system_config->get('timezone', $permission);
        if (is_object($old_data)) {
            $old_data = (array)$old_data;
        } elseif (is_numeric($old_data)) {
            $booking_id = $old_data;
            $old_data = $this->load->table_model('booking')
                ->get_detail(array('booking' => array('id' => $booking_id, 'status' => array(BOOKING_STATUS('Complete'), BOOKING_STATUS('Hold')), 'is_bypass' => 0)));
            if (count($old_data)) {
                $old_data = $old_data[0];
                $old_data = (array)$old_data;
            } else {
                $old_data = array();
            }
        }
        $condition = array();

        if (isset($old_data['bundle_id'])) {
            $condition['booking.id !'] = $old_data['id'];
        } elseif (isset($old_data['room_id'])) {
            $condition['booking.id !'] = $old_data['id'];
        }

        $service_duration = $this->load->table_model('item_service')->get(array('item_id' => $service_id));
        $service_duration = count($service_duration) ? $service_duration[0]->duration : 0;

        $end_date_time = $start_date_time + $service_duration * 60;
        $st = get_database_date(date('Y-m-d H:i:s', $start_date_time + 1), $timezone);
        $et = get_database_date(date('Y-m-d H:i:s', $end_date_time - 1), $timezone);

        $step = 0;
        if (!empty($this->session->all_userdata()['booking'])) {
            $step = $this->session->all_userdata()['booking']['booking_step'];
        }
        if ($isBooking && $step != 6) {
            $service_rooms = $this->load->table_model('room')->select(array(
                'select' => array(
                    'room' => array('id', 'bed_quantity', 'origin_bed_quantity' => 'bed_quantity', 'name', 'status')
                ),
                'from' => array(
                    'room'          => array('table' => 'room'),
                //    'room_branch'   => array('table' => 'room_branch', 'condition' => 'room.id = room_branch.room_id', 'type' => 'LEFT')
                ),
                'where' => array(
                    'room.status' => 1 ,
                //    'room_branch.branch_id' => $branch_id,
                ),// only get the room is enable
                'user_level' => $permission['user_level'],
                'permission' => $permission['field'],
            ))->result();
        }
        else{
            $service_rooms = $this->load->table_model('room')->select(array(
                'select' => array(
                    'room' => array('id','bed_quantity', 'origin_bed_quantity' => 'bed_quantity','name','status')
                ),
                'from' => array(
                    'category_item'     => array('table' => 'category_item',      'condition' => 'item.id = category_item.item_id'),
                    'room_type_category'=> array('table' => 'room_type_category', 'condition' => 'room_type_category.category_id = category_item.category_id'),
                    'room'              => array('table' => 'room',               'condition' => 'room.room_type_id = room_type_category.room_type_id')
                ),
                'where' => array(
                    'category_item.item_id' => $service_id,
                    'room.status' => 1,
                    'room_branch.branch_id' => $branch_id
                ),// only get the room is enable
                'group' => 'room.id',
                'user_level' => $permission['user_level'],
                'permission' => $permission['field'],
            ))->result();
        }

        $service_rooms = convert_to_array($service_rooms,'id','',true);

        if($isBooking){
            $permission['field']['branch_id'] = $branch_id;
        }
        else{
            $permission['field']['branch_id'] = $this->user_check->get('branch_id');
        }
        $booked_room = $this->load->table_model('booking_service')->get_booking_range(array(
                'start_time'=> $st,
                'end_time'  => $et,
                'condition' => $condition,
                'permission'=> $permission
            )
        );
        foreach($service_rooms as $key=>$room){
            foreach($booked_room as $booked){
                if($room->id === $booked->room_id){
                    $service_rooms[$key]->bed_quantity = intval($service_rooms[$key]->bed_quantity) - 1;
                }
            }
        }

        foreach($service_rooms as $key => $item){
            if($item->bed_quantity <= 0){
                unset($service_rooms[$key]);
            }
        }

        return $service_rooms;
    }

    function _check_each_service($fields,$old_data,$permission = null,$is_return = FALSE){
        /* ------ Check service is exists or not on the branch ----- */
        if(!$permission){
            $permission = array(
                'user_level' => '',
                'field'      => array()
            );
        }
        $timezone = $this->system_config->get('timezone',$permission);
        $service = $this->load->table_model('item')->select(array(
            'select' => array(
                'item' => array('id','name'),
                'item_service' => array('duration')
            ),
            'from'  =>  array(
                'item'          => array('table'=>'item'),
                'item_service'  => array('table'=>'item_service', 'condition' => 'item_service.item_id = item.id')
            ),
            'where' =>  array('item.type' => ITEM_TYPE('Service'), 'item.id' => $fields['service_id']),
            'user_level' => $permission['user_level'],
            'permission' => $permission['field']
        ))->result();
        if(!count($service)){
            if($is_return){
                return 'The service is not available in this branch';
            }
            throw new Exception('The service is not available in this branch');
        }
        /* ---- Check employee ----- */
        $date = $fields['start_time'];
        $date = convert_date($date);
        $start_date_time = strtotime($date);
        $end_date_time = strtotime($date) + $service[0]->duration * 60;
        $st      = date('Y-m-d H:i:s',$start_date_time);
        $et      = date('Y-m-d H:i:s',$end_date_time);

        $_st = get_database_date($st,$timezone);
        $_et = get_database_date($et,$timezone);
        /* ---- Check if in branch working time ----*/
        $branch_start_time = $this->system_config->get('start_time',$permission);
        $branch_end_time = $this->system_config->get('end_time',$permission);

        $_stt = date('H:i',$start_date_time) ;
        $_ett = date('H:i',$end_date_time);
        if($branch_start_time > $_stt || $branch_end_time < $_ett){
            if($is_return){
                return 'The branch is not yet working in this time<br>Open at : '.$branch_start_time.'<br>Close at: '.$branch_end_time;
            }
            throw new Exception('The branch is not yet working in this time<br>Open at : '.$branch_start_time.'<br>Close at: '.$branch_end_time);
        }

        $condition[] = array("((start_time < '{$_st}' AND end_time > '{$_st}')
            OR (start_time > '{$_st}' AND start_time < '{$_et}' AND end_time > '{$_st}'))"
        );
        $condition['branch_id'] = isset($permission['field']['branch_id'])? $permission['field']['branch_id']:$this->user_check->get('branch_id');
        $blocked_date = $this->load->table_model('branch_offline')->get($condition);
        if(count($blocked_date)){
            if($is_return){
                return 'The branch working time is not available at this time';
            }
            throw new Exception('The branch working time is not available at this time');
        }
        /* ---- End of check if in branch working time ----*/

        /* ---- Check if employee working time is available --- */
        $employees = $this->_getAvailEmployee($start_date_time,$service[0]->id,$old_data,$permission);
        if(!count($employees) || !isset($employees[$fields['employee_id']])){
            if($is_return){
                return 'Unfortunately, the staff you chose are not available at this time, please choose another staff or another time';
            }
            throw new Exception('Unfortunately, the staff you chose are not available at this time, please choose another staff or another time');
        }
        /* ---- End of check employee ---- */

        /*--- Check if there is any room left ----*/
        $rooms = $this->_getAvailRoom($start_date_time,$service[0]->id,$old_data,$permission);
        if(isset($fields['room_id']) && ! isset($rooms[$fields['room_id']])){
            $available_r = reset($rooms);
            if(!empty($available_r)){
            //    $get_id = $this->load->table_model('booking_service')->get(array('booking_id' => $fields['booking_id']));
            //    $this->load->table_model('booking_service')->update($get_id[0]->id,array('room_id'=>$available_r->id));
            }
            else{
                if($is_return){
                    return "There is no more available room during this time slot. (1)";
                }
                throw new Exception("There is no more available room during this time slot. (1)");
            }
        }else if(!count($rooms)){
            if($is_return){
                return "There is no more available room during this time slot. (2)";
            }
            throw new Exception("There is no more available room during this time slot. (2)");
        }
        if($is_return){
            return '';
        }
        else {
            return array(
                'st' => $st,
                'et' => $et,
                'room' => reset($rooms)
            );
        }
    }

    function _getReadyData($fields,$old_data = array(), $permission = array()){
        if(is_object($fields))
        {
            $fields = (array)$fields;
        }
        if(isset($fields['bundle_id'])){
            $ret = array();
            foreach($fields['sub_service'] as $service_id=>$service_fields){
                $ret[$service_id] = $this->_check_each_service(array(
                    'service_id'    => $service_id,
                    'employee_id'   => $service_fields['employee_id'],
                    'room_id'       => isset($service_fields['room_id'])?$service_fields['room_id']:null,
                    'start_time'    => $service_fields['start_time'],
                ),$old_data,$permission);
            }
            return $ret;
        }else{
            return $this->_get_each_service($fields,$old_data,$permission);
        }
    }

    function _get_each_service($fields,$old_data,$permission,$is_return = FALSE){
        /* ------ Check service is exists or not on the branch ----- */
        if(!$permission){
            $permission = array(
                'user_level' => '',
                'field'      => array()
            );
        }
        $service = $this->load->table_model('item')->select(array(
            'select' => array(
                'item' => array('id','name'),
                'item_service' => array('duration')
            ),
            'from'  =>  array(
                'item'          => array('table'=>'item'),
                'item_service'  => array('table'=>'item_service', 'condition' => 'item_service.item_id = item.id')
            ),
            'where' =>  array('item.type' => ITEM_TYPE('Service'), 'item.id' => $fields['service_id']),
            'user_level' => $permission['user_level'],
            'permission' => $permission['field']
        ))->result();
        if(!count($service)){
            if($is_return){
                return 'The service is not available in this branch';
            }
            throw new Exception('The service is not available in this branch');
        }
        /* ---- Check employee ----- */
        $date = $fields['start_time'];
        $date = convert_date($date);
        $start_date_time = strtotime($date);
        $end_date_time = strtotime($date) + $service[0]->duration * 60;
        $st      = date('Y-m-d H:i:s',$start_date_time);
        $et      = date('Y-m-d H:i:s',$end_date_time);

        /*--- Check if there is any room left ----*/
        $rooms = array();
        if((!isset($fields['byPass']) || (isset($fields['byPass']) && !$fields['byPass']))) {
            if(isset($fields['is_bypass']) && !$fields['is_bypass']) {
                $rooms = $this->_getAvailRoom($start_date_time, $service[0]->id, $old_data, $permission);
                if (isset($fields['room_id']) && !isset($rooms[$fields['room_id']])) {
                    $available_r = reset($rooms);
                    if (!empty($available_r)) {
//                $get_id = $this->load->table_model('booking_service')->get(array('booking_id' => $fields['booking_id']));
//                $this->load->table_model('booking_service')->update($get_id[0]->id,array('room_id'=>$available_r->id));
                    } else {
                        if ($is_return) {
                            return "There is no more available room during this time slot. (1)";
                        }
                        throw new Exception("There is no more available room during this time slot. (1)");
                    }
                } else if (!count($rooms)) {
                    if ($is_return) {
                        return "There is no more available room during this time slot.";
                    }
                    throw new Exception("There is no more available room during this time slot.");
                }
            }
        }


        if($is_return){
            return '';
        }
        else {
            return array(
                'st' => $st,
                'et' => $et,
                'room' => reset($rooms)
            );
        }
    }

    function _checkDataBeforeSave($fields,$old_data = array(),$permission = array()){
        if(is_object($fields))
        {
            $fields = (array)$fields;
        }
        if(isset($fields['bundle_id'])){
            $ret = array();
            foreach($fields['sub_service'] as $service_id=>$service_fields){
                $ret[$service_id] = $this->_check_each_service(array(
                    'service_id'    => $service_id,
                    'employee_id'   => $service_fields['employee_id'],
                    'room_id'       => isset($service_fields['room_id'])?$service_fields['room_id']:null,
                    'start_time'    => $service_fields['start_time'],
                ),$old_data,$permission);
            }
            return $ret;
        }else{
            return $this->_check_each_service($fields,$old_data,$permission);
        }
    }

    function getDataForTimeChange($service_id, $start_date_time, $booking_id = 0, $database_date = false, $permission = array(), $type="booking"){
        if($database_date){
            $start_date_time = get_user_date($start_date_time,$this->system_config->get('timezone',$permission),'Y-m-d H:i:s');
        }

        $old_data = null;
        if($booking_id){
            //$old_data = $this->load->table_model('booking')->get_detail(array('booking' => array('id' => $booking_id, 'status' => array(BOOKING_STATUS('Complete'),BOOKING_STATUS('Hold')))));
            $old_data = $this->select(array(
                'select' => array(
                    'booking' => array('id'),
                    'booking_service' => array('employee_id')
                ),
                'from' => array(
                    'booking' => array('table' => 'booking'),
                    'booking_service' => array('table' => 'booking_service','condition' => 'booking.id = booking_service.booking_id','type'=>'LEFT')
                ),
                'where' => array(
                    'booking.id' => $booking_id
                )
            ))->result_array();
            if(count($old_data)){
                $old_data = $old_data[0];
            }else{
                $old_data = array();
            }
        }
        $item               = $this->load->table_model('item')->getByID($service_id);
        $service            = array();
        $service_duration   = array();
        $is_bundle          = 0;
        $total_duration     = 0;

        if($item == NULL)
            return array(
                'total_duration'    => $total_duration,
                'is_bundle'         => $is_bundle,
                'service'           => $service
            );

//        if($item->type == ITEM_TYPE('Bundle')){
//            $is_bundle = 1;
//            $total_duration = 0;
//            $marked_start_date_time = strtotime($start_date_time);
//            foreach($item->bundle->sub_items as $row){
//                if($row->type != ITEM_TYPE('Service')){
//                    continue;
//                }
//                $sub_service_duration           = $this->load->table_model('item_service')->get(array('item_id' => $row->id));
//                $sub_service_duration           = count($sub_service_duration)?$sub_service_duration[0]->duration:0;
//                $service_duration[$row->id]     = $sub_service_duration;
//                $total_duration                 += $sub_service_duration;
//                $start_date_time                = $marked_start_date_time;
//                $marked_start_date_time         = add_duration($start_date_time,$sub_service_duration);
//                $row->list_employee             = ($type == 'scheduler' ? $this->_getAvailEmployeeWithoutCheck($start_date_time,$row->id, $old_data,$permission)  : $this->_getAvailEmployee($start_date_time,$row->id, $old_data,$permission));
//                $row->list_room                 = $this->_getAvailRoom($start_date_time,$row->id,$old_data,$permission);
//                $row->duration                  = $sub_service_duration;
//                $service[$row->id]              = $row;
//            }
//        }
//        else{
            $is_bundle = 0;
            $total_duration       = $this->load->table_model('item_service')->get(array('item_id' => $service_id));
            $total_duration       = count($total_duration)?$total_duration[0]->duration:0;
            $start_date_time        = strtotime($start_date_time);
            $list_employee          = ($type == 'scheduler' ? $this->_getAvailEmployeeWithoutCheck($start_date_time,$service_id, $old_data,$permission)  : $this->_getAvailEmployee($start_date_time,$service_id, $old_data,$permission));
            $list_room              = $this->_getAvailRoom($start_date_time,$service_id,$old_data,$permission);
            $item->list_employee    = $list_employee;
            $item->list_room        = $list_room;
            $item->duration         = $total_duration;
            $service                = $item;
//        }

        return array(
            'total_duration'    => $total_duration,
            'is_bundle'         => $is_bundle,
            'service'           => $service
        );
    }

    function getDataForRoomNotify($available_employee){
        $current_date = date('Y-m-d');
        $booking_status = array(2,1,8,9);
        $booking_service = $this->select(array(
            'select' => array('booking_service' => '*'),
            'from'   => array(
                'booking_service'  => array('table' => 'booking_service'),
                'booking'          => array('table' => 'booking','condition' => 'booking.id = booking_service.booking_id')
            ),
            'where' => array(
                "booking_service.start_time >= '{$current_date} 00:00:00'",
                "booking_service.end_time <= '{$current_date} 23:59:59'",
                'booking_service.employee_id' => $available_employee,
                'booking.status' => $booking_status
            )            
        ))->result();
        return $booking_service;
    }

    function getDataForViewForm($id,$employee_id, $room_id){
        $return = array();
        if($id != 0){
            $book = $this->load->table_model('booking')->get_detail(array(
                'booking' => array(
                    'id'=>$id,
                    'status' => array(BOOKING_STATUS('Complete'),
                        BOOKING_STATUS('Payment'),
                        BOOKING_STATUS('Hold'),
                        BOOKING_STATUS('Reserve'),
                        BOOKING_STATUS('HoldBill')
                    )
                )
            ));
            if(count($book))
                $book= $book[0];

            if($book->status == BOOKING_STATUS('Hold')){
                $customer           = $this->load->table_model('customer_temp')->getByID($book->customer_id);
            }
            else {
                $customer = $this->load->table_model('customer')->getByID($book->customer_id);
            }
            $book->customer         = $customer;
            $service                = $this->load->table_model('item')->getByID($book->bundle_id);
            $book->service          = $service;
            if(isset($book->bundle_id)){
                $temp                   = $this->getDataForTimeChange($book->bundle_id,$book->start_time, $book->id,true);
                foreach($book->services as $sub_service){
                    $room                       = $this->load->table_model('room')->getByID($sub_service->room_id);
                    $employee                   = $this->load->table_model('employee')->getByID($sub_service->employee_id);
                    $sub_service->room          = $room;
                    $sub_service->employee      = $employee;
                    $sub_service->service_list  = $temp['service'][$sub_service->service_id];
                    $sub_service->duration      = $temp['service'][$sub_service->service_id]->duration;
                    $sub_service->name          = $temp['service'][$sub_service->service_id]->name;
                }
                $book->service_duration = $temp['total_duration'];
            }else{
                $room                   = $this->load->table_model('room')->getByID($book->room_id);
                $service                = $this->load->table_model('item')->getByID($book->service_id);
                $employee               = $this->load->table_model('employee')->getByID($book->employee_id);
                $book->room             = $room;
                $book->service          = $service;
                $book->employee         = $employee;
                /*-----Get List-----*/;
                $temp                   = $this->getDataForTimeChange($book->service_id,$book->start_time, $book->id,true);
                $book->service_list     = $temp['service'];
                $book->service_duration = $temp['total_duration'];
                /*-----------------*/
            }
            $return['book'] = $book;
        }
        if($employee_id && $id == 0){
            $return['employee']         = $this->load->table_model('employee')->get(array('id'=>$employee_id));
            if(count($return['employee'])){
                $return['employee']     = $return['employee'][0];
            }
        }
        elseif($room_id && $id == 0){
            $return['room']         = $this->load->table_model('room')->get(array('id'=>$room_id));
            if(count($return['room'])){
                $return['room']     = $return['room'][0];
            }
            $return['service_list'] = $this->select(array(
                'select'    => array(
                    'item'              => array('id','name'),
                    'room'              => array('room_id' => 'id'),
                ),
                'from'      => array(
                    'item'              => array('table' => 'item'),
                    'category_item'     => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id'),
                    'room_type_category'=> array('table' => 'room_type_category', 'condition' => 'room_type_category.category_id = category_item.category_id'),
                    'room'              => array('table' => 'room', 'condition' => 'room.room_type_id = room_type_category.room_type_id')
                ),
                'where'     => array('room.id' => $room_id)
            ))->result();
        }
        return $return;
    }

    function insert_booking($fields,$permission = null, $type = 'scheduler'){
        /**
         * Customer ID
         * Comment
         * If(Bundle):
         * Sub-service : array(employee_id,room_id,start_time)
         *
         */
        if(!$permission){
            $permission = array(
                'field' => array(),
                'user_level' => ''
            );
        }
        $timezone = $this->system_config->get('timezone',$permission);
        $prefixCode = '';
        if($type == 'scheduler'){
            $prefixCode = 'M_';
            $data = $this->_getReadyData($fields,null,$permission);
        }
        elseif($type == 'booking'){
            $prefixCode = 'B_';
            $data = $this->_checkDataBeforeSave($fields,null,$permission);
        }
        /* ---- Start to process appointment ---- */
        while(1){
            $code = $prefixCode.substr(md5($fields['customer_id'].strtotime('NOW')),16,11);
            $check = $this->load->table_model('booking')->get(array('code'=>$code));
            if(!count($check)){
                break;
            }
        }
        $this->db->trans_begin();
        $branch_id = isset($permission['field']['branch_id'])?$permission['field']['branch_id']:$this->user_check->get('branch_id');
        if(isset($fields['bundle_id'])){
            $id = $this->load->table_model('booking')->insert(array(
                'branch_id'     => $branch_id,
                'customer_id'   => $fields['customer_id'],
                'code'          => $code,
                'comment'       => $fields['comment'],
                'status'        => isset($fields['status'])?$fields['status']:BOOKING_STATUS('Complete'),
                'bundle_id'     => $fields['bundle_id'],
                'type'          => isset($fields['type'])?$fields['type']:BOOKING_TYPE('Online'),
                'no_preference' => $fields['no_preference'],
                'is_bypass'     => (isset($fields['byPass'])) ? (($fields['byPass']) ? 1 : 0) : 0
            ));
            foreach($fields['sub_service'] as $service_id=>$row){
                $this->load->table_model('booking_service')->insert(array(
                    'booking_id'    => $id,
                    'service_id'    => $service_id,
                    'employee_id'   => $row['employee_id'],
                    'room_id'       => isset($row['room_id'])?$row['room_id']:$data[$service_id]['room']->id,
                    'start_time'    => get_database_date($data[$service_id]['st'],$timezone),
                    'end_time'      => get_database_date($data[$service_id]['et'],$timezone)
                ));
            }
        }else{
            $id = $this->load->table_model('booking')->insert(array(
                'branch_id'     => $branch_id,
                'customer_id'   => $fields['customer_id'],
                'code'          => $code,
                'comment'       => $fields['comment'],
                'status'        => isset($fields['status'])?$fields['status']:BOOKING_STATUS('Complete'),
                'type'          => isset($fields['type'])?$fields['type']:BOOKING_TYPE('Online'),
                'no_preference' => (isset($fields['no_preference'])? $fields['no_preference'] : 0),
                'is_bypass'     => (isset($fields['byPass'])) ? (($fields['byPass']) ? 1 : 0) : 0
            ));
            $this->load->table_model('booking_service')->insert(array(
                'booking_id'    => $id,
                'service_id'    => $fields['service_id'],
                'employee_id'   => $fields['employee_id'],
                'room_id'       => isset($fields['room_id'])?$fields['room_id']:$data['room']->id,
                'start_time'    => get_database_date($data['st'],$timezone),
                'end_time'      => get_database_date($data['et'],$timezone)
            ));
        }
        $this->db->trans_complete();
        /* ---- End process booking ------*/
        return $id;
    }

    function edit_bao_booking($fields, $permission = null){
        $timezone = $this->system_config->get('timezone',$permission);
        $this->db->trans_begin();

        if($fields['data']['type'] == 3){
            $fields['data']['type'] = BOOKING_STATUS('Block');
        }
        elseif($fields['data']['type'] == 2){
            $fields['data']['type'] = BOOKING_STATUS('Offline');
        }
        elseif($fields['data']['type'] == 1){
            $fields['data']['type'] = BOOKING_STATUS('Away');
        }

        $fields['data']['comment'] = $fields['data']['description'];
        while(strpos($fields['data']['comment'],'<br>') > 0){
            $fields['data']['comment'] = substr($fields['data']['comment'],strpos($fields['data']['comment'],'<br>')+4);
        }
        $this->load->table_model('booking')->update(array('id'=>$fields['data']['booking_id']),array(
            'comment'       => $fields['data']['comment'],
            'status'        => $fields['data']['type']
        ));
        $get_id = $this->load->table_model('booking_service')->get(array('booking_id'=>$fields['data']['booking_id']));
        $this->load->table_model('booking_service')->update($get_id[0]->id,array(
            'start_time'    => get_database_date(date('Y-m-d H:i:s',strtotime($fields['data']['start_time'])),$timezone),
            'end_time'      => get_database_date(date('Y-m-d H:i:s',strtotime($fields['data']['end_time'])),$timezone)
        ));
        $this->db->trans_complete();
        return TRUE;
    }

    function insert_bao_booking($fields, $permission = null){
        $timezone = $this->system_config->get('timezone',$permission);
        while(1){
            $code = uniqid();
            $check = $this->load->table_model('booking')->get(array('code'=>$code));
            if(!count($check)){
                break;
            }
        }
        $this->db->trans_begin();
        $branch_id = isset($permission['field']['branch_id'])?$permission['field']['branch_id']:$this->user_check->get('branch_id');

        if($fields['data']['type'] == 3){
            $fields['data']['type'] = BOOKING_STATUS('Block');
        }
        elseif($fields['data']['type'] == 2){
            $fields['data']['type'] = BOOKING_STATUS('Offline');
        }
        elseif($fields['data']['type'] == 1){
            $fields['data']['type'] = BOOKING_STATUS('Away');
        }
        $fields['data']['comment'] = $fields['data']['description'];
        while(strpos($fields['data']['comment'],'<br>') > 0){
            $fields['data']['comment'] = substr($fields['data']['comment'],strpos($fields['data']['comment'],'<br>')+4);
        }

        $id = $this->load->table_model('booking')->insert(array(
            'branch_id'     => $branch_id,
            'code'          => $code,
            'comment'       => $fields['data']['comment'],
            'status'        => $fields['data']['type']
        ));
        $this->load->table_model('booking_service')->insert(array(
            'booking_id'    => $id,
            'employee_id'   => $fields['id'],
            'start_time'    => get_database_date(date('Y-m-d H:i:s',strtotime($fields['data']['start_time'])),$timezone),
            'end_time'      => get_database_date(date('Y-m-d H:i:s',strtotime($fields['data']['end_time'])),$timezone)
        ));
        $this->db->trans_complete();
        return $id;
    }

    function insert_comment_booking($fields,$permission = null){
        $timezone = $this->system_config->get('timezone',$permission);
        while(1){
            $code = uniqid();
            $check = $this->load->table_model('booking')->get(array('code'=>$code));
            if(!count($check)){
                break;
            }
        }
        $this->db->trans_begin();
        $branch_id = isset($permission['field']['branch_id'])?$permission['field']['branch_id']:$this->user_check->get('branch_id');

        $id = $this->load->table_model('booking')->insert(array(
            'branch_id'     => $branch_id,
            'code'          => $code,
            'comment'       => $fields['comment'],
            'status'        => BOOKING_STATUS('Comment')
        ));
        if((strtotime($fields['hidden_end_time']) - strtotime($fields['start_time'])) < 900){
            $fields['end_time'] = date('Y-m-d H:i:s',strtotime($fields['start_time']) + 900);
        }
        else{
            $fields['end_time'] = date('Y-m-d H:i:s',strtotime($fields['hidden_end_time']));
        }
        $this->load->table_model('booking_service')->insert(array(
            'booking_id'    => $id,
            'employee_id'   => $fields['hidden_employee_id'],
            'start_time'    => get_database_date($fields['start_time'],$timezone),
            'end_time'      => get_database_date($fields['end_time'],$timezone)
        ));
        $this->db->trans_complete();
        return $id;
    }

    function update_comment_booking($fields,$permission = null){
        $user_data = $this->session->userdata('login');
        $this->db->trans_begin();
        while(strpos($fields['comment'],'<br>') > 0){
            $fields['comment'] = substr($fields['comment'],strpos($fields['comment'],'<br>')+4);
        }
        $result = $this->load->table_model('booking')->update($fields['booking_id'],array(
            'branch_id'   => $this->user_check->get('branch_id'),
            'comment'     => $fields['comment'],
            'updater'     => $user_data->user_id
        ));
        $id = $fields['booking_id'];
        $fields['employee_id'] = isset($fields['hidden_employee_id'])? $fields['hidden_employee_id'] : $fields['employee_id'];
        $get_id = $this->load->table_model('booking_service')->get(array('booking_id'=>$id));
        if((strtotime($fields['hidden_end_time']) - strtotime($fields['start_time'])) < 900){
            $fields['end_time'] = date('Y-m-d H:i:s',strtotime($fields['start_time']) + 900);
        }
        else{
            $fields['end_time'] = date('Y-m-d H:i:s',strtotime($fields['hidden_end_time']));
        }
        $this->load->table_model('booking_service')->update($get_id[0]->id,array(
            'employee_id'   => $fields['employee_id'],
            'start_time'    => get_database_date($fields['start_time']),
            'end_time'      => get_database_date($fields['end_time'])
        ));
        $this->db->trans_complete();
        if($result){
            $result = $fields['booking_id'];
        }
        /* ---- End process booking ------*/
        return $result;
    }

    function save_booking_recurring($booking_id,$recurring_data){
        if(isset($recurring_data['repeat_on'])){
            $recurring_data['repeat_on'] = ($recurring_data['recurring_type'] == 2 ? implode(',',$recurring_data['repeat_on']) : $recurring_data['repeat_on']);
        }
        else{
            $recurring_data['repeat_on'] = '';
        }
        $result = $this->load->table_model('booking_recurring')->insert(array(
            'booking_id'    => $booking_id,
            'parent_booking_id' => $recurring_data['parent_booking_id'],
            'recurring_type'=> $recurring_data['recurring_type'],
            'repeat_times'  => $recurring_data['repeat_time'],
            'repeat_on'     => $recurring_data['repeat_on'],
            'repeat_to'     => $recurring_data['repeat_type'],
            'repeat_to_data'=> ($recurring_data['repeat_type'] == 1 ? $recurring_data['until_date'] : $recurring_data['number_times'])
        ));
        return $result;
    }

    function reserve_booking($id,$comment){
        $st_now = $this->load->table_model('booking')->get_detail(array('booking'=>array('id'=>$id)));
        $st_now = count($st_now)?(array)$st_now[0]:array();
        $result = $this->load->table_model('booking')->update($id,array(
            'status' => BOOKING_STATUS('Reserve'),
            'reserve_comment' => $comment,
            'old_status' => $st_now['status']
        ));
        return $result;
    }

    function update_booking($fields, $type = 'scheduler'){
        $user_data = $this->session->userdata('login');
        $old_data = $this->load->table_model('booking')->get_detail(array('booking'=>array('id'=>$fields['booking_id'])));
        $old_data = count($old_data)?(array)$old_data[0]:array();
        if($type == 'scheduler'){
            $data = $this->_getReadyData($fields,$old_data);
        }
        elseif($type == 'booking'){
            $data = $this->_checkDataBeforeSave($fields,$old_data);
        }
        if(is_string($data)){
            return $data;
        }

        $type_booking = $old_data['type'];
        if($old_data['status'] == BOOKING_STATUS('Hold')){
            $type_booking = BOOKING_TYPE('Online');
        }
        else if($old_data['status'] == BOOKING_STATUS('Complete') || $old_data['status'] == BOOKING_STATUS('Hold')){
            $type_booking = $old_data['type'];
        }

        $this->db->trans_begin();
        $bypass = (isset($fields['byPass'])) ? ($fields['byPass']) : 0;
        $result = $this->load->table_model('booking')->update($fields['booking_id'],array(
            'branch_id'   => $this->user_check->get('branch_id'),
            'comment'     => ($type_booking == 0 ? $fields['comment'] . '<br>WRONG TYPE_BOOKING(=0).Pls feedback to Ranen if you see this text.' : $fields['comment']),
            'customer_id' => $fields['customer_id'],
            'status'      => ($old_data['status'] == BOOKING_STATUS('Payment') ? BOOKING_STATUS('Payment') : BOOKING_STATUS('Complete')),
            'type'        => $type_booking,
            'no_preference' => $fields['no_preference'],
            'is_bypass'     => $bypass,
            'updater' => $user_data->user_id
        ));
        $id = $fields['booking_id'];
        if(isset($fields['bundle_id'])){
             $this->load->table_model('booking')->update($fields['booking_id'],array(
                'branch_id'     => $this->user_check->get('branch_id'),
                'customer_id'   => $fields['customer_id'],
                'comment'       => ($type_booking == 0 ? $fields['comment'] . '<br>WRONG TYPE_BOOKING(=0).Pls feedback to Ranen if you see this text.' : $fields['comment']),
                'status'        => ($old_data['status'] == BOOKING_STATUS('Payment') ? BOOKING_STATUS('Payment') : BOOKING_STATUS('Complete')),
                 'type'        => $type_booking,
                'bundle_id'     => $fields['bundle_id'],
                 'no_preference' => $fields['no_preference'],
                 'is_bypass'     => $bypass,
                 'updater' => $user_data->user_id
            ));
            $this->load->table_model('booking_service')->delete(array('booking_id' => $id));
            foreach($fields['sub_service'] as $service_id=>$row){
                $this->load->table_model('booking_service')->insert(array(
                    'booking_id'    => $id,
                    'service_id'    => $service_id,
                    'employee_id'   => $row['employee_id'],
                    'room_id'       => $row['room_id'],
                    'start_time'    => get_database_date($data[$service_id]['st']),
                    'end_time'      => get_database_date($data[$service_id]['et'])
                ));
            }
        }else{
            $status_fill = BOOKING_STATUS('Complete');
            if($fields['booking_status'] == BOOKING_STATUS('Hold')){
                if((empty($fields['customer_id']) && !empty($fields['customer_temp_id']))){
                    $status_fill = BOOKING_STATUS('Hold');
                    $fields['customer_id'] = $fields['customer_temp_id'];
                }
                else if(!empty($fields['customer_id']) && empty($fields['customer_temp_id'])){
                    $status_fill = BOOKING_STATUS('Hold');
                }
                else if(!empty($fields['customer_id']) && !empty($fields['customer_temp_id'])){
                    $status_fill = BOOKING_STATUS('Complete');
                    $this->load->table_model('customer_temp')->delete(array('id' => $fields['customer_temp_id']));
                }
            }
            else if($fields['booking_status'] == BOOKING_STATUS('Payment')){
                $status_fill = BOOKING_STATUS('Payment');
            }
            $this->load->table_model('booking')->update($fields['booking_id'],array(
                'branch_id'     => $this->user_check->get('branch_id'),
                'customer_id'   => $fields['customer_id'],
                'comment'       => ($type_booking == 0 ? $fields['comment'] . '<br>WRONG TYPE_BOOKING(=0).Pls feedback to Ranen if you see this text.' : $fields['comment']),
                'status'        => $status_fill,
                'type'        => $type_booking,
                'bundle_id'     => null,
                'no_preference' => $fields['no_preference'],
                'is_bypass'     => $bypass,
                'updater' => $user_data->user_id
            ));
            $booking_service = $this->load->table_model('booking_service')->get(array('booking_id' => $fields['booking_id']));
            if(count($booking_service) == 1) {
                $this->load->table_model('booking_service')->update($booking_service[0]->id, array(
                    'booking_id' => $id,
                    'service_id' => $fields['service_id'],
                    'employee_id' => $fields['employee_id'],
                    'room_id' => $fields['room_id'],
                    'start_time' => get_database_date($data['st']),
                    'end_time' => get_database_date($data['et'])
                ));
            }
        }
        $this->db->trans_complete();
        if($result){
            $result = $fields['booking_id'];
        }
        /* ---- End process booking ------*/
        return $result;
    }

    function get_list_recurring_booking_id($id){
        $result = $this->select(array(
            'select' => array('booking_recurring' => array('parent_booking_id')),
            'from'  => array('booking_recurring' => array('table' => 'booking_recurring')),
            'where' => array('booking_id'=>$id)
        ))->result();
        $result = $result[0]->parent_booking_id;
        $result = $this->select(array(
            'select'=> array('booking_recurring' => array('booking_id')),
            'from'  => array('booking_recurring'  => array('table' => 'booking_recurring')),
            'where' => 'parent_booking_id = '.$result.' AND booking_id >='.$id
        ))->result();

        return $result;
    }

    function delete_recurring_booking($list_booking){
        foreach($list_booking as $booking){
            $result = $this->load->table_model('booking_recurring')->delete(array('booking_id'=>$booking->booking_id));
            $result = $this->load->table_model('booking')->delete(array('id'=>$booking->booking_id));
        }
        if($result)
            return TRUE;
        else
            return FALSE;
    }

    function delete_booking($id){
        $result = $this->load->table_model('booking')->delete(array('id'=>$id));
        return $result;
    }


    /**
     * @author  Tran Minh Thao added -- Begin add
     * Update employee order into new table : employee_scheduler
     * @param  numeric $employee_id : id of the employee need to update ordering
     * @param  numeric $order_number : new order of the employee
     * @return boolen : tell the update ordering is true or false
     */
    function update_order($employee_id,$order_number){
        $branch_id = $this->user_check->get('branch_id');
        if($branch_id == null)
            $branch_id = 0;

        $employee_model = $this->load->table_model('employee');
        $employee_scheduler_model = $this->load->table_model('employee_scheduler');

        // Get employee list of this branch
        $employee_list  = $this->select(array(
            'select' => array(
                'employee' => array('id','first_name','last_name','ordering','department_id'),
                'employee_branch'   => array('branch_id')
            ),
            'from'   => array(
                'employee' => array('table'=>'employee'),
            ),
            'where' => array(
                'department_id'  => $this->load->table_model('department')->get_department_serve_booking()
            ),
            'order' => "ordering asc"
        ))->result();

        // Find each employee in the employee_scheduler. If found, do nothing.
        // If not found the employee in the employee_scheduler table, insert that employee into employee_scheduler table
        $this->db->trans_start();
        $new_order = 1;
        for($i=0;$i<count($employee_list);$i++) {
            $employees_order = $employee_scheduler_model->get_minify(
                array('order_by' => 'ordering asc',
                    'condition' => array('employee_id' => $employee_list[$i]->id,'department_id' => $employee_list[$i]->department_id,'branch_id' => $branch_id),
                    'user_level' => Permission_Value::ADMIN
                ));

            if(!count($employees_order))
                $this->insert_employee_order($employee_list[$i]->id,$employee_list[$i]->department_id,$branch_id,$new_order);

            $new_order++;
        }

        // Get all order of employees after update
        $employee_scheduler = $this->select(array(
            'select' => array(
                'employee_scheduler' => array('employee_id','branch_id','department_id','ordering')
            ),
            'from'   => array(
                'employee_scheduler' => array('table'=>'employee_scheduler'),
            ),
            'where' => array(
                'branch_id = '.$branch_id
            ),
            'order' => "ordering asc"
        ))->result();

        $order = 1;
        for($i=0;$i<count($employee_scheduler);$i++) {
            if($employee_scheduler[$i]->employee_id != $employee_id) {
                if($order == $order_number) {
                    $order++;
                }

                $employee_scheduler_model->update_employee_order($employee_scheduler[$i]->employee_id,$employee_scheduler[$i]->department_id,$branch_id,array('ordering' => $order),false);
                $order++;
            }
        }

        $current_employee = $employee_model->get_minify(
            array('order_by' => 'ordering asc',
                'condition' => array('id' => $employee_id ,'department_id' => $this->load->table_model('department')->get_department_serve_booking()),
                'user_level' => Permission_Value::ADMIN
            ));

        if(count($current_employee))
            $employee_scheduler_model->update_employee_order($current_employee[0]->id,$current_employee[0]->department_id,$branch_id,array('ordering' => $order_number),false);

        $this->db->trans_complete();
        return true;
    }

    function insert_employee_order($employee_id,$department_id,$branch_id,$ordering){
        $this->db->trans_start();
        $result = $this->load->table_model($this->main_table)->insert_to_employee_scheduler($employee_id,$department_id,$branch_id,$ordering);
        $this->db->trans_complete();
        return $result;
    }

    function check_double_comment($start_time,$end_time,$employee_id,$booking_id=0){
        $is_double = 0;
        if(isset($booking_id) && $booking_id != 0 && $booking_id != '') {
            $condition = array('booking_service.employee_id' => $employee_id, 'booking_service.booking_id =' . $booking_id);
            $check = $this->select(array(
                'select' => array(
                    'booking_service' => array('employee_id', 'booking_id', 'start_time', 'end_time')
                ),
                'from' => array(
                    'booking_service' => array('table' => 'booking_service'),
                ),
                'where' => $condition
            ))->result();
            if(!empty($check) && $check[0]->start_time == $start_time && $check[0]->end_time == $end_time){
                return 0;
            }
        }

        $condition = array('employee_id' => $employee_id,'booking_id !='.$booking_id);
        $comment_block = $this->select(array(
            'select' => array(
                'booking_service' => array('employee_id','booking_id','start_time','end_time')
            ),
            'from'   => array(
                'booking_service' => array('table'=>'booking_service'),
            ),
            'where' => $condition
        ))->result();

        foreach($comment_block as $block){
            $blocks = $this->select(array(
                'select' => array(
                    'booking' => array('status')
                ),
                'from'   => array(
                    'booking' => array('table'=>'booking'),
                ),
                'where' => array('id'=>$block->booking_id)
            ))->result();

            $block_start_time = get_user_date($block->start_time,'','Y-m-d H:i:s');
            $block_end_time = get_user_date($block->end_time,'','Y-m-d H:i:s');

            if(strtotime($start_time) < strtotime($block_start_time)){
                if(strtotime($end_time) > strtotime($block_start_time))
                    $is_double = 1;
            }
            if(strtotime($start_time) == strtotime($block_start_time)){
                $is_double = 1;
            }
            if(strtotime($start_time) > strtotime($block_start_time)){
                if(strtotime($start_time) < strtotime($block_end_time))
                    $is_double = 1;
            }

            if($blocks[0]->status == 0) $is_double = 0;
            if($is_double) return $is_double;
        }
        return $is_double;
    }

    function update_online_offline($employee_id,$date,$first_hour,$last_hour){
        $user_data = $this->session->userdata('login');
        $booking_array = array(BOOKING_STATUS('Complete'),BOOKING_STATUS('Hold'),BOOKING_STATUS('Reserve'),BOOKING_STATUS('Payment'),BOOKING_STATUS('HoldBill'));
        $start_time = $date." 02:00:00";
        $end_time = $date." 15:00:00";
        $count_book = 0;
        $count_block = 0;
        $full_day_flag = 0;
        $current_booking = $this->select(array(
            'select'    => array('booking' => '*','booking_service' => 'employee_id,start_time,end_time'),
            'from'      => array(
                'booking'              => array('table' => 'booking'),
                'booking_service'     => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id')
            ),
            'where'     => array('booking.branch_id' => $user_data->branch_id,
                                'booking.status != 0',
                                'booking_service.employee_id' => $employee_id,
                                "booking_service.start_time >= '{$start_time}'",
                                "booking_service.end_time <= '{$end_time}'")
        ))->result();
        
        foreach($current_booking as $value){
            if(in_array($value->status, $booking_array))
                $count_book++;
            else{
                $count_block++;
                if($value->start_time == $start_time && $value->end_time == $end_time)
                    $full_day_flag = $value->id;
            }
        }
        
        if($count_book){
            return 0;
        }

        $this->db->trans_begin();
        if($full_day_flag){
            $result = $this->load->table_model('booking')->update(array('id' => $full_day_flag),array(
                'code'          => "",
                'comment'       => "",
            ));
            $data = array(
                'start_time'     => $date." 02:00:00",
                'end_time'       => $date." 03:00:00"
            );
            $this->db->where('booking_id', $full_day_flag);
            $this->db->update('booking_service', $data); 
        }
        else{
            if($count_block){
                foreach($current_booking as $value){
                    if(!in_array($value->status, $booking_array)){
                        $result = $this->load->table_model('booking')->update($value->id,array(
                            'status' => 0
                        ));
                    }
                }
            }
            $booking_id = $this->load->table_model('booking')->insert(array(
                'branch_id'     => $user_data->branch_id,
                'code'          => "OFFDAY",
                'comment'       => "OFF DAY",
                'status'        => BOOKING_STATUS('Block'),
                'created_date'  => date('Y-m-d H:i:s'),
            ));
            if($booking_id){
                $this->load->table_model('booking_service')->insert(array(
                    'booking_id'     => $booking_id,
                    'employee_id'    => $employee_id,
                    'start_time'     => $start_time,
                    'end_time'       => $end_time
                ));
            }
        }
        $this->db->trans_complete();
        return 1;
    }

    function check_timeslot($data){
        $branch_id = $this->session->all_userdata()['staff']['login']->branch_id;
        $data['start_time'] = date('Y-m-d H:i:s', strtotime($data['start_time']) + 1);
        $data['end_time']   = date('Y-m-d H:i:s', strtotime($data['end_time']) - 1);
        $condition = array(
            'booking.branch_id' => $branch_id,
            'booking.status' => array(BOOKING_STATUS('Complete'),BOOKING_STATUS('Hold'),BOOKING_STATUS('Comment'),BOOKING_STATUS('Block'),BOOKING_STATUS('Away'),BOOKING_STATUS('Offline'),BOOKING_STATUS('Payment'),BOOKING_STATUS('HoldBill')),
            'booking_service.employee_id' => $data['employee_id'],
            '((booking_service.start_time < "'.get_database_date($data['start_time']).'" AND booking_service.end_time > "'.get_database_date($data['start_time']).'") OR
              (booking_service.start_time < "'.get_database_date($data['end_time']).'" AND booking_service.end_time > "'.get_database_date($data['end_time']).'") OR
              (booking_service.start_time > "'.get_database_date($data['start_time']).'" AND booking_service.end_time < "'.get_database_date($data['end_time']).'"))'
        );

        if(!empty($data['booking_id'])){
            $condition[] = 'booking.id != '.$data['booking_id'];
        }
        $select = $this->select(array(
            'select' => array(
                'booking_service' => array('id','booking_id')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id')
            ),
            'where' => $condition
        ))->result();
        if(count($select) > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function check_pending_appointment($data){
        $branch_id = $this->session->all_userdata()['staff']['login']->branch_id;
        $data['start_time'] = strtotime($data['start_time']) + 1;
        $data['end_time']   = strtotime($data['end_time']) - 1;

        $condition = array(
            'branch_id' => $branch_id,
            'staff_id' => $data['employee_id']
        );

        if(!empty($data['booking_id'])){
            $condition[] = 'booking.id != '.$data['booking_id'];
        }
        $select = $this->select(array(
            'select' => array(
                'booking_pending_appointment' => array('id','date_time','duration')
            ),
            'from' => array(
                'booking_pending_appointment' => array('table' => 'booking_pending_appointment')
            ),
            'where' => $condition
        ))->result();

        if(count($select) > 0){
            foreach($select as $item){
                $start_time = strtotime($item->date_time);
                $end_time = $start_time + ($item->duration * 60);
                if(($start_time < $data['start_time'] && $end_time > $data['start_time']) ||
                    $start_time < $data['end_time']   && $end_time > $data['end_time'] ||
                    $start_time > $data['start_time'] && $end_time < $data['end_time']){

                    return true;
                }
            }
            return false;
        }
        else{
            return false;
        }
    }


    // Update appointment as read
    function mark_as_read_appointment($id){
        $result = $this->load->table_model('booking')->update($id,array(
            'is_new' => 0
        ));
        return $result;
    }

    function getPrepareData($id){
        $result = $this->select(array(
            'select' => array(
                'booking_service' => array('start_time','end_time'),
                'customer' => array('customer' => 'code'),
                'branch' => array('branch' => 'name'),
                'room' => array('room' => 'name'),
                'item' => array('item' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id=booking_service.booking_id','LEFT'),
                'customer' => array('table' => 'customer', 'condition' => 'booking.customer_id=customer.id','LEFT'),
                'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id=branch.id', 'LEFT'),
                'room' => array('table' => 'room', 'condition' => 'booking_service.room_id=room.id','LEFT'),
                'item' => array('table' => 'item', 'condition' => 'booking_service.service_id=item.id', 'LEFT')
            ),
            'where' => array(
                'booking.id' => $id
            )
        ))->result_array();
        if(count($result) > 0){
            return $result[0];
        }
        else{
            return FALSE;
        }
    }

    function getHistoryBooking($searchText){
        $searchText = trim(strtolower($searchText));
        $custList = $this->select(array(
            'select' => array(
                'booking' => array('id' => 'customer_id')
            ),
            'from' => array(
                'booking' => array('table' => 'booking')
            ),
            'where' => array(
                'code' => $searchText
            )
        ))->result();

        $cCustList = array();
        if(!empty($custList)) {
            foreach ($custList as $item) {
                $cCustList[] = $item->id;
            }
        }
        $validHistory = $this->_getHistory($cCustList,$searchText,'customer');
        $tempHistory  = $this->_getHistory($cCustList,$searchText,'customer_temp');

        foreach($tempHistory as $item){
            $item->customerCode = $item->customerFirstName . $item->customerLastName . '<br><b style="color:red;">(unconfirm)</b>';
            $validHistory[] = $item;
        }

        if(empty($validHistory)){
            return array();
        }
        else{
            foreach($validHistory as $key => $item){
                $validHistory[$key]->created_date = get_user_date($item->created_date,'','Y-m-d H:i:s');
                $validHistory[$key]->start_time = get_user_date($item->start_time,'','Y-m-d H:i');
                $validHistory[$key]->end_time = get_user_date($item->end_time,'','Y-m-d H:i');
                if($item->status == 0){
                    $validHistory[$key]->status = 'Delete';
                }
                elseif($item->status == 1){
                    $validHistory[$key]->status = 'Confirmed';
                }
                elseif($item->status == 2){
                    $validHistory[$key]->status = 'Not Confirmed';
                }
                elseif($item->status == 3){
                    $validHistory[$key]->status = 'Edited';
                }
                elseif($item->status == 8){
                    $validHistory[$key]->status = 'Payment';
                }
                elseif($item->status == 9){
                    $validHistory[$key]->status = 'Hold Bill';
                }
            }
            return $validHistory;
        }
    }

    function _getHistory($cCustList, $searchText, $type = 'customer'){
        if($type == 'customer'){
            $table = 'customer';
            $tableField = array(
                'customerCode' => 'code',
                'customerFirstName' => 'first_name',
                'customerLastName' => 'last_name',
                'customerMobileNumber' => 'mobile_number' ,
                'customerEmail' => 'email'
            );
             $condition = array(BOOKING_STATUS('Complete'),
                BOOKING_STATUS('Reserve'),
                BOOKING_STATUS('Delete'),
                BOOKING_STATUS('Payment'),
                BOOKING_STATUS('HoldBill'));
        }
        else{
            $table = 'customer_temp';
            $tableField = array(
                'customerFirstName' => 'first_name',
                'customerLastName' => 'last_name',
                'customerMobileNumber' => 'mobile_number',
                'customerEmail' => 'email'
            );
            $condition = array(BOOKING_STATUS('Hold'));
        }

        if(empty($cCustList)){
            $custList = $this->select(array(
                'select' => array($table => array('id')),
                'from' => array($table => array('table' => $table)),
                'where' => array('mobile_number' => $searchText)
            ))->result();
            foreach($custList as $item){
                $cCustList[] = $item->id;
            }
        }

        return $this->select(array(
            'select' => array(
                'booking' => array('id','bookingCode' => 'code', 'comment', 'reserve_comment', 'created_date', 'status'),
                'booking_service' => array('start_time', 'end_time'),
                $table => $tableField,
                'branch' => array('branch_name' => 'name'),
                'item' => array('itemName' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id', 'type' => 'LEFT'),
                'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id', 'type' => 'LEFT'),
                $table => array('table' => $table, 'condition' => 'booking.customer_id = '.$table.'.id', 'type' => 'LEFT'),
                'item' => array('table' => 'item', 'condition' => 'booking_service.service_id = item.id', 'type' => 'LEFT')
            ),
            'where' => array(
                'booking.customer_id' => $cCustList,
                'booking.status' => $condition
            ),
            'order' => 'id DESC'
        ))->result();
    }

    function getExcelCancelledApptsData($postData){
        $timezone = $this->system_config->get('timezone', array());
        $filterSD = get_database_date($postData['startDate'],$timezone);
        $filterED = get_database_date($postData['endDate'],$timezone);
        $res = $this->select(array(
            'select' => array(
                'booking' => array('Code' => 'code', '"Booking Date"' => 'created_date', 'Comment' => 'comment', '"Reserve Comment"' => 'reserve_comment'),
                'booking_service' => array('"Start Time"' => 'start_time', '"End Time"' => 'end_time'),
                'branch' => array('Branch' => 'name'),
                'customer' => array('"Customer Code"' => 'code', '"Customer Name"' => 'first_name', '"Customer Phone Number"' => 'mobile_number', '"Customer Email"' => 'email'),
                'employee' => array('Employee' => 'first_name'),
                'item' => array('Service' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id','type' => 'LEFT'),
                'customer' => array('table' => 'customer', 'condition' => 'booking.customer_id = customer.id', 'type' => 'LEFT'),
                'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id', 'type' => 'LEFT'),
                'item' => array('table' => 'item', 'condition' => 'booking_service.service_id = item.id','type' => 'LEFT'),
                'employee' => array('table' => 'employee', 'condition' => 'booking_service.employee_id = employee.id','type' => 'LEFT')
            ),
            'where' => array(
                'booking.status' => BOOKING_STATUS('Reserve'),
                'booking.branch_id' => $postData['branchId'],
                'booking.created_date >' => $filterSD,
                'booking.created_date <' => $filterED,
            )
        ))->result_array();

        foreach($res as $key => $item){
            $res[$key]['Booking Date'] = get_user_date($item['Booking Date'],'','Y-m-d H:i:s');
            $res[$key]['Start Time'] = get_user_date($item['Start Time'],'','Y-m-d H:i:s');
            $res[$key]['End Time'] = get_user_date($item['End Time'],'','Y-m-d H:i:s');
        }
        return $res;
    }
}