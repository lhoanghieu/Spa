<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Questionaire_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "question";
        $this->suggestionSearch = array('title');
        $this->suggestionDisplay = array('title');
    }

    function insert($values)
    {
        $values = (array) $values;
        if($values['type'] == 1){
            $values['type_values'] = '["Yes","No"]';
        }else{
            $values['type_values'] = json_encode(explode("\n", str_replace("\n\r", "\n", $values['type_values'])));
        }
        return $this->load->table_model('question')->insert($values);
    }

    function update($id, $values){
        $values = (array) $values;
        if($values['type'] == 1){
            $values['type_values'] = '["Yes","No"]';
        }else{
            $values['type_values'] = json_encode(explode("\n", str_replace("\n\r", "\n", $values['type_values'])));
        }
        return $this->load->table_model('question')->update($id,$values);
    }

    function renderDataSet($data){
        foreach($data as $row){
            $row->type = QUESTION_TYPE($row->type);
            $row->type_values = implode(', ', json_decode($row->type_values));
        }
        return $data;
    }
}