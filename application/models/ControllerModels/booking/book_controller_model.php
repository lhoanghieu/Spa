<?php

class Book_Controller_model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_data_branch(){
        if(!empty($this->session->userdata('pending_appointment')['pending_id'])){
            $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
        }

        // PROCESS STRING URL
        $urlHost = parse_url(base_url(),PHP_URL_HOST);

        $model = $this->load->table_model('category');
        $branches = $model->select(array(
            'select' => array(
                'branch' => array('id','name','address','phone_number','branch_group_id'),
            ),
            'from'  =>  array(
                'branch'      => array('table'=>'branch')
            )
        ))->result();
        $config = $model->select(array(
            'select' => array('config' => 'id','code','value'),
            'from'   => array('config' => array('table' => 'config')),
        ))->result();

        $config = convert_to_array($config,'code');
        $new_branches = array();
        foreach($branches as $branch){
            if($this->getConfig('website',$branch->id) === $urlHost){
                $check = $model->select(array(
                    'select' => array('config_branch' => array('config_id','value')),
                    'from'   => array('config_branch' => array('table' => 'config_branch')),
                    'where'  => array('config_id' => 19, 'branch_id' => $branch->id) // find the function on code later
                ))->result();
                if(count($check) > 0 && ($check[0]->value == 'inherit' || $check[0]->value == 1) && $config['show_in_booking']->value == 1){
                    $branch->active = 1;
                }
                else{
                    $branch->active = 0;
                }

                // CHECK BRANCH CONFIG
                $check = $model->select(array(
                    'select' => array('config_branch_group' => array('config_id','value')),
                    'from'   => array('config_branch_group' => array('table' => 'config_branch_group')),
                    'where'  => array('config_id' => 19, 'branch_group_id' => $branch->branch_group_id) // find the function on code later
                ))->result();
                if(count($check) > 0 && $check[0]->value == 2){
                    $branch->active = 0;
                }

                $new_branches[] = $branch;
            }
        }
        return array('branches'=>$new_branches);
    }

    function getCurrentBranch($branch_id){
        $config = $this->load->table_model('config')->get();
        $branch = $this->load->table_model('branch')->get(array('id'=>$branch_id));
        $model = $this->load->table_model('config_branch');
        $result = array();
        foreach($config as $value){
            $branch_info = $model->select(array(
                'select' => array(
                    'config_branch' => '*',
                ),
                'from'  =>  array(
                    'config_branch'      => array('table'=>'config_branch'),
                ),
                'where' =>  array('branch_id' => $branch_id,'config_id' => $value->id)
            ))->result();


            if(isset($branch_info[0]->value) && $branch_info[0]->value != "inherit")
                $value->value = $branch_info[0]->value;

            $result[$value->code]["code"] = $value->code;
            $result[$value->code]["name"] = $value->name;
            $result[$value->code]["value"] = $value->value;
        }
        $result["branch_name"] = $branch[0]->name;
        $result["branch_phone"] = $branch[0]->phone_number;
        $result["branch_add"] = $branch[0]->address;
        return $result;
    }

    function get_data_service($data)
    {
        if (!empty($this->session->userdata('pending_appointment')['pending_id'])) {
            $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
        }

        $booking_data = $this->session->userdata('booking_data')[$this->session->userdata('currentUniqueId')];
        $model = $this->load->table_model('category');
        $items = $this->load->table_model('item')->get(array('type' => ITEM_TYPE(array('Service', 'Bundle'))));
        /*
        // Thao Tran Minh - Edited at 2015/09/11 ---- Begin edit ------------
         */
//        $cat_booking = $this->load->table_model('category_booking')->get(array('branch_id' => $booking_data->branch_id));
        $categories = $model->select(array(
            'select' => array(
                'category' => array('id', 'name')
            ),
            'from' => array(
                'category' => array('table' => 'category'),
                'category_booking' => array('table' => 'category_booking', 'condition' => 'category.id = category_booking.category_id', 'type' => 'LEFT')
            ),
            'where' => array(
                'category_booking.branch_id' => $booking_data->branch_id,
                'category_booking.is_show' => 1
            )
        ))->result();

        $item_booking = $model->select(array(
            'select' => array(
                'item_booking' => array('item_id', 'branch_id', 'is_show', 'is_price_show', 'show_date_from', 'show_date_to', 'show_time_from', 'show_time_to', 'show_date_loop'),
                'item' => array('name', 'description', 'promotion')
            ),
            'from' => array(
                'item_booking' => array('table' => 'item_booking'),
                'item' => array('table' => 'item', 'condition' => array('item_booking.item_id = item.id')),
            ),
            'where' => array(
                'item_booking.branch_id' => $booking_data->branch_id
            )
        ))->result();
        foreach($items as $key => $item){
            foreach($item_booking as $value){
                if($item->id == $value->item_id){
                    $item->is_price_show = $value->is_price_show;
                    if(!$value->is_show){
                        unset($items[$key]);
                    }
                    break;
                } else {
                    $item->is_price_show = 1;
                }
            }
        }
        /*
        // --------- End edit -------------------
         */

        $new_items = array();
        foreach ($items as $key => $value) {
            if ($value->promotion == 1) {
                $new_items[] = $value;
                unset($items[$key]);
            }
        }
        foreach ($items as $key => $value) {
            $new_items[] = $value;
        }

        $items = $new_items;

        $categories = convert_to_array($categories,'id','',true);

        foreach($items as $item){
            if(! isset($item->categories)){
                continue;
            }
            $category_id = convert_to_array($item->categories, '', 'id');
            foreach ($category_id as $each_id) {
                if (!isset($categories[$each_id])) {
                    continue;
                }
                if (!isset($categories[$each_id]->items)) {
                    $categories[$each_id]->items = array();
                }
                $categories[$each_id]->items[] = $item;
            }
        }
        return array('categories' => $categories);
    }

    function get_data_staff($data){
        if(!empty($this->session->userdata('pending_appointment')['pending_id'])){
            $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
        }

        $branch_id = $data->branch_id;
        $therapist_id = $this->load->table_model('department')->get_department_serve_booking();
        $employee_model = $this->load->table_model('employee');
        $employees = $employee_model->select(array(
            'select' => array(
                'employee' => array('id','first_name','last_name'),
                'employee_branch'   => array('branch_id')
            ),
            'from'   => array(
                'employee' => array('table'=>'employee'),
            ),
            'where' => array(
                'department_id'  => $therapist_id
            ),
            'user_level' => 1,
            'permission' => array('branch_id' => $branch_id)
        ))->result();
        $temp = array();
        foreach($employees as $employee){
            if($employee->employee_permission)
            {
                $temp[] = $employee;
            }
        }
        $employees = convert_to_array($temp,'id','',true);

        return array('employees' => $employees);
    }

    function get_data_date($data)
    {
        if (!empty($this->session->userdata('pending_appointment')['pending_id'])) {
            $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
        }

        $booking_data = $this->session->userdata('booking_data')[$this->session->userdata('currentUniqueId')];
        $branch_offline = $this->load->table_model('branch_offline')->get(array('branch_id' => $booking_data->branch_id));
        $timezone = $this->system_config->get('timezone', array('login_as_type' => Permission_Value::BRANCH, 'branch_id' => $booking_data->branch_id));

        $temp = array();
        foreach ($branch_offline as $row) {
            $row->start_time = get_user_date($row->start_time, $timezone, 'Y-m-d H:i:s');
            $row->end_time = get_user_date($row->end_time, $timezone, 'Y-m-d H:i:s');
            $temp[] = $row;
        }

        $service = $this->load->table_model('item')->get_service_detail(array('item' => array('id' => $booking_data->service_id)));
        $service = $service[0];
        $branch_offline = $temp;
        $branch_offline_data = array();

        foreach ($branch_offline as $row) {
            $start_time = $row->start_time;
            $end_time = $row->end_time;
            $time = new DateTime($start_time);
            $date_key = $time->format('Y-m-d');
            $time = $time->sub(new DateInterval('PT' . ($service->duration * 60) . 'S'));
            $after_sub = $time->format('Y-m-d');
            if ($after_sub != $date_key) {
                $start_time = '00:00';
            } else {
                $start_time = $time->format('H:i');
            }
            $time = new DateTime($end_time);
            $end_time = $time->format('H:i');

            if ($start_time == '00:00' && $end_time == '23:59') {
                if (!in_array($date_key, $branch_offline_data)) {
                    $branch_offline_data[] = $date_key;
                }
            }
        }

        $date_data = $this->get_time_slot(get_database_date());
        return array(
            'current_date' => get_database_date(),
            'branch_offline_data' => $branch_offline_data,
            'time_range' => $date_data['booking_slot'],
            'limit_booking' => $date_data['limit_booking'],
            'service_duration' => $service->duration
        );
    }

    function get_data_comment($data)
    {
        // getting the real therapist
        $session_data = $data;

        $get_duration = $this->load->table_model('item_service')->get(array('item_id' => $session_data->service_id));
        $get_duration = $get_duration[0]->duration;
        $check_b = TRUE;

        if ($session_data->staff_id == -1) {
            $session_data->staff_real = -1;
            $data = (array)$session_data;
            $data['date'] = $data['date_time'];
            unset($data['date_time']);
            $employee_data = $this->get_employee_avail($data);

            if (isset($employee_data['msg']) && !isset($employee_data['service'])) {
                return array('error' => $employee_data['msg']);
            }

            $employee_data = $employee_data['service']->list_employee;
            $list_random = array();
            foreach ($employee_data as $item) {
                $list_random[] = $item->id;
            }
            foreach ($list_random as $key => $staff) {
                $random = rand(0, count($list_random) - 1);
                $condition = array(
                    'branch_id' => $session_data->branch_id,
                    'staff_id' => $list_random[$random],
                    'service_id' => $session_data->service_id,
                    'date_time' => $session_data->date_time,
                    'duration' => $get_duration
                );
                $check_booking   = $this->load->table_model('booking_pending_appointment')->check_pending_appointment($condition);
                $check_booking_2 = $this->load->table_model('booking')->check_customize_employee_working_hours($condition);
                $check_booking_3 = $this->load->table_model('booking')->check_employee_working_hours($condition);
                if (!$check_booking || !$check_booking_2 || !$check_booking_3) {
                    unset($list_random[$random]);
                    $list_random = array_values($list_random);
                    $check_b = FALSE;
                } else {
                    $check_b = TRUE;
                    $session_data->staff_id = $list_random[$random];
                    $currentUniqueId = $this->session->userdata('currentUniqueId');
                    $booking_data = $this->session->userdata('booking_data');
                    $booking_data[$currentUniqueId] = $session_data;
                    $this->session->set_userdata('booking_data', $booking_data);
                    break;
                }
            }
        } else {
            unset($session_data->staff_real);
        }
        if ($check_b) {
            if (isset($this->session->userdata('pending_appointment')['pending_id'])) {
                $temp = $this->session->userdata('booking_previous');
                if (!empty($temp)) {
                    // delete old booking;
                    $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
                    $timestamp = time();
                    $dt = new DateTime("now", new DateTimeZone('Asia/Singapore')); //first argument "must" be a string
                    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
                    $start_time = $dt->format('Y-m-d H:i:s');

                    $pending_data = array(
                        'start_time' => $start_time,
                        'staff_id' => $session_data->staff_id,
                        'service_id' => $session_data->service_id,
                        'branch_id' => $session_data->branch_id,
                        'date_time' => $session_data->date_time,
                        'duration' => $get_duration
                    );
                    $get_list_appointment = $this->load->table_model('booking_pending_appointment')->insert_list_pending($pending_data);
                    $this->session->set_userdata('pending_appointment', array('pending_id' => $get_list_appointment['data'], 'start_time' => $start_time, 'end_time' => '', 'countdown' => PENDING_TIME('Booking')));
                    $this->session->set_userdata('booking_previous', '');
                }
            } else {
                // create pending appointment
                $timestamp = time();
                $dt = new DateTime("now", new DateTimeZone('Asia/Singapore')); //first argument "must" be a string
                $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
                $start_time = $dt->format('Y-m-d H:i:s');
                $pending_data = array(
                    'start_time' => $start_time,
                    'staff_id' => $session_data->staff_id,
                    'service_id' => $session_data->service_id,
                    'branch_id' => $session_data->branch_id,
                    'date_time' => $session_data->date_time,
                    'duration' => $get_duration
                );
                $get_list_appointment = $this->load->table_model('booking_pending_appointment')->insert_list_pending($pending_data);
                $this->session->set_userdata('pending_appointment', array('pending_id' => $get_list_appointment['data'], 'start_time' => $start_time, 'end_time' => '', 'countdown' => PENDING_TIME('Booking')));
            }
            return TRUE;
        } else {
            return array('error' => 'This employee is not available for this service at this time. Please choose another time');
        }
    }

    function get_data_confirm($data)
    {
        $data_session = $this->session->userdata('booking_data');

        $appointments = array();
        foreach ($data_session as $key => $appointment) {
            $staffReal = isset($data->staff_real) ? $data->staff_real : '';
            $staff_id = $appointment->staff_id;

            if($staffReal != '' && $staffReal == -1){
                $staff_id = 'No preference';
            }
            elseif (($staffReal != '' && $staffReal != -1) || empty($staffReal)) {
                $staff_id = $this->load->table_model('employee')->get(array('id' => $appointment->staff_id))[0]->first_name;
            }
            $temp = [
                'service_name' => $this->load->table_model('item')->get(array('id' => $appointment->service_id))[0]->name,
                'branch_name' => $this->load->table_model('branch')->get(array('id' => $appointment->branch_id))[0]->name,
                'staff_name' => $staff_id,
                'date_time' => $appointment->date_time
            ];
            $appointments[$key] = $temp;
        }
        return array(
            'appointments' => $appointments,
            'ctm_information' => $this->session->userdata('ctm_information')
        );
    }

    function get_data_finished($data){
        if (!empty($this->session->userdata('pending_appointment')['pending_id'])) {
            $this->load->table_model('booking_pending_appointment')->delete_list_pending($this->session->userdata('pending_appointment')['pending_id']);
        }
        /* ------ Check customer login ------*/
        $session_data = $this->session->userdata('booking_data');
        $ctm_information = $this->session->userdata('ctm_information');
        $logged_user = $this->session->userdata('login');
        $this->db->trans_start();
        $ret_appointment = array();
        foreach($session_data as $key => $appointment){
            $date_time = $appointment->date_time;
            $staff_id = $appointment->staff_id;
            $branch_id = $appointment->branch_id;
            $service_id = $appointment->service_id;
            $service = $this->load->table_model('item')->getByID($service_id);

            if(substr($ctm_information->phone, 0,2) != 65){
                $ctm_information->phone = '65' . $ctm_information->phone;
            }

            /* NEW BOOKING REGISTRATION WORKFLOW */
            if(!$logged_user){
                $customer_id = $this->load->controller_model('customers')->InsertTemp(array('first_name' => trim($ctm_information->firstname), 'last_name' => trim($ctm_information->lastname), 'mobile_number' => trim($ctm_information->phone), 'email' => trim($ctm_information->email)));
            }
            else{
                $customer_id = $logged_user->id ;
            }
            /* END NEW BOOKING REGISTRATION WORKFLOW */
            if(empty($customer_id)){
                throw new Exception ('Error: No customer id selected');
            }

            $getStatus = (!$logged_user ? BOOKING_STATUS('Hold') : BOOKING_STATUS('Complete'));


        if(!empty($customer_id)){
            $field = array(
                'customer_id' => $customer_id,
                'comment'     => $ctm_information->comment,
                'status'      => $getStatus,
                'service_id'  => $service->id,
                'employee_id' => $staff_id,
                'start_time'  => $date_time,
                'type'        => BOOKING_TYPE('Online')
            );
        }
        else{
            throw new Exception ('Error: No customer id selected');
        }

            if (isset($session_data->staff_real) && $session_data->staff_real == -1)
                $field['no_preference'] = 1;
            else
                $field['no_preference'] = 0;

            // INSERT BOOKING
            $permission = array('user_level' => Permission_Value::BRANCH, 'field' => array('branch_id' => $branch_id));
            $booking_id = $this->load->controller_model('scheduler', 'staff')->insert_booking($field, $permission, 'booking');
            $is_check = $this->session->userdata('is_reschedule');
            if(!empty($is_check)) {
                $is_reschedule = $this->session->userdata('is_reschedule');
                $this->reschedule_appointment($is_reschedule->bid, $booking_id);
            }
            $booking = $this->load->table_model('booking')->get_detail(array('booking' => array('id' => $booking_id, 'status' => $getStatus)), 1);
            $booking = $booking[0];

            // PREPARE DATA TO PRINT
            $ret_appointment[] = array(
                'customer_name' => $ctm_information->firstname . ' ' . $ctm_information->lastname,
                'date_time' => $date_time,
                'branch_name' => $this->load->table_model('branch')->get(array('id' => $branch_id))[0]->name,
                'service_name' => $service->name,
                'staff_name' => $booking->employee_first_name,
                'code' => $booking->code,
                'no_preference' => $field['no_preference'],
                'comment' => $ctm_information->comment
            );
            $session_data[$key]->code = $booking->code;
        }
        $this->session->set_userdata('booking_data',$session_data);
        $this->db->trans_complete();
        return array('appointments' => $ret_appointment,'ctm_information' => $ctm_information);
    }

    function get_booking_for_save_data($code)
    {
        $booking = $this->load->table_model('booking')->get_detail(array('booking' => array('code' => $code), 'status' => BOOKING_STATUS('Hold')));
        $booking[0]->default_start_time = $booking[0]->start_time;
        $booking[0]->start_time = get_user_date($booking[0]->start_time, '', 'Y-m-d H:i:s');

        $item_service = $this->load->table_model('item')->get_service_detail(array('item' => array('id' => $booking[0]->service_id)));

        $item_description = $this->load->table_model('item')->get(array('id' => $booking[0]->service_id))[0]->description;

        $booking[0]->service_duration = $item_service[0]->duration;
        if (!count($booking)) {
            return null;
        }
        $booking = $booking[0];

        if (isset($booking->bundle_id)) {
            $staff_name = array();
            foreach ($booking->services as $each_service) {
                $name = to_full_name($each_service->employee_first_name, $each_service->employee_last_name);;
                if (in_array($name, $staff_name)) {
                    break;
                }
                $staff_name[] = $name;
            }
            $staff_name = implode(', ',$staff_name);
            $staff_first_name = implode(', ',$staff_name);
            $service_name = $booking->bundle->name;
        }else{
            $staff_name = to_full_name($booking->employee_first_name,$booking->employee_last_name);
            $staff_first_name = $booking->employee_first_name;
            $service_name = $booking->item_name;
        }
        $booking->customer_name = to_full_name($booking->customer_first_name,$booking->customer_last_name);
        $booking->employee_name = $staff_name;
        $booking->employee_first_name = $staff_first_name;
        $booking->service_name = $service_name;
        $booking->description = $item_description;

        return $booking;
    }

    function get_branch_employee($data)
    {
        $session = (object)$data;
        $data = $this->select(array(
            'select' => array(
                'employee' => array('id', 'first_name')
            ),
            'from' => array(
                'employee' => array('table' => 'employee'),
                'employee_branch' => array('table' => 'employee_branch', 'condition' => 'employee.id = employee_branch.employee_id'),
                'employee_service' => array('table' => 'employee_service', 'condition' => 'employee.id = employee_service.employee_id')
            ),
            'where' => array(
                'employee.status' => 1,
                'employee.department_id' => $this->load->table_model('department')->get_department_serve_booking(),
                'employee_branch.branch_id' => $session->branch_id,
                'employee.id NOT' => array(165, 167),
                'employee_service.item_id' => $session->service_id
            ),
            'group' => 'id'
        ))->result();


        return $data;
    }

    function get_no_preference_timeslot($data){
        $staff_list = $this->get_branch_employee($data);
        $all_calendar = array();
        foreach($staff_list as $key => $staff){
            $data['employee_id'] = $staff->id;
            $all_calendar[$staff->id] = $this->get_available_timeslot($data);
        }

        $ret = array();

        foreach($all_calendar as $key => $staff){
            if($staff){
                if(!isset($ret['default_date'])){
                    $ret['default_date'] = $staff['default_date'];
                }
                else{
                    if(strtotime($ret['default_date']) > strtotime($staff['default_date'])){
                        $ret['default_date'] = $staff['default_date'];
                    }
                }

                if(!isset($ret['minDate'])){
                    $ret['minDate'] = $staff['minDate'];
                }
                else{
                    if(strtotime($ret['minDate']) > strtotime($staff['minDate'])){
                        $ret['minDate'] = $staff['minDate'];
                    }
                }

                if(!isset($ret['maxDate'])){
                    $ret['maxDate'] = $staff['maxDate'];
                }
                else{
                    if(strtotime($ret['maxDate']) < strtotime($staff['maxDate'])){
                        $ret['maxDate'] = $staff['maxDate'];
                    }
                }

                if(!isset($ret['disabledDates'])){
                    $ret['disabledDates'] = $staff['disabledDates'];
                }
                else{
                    $ret['disabledDates'] = array_intersect($ret['disabledDates'],$staff['disabledDates']);
                }
            }
        }
        $temp_disabledDates = array();
        foreach ($ret['disabledDates'] as $item) {
            $temp_disabledDates[] = $item;
        }
        $ret['disabledDates'] = $temp_disabledDates;
        foreach ($all_calendar as $staff) {
            if ($staff['default_date'] == $ret['default_date']) {
                if (!isset($ret['allowTimes'])) {
                    $ret['allowTimes'] = $staff['allowTimes'];
                } else {
                    foreach ($staff['allowTimes'] as $slot) {
                        if (!in_array($slot, $ret['allowTimes'])) {
                            $ret['allowTimes'][] = $slot;
                        }
                    }
                }
            }
        }

        foreach ($ret['allowTimes'] as $key => $item) {
            $check_date = substr($data['date_string'], 0, 10) . ' ' . $item;
            $recheck = $this->load->controller_model('scheduler', 'staff')->_getAvailRoom(strtotime($check_date), $data['service_id']);
            if (count($recheck) <= 0) {
                unset($ret[$key]['allowTimes']);
            }
        }

        $ret['allowTimes'] = array_values($ret['allowTimes']);
        sort($ret['allowTimes']);
        return $ret;
    }

    function get_available_timeslot($data)
    {
        $first = 0;
        do {
            if ($first < 5) {
                if ($first == 0) {
                    $first++;
                } else {
                    $data['date_string'] = date('Y-m-d', strtotime($data['date_string']) + 86400);
                    $first++;
                }
                $ret = $this->_get_available_timeslot($data, $first);
            } else {
                return FALSE;
            }
        } while (empty($ret['allowTimes']));
        return $ret;
    }

    function _get_available_timeslot($data, $check = 1)
    {
        $timezone = $this->system_config->get('timezone', array('login_as_type' => Permission_Value::BRANCH, 'branch_id' => $data['branch_id']));
        $start_time_working = $this->system_config->get_by_code('start_time', $data['branch_id']);
        $lead_time_min = $this->system_config->get('lead_time_min', array('user_level' => Permission_Value::BRANCH, 'field' => array('branch_id' => $data['branch_id'])));
        $lead_time_max = $this->system_config->get('lead_time_max', array('user_level' => Permission_Value::BRANCH, 'field' => array('branch_id' => $data['branch_id'])));

        if (empty($data['choose_date'])) {
            $now = get_database_date();
            //   $now = get_user_date(get_database_date(),$timezone,'Y-m-d H:i');
        } else {
            $now = get_database_date();
            //  $now = get_user_date(get_database_date(),$timezone,'Y-m-d H:i'
        }
        $start_time = strtotime($now) + ($lead_time_min * 60);
        $end_time = strtotime($now) + ($lead_time_max * 60);

        $default_date = get_user_date(get_database_date(), $timezone, 'Y-m-d H:i');

        $condition = array(
            'booking.status' => array(BOOKING_STATUS('Complete'), BOOKING_STATUS('Hold'), BOOKING_STATUS('Comment'), BOOKING_STATUS('Block'), BOOKING_STATUS('Away'), BOOKING_STATUS('Offline'), BOOKING_STATUS('Payment'), BOOKING_STATUS('HoldBill')),
            'booking.branch_id' => $data['branch_id'],
            'employee_id' => $data['employee_id'],
            '((date(booking_service.start_time) > "' . date('Y-m-d', $start_time) . '" AND date(booking_service.start_time) < "' . date('Y-m-d', $end_time) . '") OR
              (date(booking_service.start_time) <= "' . date('Y-m-d', $start_time) . '" AND date(booking_service.end_time) >= "' . date('Y-m-d', $start_time) . '"))'
        );

        // GET ALL THE APPOINTMENT/BLOCK/AWAY/OFFLINE
        $get_off_day = $this->select(array(
            'select' => array(
                'booking' => array('id'),
                'booking_service' => array('start_time', 'end_time')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id'),
            ),
            'where' => $condition
        ))->result();

        // GET THE SERVICE DURATION
        $service_duration = $this->select(array(
            'select' => array(
                'item_service' => array('duration')
            ),
            'from' => array('item_service' => array('table' => 'item_service')),
            'where' => array(
                'item_id' => $data['service_id']
            )
        ))->result();

        // GET THE EMPLOYEE CALENDAR
        $employee_calendar = $this->select(array(
            'select' => array(
                'employee_calendar' => array('day_id', 'start_time', 'end_time')
            ),
            'from' => array('employee_calendar' => array('table' => 'employee_calendar')),
            'where' => array(
                'employee_id' => $data['employee_id'],
                'branch_id' => $data['branch_id']
            )
        ))->result();

        if (count($employee_calendar) == 0) {
            return FALSE;
        }

        // GET THE EMPLOYEE CALENDAR CUSTOMIZE
        $employee_calendar_customize = $this->select(array(
            'select' => array(
                'employee_calendar_customize' => array('employee_id', 'day_id', 'start_time', 'end_time', 'date', 'working')
            ),
            'from' => array('employee_calendar_customize' => array('table' => 'employee_calendar_customize')),
            'where' => array(
                'employee_id' => $data['employee_id'],
                'branch_id' => $data['branch_id']
            )
        ))->result();

        // RESTRICT DATE
        $restrictData = $this->select(array(
            'select' => array('item_booking' => '*'),
            'from'   => array(
                'item_booking' => array('table' => 'item_booking')
            ),
            'where' => array('branch_id' =>  $data['branch_id'], 'item_id' => $data['service_id'])
        ))->result();
        if(!empty($restrictData)){
            $restrictData = $restrictData[0];
            if(!empty($restrictData->restrict_to_date) && $restrictData->restrict_to_date < $end_time){
                $end_time = strtotime($restrictData->restrict_to_date);
            }
        }
        else{
            $restrictData = array();
        }

        $service_duration = intval($service_duration[0]->duration);
        $item_last_appointment = $this->load->table_model('item_last_appointment')->get(array('item_id' => $data['service_id'], 'branch_id' => $data['branch_id']));
        $last_appointment = $this->system_config->get_by_code('last_app_time', $data['branch_id']);

        if (count($item_last_appointment)) {
            if ($item_last_appointment[0]->last_appointment != "inherit") {
                if ($item_last_appointment[0]->last_appointment <= $last_appointment)
                    $last_appointment = $item_last_appointment[0]->last_appointment;
            }
        }
        $time_slot = array();
        $last_app = $last_appointment;
        $time_slot[] = $start_time_working;
        $temp_time = $start_time_working;
        while ($temp_time < $last_app) {
            $num = explode(':', $temp_time);
            $num = intval($num[0]) * 60 + intval($num[1]) + 30;
            $temp = floor($num / 60);
            $num_str = (($temp > 10) ? $temp : '0' . $temp) . ':' . ((($num % 60) > 0) ? '30' : '00');
            $time_slot[] = $num_str;
            $temp_time = $num_str;
        }
        $time_slot_2 = $time_slot;
        $dateTimeZone = new DateTimeZone($timezone);
        $timezone_offset = new DateTime(null, $dateTimeZone);
        $timezone_offset = $dateTimeZone->getOffset($timezone_offset)/60/60;
        $disabledDates = array();

        foreach($get_off_day as $item){
            $item_start_time = strtotime($item->start_time)+(intval($timezone_offset) * 3600 + 1);
            $item_end_time = strtotime($item->end_time)+(intval($timezone_offset) * 3600 - 1);

            if(date('Y-m-d',$item_start_time) == date('Y-m-d',$item_end_time)){
                $cur_date = date('Y-m-d',$item_start_time);
                if(!isset($disabledDates[$cur_date])){
                    $disabledDates[$cur_date]['time_slot'] = $time_slot;
                }
                foreach($disabledDates[$cur_date]['time_slot'] as $key => $slot){
                    $slot = strtotime($cur_date.' '.$slot);
                    $end_slot = $slot + ($service_duration*60);
                    if(($slot < $item_start_time &&  $item_start_time < $end_slot) || ($slot < $item_end_time &&  $item_end_time < $end_slot) ||
                        ($slot > $item_start_time && $end_slot < $item_end_time)){
                        unset($disabledDates[$cur_date]['time_slot'][$key]);
                    }
                }
            }
            else{
                if(date('Y-m-d',$item_start_time) <= date('Y-m-d',$item_end_time)){
                    while(date('Y-m-d',$item_start_time) < date('Y-m-d',$item_end_time)){
                        $disabledDates[date('Y-m-d',$item_start_time)]['time_slot'] = array();
                        $item_start_time += 86400;
                    }
                }
            }
        }

        $temp_disabledDates = $disabledDates;
        $disabledDates = array();
        foreach($temp_disabledDates as $key => $item){
            if(date('Y-m-d',strtotime($default_date))  == $key){
                $now  = explode(':',date('H:i',strtotime($default_date))); $now = 60*$now[0] + $now[1];
                foreach($item['time_slot'] as $key2 => $item2){
                    $time = explode(':',$item2);
                    $time = 60*$time[0] + $time[1];
                    if($time < $now){
                        unset($item['time_slot'][$key2]);
                    }
                }
            }
            if(empty($item['time_slot'])){
                $disabledDates[] = $key;
            }
        }

        $overrideWorkingDate = array();
        // CHECK IN CALENDAR CUSTOMIZE TO FIND IF EMPLOYEE HAVE SOME CUSTOMIZE OFF DAY
        foreach ($employee_calendar_customize as $date) {
            if ($date->working == 0 && !in_array($date->date, $disabledDates))
                $disabledDates[] = $date->date;
            if($date->working == 1 && !in_array($date->date, $overrideWorkingDate)){
                $overrideWorkingDate[] = $date->date;
            }
        }

        // CHECK AND REMOVE THE NON WORKING AT THE EMPLOYEE_SETTING_SCHEDULER
        $check_date = array(0,1,2,3,4,5,6);
        $work_date = array();

        foreach($employee_calendar as $day){
            if(in_array($day->day_id,$check_date)){
                unset($check_date[$day->day_id]);
            }
        }

        foreach($check_date as $item){
            if($item == 0){
                $work_date[] = 'Sun';
            }
            else if($item == 1){
                $work_date[] = 'Mon';
            }
            else if($item == 2){
                $work_date[] = 'Tue';
            }
            else if($item == 3){
                $work_date[] = 'Wed';
            }
            else if($item == 4){
                $work_date[] = 'Thu';
            }
            else if($item == 5){
                $work_date[] = 'Fri';
            }
            else if($item == 6){
                $work_date[] = 'Sat';
            }
        }
        if(count($work_date) > 0){
            $temp_start_time = $start_time;
            while ($temp_start_time < $end_time) {
                if (in_array(date('D', $temp_start_time), $work_date) && !in_array(date('Y-m-d', $temp_start_time), $disabledDates)) {
                    $disabledDates[] = date('Y-m-d', $temp_start_time);
                }
                $temp_start_time += 86400;
            }
        }

        // PROCESSING THE START TIME - END TIME AGAIN AFTER CALCULATE THE DISABLED DATES
        $client_now = $start_time;
        while (in_array(date('Y-m-d', $start_time), $disabledDates)) {
            $start_time += 86400;
        }

        while(!empty($restrictData->restrict_from_date) && (strtotime($restrictData->restrict_from_date) > $start_time)){
            $start_time += 86400;
        }
        // GET TIMESLOT FOR MINI CALENDAR

        $default_date = date('Y-m-d', $start_time);

        $allowTimes = array();

        if(isset($data['date_string'])){
            $send_date = date('Y-m-d',strtotime($data['date_string']));
            if(in_array($start_time,$disabledDates)) {
                $allowTimes = $temp_disabledDates[date('Y-m-d',$start_time)]['time_slot'];
            }
            else{
                if($send_date <= date('Y-m-d',$start_time)){
                    $send_date = date('Y-m-d',$start_time);
                }
                // REMOVE TIME SLOT BASE ON REALTIME - NOW
                if(date('Y-m-d',$client_now) == $send_date){
                    $now  = explode(':',date('H:i',$start_time)); $now = 60*$now[0] + $now[1] + ($timezone_offset*60);
                    foreach($time_slot_2 as $key => $item){
                        $time = explode(':',$item); $time = 60*$time[0] + $time[1];
                        if($time < $now){
                            unset($time_slot_2[$key]);
                        }
                        else{
                            $allowTimes[] = $time_slot_2[$key];
                        }
                    }
                }
                else{
                    $allowTimes = $time_slot_2;
                }
                // REMOVE TIME SLOT BASE ON WORKING TIME OF EMPLOYEE
                $employee_calendar = convert_to_array($employee_calendar, 'day_id');
                if (!in_array($send_date, $overrideWorkingDate)) {
                    foreach ($allowTimes as $key => $slot) {
                        $time = explode(':', $slot);
                        $time = 60 * $time[0] + $time[1];
                        $end_slot = $time + $service_duration;
                        $date = date('D', strtotime($send_date));
                        $date = $this->convert_day_to_num($date);
                        if (isset($employee_calendar[$date])) {
                            $item_start_time = explode(':', $employee_calendar[$date]->start_time);
                            $item_start_time = 60 * $item_start_time[0] + $item_start_time[1];
                            if ($item_start_time > $time || $item_start_time > $end_slot) {
                                unset($allowTimes[$key]);
                            }
                            $item_end_time = explode(':', $employee_calendar[$date]->end_time);
                            $item_end_time = 60 * $item_end_time[0] + $item_end_time[1];
                            if ($item_end_time < $time || $item_end_time < $end_slot) {
                                unset($allowTimes[$key]);
                            }
                        }
                    }
                }
                else{
                    // FIND AND REMOVE TIME SLOT THAT NOT VALID WITH CALENDAR CUSTOMIZE
                    foreach ($employee_calendar_customize as $day) {
                        if ($day->date == date('Y-m-d', strtotime($data["date_string"]))) {
                            foreach ($allowTimes as $key => $value)
                                if ($value < $day->start_time || $value > $day->end_time)
                                    unset($allowTimes[$key]);
                        }
                    }
                }
                // REMOVE TIME SLOT BASE ON BOOKING APPOINTMENT/BLOCK/COMMENT/OFFLINE/AWAY DATETIME
                foreach($allowTimes as $key => $slot){
                    $slot = strtotime($send_date.' '.$slot);
                    $end_slot = $slot + ($service_duration*60);
                    foreach($get_off_day as $item){
                        if(date('Y-m-d',strtotime($item->start_time) == $client_now)){
                            $item_start_time = strtotime($item->start_time)+(intval($timezone_offset) * 3600 + 1);
                            $item_end_time = strtotime($item->end_time)+(intval($timezone_offset) * 3600 - 1);
                            if(($slot < $item_start_time &&  $item_start_time < $end_slot) || ($slot < $item_end_time &&  $item_end_time < $end_slot) ||
                                ($slot > $item_start_time && $end_slot < $item_end_time)){
                                unset($allowTimes[$key]);
                            }
                        }
                    }
                }
                // REMOVE TIME SLOT BASE ON RESTRICT AT ITEM LEVEL
                if(isset($restrictData->restrict_from_date) && isset($restrictData->restrict_to_date) &&
                  ($default_date >= $restrictData->restrict_from_date) && ($default_date <= $restrictData->restrict_to_date)) {
                    foreach ($allowTimes as $key => $slot) {
                        $time = explode(':', $slot);
                        $time = 60 * $time[0] + $time[1];
                        if (isset($restrictData->restrict_from_time)) {
                            $resft = explode(':',$restrictData->restrict_from_time);
                            $resft = 60 * $resft[0] + $time[1];
                            if ($resft > $time) {
                                unset($allowTimes[$key]);
                            }
                        }
                        if (isset($restrictData->restrict_to_time)) {
                            $restt = explode(':',$restrictData->restrict_to_time);
                            $restt = 60 * $restt[0] + $restt[1];
                            if ($restt < $time) {
                                unset($allowTimes[$key]);
                            }
                        }
                    }
                }
            }
        } else {
            $allowTimes = $time_slot_2;
        }

        $allowTimes_temp = array();
        foreach ($allowTimes as $item) {
            $allowTimes_temp[] = $item;
        }

        if($start_time > $end_time){
            $default_date = '';
        }

        return array('default_date' => $default_date,
            'minDate' => date('Y-m-d', $start_time),
            'maxDate' => date('Y-m-d', $end_time),
            'disabledDates' => $disabledDates,
            'allowTimes' => $allowTimes_temp
        );
    }

    function get_employee_avail($data){
        $ret_data = array();
        $session_data = $this->session->userdata('booking_data')[$this->session->userdata('currentUniqueId')];
        $data = $this->load->controller_model('scheduler','staff')->getDataForTimeChange($session_data->service_id,$data['date'],'','',array(
            'field'         => array('branch_id' => $session_data->branch_id),
            'user_level'    => Permission_Value::BRANCH
        ));

        if($data['is_bundle']){
            foreach($data['service'] as $each_service){
                if(! count($each_service->list_room)){
                    return array(
                        'msg' => array("There is no space for {$each_service->name} at this time")
                    );
                }
                if(! count($each_service->list_employee)){
                    return array(
                        'msg' => array("There is no therapist for {$each_service->name} at this time")
                    );
                }
                $ret_data['service'][$each_service->id] = $each_service;
            }
        }else{
            if(! count($data['service']->list_room)){
                return array(
                    'msg' => array("There is no space for {$data['service']->name} at this time")
                );
            }
            if(! count($data['service']->list_employee)){
                return array(
                    'msg' => array("There is no therapist for {$data['service']->name} at this time")
                );
            }
            $ret_data['service'] = $data['service'];
        }
        $ret_data['is_bundle'] = $data['is_bundle'];
        return $ret_data;
    }

    function getConfig($codename = "", $branch = ''){
        $config = $this->select(array(
            'select' => array(
                'config' => array('value')
            ),
            'from' => array(
                'config'  => array('table'=>'config'),
            ),
            'where' => array('code' => $codename)
        ))->result();

        $inherite = $this->select(array(
            'select' => array(
                'config_branch' => array('value','branch_id')
            ),
            'from' => array(
                'config' => array('table' => 'config'),
                'config_branch'  => array('table'=>'config_branch','condition' => 'config.id = config_branch.config_id'),
            ),
            'where' => array('config.code' => $codename,'config_branch.branch_id' => $branch)
        ))->result();

        if($inherite[0]->value == 'inherit'){

            $branchGroupInherite = $this->select(array(
                'select' => array(
                    'config_branch_group' => array('value','branch_group_id'),
                    'branch' => array('branch_id' => 'id')
                ),
                'from' => array(
                    'config' => array('table' => 'config'),
                    'config_branch_group'  => array('table'=>'config_branch_group','condition' => 'config.id = config_branch_group.config_id'),
                    'branch' => array('table' => 'branch', 'condition' => 'branch.branch_group_id = config_branch_group.branch_group_id')
                ),
                'where' => array('config.code' => $codename,'branch.id' => $branch)
            ))->result();

            if($branchGroupInherite[0]->value == 'inherit'){
                return $config[0]->value;
            }
            else{
                return $branchGroupInherite[0]->value;
            }
            return $config[0]->value;
        }
        else {
            return $inherite[0]->value;
        }
    }

    function check_avail_appointment($data){
        // check pending appointment
        $get_duration = $this->load->table_model('item_service')->get(array('item_id' => $data['service_id']));
        $data['duration'] = $get_duration[0]->duration;
        $check_pending = $this->load->table_model('booking_pending_appointment')->check_pending_appointment($data);
        return $check_pending;
    }

    function delete_pending_appointment($pending_id){
        $delete_pending = $this->load->table_model('booking_pending_appointment')->delete_list_pending($pending_id);
        return $delete_pending;
    }

    function get_time_slot($date)
    {
        $booking_data = $this->session->userdata('booking_data');
        $currentUniqueId = $this->session->userdata('currentUniqueId');
        $booking_data = $booking_data[$currentUniqueId];

        $timezone = $this->system_config->get('timezone', array('login_as_type' => Permission_Value::BRANCH, 'branch_id' => $booking_data->branch_id));

        $lead_time_min = $this->system_config->get('lead_time_min', array('user_level' => Permission_Value::BRANCH, 'field' => array('branch_id' => $booking_data->branch_id)));
        $lead_time_max = $this->system_config->get('lead_time_max', array('user_level' => Permission_Value::BRANCH, 'field' => array('branch_id' => $booking_data->branch_id)));

        $now = get_user_date(get_database_date(), $timezone, 'Y-m-d H:i:s');
        $start_time = strtotime($now) + ($lead_time_min * 60);
        $end_time = strtotime($now) + ($lead_time_max * 60);

        if ($start_time < strtotime('2015-10-30')) {
            $start_time = strtotime('2015-10-29') + ($lead_time_min * 60);
            $end_time = strtotime('2015-10-29') + ($lead_time_max * 60);
        }

        $item_last_appointment = $this->load->table_model('item_last_appointment')->get(array('item_id' => $booking_data->service_id, 'branch_id' => $booking_data->branch_id));
        $full_booking_slot = array();
        $last_appointment = $this->system_config->get_by_code('last_app_time', $booking_data->branch_id);
        if (count($item_last_appointment)) {
            if ($item_last_appointment[0]->last_appointment != "inherit") {
                if ($item_last_appointment[0]->last_appointment <= $last_appointment)
                    $last_appointment = $item_last_appointment[0]->last_appointment;
            }
        }

        $start_time_working = $this->system_config->get_by_code('start_time', $booking_data->branch_id);
        $start_time_2 = $start_time_working;
        $end_time_working = $this->system_config->get_by_code('start_time', $booking_data->branch_id);
        $end_time_working = explode(':', $end_time_working);
        $end_time_working = intval($end_time_working[0]) * 60 + intval($end_time_working[1]);
        $start_time_working = explode(':', $start_time_working);
        $start_time_working = intval($start_time_working[0]) * 60 + intval($start_time_working[1]);
        $last_app = $last_appointment;
        $last_appointment_s = explode(':', $last_appointment);
        $last_appointment_s = intval($last_appointment_s[0]) * 60 + intval($last_appointment_s[1]);

        $now = date('H:i', $start_time);
        $now = explode(':', $now);
        $now = intval($now[0]) * 60 + intval($now[1]);

        $date = date('Y-m-d', strtotime($date));
        $compate_now = get_user_date(get_database_date(), $timezone, 'Y-m-d');
        if (empty($full_booking_slot) && $date == $compate_now) {
            $full_booking_slot = array('08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00',
                '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
                '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00',
                '21:30', '22:00', '22:30', '23:00', '23:30');
            $bk_full_booking = $full_booking_slot;
            foreach ($full_booking_slot as $key => $item) {
                $arr = explode(':', $item);
                $time = intval($arr[0]) * 60 + intval($arr[1]);
                if ($time < $now) {
                    unset($full_booking_slot[$key]);
                }
                if ($time < $start_time_working) {
                    if (isset($full_booking_slot[$key])) {
                        unset($full_booking_slot[$key]);
                        unset($bk_full_booking[$key]);
                    }
                }
                if ($time > $last_appointment_s) {
                    if (isset($full_booking_slot[$key])) {
                        unset($full_booking_slot[$key]);
                    }
                }
            }
        }
        if (empty($full_booking_slot)) {
            $full_booking_slot[] = $start_time_2;
            while ($start_time_2 < $last_app) {
                $num = explode(':', $start_time_2);
                $num = intval($num[0]) * 60 + intval($num[1]) + 30;
                $temp = floor($num / 60);
                $num_str = (($temp > 10) ? $temp : '0' . $temp) . ':' . ((($num % 60) > 0) ? '30' : '00');
                $full_booking_slot[] = $num_str;
                $start_time_2 = $num_str;
            }
            $start_time += 86400;
            $end_time += 86400;
        }
        $full_booking_slot = array_values($full_booking_slot);

        return array('status' => TRUE,
            'booking_slot' => $full_booking_slot,
            'limit_booking' => array(
                'start_date' => date('Y-m-d', $start_time),
                'end_date' => date('Y-m-d', $end_time),
                'start_time' => $full_booking_slot[0],
                'end_time' => $full_booking_slot[count($full_booking_slot) - 1]));
    }

    function get_cancel_app($bid, $isLogged = FALSE)
    {
        if($isLogged){
            $table = 'customer';
        }
        else{
            $table = 'customer_temp';
        }

        $cancel_app = $this->select(array(
            'select' => array(
                'booking.id as bid', 'booking.status', 'booking.comment', 'booking.code','booking.no_preference',
                $table.'.first_name as cfirstname', $table.'.last_name as clastname',$table.'.email as cemail', $table.'.mobile_number cnumber',
                'employee.first_name as employeename',
                'branch.name as branch_name', 'booking_service.start_time','branch.id as branch_id',
                'item.id as service_id','employee.id as staff_id','item.name as service_name'
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                $table => array('table' => $table, 'condition' => $table.'.id = booking.customer_id'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking_service.booking_id = booking.id'),
                'employee' => array('table' => 'employee', 'condition' => 'employee.id = booking_service.employee_id'),
                'branch' => array('table' => 'branch', 'condition' => 'branch.id = booking.branch_id'),
                'item'  => array('table'=>'item','condition' => 'item.id = booking_service.service_id'),
            ),
            'where' => array(
                'booking.id' => $bid,
                'booking.status NOT' => array(BOOKING_STATUS('Payment'),BOOKING_STATUS('HoldBill'),BOOKING_STATUS('Reserve'),BOOKING_STATUS('Comment'),BOOKING_STATUS('Block'),BOOKING_STATUS('Away'))),
                'TIMESTAMPDIFF(HOUR,NOW().booking_service.start_date) > 1'
        ))->result();

        return $cancel_app;
    }

    function cancel_app($bid)
    {
        $cancel_app = $this->select(array(
            'select' => array('*'),
            'from' => array(
                'booking' => array('table' => 'booking'),
            ),
            'where' => array('id' => $bid)
        ))->result();

        $status = 0;
        $curenttime = date('Y-m-d H:i:s');
        if(!empty($cancel_app)){
            $this->db->where('id', $bid);
            $status = $this->db->update('booking',array('status' => BOOKING_STATUS('Reserve'),'reserve_comment' => 'Has been cancelled by customer', 'updated_date' => $curenttime));
            if($cancel_app[0]->bill_id != null){
                $cancel_bill = $this->select(array(
                    'select' => array('*'),
                    'from' => array(
                        'bill' => array('table' => 'bill'),
                    ),
                    'where' => array('id' => $cancel_app[0]->bill_id)
                ))->result();

                $this->db->where('id', $cancel_app[0]->bill_id);
                $this->db->update('bill', array('status' => BILL_STATUS('Void'), 'description' => 'Has been voided by customer', 'updated_date' => $curenttime));
            }
        }
        return $status;
    }

    function reschedule_appointment($bid,$newbid)
    {
        $curenttime = date('Y-m-d H:i:s');
        $reserve_comment = 'Has been cancelled and reschedule by customer with new booking id is ' . $newbid;
        $this->db->where('id', $bid);
        $this->db->update('booking', array('status' => BOOKING_STATUS('Delete'), 'reserve_comment' => $reserve_comment, 'updated_date' => $curenttime));
        $this->session->set_userdata('is_reschedule', array());
    }

    function convert_day_to_num($day)
    {
        if ($day == 'Mon') {
            $day = 1;
        } else if ($day == 'Tue') {
            $day = 2;
        } else if ($day == 'Wed') {
            $day = 3;
        } else if ($day == 'Thu') {
            $day = 4;
        } else if ($day == 'Fri') {
            $day = 5;
        } else if ($day == 'Sat') {
            $day = 6;
        } else if ($day == 'Sun') {
            $day = 0;
        }
        return $day;
    }

    function getPrepareData()
    {
        return array(
            'branchData' => $this->load->table_model('branch')->getSimpleData(),
            'serviceData' => $this->load->table_model('item')->getSimpleData(),
            'staffData' => $this->load->table_model('employee')->getSimpleData()
        );
    }

    function getBookingConfig(){
        return array(
            'popup_message' => $this->system_config->get('popup_sms'),
            'max_num_booking' => $this->system_config->get('max_num_booking')
        );
    }

    function getRestrictDateTime($service_id, $branch_id){
        return $this->select(array(
            'select' => array('item_booking' => '*'),
            'from'   => array(
                'item_booking' => array('table' => 'item_booking')
            ),
            'where' => array('branch_id' => $branch_id, 'item_id' => $service_id)
        ))->result();
    }
}
