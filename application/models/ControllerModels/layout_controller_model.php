<?php

/**
 * @author Vũ Hoàng Huy
 * @copyright 2014
 */

class Layout_Controller_Model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getLayoutData($code){
        $layout = new stdClass();
        $layout->components = array();
        $layout_info = $this->load->table_model('layout')->get(array('code' => $code));
        if(count($layout_info) > 0){
            $layout->info = $layout_info[0];
            $componentIDs = $this->load->table_model('layout_component')->get(array('layout_id' => $layout->info->id));

            foreach($componentIDs as $componentID) {
                $component = $this->load->table_model('component')->getByID($componentID->component_id);
                $type = $this->load->table_model('component_type')->getByID($component->component_type_id);

                $willInsertComponent = (object) array(
                    'path'  => $type->path.'/'.$component->code,
                    'code'  => $type->code,
                );
                $layout->components[] = $willInsertComponent;
            }
        }
        return $layout;
    }

}
