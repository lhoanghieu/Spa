<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Reports_Controller_Model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_report_form_data($name){
        $report_model = $this->load->report_model($name);
        if($report_model !== null){
            return $report_model->get_form_data();
        }
        return new stdClass();
    }

    function get_report_detail_data($name, $post_data){
        $report_model = $this->load->report_model($name);
        if($report_model !== null){

            return $report_model->admin_get_detail_data($post_data);
        }
        return new stdClass();
    }

    function get_report_export_data($name, $data_content, $data){
        $report_model = $this->load->report_model($name);
        if($report_model !== null){
            $ret_array = array(array($data->name.' - '.$data->branch_name));
            $ret_array[] = array((isset($data->start_date)?($data->start_date. ' - '):' - ').(isset($data->end_date)?($data->end_date):''));
            $ret_array[] = array();
            $ret_array = array_merge($ret_array,$report_model->get_export_data( $data_content));
            return $ret_array;
        }
        return new stdClass();
    }

    function to_original_format($name, $data, $type = false){
        $report_model = $this->load->report_model($name);
        if($report_model !== null){
            return $report_model->get_original_data($data, $type);
        }
        return null;
    }
}

